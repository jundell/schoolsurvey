<?php

App::uses('AppModel', 'Model');

/**
 * Question Model
 *
 * @property Survey $Survey
 * @property Group $Group
 * @property SurveyResult $SurveyResult
 */
class Question extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'survey_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Name is required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'unique' => array(
                'rule' => array('checkUnique', array('name', 'survey_id'), false),
                'message' => 'Question already added.',
            ),
        ),
        'score' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Score must be a valid number',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'minimum' => array(
                'rule' => array('comparison', '>=', 0),
                'message' => 'Score must be greater than or equal to zero.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Survey' => array(
            'className' => 'Survey',
            'foreignKey' => 'survey_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'SurveyTheme' => array(
            'className' => 'SurveyTheme',
            'foreignKey' => 'survey_theme_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
//		'Group' => array(
//			'className' => 'Group',
//			'foreignKey' => 'question_id',
//			'dependent' => false,
//			'conditions' => '',
//			'fields' => '',
//			'order' => '',
//			'limit' => '',
//			'offset' => '',
//			'exclusive' => '',
//			'finderQuery' => '',
//			'counterQuery' => ''
//		),
//		'SurveyResult' => array(
//			'className' => 'SurveyResult',
//			'foreignKey' => 'question_id',
//			'dependent' => false,
//			'conditions' => '',
//			'fields' => '',
//			'order' => '',
//			'limit' => '',
//			'offset' => '',
//			'exclusive' => '',
//			'finderQuery' => '',
//			'counterQuery' => ''
//		)
        'SurveyAnswer' => array(
            'className' => 'SurveyAnswer',
            'foreignKey' => 'question_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'QuestionsOption' => array(
            'className' => 'QuestionsOption',
            'foreignKey' => 'question_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => 'QuestionsOption.id ASC',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    // for Question Type select options
    public function get_question_type_array() {
        $typeData = $this->getColumnType('type');
        preg_match_all("/'(.*?)'/", $typeData, $enums);
        $typeEnums = $enums[1];
        $types = array();
        foreach ($typeEnums as $s) {
            $types[$s] = $s;
        }
        return $types;
    }

    public function checkUnique($ignoredData, $fields, $or = true) {
        return $this->isUnique($fields, $or);
    }

}
