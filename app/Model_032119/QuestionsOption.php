<?php
App::uses('AppModel', 'Model');
/**
 * QuestionsOption Model
 *
 * @property Question $Question
 */
class QuestionsOption extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'question_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'score' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Score must be a valid number',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
                        'minimum' => array(
				'rule' => array('comparison', '>=', 0),
				'message' => 'Score must be greater than or equal to zero.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
//		'Question' => array(
//			'className' => 'Question',
//			'foreignKey' => 'question_id',
//			'conditions' => '',
//			'fields' => '',
//			'order' => ''
//		)
	);
}
