<?php

App::uses('AppModel', 'Model');

/**
 * Group Model
 *
 * @property Question $Question
 * @property SurveyResult $SurveyResult
 */
class Group extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'question_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'unique' => array(
                'rule' => array('checkUnique', array('name', 'survey_id'), false),
                'message' => 'Group name already taken.',
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    /* public $belongsTo = array(
      'Question' => array(
      'className' => 'Question',
      'foreignKey' => 'question_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
      )
      ); */

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
//        'SurveyResult' => array(
//            'className' => 'SurveyResult',
//            'foreignKey' => 'group_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//            )
        'RespondentPasscode'=>array(
            'className' => 'RespondentPasscode',
            'foreignKey' => 'group_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function is_unique_name($name, $surveyId) {
        return !$this->hasAny(array('Group.name' => $name, 'Group.survey_id' => $surveyId, 'Group.is_deleted' => 'no'));
    }

    public function checkUnique($ignoredData, $fields, $or = true) {
        return $this->isUnique($fields, $or);
    }

}
