<?php

App::uses('AppModel', 'Model');

/**
 * SurveyTheme Model
 *
 * @property Survey $Survey
 */
class SurveyTheme extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'unique' => array(
                'rule' => array('checkUnique', array('name', 'survey_id'), false),
                'message' => 'Theme name already taken.',
            ),
        ),
        'survey_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Survey' => array(
            'className' => 'Survey',
            'foreignKey' => 'survey_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public $hasMany = array(
        'Question' => array(
            'className' => 'Question',
            'foreignKey' => 'survey_theme_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
    );
    
    public $actsAs = array(
        'Uploader.Attachment' => array(
            // Do not copy all these settings, it's merely an example
            'icon' => array(
                'tempDir' => TMP,
                'uploadDir' => 'files/uploads/theme_icons/',
                'finalPath' => 'files/uploads/theme_icons/',
                'dbColumn' => 'icon'
            )
        ),
        'Uploader.FileValidation' => array(
            'icon' => array(
                'maxWidth' => 50,
                'maxHeight' => 50,
                'extension' => array('gif', 'jpg', 'png', 'jpeg'),
                'type' => 'image',
                'required' => false
            )
        )
    );

    public function is_unique_name($name, $surveyId) {
        return !$this->hasAny(array('SurveyTheme.name' => $name, 'SurveyTheme.survey_id' => $surveyId, 'SurveyTheme.is_deleted' => '0'));
    }
    
    public function checkUnique($ignoredData, $fields, $or = true) {
        return $this->isUnique($fields, $or);
    }

}
