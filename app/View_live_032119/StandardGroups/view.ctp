<div class="standardGroups view">
<h2><?php echo __('Standard Group'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($standardGroup['StandardGroup']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($standardGroup['StandardGroup']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($standardGroup['StandardGroup']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Deleted'); ?></dt>
		<dd>
			<?php echo h($standardGroup['StandardGroup']['is_deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($standardGroup['StandardGroup']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($standardGroup['StandardGroup']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Standard Group'), array('action' => 'edit', $standardGroup['StandardGroup']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Standard Group'), array('action' => 'delete', $standardGroup['StandardGroup']['id']), null, __('Are you sure you want to delete # %s?', $standardGroup['StandardGroup']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Standard Groups'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Standard Group'), array('action' => 'add')); ?> </li>
	</ul>
</div>
