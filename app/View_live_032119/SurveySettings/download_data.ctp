<?php

$this->PhpExcel->createWorksheet();
$activeSheet = $this->PhpExcel->getActiveSheet();
$activeSheet->getDefaultStyle()->getAlignment()->setWrapText(true);

$titleStep = sizeof($questions)*2+4;
//title
$title = array(
    array('label' => __($survey['Survey']['name']), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $titleStep))
);
//description
$description = array(
    array('label' => __('Description : '.$survey['Survey']['description']), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $titleStep))
);
//date created
$created = array(
    array('label' => __('Date Created : '.$this->Time->format('F d, Y',$survey['Survey']['created'])), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $titleStep))
);

$this->PhpExcel->addTableHeader($title, array('size' => '15', 'bold' => true, 'align'=>'left'));
$this->PhpExcel->addTableHeader($description, array('size' => '12', 'align'=>'left'));
$this->PhpExcel->addTableHeader($created, array('size' => '12', 'align'=>'left'));

$this->PhpExcel->addTableRow(array(array('merge'=>array('start'=>0,'end'=> $titleStep))));

$respondentsTitle = array(
    array('label' => __('Respondents'), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $titleStep))
);
$this->PhpExcel->addTableHeader($respondentsTitle, array('size' => '13', 'bold' => true, 'align'=>'left'));

//respondents table header
if($survey['Survey']['open']){
    $questionTableHeader = array(
        array('label' => __('Date'), 'width' => 'auto'),
        array('label' => __('Email Address'), 'width' => 'auto'),
        array('label' => __('IP Address'), 'width' => 'auto'),
        array('label' => __('Group'), 'width' => 'auto')
    );
}else{
    $questionTableHeader = array(
        array('label' => __('Date'), 'width' => 'auto'),
        array('label' => __('Name'), 'width' => 'auto'),
        array('label' => __('IP Address'), 'width' => 'auto'),
        array('label' => __('Group'), 'width' => 'auto')
    );
}

$answerScoreHeader = array();
$qCnt = 0;
$startStep = 4;
foreach($questions as $q){
    $qCnt++;
    $questionTableHeader[] = array('label' => __($qCnt.'. '.$q['name']), 'width' => 'auto', 'merge'=>array('start'=>$startStep,'end'=> $startStep+1));
    $answerScoreHeader[] = array('label' => __('Answer'), 'width' => 'auto', 'merge'=>array('start'=>$startStep,'end'=> $startStep));
    $answerScoreHeader[] = array('label' => __('Score'), 'width' => 'auto', 'merge'=>array('start'=>$startStep+1,'end'=> $startStep+1));
    $startStep = $startStep+2;
}
  
$this->PhpExcel->addTableHeader($questionTableHeader, array('size' => '10', 'bold' => true));




$cellIterator = $activeSheet->getRowIterator()->current()->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(true);
    /** @var PHPExcel_Cell $cell */
    foreach ($cellIterator as $cell) {
        $activeSheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
    }

$this->PhpExcel->addTableHeader($answerScoreHeader, array('size' => '10', 'bold' => true));
$activeSheet->mergeCells("A6:A7");
$activeSheet->mergeCells("B6:B7");
$activeSheet->mergeCells("C6:C7");
$activeSheet->mergeCells("D6:D7");


//respondents' answers
foreach($surveyRespondents as $resp){
    $answerRow = array();
    $answerRow[] = array('label' => __($this->Time->format('j-M-y',$resp['SurveyResult']['created'])), 'width' => 'auto');
    if($survey['Survey']['open']){
        $answerRow[] = array('label' => __($resp['SurveyResult']['email']), 'width' => 'auto');
    }else{
        $answerRow[] = array('label' => __($resp['SurveyResult']['respondent']), 'width' => 'auto');
    }
    $answerRow[] = array('label' => __($resp['SurveyResult']['ipaddress']), 'width' => 'auto');
    $answerRow[] = array('label' => __($resp['Group']['name']), 'width' => 'auto');
    foreach($resp['SurveyAnswer'] as $key2=>$answer){
        $answerRow[] = array(
            'label' => __($questions[$answer['question_id']]['options'][$answer['question_option_id']]['name']?$questions[$answer['question_id']]['options'][$answer['question_option_id']]['name']:$answer['question_answer']),
            'width' => $questions[$answer['question_id']]['options'][$answer['question_option_id']]['name']?'auto':100
        );
        
        $answerRow[] = array(
            'label' => __($questions[$answer['question_id']]['options'][$answer['question_option_id']]['score']),
            'width' => 'auto');
    }
    $this->PhpExcel->addTableRow($answerRow, array('size' => '10', 'align'=>'left'));
}

$this->PhpExcel->addTableFooter();

//excel output
$this->PhpExcel->output($survey['Survey']['short_name'].'.xls');
?>
