<div class="themes form">
<?php echo $this->Form->create('SurveyTheme'); ?>
    <legend><?php echo __('Add Theme'); ?></legend>
    <?php echo $this->Form->input('survey_id', array('type'=>'hidden', 'value'=>$survey_id)); ?>
    <div class="add_more_fieldsets" data-fields="name">
        <fieldset class="add_more">

	<?php
		echo $this->Form->input('name', array('label'=>'Name','div'=>array('class'=>'form-group input text'),'class'=>'form-control','name'=>'name[0]'));
	
	?>
            <a href="#" class="btn btn-default add_another">Add another theme</a>
        </fieldset>
    </div>
<?php echo $this->Form->end(__(array('label' => 'Save', 'class' => 'btn btn-default'))); ?>
</div>

<div class="related"> 
    <br />
        <?php if(!sizeof($themeslist)){ ?>
    <p><br />No themes added.</p>
        <?php } ?>
	<?php if ($themeslist): ?>
    <p><?php echo __('Themes'); ?></p>
    <table class="table table-hover" cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('name'); ?></th>
            <th><?php echo $this->Paginator->sort('description'); ?></th>
            <th><?php echo $this->Paginator->sort('icon'); ?></th>
            <th><?php echo $this->Paginator->sort('color'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
	<?php foreach ($themeslist as $theme): ?>
        <tr>
            <td><?php echo h($theme['SurveyTheme']['name']); ?>&nbsp;</td>
            <td><?php echo h($theme['SurveyTheme']['description']); ?>&nbsp;</td>
            <td><img class="img-responsive" src="<?php echo $theme['SurveyTheme']['icon']?APP_URL.$theme['SurveyTheme']['icon']:APP_URL.'files/uploads/standard_theme_icons/icon-default.png'; ?>" />&nbsp;</td>
            <td style="color:<?php echo $theme['SurveyTheme']['color']; ?>"><?php echo h($theme['SurveyTheme']['color']); ?>&nbsp;</td>
            <td class="actions">
                    <?php echo $this->Html->link(__('Edit'), array('controller' => 'survey_themes', 'action' => 'edit', $theme['SurveyTheme']['id'],$survey_id)); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'survey_themes', 'action' => 'delete', $theme['SurveyTheme']['id'],$survey_id,$return), null, __('Are you sure you want to delete # %s?', $k)); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
<?php endif; ?>

</div>