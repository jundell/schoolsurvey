<?php if(!isset($status) || $status){ ?>
<div class="surveys view take-survey">
    <?php echo $this->Form->create('Question'); ?>
    <legend><?php echo $group_msg; ?></legend>
    <?php
    echo $this->Form->input('group', array(
        'div'=>array('class'=>'form-group input'),
                    'separator'=> '</label></div><div class="radio"><label>',
                    'before' => '<div class="radio"><label>',
                    'after' => '</label></div>',
        'options' => $groups,
        'type' => 'radio',
        'legend' => false,
//        'value' => array_shift(array_keys($groups)),
    ));
    ?>
    
    <br />
    <?php echo $this->Form->end(array('label' => 'Next >>>', 'class' => 'btn btn-success')); ?>
</div>
<div class="progress progress-striped active">
    <div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
        <span class="sr-only">0% Complete</span>
    </div>
</div>
<?php } ?>