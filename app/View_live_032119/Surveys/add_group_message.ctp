<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="survey-buttons">
    <?php // echo $this->Html->link(__('Choose from standard themes'), array('controller'=>'standard_themes', 'action' => 'choose', $survey_id), array('type'=>'button','class'=>'btn btn-success')); ?>
    <?php echo $this->Html->link(__('Click here to add dimensions'), array('controller'=>'survey_themes','action' => 'add', $survey_id, 1), array('type'=>'button','class'=>'btn btn-success')); ?>
</div>

<div class="survey-build">
    <?php //survey details ?>
    <h1><?php echo $survey['Survey']['name']; ?></h1>
    <h4>Description</h4>
    <p><?php echo $survey['Survey']['description']; ?></p>
    <h4>Welcome Message</h4>
    <p><?php echo $survey['Survey']['welcome_message']; ?></p>
    <h4>Group Message</h4>
    <p><?php echo $group_message; ?></p>
    <h4>Closing Message</h4>
    <p><?php echo $survey['Survey']['closing_message']; ?></p>
    
    <?php //groups ?>
    <div class="groups">
        <h3>Groups</h3>
        <ul class="list-unstyled">
        <?php foreach($groups as $group){ ?>
            <li><?php echo $group; ?></li>
        <?php } ?>
        </ul>
    </div>
</div>