<div class="respondentPasscodes form">
<?php echo $this->Form->create('RespondentPasscode'); ?>
    <fieldset>
        <legend><?php echo __('Edit Respondent'); ?></legend>
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-4">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name',array('class'=>'form-control','div'=>array('class'=>'form-group input text')));
		echo $this->Form->input('passcode',array('class'=>'form-control','div'=>array('class'=>'form-group input text')));
                ?>
            </div>
        </div>
    </fieldset>
<?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-success'))); ?>
</div>
<br /></br />
<?php echo $this->Html->link(__('Go back to Respondents List'), array('controller' => 'groups', 'action' => 'respondents/'.$groupId), array('class'=>'btn btn-default')); ?>
<br /></br />
<?php echo $this->Html->link(__('Go back to Groups List'), array('controller' => 'groups', 'action' => 'add/'.$surveyId), array('class'=>'btn btn-default')); ?>
<br /></br />
<?php echo $this->element('view_manage_survey_btn'); ?>