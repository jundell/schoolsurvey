<div class="related">
    <h2><?php echo __('Standard Contents'); ?></h2>
        <?php if(!sizeof($standardContents)){ ?>
             <br />No standard contents added.
        <?php }else{ ?>
             <div class="table-responsive">
            <table class="table table-hover" cellpadding="0" cellspacing="0" >
                <tr>
                    <th><?php echo $this->Paginator->sort('name'); ?></th>
                    <th><?php echo $this->Paginator->sort('value'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                <?php foreach ($standardContents as $standardContent): ?>
                <tr>
                    <td><?php echo h($standardContent['StandardContent']['name']); ?>&nbsp;</td>
                    <td><?php echo h($standardContent['StandardContent']['value']); ?>&nbsp;</td>
                    <td class="actions">
                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $standardContent['StandardContent']['id'])); ?>
                    </td>
                </tr>
        <?php endforeach; ?>
            </table>
             </div>
            <p>
                <?php
                echo $this->Paginator->counter(array(
                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                ));
                ?>	</p>
            <div class="paging">
                <?php
                        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                        echo $this->Paginator->numbers(array('separator' => ''));
                        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                ?>
            </div>
        <?php } ?>
</div>
<?php echo $this->Html->link(__('Back to survey builder menu'), array('controller'=>'survey_builder_components'), array('type'=>'button','class'=>'btn btn-default', 'style'=>'margin-top: 30px;')); ?>
