<?php if(!isset($status) || $status){ ?>
<?php if(!$hasError){ ?>
    <div class="surveys view" >
        <h2><?php echo h($survey['Survey']['name']); ?></h2>
        <dl>
            <dt></dt>
            <dd>
                <?php echo h($survey['Survey']['description']); ?>
            </dd>
        </dl>
    </div>
    <div class="progress progress-striped active">
        <div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
            <span class="sr-only">0% Complete</span>
        </div>
    </div>

    <a href="<?php echo $this->webroot; ?>surveys/start/<?php echo $survey['Survey']['id']; ?>" class="btn btn-default">Start >>> </a><br />
<?php } ?>
<?php }?>