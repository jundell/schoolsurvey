<?php

$this->PhpExcel->createWorksheet();

$groupCells = sizeof($result["groups"]) * 2;

//title
$title = array(
    array('label' => __($result['survey']['Survey']['name']), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $groupCells + 4))
);

//h1
$h1 = array(
    array('label' => __(isset($settings['report_respondent_header'])?$settings['report_respondent_header']:'Respondent'), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $groupCells + 2)),
    array('label' => __(isset($settings['report_admin_header'])?$settings['report_admin_header']:'Admin'), 'width' => 'auto', 'merge'=>array('start'=>$groupCells + 3,'end'=>$groupCells + 3)),
    array('label' => __(isset($settings['report_overall_header'])?$settings['report_overall_header']:'Overall'), 'width' => 'auto', 'merge'=>array('start'=>$groupCells + 4,'end'=>$groupCells + 4)),
);

//h2
$h2 = array(
    array('label' => __(''), 'width' => 'auto'),
);
$groupStep = 1;
foreach ($result["groups"] as $k => $v) {
    $h2[] = array('label' => __($k), 'width' => 'auto', 'merge'=>array('start'=>$groupStep,'end'=>$groupStep+1));
    $groupStep = $groupStep+2;
}
$h2[] = array('label' => __(isset($settings['report_total_header'])?$settings['report_total_header']:'Total'), 'width' => 'auto', 'merge'=>array('start'=>$groupStep,'end'=>$groupStep+1));
$groupStep = $groupStep+2;
$h2[] = array('label' => __(isset($settings['report_admin_second_row_header'])?$settings['report_admin_second_row_header']:'Admin'), 'width' => 'auto', 'merge'=>array('start'=>$groupStep,'end'=>$groupStep));
$h2[] = array('label' => __(isset($settings['report_awareness_quotient_header'])?$settings['report_awareness_quotient_header']:'Awareness Quotient'), 'width' => 10, 'merge'=>array('start'=>$groupStep+1,'end'=>$groupStep+1));

//h3
$h3 = array(
    array('label' => __(isset($settings['report_questions_header'])?$settings['report_questions_header']:'Questions'), 'width' => 'auto'),
);
foreach ($result["groups"] as $k=>$v) {
    $h3[] = array('label' => __(isset($settings['report_average_score_header'])?$settings['report_average_score_header']:'Ave Score'), 'width' => 'auto');
    $h3[] = array('label' => __(isset($settings['report_number_of_respondents_header'])?$settings['report_number_of_respondents_header']:'# of Respondents'), 'width' => 'auto');
}
$h3[] = array('label' => __(isset($settings['report_average_score_header'])?$settings['report_average_score_header']:'Ave Score'), 'width' => 'auto');
$h3[] = array('label' => __(isset($settings['report_number_of_respondents_header'])?$settings['report_number_of_respondents_header']:'# of Respondents'), 'width' => 'auto');
$h3[] = array('label' => __(isset($settings['report_score_header'])?$settings['report_score_header']:'Score'), 'width' => 'auto');
$h3[] = array('label' => __(isset($settings['report_difference_header'])?$settings['report_difference_header']:'Difference'), 'width' => 'auto');
$this->PhpExcel->addTableHeader($title, array('size' => '15', 'bold' => true, 'align'=>'left'));
$this->PhpExcel->addTableHeader($h1, array('size' => '10', 'bold' => true));
$this->PhpExcel->addTableHeader($h2, array('size' => '10', 'bold' => true));
$this->PhpExcel->addTableHeader($h3, array('size' => '10', 'bold' => true));

//questions
$cnt = 1;
foreach ($result["questions"] as $k => $v) {

    $color = str_replace('#', '', $v['SurveyTheme']['color']);
    $row = array(array('label' => __($cnt), 'width' => 'auto', 'color'=>$color));
        if($v['Question']['is_calculating']=='1' && $v['Question']['type']!='open ended'){
            foreach ($result["groups"] as $gk => $gv) {
                $row[] = array('label' => __($gv['avgScores'][$v['Question']['id']]), 'width' => 'auto', 'color'=>$color, 'numeric'=>true);
                $row[] = array('label' => __($gv['respCount'][$v['Question']['id']]), 'width' => 'auto', 'color'=>$color, 'numeric'=>true);
            }
            $row[] = array('label' => __($result['totalAvg']['avgScores'][$v['Question']['id']]), 'width' => 'auto', 'color'=>$color, 'numeric'=>true);
            $row[] = array('label' => __($result['totalAvg']['respCount'][$v['Question']['id']]), 'width' => 'auto', 'color'=>$color, 'numeric'=>true);
            $row[] = array('label' => __($result['admin']['scores'][$v['Question']['id']]), 'width' => 'auto', 'color'=>$color, 'numeric'=>true);
            $row[] = array('label' => __($result['AQ']['aqs'][$v['Question']['id']].(!$result['admin']['scores'][$v['Question']['id']]?' *':'')), 'width' => 'auto', 'color'=>$color, 'numeric'=>true);
        }else{
            $oeText = isset($settings['report_open_ended_label'])?$settings['report_open_ended_label']:'Open Ended';
            $ncText = isset($settings['report_non_calculating_label'])?$settings['report_non_calculating_label']:'Non-calculating';
            $row[] = array('label' => __($v['Question']['type']=='open ended'?$oeText:$ncText), 'width' => 'auto', 'color'=>$color, 'merge'=>array('start'=>1,'end'=>$groupCells + 4));
        }
        $cnt++;
        $this->PhpExcel->addTableRow($row, array('size' => '10'));
}

//overall total
$rowOverall = array(array('label' => __(isset($settings['report_overall_total_label'])?$settings['report_overall_total_label']:'Overall Total'), 'width' => 'auto'));
foreach ($result["groups"] as $gk => $gv) {
    $rowOverall[] = array('label' => __($gv['totalAvgScore']), 'width' => 'auto', 'numeric'=>true);
    $rowOverall[] = array('label' => __($gv['totalRespCount']), 'width' => 'auto', 'numeric'=>true);
}
$rowOverall[] = array('label' => __($result['totalAvg']['totalAvgScore']), 'width' => 'auto', 'numeric'=>true);
$rowOverall[] = array('label' => __($result['totalAvg']['totalRespCount']), 'width' => 'auto', 'numeric'=>true);
$rowOverall[] = array('label' => __($result['admin']['totalAvgScore']), 'width' => 'auto', 'numeric'=>true);
$rowOverall[] = array('label' => __($result['AQ']['total']), 'width' => 'auto', 'numeric'=>true);
$this->PhpExcel->addTableRow($rowOverall, array('size' => '10', 'bold' => true));

//legend
$this->PhpExcel->addTableRow(array(array('merge'=>array('start'=>0,'end'=> $groupCells + 4))));
if(sizeof($themes)){
    $row = array(array('label' => __(isset($settings['report_legend_label'])?$settings['report_legend_label']:'Legend:'), 'width' => 'auto','merge'=>array('start'=>0,'end'=> $groupCells + 4)));
    $this->PhpExcel->addTableRow($row, array('size' => '12', 'bold' => true, 'align'=>'left'));
    foreach($themes as $theme){
        $rowLegend = array(
            array('label' => __(''), 'width' => 'auto', 'color'=>str_replace('#', '', $theme['SurveyTheme']['color'])),
            array('label' => __($theme['SurveyTheme']['name']), 'width' => 'auto', 'merge'=>array('start'=>1,'end'=> $groupCells + 4)),
        );
        $this->PhpExcel->addTableRow($rowLegend, array('size' => '9','align'=>'left'));
    }
}

//note
$this->PhpExcel->addTableRow(array(array('label' => __(isset($settings['report_admin_answer_note'])?$settings['report_admin_answer_note']:'Please note that questions with * are not yet answered by the Admin.'), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $groupCells + 4))), array('size' => '9','align'=>'left', 'italic'=>true));

$this->PhpExcel->addTableRow(array(array('merge'=>array('start'=>0,'end'=> $groupCells + 4))));

//open ended questions
if(sizeof($openEnded)){
    $this->PhpExcel->addTableRow(
            array(array('label' => __(isset($settings['report_open_ended_questions_label'])?$settings['report_open_ended_questions_label']:'OPEN ENDED QUESTIONS'), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $groupCells + 4))), array('size' => '12', 'bold' => true, 'align'=>'left'));
    $qCount = 1;
    $this->PhpExcel->addTableRow(array(array('merge'=>array('start'=>0,'end'=> $groupCells + 4))));
    foreach($result["questions"] as $k => $v){
	if($v['Question']['type']=='open ended'){
            $this->PhpExcel->addTableRow(array(array('label' => __($qCount.'. '.$openEnded[$v['Question']['id']]['question_name']), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $groupCells + 4))), array('size' => '12', 'bold' => true, 'align'=>'left'));        
            foreach($openEnded[$v['Question']['id']]['groups'] as $g){
                if(sizeof($g['resp'])){
                    $this->PhpExcel->addTableRow(array(array('label' => __($g['name']), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $groupCells + 4))), array('size' => '12', 'bold' => true, 'align'=>'left'));
                    
                    $this->PhpExcel->addTableRow(array(
                            array('label' => __(isset($settings['report_open_ended_email_header'])?$settings['report_open_ended_email_header']:'Email'), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> 1)),
                            array('label' => __(isset($settings['report_open_ended_answer_header'])?$settings['report_open_ended_answer_header']:'Answer'), 'width' => 'auto', 'merge'=>array('start'=>2,'end'=> $groupCells + 4)),
                            
                        ), array('size' => '12', 'bold' => true)  
                    );
                
                    foreach($g['resp'] as $r){
                        $this->PhpExcel->addTableRow(array(
                                array('label' => __($r['email']), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> 1)),
                                array('label' => __($r['answer']), 'width' => 'auto', 'merge'=>array('start'=>2,'end'=> $groupCells + 4)),
                            ),array('size' => '12', 'align'=>'left')
                        );
                    }
                    $this->PhpExcel->addTableRow(array(array('merge'=>array('start'=>0,'end'=> $groupCells + 4))));
		}
            }
	}
	$qCount++;
    }
}

$this->PhpExcel->addTableRow(array(array('merge'=>array('start'=>0,'end'=> $groupCells + 4))));

//non calculating
 if(sizeof($nonCalculating)){
    $this->PhpExcel->addTableRow(array(array('label' => __(isset($settings['report_non_calculating_questions_label'])?$settings['report_non_calculating_questions_label']:'NON-CALCULATING QUESTIONS'), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $groupCells + 4))), array('align'=>'left','size'=>12,'bold'=>true));
    $this->PhpExcel->addTableRow(array(array('merge'=>array('start'=>0,'end'=> $groupCells + 4))));
    $qCount = 1;
    $noOfRespText = isset($settings['report_non_calculating_number_of_respondents_header'])?$settings['report_non_calculating_number_of_respondents_header']:'# of respondents';
    foreach($result["questions"] as $k => $v){ 
        if(in_array($v['Question']['type'],array('likert','radio button')) && !$v['Question']['is_calculating']){ 
            $this->PhpExcel->addTableRow(array(array('label' => __($qCount.'. '.$nonCalculating[$v['Question']['id']]['question_name']), 'width' => 'auto', 'merge'=>array('start'=>0,'end'=> $groupCells + 4))), array('bold'=>true, 'size'=>12, 'align'=>'left'));
            $ncHeader = array(
                array('label' => __(isset($settings['report_non_calculating_options_header'])?$settings['report_non_calculating_options_header']:'Options'), 'width' => 'auto')
            );
            $ncSubHeader = array(array('label' => __(), 'width' => 'auto'));
            foreach($nonCalculating[$v['Question']['id']]['groups'] as $g){ 
                $ncHeader[] = array('label' => __($g['name']), 'width' => 'auto');
                $ncSubHeader[] = array('label' => __($noOfRespText), 'width' => 'auto');
            }
            $ncHeader[] = array('label' => __(isset($settings['report_non_calculating_admin_header'])?$settings['report_non_calculating_admin_header']:'Admin'), 'width' => 'auto');
            $ncHeader[] = array('label' => __(isset($settings['report_non_calculating_total_header'])?$settings['report_non_calculating_total_header']:'Total'), 'width' => 'auto');
            $ncSubHeader[] = array('label' => __($noOfRespText), 'width' => 'auto');
            $ncSubHeader[] = array('label' => __($noOfRespText), 'width' => 'auto');
            $this->PhpExcel->addTableRow($ncHeader, array('size'=>12, 'bold'=>true));
            $this->PhpExcel->addTableRow($ncSubHeader, array('size'=>9));

            foreach($nonCalculating[$v['Question']['id']]['question_options'] as $kqo => $qo){
                $ncValues = array(array('label' => __($qo), 'width' => 'auto'));
                $total = 0; 
                foreach($nonCalculating[$v['Question']['id']]['groups'] as $g){ 
                    $count = isset($g[$kqo])?$g[$kqo]:0; 
                    $total+= $count;
                    $ncValues[] = array('label' => __($count), 'width' => 'auto', 'numeric'=>true);
                } 
                $adminResp = intval($nonCalculating[$v['Question']['id']]['admin'][$kqo]); 
                $ncValues[] = array('label' => __($adminResp), 'width' => 'auto', 'numeric'=>true);
                $ncValues[] = array('label' => __($total+$adminResp), 'width' => 'auto', 'numeric'=>true);
                $this->PhpExcel->addTableRow($ncValues);
            }
            $this->PhpExcel->addTableRow(array(array('merge'=>array('start'=>0,'end'=> $groupCells + 4))));
        }
        $qCount++; 
    }
}

$this->PhpExcel->addTableFooter();
$this->PhpExcel->output($result['survey']['Survey']['name'].'.xls');
?>