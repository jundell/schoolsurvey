<div class="questionGroups view">
<h2><?php  echo __('Question Group'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($questionGroup['QuestionGroup']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($questionGroup['Question']['id'], array('controller' => 'questions', 'action' => 'view', $questionGroup['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Uq Group'); ?></dt>
		<dd>
			<?php echo $this->Html->link($questionGroup['UqGroup']['name'], array('controller' => 'uq_groups', 'action' => 'view', $questionGroup['UqGroup']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($questionGroup['QuestionGroup']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($questionGroup['QuestionGroup']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Question Group'), array('action' => 'edit', $questionGroup['QuestionGroup']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Question Group'), array('action' => 'delete', $questionGroup['QuestionGroup']['id']), null, __('Are you sure you want to delete # %s?', $questionGroup['QuestionGroup']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Question Groups'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question Group'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Uq Groups'), array('controller' => 'uq_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Uq Group'), array('controller' => 'uq_groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
