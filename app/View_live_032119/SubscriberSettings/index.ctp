<div class="subscriberSettings index">
	<h2><?php echo __('Subscriber Settings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('slug'); ?></th>
			<th><?php echo $this->Paginator->sort('value'); ?></th>
			<th><?php echo $this->Paginator->sort('subscriber_id'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('font-size'); ?></th>
			<th><?php echo $this->Paginator->sort('alignment'); ?></th>
			<th><?php echo $this->Paginator->sort('color'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($subscriberSettings as $subscriberSetting): ?>
	<tr>
		<td><?php echo h($subscriberSetting['SubscriberSetting']['id']); ?>&nbsp;</td>
		<td><?php echo h($subscriberSetting['SubscriberSetting']['name']); ?>&nbsp;</td>
		<td><?php echo h($subscriberSetting['SubscriberSetting']['slug']); ?>&nbsp;</td>
		<td><?php echo h($subscriberSetting['SubscriberSetting']['value']); ?>&nbsp;</td>
		<td><?php echo h($subscriberSetting['SubscriberSetting']['subscriber_id']); ?>&nbsp;</td>
		<td><?php echo h($subscriberSetting['SubscriberSetting']['description']); ?>&nbsp;</td>
		<td><?php echo h($subscriberSetting['SubscriberSetting']['font-size']); ?>&nbsp;</td>
		<td><?php echo h($subscriberSetting['SubscriberSetting']['alignment']); ?>&nbsp;</td>
		<td><?php echo h($subscriberSetting['SubscriberSetting']['color']); ?>&nbsp;</td>
		<td><?php echo h($subscriberSetting['SubscriberSetting']['created']); ?>&nbsp;</td>
		<td><?php echo h($subscriberSetting['SubscriberSetting']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $subscriberSetting['SubscriberSetting']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $subscriberSetting['SubscriberSetting']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $subscriberSetting['SubscriberSetting']['id']), null, __('Are you sure you want to delete # %s?', $subscriberSetting['SubscriberSetting']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Subscriber Setting'), array('action' => 'add')); ?></li>
	</ul>
</div>
