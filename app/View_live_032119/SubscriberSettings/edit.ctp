<div class="subscriberSettings form">
<?php echo $this->Form->create('SubscriberSetting'); ?>
	<fieldset>
		<legend><?php echo __('Edit Subscriber Setting'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('slug');
		echo $this->Form->input('value');
		echo $this->Form->input('subscriber_id');
		echo $this->Form->input('description');
		echo $this->Form->input('font-size');
		echo $this->Form->input('alignment');
		echo $this->Form->input('color');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SubscriberSetting.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('SubscriberSetting.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Subscriber Settings'), array('action' => 'index')); ?></li>
	</ul>
</div>
