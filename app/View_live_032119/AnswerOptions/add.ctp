<div class="answerOptions form">
<?php echo $this->Form->create('AnswerOption'); ?>
	<fieldset>
		<legend><?php echo __('Add Answer Option'); ?></legend>
	<?php
		echo $this->Form->input('option');
		echo $this->Form->input('value');
		echo $this->Form->input('question_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Answer Options'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions Answers'), array('controller' => 'questions_answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Questions Answer'), array('controller' => 'questions_answers', 'action' => 'add')); ?> </li>
	</ul>
</div>
