<?php ?>
<div class="col-xs-12">
    <legend>Choose from these standard themes.</legend>
<?php echo $this->Form->create('StandardTheme', array('type'=>'file')); ?>
    <div class="survey-buttons">
    <?php echo $this->Html->link(__('Check All'), '#' , array('type'=>'button','class'=>'check-all btn btn-default', 'data-checkbox'=>'standard-theme', 'style'=>'margin-top: 30px;')); ?>
    <?php echo $this->Html->link(__('Uncheck All'), '#' , array('type'=>'button','class'=>'uncheck-all btn btn-default', 'data-checkbox'=>'standard-theme', 'style'=>'margin-top: 30px;')); ?>
    </div>
<?php
    $cnt = 0;
    foreach($standardThemes as $theme){ ?>
    <div class="checkbox">

        <label for="theme[<?php echo $cnt; ?>]">
            <input type="checkbox" class="standard-theme" name="theme[<?php echo $cnt; ?>]" value="<?php echo $theme['StandardTheme']['id']; ?>"/>
            <img class="standard-theme-icon img-responsive" src="<?php echo $theme['StandardTheme']['icon']?APP_URL.$theme['StandardTheme']['icon']:APP_URL.'files/uploads/standard_theme_icons/icon-default.png'; ?>" /><span class="standard-theme-name" style="color:#<?php echo $theme['StandardTheme']['color'] ?>;"><?php echo $theme['StandardTheme']['name']; ?></span>
        </label>
    </div>
    <?php $cnt++; ?>
<?php } ?>
    <br />
    <strong>Add Your Own Theme</strong>
    <input name="add_another_theme" class="add_another_theme" value="0" type="hidden">
    <div class="choose-theme-add add_more_fields" data-fields="ownTheme[name],ownTheme[description],ownTheme[icon],ownTheme[color]">
        <fieldset class="add_more form-theme">
                <div class="row">
                    <div class="col-xs-12 col-sm-3">
                <?php
                            echo $this->Form->input('name', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control', 'name'=>'ownTheme[name]', 'required'=>false));
                            echo $this->Form->input('description', array('div'=>array('class'=>'form-group input textarea'),'class'=>'form-control', 'name'=>'ownTheme[description]', 'required'=>false));
                            echo $this->Form->input('icon', array('div'=>array('class'=>'form-group input file'), 'type'=>'file', 'name'=>'data[ownTheme][icon]'));
                            echo $this->Form->input('color', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control color','style'=>'width:80px;', 'default'=>'000000', 'name'=>'ownTheme[color]'));
                    ?>
                    </div>
                </div>
        </fieldset>
        
    </div>
<br />
    <a href="#" class="btn btn-default another_theme" id="save-and-add">Save and add another theme</a><br /><br />
    <?php echo $this->Form->end(__(array('label' => 'Finish', 'class' => 'btn btn-success'))); ?>
</div>