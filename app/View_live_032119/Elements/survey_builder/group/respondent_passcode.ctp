    <fieldset class="add_more_row form-respondent">
        <div class="row row-wrap">
            
            <div class="col-xs-12 col-md-4">
                <?php echo $this->Form->input('name', array('disabled'=>sizeof($grouplist) < Configure::read('site_config.max_groups')?false:'disabled','class' => 'form-control','name'=>$name, 'required'=>false, 'default'=>isset($defaultName)?$defaultName:'')); ?>
            </div>
            <div class="col-xs-12 col-md-4 last-col-input">
                <?php echo $this->Form->input('passcode', array('disabled'=>sizeof($grouplist) < Configure::read('site_config.max_groups')?false:'disabled','class' => 'form-control','name'=>$passcode, 'required'=>false, 'default'=>isset($defaultPasscode)?$defaultPasscode:'')); ?>
            </div>
            <?php if(isset($addBtn)&& $addBtn && sizeof($grouplist) < Configure::read('site_config.max_groups')){ ?>
                <div class="col-xs-8 col-md-3 add-row-btn-wrapper">
                    <a href="#" class="btn btn-default add_another_row">Add another respondent</a>
                </div>
            <?php } ?>
            <?php if(isset($removeBtn)&&$removeBtn){ ?>
                <div class="col-xs-4 col-md-1 remove-row-btn-wrapper">
                    <a href="#" class="btn btn-default remove-row">X</a>
                </div>
            <?php } ?>
        </div>
    </fieldset>
