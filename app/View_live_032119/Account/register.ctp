 <div class="col-xs-12 col-sm-8 col-sm-offset-2">    
<?php echo $this->Form->create(array('url' => '/account/register', 'autocomplete' => 'off', 'class' => 'form-signup', 'novalidate' => true)); ?>
    <h2 class="form-signup-heading">Register</h2>
    
    <div class="form-group row">
        <?php echo $this->Form->input('first_name', array('label' => '<span class="required">* </span>First Name','class' => 'form-control','div'=>array('class'=>'col-xs-12 col-sm-6 input text'))); ?> 
        <?php echo $this->Form->input('last_name', array('label' => '<span class="required">* </span>Last Name','class' => 'form-control','div'=>array('class'=>'col-xs-12 col-sm-6 input text'))); ?> 
    </div>

    <div class="form-group row">
        <?php echo $this->Form->input('email_address', array('label' => '<span class="required">* </span>Email Address','class' => 'form-control','div'=>array('class'=>'col-xs-12 col-sm-6 input text'))); ?> 
        <?php echo $this->Form->input('email_address_confirm', array('label' => '<span class="required">* </span>Email Address Confirm','class' => 'form-control','div'=>array('class'=>'col-xs-12 col-sm-6 input text'))); ?> 
    </div>

    <div class="form-group row">
        <?php echo $this->Form->input('password', array('label' => '<span class="required">* </span>Password', 'type' => 'password','class' => 'form-control','div'=>array('class'=>'col-xs-12 col-sm-6 input text'))); ?> 
        <?php echo $this->Form->input('password_confirm', array('label' => '<span class="required">* </span>Password Confirm', 'type' => 'password','class' => 'form-control','div'=>array('class'=>'col-xs-12 col-sm-6 input text'))); ?> 
    </div><br />

    <div class="form-group">
        <?php   echo $this->Recaptcha->display();?>    
    </div>

<?php   echo $this->Form->submit('Submit', array('class' => 'btn btn-lg btn-primary')); ?>
<?php   echo $this->Form->end() ?>
</div>