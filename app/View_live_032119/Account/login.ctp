<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
<?php echo $this->Form->create('User', array('class' => 'form-signin'));?>
    <h2 class="form-signin-heading">Please sign in</h2>
    <?php echo $this->Form->input('email_address', array('label' => false, 'class' => 'form-control', 'placeholder' => 'Email address')); ?>
    <?php echo $this->Form->input('password', array('label' => false, 'class' => 'form-control', 'placeholder' => 'Password')); ?>
    <button class="btn btn-lg btn-primary" type="submit">Sign in</button>
    <br /><br />
    <p><a href="<?php echo $this->webroot; ?>account/forgot_password/">Forgot your password?</a></p>
<?php echo $this->Form->end();?>
</div>