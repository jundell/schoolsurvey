<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1 user-scalable=no">
        <meta name="description" content="">
        <meta http-equiv="content-type" content="text/plain; charset=UTF-8"/>
        <meta name="author" content="">
        <?php echo $this->Html->charset(); ?>
        <title>
		<?php echo $title_for_layout; ?>
        </title>
	<?php
		echo $this->Html->meta('icon');
                 echo $this->fetch('meta');
	?>
        <?php
            echo $this->Html->css(array('/report/css/bootstrap.min.css','/report/style.css'));
        ?>
    </head>  
    <body>
	<?php echo $content_for_layout; ?>
        <?php echo $this->Html->script(array('/report/js/jquery1.11.3.min','/report/js/bootstrap.min', '/report/js/ie10-viewport-bug-workaround')); ?>
        <?php echo $this->element('footer'); //footer element ?>
    </body>
</html>
