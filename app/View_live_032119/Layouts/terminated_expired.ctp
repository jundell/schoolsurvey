<!DOCTYPE html>
<html lang="en">
    <head>
	<?php echo $this->Html->charset(); ?>
        <title>
		<?php echo $title_for_layout; ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
	<?php
		echo $this->Html->meta('icon');
                
                echo $this->fetch('meta');

		echo $this->Html->css(array('bootstrap', 'bootstrap-responsive.min', 'style'));
		
		echo $this->fetch('css');

                echo $this->Html->script(array('jquery-1.10.2', 'bootstrap.min','survey'));
        
		echo $this->fetch('script');
	?>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src=".js/html5shiv.js"></script>
        <![endif]-->
        <script type="text/javascript">
            window.history.forward();
            function noBack() {
                window.history.forward();
            }
        </script>
    </head>
    <body onload="noBack();"
        onpageshow="if (event.persisted) noBack();" onunload="">

     
        <div class="container">
    <?php
        //echo $this->Session->flash('auth');
        echo $this->Session->flash();
    ?>
	<?php echo $content_for_layout ?>

            <hr>

        </div>
        <footer>
            <?php // echo $footer; ?>
            <!--&copy; CopyRight 2015 - Larry Lobert and Associates - All Rights Reserved-->
        </footer>
        <?php // echo $this->element('footer'); //footer element ?>
    </body>
</html>
