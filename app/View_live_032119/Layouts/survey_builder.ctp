<!DOCTYPE html>
<html lang="en">
    <head>
	<?php echo $this->Html->charset(); ?>
        <title>
		<?php echo $title_for_layout; ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta http-equiv="content-type" content="text/plain; charset=UTF-8"/>
        <meta name="author" content="">
	<?php
		echo $this->Html->meta('icon');
                 echo $this->fetch('meta');

		/*echo $this->Html->css(array('bootstrap.min', 'bootstrap-responsive.min', 'style'));*/
                echo $this->Html->css(array('bootstrap.min', 'style', 'survey_builder'));
		
		echo $this->fetch('css');

                /*echo $this->Html->script(array('jquery-1.10.2', 'bootstrap.min','survey'));*/
                echo $this->Html->script(array('jquery1.11.3.min', 'bootstrap.min','jscolor/jscolor', 'survey', 'survey_builder'));
        
		echo $this->fetch('script');
	?>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src=".js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>

    <?php echo $this->element('report/menu_header'); //header menu ?>

        <div class="container"> 
            <div class="row">
                <div class="col-xs-12">
                    <?php
                        //echo $this->Session->flash('auth');
                        echo $this->Session->flash();
                    ?>
                    <?php echo $content_for_layout ?>

                    <div class="clearfix"></div><hr> 
                </div>
            </div>
        </div>
        <?php echo $this->element('footer'); //footer element ?>

    </body>
</html>
