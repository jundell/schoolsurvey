<?php

echo $this->Html->script(array('jquery.numeric.min')); ?>
<?php if(sizeof($themes)){ ?>
<div class="standardQuestions form">
<?php echo $this->Form->create('StandardQuestion'); ?>
    <div class="question_fieldsets">
        <fieldset>
            <legend><?php echo __('Add Standard Descriptor-Question'); ?></legend>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
	<?php
		echo $this->Form->input('name', array('label'=>'Descriptor-Question','div'=>array('class'=>'form-group input text'),'class'=>'form-control','maxlength'=>5000));
		echo $this->Form->input('theme_id', array(
                            'label' => 'Dimension',
                            'options' => $themes,
                                                'class'=>'form-control',
                                                'div'=>array('class'=>'form-group input text'),
                        ));
                echo $this->Form->input('type', array(
                            'options' => $types,
                            'div'=>array('class'=>'form-group input select'),
                            'class'=>'form-control'
                        ));?>


                    <div class="input checkbox " id="non-calculating">
                    <?php 
                        $is_calculating = true;
                        if(isset($this->request->data['StandardQuestion']['is_calculating'])){
                            $is_calculating = $this->request->data['StandardQuestion']['is_calculating']?true:false;
                        }
                    ?>
                    <?php echo $this->Form->input('is_calculating',array('type'=>'checkbox','div'=>false, 'label'=>'Calculating', 'checked'=>$is_calculating)); ?>
                    </div>

                </div>
            </div>
        </fieldset>
        <div class="add_more_fieldsets" data-fields="option,score">
                <?php if($this->request->is('post')){ ?>
                <?php
                    $options = $this->request->data['option'];
                    $scores = $this->request->data['score'];
                ?>
            <?php if(isset($options)&& sizeof($options)){ ?>
                <?php $size = sizeof($options); ?>
                <?php $cnt = 1; ?>
                <?php foreach($options as $k=>$o){ ?>
            <fieldset class="add_more form-group">
                    <?php
                            echo $this->Form->input('option', array('label'=>'Option','class'=>'form-control','type'=>'text','name'=>'option['.$k.']', 'value'=>$o));
                            echo $this->Form->input('score', array('label'=>'Score','name'=>'score['.$k.']','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>$scores[$k]));
                    ?>
                         <?php if($cnt==$size){ ?>
                <a href="#" class="btn btn-default add_another" data-lastId="">Add another option</a>
                        <?php } ?>
                        <?php if($cnt>2){ ?>
                <a class="btn btn-default remove" href="#">X</a>
                        <?php } ?>
                        <?php $cnt++; ?>
            </fieldset>

                <?php } ?>
            <?php }}else{ ?>
            <fieldset class="add_more form-group">
            <?php
                    echo $this->Form->input('option', array('label'=>'Option','class'=>'form-control','name'=>'option[0]'));
                    echo $this->Form->input('score', array('label'=>'Score','name'=>'score[0]','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>'1'));
            ?>
            </fieldset>
            <fieldset class="add_more form-group">
            <?php
                    echo $this->Form->input('option', array('label'=>'Option','class'=>'form-control','name'=>'option[1]'));
                    echo $this->Form->input('score', array('label'=>'Score','name'=>'score[1]','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>'2'));
            ?>
                <a href="#" class="btn btn-default add_another" data->Add another option</a>
            </fieldset>
            <?php } ?>
        </div>
    </div>
<?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-default'))); ?>
</div>

<div class="related">
    <br />
        <?php if(!sizeof($standardQuestions)){ ?>
            <p><br/>No standard descriptors-questions added.</p>
        <?php } ?>
    <?php if ($standardQuestions): ?>
    <p><?php echo __('Standard Descriptors-Questions'); ?></p>
    <table class="table table-hover" cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo __('#'); ?></th>
            <th><?php echo __('Descriptor-Question'); ?></th>
            <th><?php echo __('Type'); ?></th>
            <th><?php echo __('Dimension'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        <?php $paginatorParams = $this->Paginator->params(); ?>
        <?php $cnt = ($paginatorParams['page']-1)*$paginatorParams['limit']+1; ?>
	<?php foreach ($standardQuestions as $standardQuestion): ?>
        <tr>
            <td><?php echo $cnt; ?>&nbsp;</td>
            <td><?php echo h($standardQuestion['StandardQuestion']['name']); ?>&nbsp;</td>
            <td><?php echo h($standardQuestion['StandardQuestion']['type']); ?>&nbsp;</td>
            <td style="color: <?php echo $standardQuestion['StandardTheme']['color'] ?>">
                <?php echo h($standardQuestion['StandardTheme']['name']); ?>&nbsp;
           </td>
            <td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $standardQuestion['StandardQuestion']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $standardQuestion['StandardQuestion']['id']), null, __('Are you sure you want to delete # %s?', $standardQuestion['StandardQuestion']['id'])); ?>
            </td>
        </tr>
        <?php $cnt++; ?>
<?php endforeach; ?>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
                echo $this->Paginator->first('<< first');
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                echo $this->Paginator->last('last >>');
	?>
    </div>
    <?php endif; ?>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        if ($("input#StandardQuestionIsCalculating").is(':checked')) {
            $('div[data-fields="option,score"] div.input.number').show();
        } else {
            $('div[data-fields="option,score"] div.input.number').hide();
        }
        if ($('select[name="data[StandardQuestion][type]"]').val() === "open ended") {
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }

        $(".option-score").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                $("#errmsg").html("Positive numbers only").show().fadeOut("slow");
                return false;
            }
        });
    });
    $('select[name="data[StandardQuestion][type]"]').change(function () {
        if ($(this).val() === "open ended") {
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }
    })
    $('input#StandardQuestionIncludeNoResponse').click(function () {
        $("#no-response-option").toggle(this.checked);
    })
    $('input#StandardQuestionIsCalculating').click(function () {
        if ($(this).is(':checked')) {
            $('div[data-fields="option,score"] div.input.number').show();
        } else {
            $('div[data-fields="option,score"] div.input.number').hide();
        }
    });
</script>
<?php }else{ ?>
    <?php echo $this->Html->link(__('Add standard themes before adding standard questions'), array('controller'=>'standard_themes','action' => 'index', $survey_id), array('type'=>'button','class'=>'btn btn-success', 'style'=>"margin-right:10px;")); ?>
<?php } ?>

<?php echo $this->Html->link(__('Back to survey builder menu'), array('controller'=>'survey_builder_components'), array('type'=>'button','class'=>'btn btn-default', 'style'=>'margin-top: 30px;')); ?>