<div class="questionGroups form">
<?php echo $this->Form->create('QuestionGroup'); ?>
	<fieldset>
		<legend><?php echo __('Edit Question Group'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('question_id');
		echo $this->Form->input('uq_group_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('QuestionGroup.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('QuestionGroup.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Question Groups'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Uq Groups'), array('controller' => 'uq_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Uq Group'), array('controller' => 'uq_groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
