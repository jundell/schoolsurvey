<?php ?>
<div class="col-xs-12">
    <legend>Choose from these standard groups.</legend>
<?php echo $this->Form->create('StandardGroup'); ?>
    <div class="survey-buttons">
    <?php echo $this->Html->link(__('Check All'), '#' , array('type'=>'button','class'=>'check-all btn btn-default', 'data-checkbox'=>'standard-group', 'style'=>'margin-top: 30px;')); ?>
    <?php echo $this->Html->link(__('Uncheck All'), '#' , array('type'=>'button','class'=>'uncheck-all btn btn-default', 'data-checkbox'=>'standard-group', 'style'=>'margin-top: 30px;')); ?>
    </div>
<?php
    $cnt = 0;
    foreach($standardGroups as $group){ ?>
    <div class="checkbox">

        <label for="group[<?php echo $cnt; ?>]">
            <?php if(in_array($group['StandardGroup']['name'], $groupslist)){ ?>
                <input type="checkbox" checked="checked" disabled="disabled" class="standard-group" name="group[<?php echo $cnt; ?>]" value="<?php echo $group['StandardGroup']['name']; ?>"/>
            <?php }else{ ?>
                <input type="checkbox" class="standard-group" name="group[<?php echo $cnt; ?>]" value="<?php echo $group['StandardGroup']['name']; ?>"/>
            <?php }?>
                <?php echo $group['StandardGroup']['name']; ?>
        </label>
    </div>
    <?php $cnt++; ?>
<?php } ?>
    <br />
    <strong>Add Your Own Group</strong>
    <div class="choose-group-add add_more_fieldsets" data-fields="ownGroup">
        <fieldset class="add_more form-group">
			<?php echo $this->Form->input('name', array('label'=>false,'class' => 'form-control','name'=>'ownGroup[0]', 'required'=>false)); ?>
            <a href="#" class="btn btn-default add_another">Add another group</a>
        </fieldset>
    </div>
<?php // echo $this->Html->link(__('Add Your Own Group'), '#' , array('type'=>'button','class'=>'btn btn-default', 'style'=>'margin: 10px 0 30px;')); ?>
<br />
    <?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-success'))); ?>
</div>