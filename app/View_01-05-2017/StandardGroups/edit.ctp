<div class="standardGroups form">
<?php echo $this->Form->create('StandardGroup'); ?>
    <fieldset>
        <legend><?php echo __('Edit Standard Group'); ?></legend>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
	?>
            </div></div>
    </fieldset>
<?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-default'))); ?>
</div>