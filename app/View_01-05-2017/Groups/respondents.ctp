<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//
//echo '<pre>';
//var_dump($respondents);
//echo '</pre>';
?>
<div class="respondentPasscode form">
<?php echo $this->Form->create('RespondentPasscode'); ?>
    <fieldset>
        <legend><?php echo __('Add Respondent'); ?></legend>
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                
		<?php echo $this->element('/survey_builder/group/respondent_passcode',
                                                    array(
                                                        "name"=>"data[RespondentPasscode][name]",
                                                        "passcode"=>"data[RespondentPasscode][passcode]"
                                                    )); //respondent passcode element ?>
               
            </div>
        </div>
    </fieldset>
    <br />
<?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-default'))); ?>
</div>
<div class="related"> <br /><br />
        <?php if(!sizeof($respondents)){ ?>
    <p><br/>No respondents added.</p>
        <?php }else{ ?>
    <p><?php echo __('Respondents'); ?></p>
    <table class="table table-hover" cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('name'); ?></th>
            <th><?php echo $this->Paginator->sort('passcode'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
	<?php foreach ($respondents as $respondent): ?>
        <tr>
            <td><?php echo h($respondent['RespondentPasscode']['name']); ?>&nbsp;</td>
            <td><?php echo h($respondent['RespondentPasscode']['passcode']); ?>&nbsp;</td>
            <td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('controller'=>'respondent_passcodes','action' => 'edit', $respondent['RespondentPasscode']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('controller'=>'respondent_passcodes','action' => 'delete', $respondent['RespondentPasscode']['id']), null, __('Are you sure you want to delete %s?', $respondent['RespondentPasscode']['name'])); ?>
            </td>
        </tr>
<?php endforeach; ?>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
                echo $this->Paginator->first('<< first');
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                echo $this->Paginator->last('last >>');
	?>
    </div>
        <?php }?>
</div>