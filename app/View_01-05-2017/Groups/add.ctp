<?php if(!$surveyStatus){ ?>
<div class="row">
    <div class="col-xs-12 col-sm-8">
        <legend>Add Groups</legend>
        <?php echo $this->Form->create('StandardGroup'); ?>
        <p>Choose from these standard groups.</p>
        <div class="survey-buttons">
            <?php echo $this->Html->link(__('Check All'), '#' , array('type'=>'button','class'=>'check-all-group btn btn-default', 'data-checkbox'=>'standard-group')); ?>
            <?php echo $this->Html->link(__('Uncheck All'), '#' , array('type'=>'button','class'=>'uncheck-all-group btn btn-default', 'data-checkbox'=>'standard-group')); ?>
        </div>
            <?php
            $cnt = 0;

            //groups in request data
            $groups = isset($this->request->data['group'])?$this->request->data['group']:array();

            //respondents in request data
            $resPass = isset($this->request->data['resPass'])?$this->request->data['resPass']:array();
            

            foreach($standardGroups as $group){

                //exists in $groups
                $gExist = array_key_exists ($cnt , $groups ); ?>

                <?php //group checkboxes ?>
        <div class="checkbox">
            <label for="group[<?php echo $cnt; ?>]">
                                    <?php if(in_array($group['StandardGroup']['name'], $grouplist)){ ?>

                                    <?php //if group already exist in survey data ?>
                <input type="checkbox" checked="checked" disabled="disabled" class="standard-group" name="group[<?php echo $cnt; ?>]" data-groupCnt="<?php echo $cnt; ?>" value="<?php echo $group['StandardGroup']['name']; ?>"/>

                                    <?php }else{ ?>

                                    <?php //if group is not yet saved 
                                        //checked if exist in $groups
                                    ?>
                <input <?php echo (sizeof($grouplist)>= Configure::read('site_config.max_groups'))?'disabled="disabled"':''; ?> type="checkbox" <?php echo $gExist?'checked="checked"':'' ?> class="standard-group" name="group[<?php echo $cnt; ?>]" value="<?php echo $group['StandardGroup']['name']; ?>" data-groupCnt="<?php echo $cnt; ?>"/>
                                    <?php }?>
                                    <?php echo $group['StandardGroup']['name']; //group name ?>
            </label>
        </div>

                <?php //if close survey, display respondent fields ?>
                <?php if(!$surveySystem){ ?>

                            <?php if($gExist){ ?>
                                    <?php //if group exist in request data ?>
        <div data-resPassCount='<?php echo $cnt; ?>' class="choose-respondent-add add_more_fieldsets" data-fields="resPass[<?php echo $cnt; ?>][name],resPass[<?php echo $cnt; ?>][passcode]">    
                                        <?php $requestResPassCount = 0; ?>
                                        <?php foreach($resPass[$cnt]['name'] as $kr=>$rP ){ ?>
                                            <?php echo $this->element('/survey_builder/group/respondent_passcode',
                                                    array(
                                                        "name"=>"resPass[$cnt][name][$kr]",
                                                        "passcode"=>"resPass[$cnt][passcode][$kr]",
                                                        "defaultName"=>$rP,
                                                        "defaultPasscode"=>$resPass[$cnt]['passcode'][$kr],
                                                        "addBtn"=>$requestResPassCount==(sizeof($resPass[$cnt]['name'])-1)?true:false,
                                                        "removeBtn"=>$requestResPassCount>0?true:false
                                                    )); //respondent passcode element 
                                                    $requestResPassCount++;
                                                ?>
                                        <?php } ?>
        </div>
        <hr data-hrCount="<?php echo $cnt; ?>"/>
                                <?php }else{ ?>
                                        <?php //if group is not in the request data and is not yet checked ?>
        <div data-resPassCount='<?php echo $cnt; ?>' class="choose-respondent-add add_more_fieldsets" data-fields="resPass[<?php echo $cnt; ?>][name],resPass[<?php echo $cnt; ?>][passcode]" style='display:none;'>
                                            <?php echo $this->element('/survey_builder/group/respondent_passcode',
                                                array(
                                                    "name"=>"resPass[$cnt][name][0]",
                                                    "passcode"=>"resPass[$cnt][passcode][0]",
                                                    "addBtn"=>true,
                                                    "removeBtn"=>false
                                                )); //respondent passcode element ?>
        </div>
        <hr data-hrCount="<?php echo $cnt; ?>"  style='display:none;'/>
                                <?php } ?>
                    <?php }?>
                <?php $cnt++; ?>
            <?php } ?>

        <br />

        <?php 
            //own group in request data
            $ownGroup = isset($this->request->data['ownGroup'])?$this->request->data['ownGroup']:array();

        ?>
        
        <div id="ownGroup">
            <p><strong>Add your own group</strong></p>
            <input name="add_another_group" class="add_another_group" value="0" type="hidden">
            <div class="add_more_fieldsets" data-fields="ownGroup">
                <div class="choose-group-add">
                    <fieldset class="form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <?php 
                                    echo $this->Form->input('name', array('disabled'=>sizeof($grouplist) < Configure::read('site_config.max_groups')?false:'disabled','label'=>false,'class' => 'form-control own-group-name','name'=>'ownGroup[name]', 'required'=>false, 'default'=>isset($ownGroup['name'])?$ownGroup['name']:''));
                                ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <?php if(!$surveySystem ){ ?>
                <div class="choose-respondent-add add_more_fieldsets" data-fields="ownGroup[resPass][name],ownGroup[resPass][passcode]">

                    <?php if(isset($ownGroup['resPass'])&& sizeof($ownGroup['resPass'])){ ?>
                            <?php $requestOwnResPassCount = 0; ?>
                        <?php foreach($ownGroup['resPass']['name'] as $kO=>$ressPassName){ ?>
                            <?php echo $this->element('/survey_builder/group/respondent_passcode',
                                                        array(
                                                            "name"=>"ownGroup[resPass][name][$kO]",
                                                            "passcode"=>"ownGroup[resPass][passcode][$kO]",
                                                            "defaultName"=>$ressPassName,
                                                            "defaultPasscode"=>$ownGroup['resPass']['passcode'][$kO],
                                                            "addBtn"=>$requestOwnResPassCount==(sizeof($ownGroup['resPass']['name'])-1)?true:false,
                                                            "removeBtn"=>$requestOwnResPassCount>0?true:false
                                                        )); //respondent passcode element ?>
                                <?php $requestOwnResPassCount++;?>                   
                        <?php } ?>
                    <?php } else { ?>
                        <?php echo $this->element('/survey_builder/group/respondent_passcode',
                                                    array(
                                                        "name"=>"ownGroup[resPass][name][0]",
                                                        "passcode"=>"ownGroup[resPass][passcode][0]",
                                                        "addBtn"=>true,
                                                        "removeBtn"=>false
                                                    )); //respondent passcode element ?>
                    <?php } ?>
                </div>
                <?php } ?>

            </div>
        </div>
        <br />
        <?php if($build){ ?>
            <a href="#" class="btn btn-default another_group" id="save-and-add"<?php echo sizeof($grouplist) < Configure::read('site_config.max_groups')?'':' disabled="disabled"'; ?>>Save and add another group</a><br /><br />
            <?php echo $this->Form->end(__(array('label' => 'Finish', 'class' => 'btn btn-success'))); ?>
        <?php }else{ ?>
            <?php echo $this->Form->end(__(array('disabled'=>sizeof($grouplist) < Configure::read('site_config.max_groups')?false:'disabled', 'label' => 'Submit', 'class' => 'btn btn-success'))); ?>
        <?php } ?>
        <?php if(sizeof($grouplist) >= Configure::read('site_config.max_groups')){ ?>
            <br />
            <p class="group-exceed-message"><?php echo $addedMoreGroups > 0 ? Configure::read('site_config.max_groups_added_message') : Configure::read('site_config.max_groups_message'); ?></p>
        <?php } ?>
            

    </div>
</div>  
<br /><br />
<?php } ?>

<div class="row">
    <div class="col-xs-12">    
        <div class="related table-responsive">
            <?php if(!sizeof($groupsPaginate)){ ?>
            <p>No groups added.</p>
            <?php }else{ ?>
            <p><?php echo __('Groups'); ?></p>
            <table class="table table-hover" cellpadding="0" cellspacing="0">
                <tr>
                    <th><?php echo $this->Paginator->sort('name'); ?></th>
                    <?php if(!$surveyStatus){ ?>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                    <?php } ?>
                </tr>
            <?php foreach ($groupsPaginate as $group): ?>
                <tr>
                    <td><?php echo h($group['Group']['name']); ?>&nbsp;</td>
                    <?php if(!$surveyStatus){ ?>
                    <td class="actions">
                        <?php if(!$surveySystem){ ?>
                            <?php echo $this->Html->link(__('Respondents'), array('controller' => 'groups', 'action' => 'respondents', $group['Group']['id'])); ?> | 
                        <?php } ?>
                        <?php echo $this->Html->link(__('Edit'), array('controller' => 'groups', 'action' => 'edit', $group['Group']['id'])); ?> | 
                        <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'groups', 'action' => 'delete',$group['Group']['id']), null, __('Are you sure you want to delete this group?')); ?>
                    </td>
                    <?php } ?>
                </tr>
    <?php endforeach; ?>
            </table>
            <?php }?>

        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        
        <?php if(!$surveySystem){ ?>
            $('input.standard-group').change(function () {
                showHideResPass($(this));
                limitGroups();
            });

        <?php }else{ ?>
            $('input.standard-group').change(function () {
                limitGroups();
            });
        <?php } ?>

        //check all
        $('.check-all-group').click(function (e) {
            var $this = $(this);
            var checkbox = $this.attr('data-checkbox');
            $('.' + checkbox).each(function () {
                if (!$(this).is(':checked') && !$(this).is(':disabled')) {
                    $(this).prop("checked", true);
                        <?php if(!$surveySystem){ ?>
                    showHideResPass($(this));
                        <?php } ?>
                }
            });
            limitGroups();
            e.preventDefault();
        });

        //uncheck all
        $('.uncheck-all-group').click(function (e) {
            var $this = $(this);
            var checkbox = $this.attr('data-checkbox');
            $('.' + checkbox).each(function () {
                if ($(this).is(':checked') && !$(this).is(':disabled')) {
                    $(this).prop("checked", false);
                        <?php if(!$surveySystem){ ?>
                    showHideResPass($(this));
                        <?php } ?>
                }
            });
            limitGroups();
            e.preventDefault();
        });
        
        $('input.own-group-name').keyup(function(){
            limitGroups();
        });
    });

    function showHideResPass($this) {
        var groupCnt = $this.attr('data-groupCnt');
        var resPassDiv = $('div[data-resPassCount="' + groupCnt + '"]');
        var groupHr = $('hr[data-hrcount="' + groupCnt + '"]');
        if ($this.is(':checked')) {
            resPassDiv.show();
            groupHr.show();
        } else {
            resPassDiv.hide();
            groupHr.hide();
        }
    }
    
    function limitGroups(){
        var maxgroup = <?php echo Configure::read('site_config.max_groups'); ?>;
        var groupList = <?php echo sizeof($grouplist); ?>;
        var checked = $('input.standard-group:checked').length;
        var cntChecked = checked > groupList?checked:groupList;
        if(cntChecked >= maxgroup){
            $('#ownGroup input[name="ownGroup[name]"]').attr('disabled', true);
            if($("#ownGroup input#StandardGroupName" ).length && $("#ownGroup input#StandardGroupPasscode").length){
                $('#ownGroup input#StandardGroupName').attr('disabled', true);
                $('#ownGroup input#StandardGroupPasscode').attr('disabled', true);
                $('#ownGroup a.add_another_row').attr('disabled', true);  
            }
            if($("a.another_group").length){
                $("a.another_group").attr('disabled', true)
            }
        }else{
            if($('input[name="ownGroup[name]"]').val()){
                cntChecked++;
            }else{
                cntChecked--;
            }
            if(cntChecked >= maxgroup){
                $('input.standard-group').each(function(){
                    $(this).attr('disabled', true);
                });
                if($("a.another_group").length){
                    $("a.another_group").attr('disabled', true)
                }
            }else{
                $('input.standard-group:not(:checked)').each(function(){
                    $(this).attr('disabled', false);
                });
                $('input[name="ownGroup[name]"]').attr('disabled', false);
                if($("#ownGroup input#StandardGroupName" ).length && $("#ownGroup input#StandardGroupPasscode").length){
                    $('#ownGroup input#StandardGroupName').attr('disabled', false);
                    $('#ownGroup input#StandardGroupPasscode').attr('disabled', false);
                    $('#ownGroup a.add_another_row').attr('disabled', false);  
                }
                if($("a.another_group").length){
                    $('a.another_group').attr('disabled', false)
                }
            }
        }
    }
</script>