<?php

echo $this->Html->script(array('jquery.numeric.min')); ?>
<div class="standardQuestions form">
<?php echo $this->Form->create('StandardQuestion'); ?>
    <fieldset>
        <legend><?php echo __('Edit Standard Question'); ?></legend>
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <?php
		echo $this->Form->input('id');
		echo $this->Form->input('name',array('label'=>'Question','div'=>array('class'=>'form-group input text'),'class'=>'form-control','maxlength'=>5000));
		echo $this->Form->input('theme_id', array(
                            'label' => 'Dimension',
                            'options' => $themes,
                                                'class'=>'form-control',
                                                'div'=>array('class'=>'form-group input text'),
                        ));
                echo $this->Form->input('type', array(
                            'options' => $types,
                            'div'=>array('class'=>'form-group input select'),
                            'class'=>'form-control'
                        ));
		
                ?>
                <div class="input checkbox " id="non-calculating">
                    <?php 
                        $is_calculating = true;
                        if(isset($this->request->data['StandardQuestion']['is_calculating'])){
                            $is_calculating = $this->request->data['StandardQuestion']['is_calculating']?true:false;
                        }
                    ?>
                    <?php echo $this->Form->input('is_calculating',array('type'=>'checkbox','div'=>false, 'label'=>'Calculating', 'checked'=>$is_calculating)); ?>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="add_more_fieldsets" data-fields="option,score">
                <?php if($this->request->is('post') || $this->request->is('put')){ ?>
                <?php
                    $options = $this->request->data['option'];
                    $scores = $this->request->data['score'];
                ?>
                <?php if(isset($options)&& sizeof($options)){ ?>
                    <?php $size = sizeof($options); ?>
                    <?php $cnt = 1; ?>
                    <?php foreach($options as $k=>$o){ ?>
                        <fieldset class="add_more form-group">
                        <?php
                                echo $this->Form->input('option', array('label'=>'Option','type'=>'text','class'=>'form-control','name'=>'option['.$k.']', 'value'=>$o));
                                echo $this->Form->input('score', array('label'=>'Score','name'=>'score['.$k.']','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>$scores[$k]));
                        ?>
                            
                            <?php if($optionEditable){ ?>
                             <?php if($cnt==$size){ ?>
                                    <a href="#" class="btn btn-default add_another" data-lastId="<?php echo $k+1; ?>">Add another option</a>
                                <?php } ?>
                                <?php if($cnt>2){ ?>
                                    <a class="btn btn-default remove" href="#">X</a>
                                <?php } ?>
                                <?php $cnt++; ?>
                            <?php } ?>
                        </fieldset>

                    <?php } ?>
                <?php }}else{ ?>
                    <?php $options_data = $this->request->data['StandardQuestionOption']; ?>
                    <?php if(isset($options_data)&& sizeof($options_data)){ ?>
                        <?php $size = sizeof($options_data); ?>
                        <?php $cnt = 1; ?>
                        <?php foreach($options_data as $k=>$o){ ?>
                            <fieldset class="add_more form-group">
                            <?php
                                    echo $this->Form->input('option', array('label'=>'Option','type'=>'text','class'=>'form-control','name'=>'option['.$o['id'].']', 'value'=>$o['name']));
                                    echo $this->Form->input('score', array('label'=>'Score','name'=>'score['.$o['id'].']','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>$o['score']));
                            ?>
                                 <?php if($cnt==$size){ ?>
                                    <a href="#" class="btn btn-default add_another" data-lastId="<?php echo $o['id']+1; ?>">Add another option</a>
                                <?php } ?>
                                <?php if($cnt>2){ ?>
                                    <a class="btn btn-default remove" href="#">X</a>
                                <?php } ?>
                                <?php $cnt++; ?>
                            </fieldset>

                        <?php } ?>
                    <?php } ?>
            <?php } ?>
        </div>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        if($("input#StandardQuestionIsCalculating").is(':checked')){
            $('div[data-fields="option,score"] div.input.number').show();
        }else{
            $('div[data-fields="option,score"] div.input.number').hide();
        }
        if($('select[name="data[StandardQuestion][type]"]').val()==="open ended"){
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }
        
        $(".option-score").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               //display error message
               $("#errmsg").html("Positive numbers only").show().fadeOut("slow");
                      return false;
           }
          });
    });
    $('select[name="data[StandardQuestion][type]"]').change(function() {
        if ($(this).val() === "open ended") {
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }
    })
    $('input#StandardQuestionIncludeNoResponse').click(function() {
        $("#no-response-option").toggle(this.checked);
    })
    $('input#StandardQuestionIsCalculating').click(function() {
        if($(this).is(':checked')){
            $('div[data-fields="option,score"] div.input.number').show();
        }else{
            $('div[data-fields="option,score"] div.input.number').hide();
        }
    });
</script>
