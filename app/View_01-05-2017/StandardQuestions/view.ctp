<div class="standardQuestions view">
<h2><?php echo __('Standard Question'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($standardQuestion['StandardQuestion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($standardQuestion['StandardQuestion']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($standardQuestion['StandardQuestion']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('With Options'); ?></dt>
		<dd>
			<?php echo h($standardQuestion['StandardQuestion']['with_options']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Calculating'); ?></dt>
		<dd>
			<?php echo h($standardQuestion['StandardQuestion']['is_calculating']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Standard Theme'); ?></dt>
		<dd>
			<?php echo $this->Html->link($standardQuestion['StandardTheme']['name'], array('controller' => 'standard_themes', 'action' => 'view', $standardQuestion['StandardTheme']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Deleted'); ?></dt>
		<dd>
			<?php echo h($standardQuestion['StandardQuestion']['is_deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($standardQuestion['StandardQuestion']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($standardQuestion['StandardQuestion']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Standard Question'), array('action' => 'edit', $standardQuestion['StandardQuestion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Standard Question'), array('action' => 'delete', $standardQuestion['StandardQuestion']['id']), null, __('Are you sure you want to delete # %s?', $standardQuestion['StandardQuestion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Standard Questions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Standard Question'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Standard Themes'), array('controller' => 'standard_themes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Standard Theme'), array('controller' => 'standard_themes', 'action' => 'add')); ?> </li>
	</ul>
</div>
