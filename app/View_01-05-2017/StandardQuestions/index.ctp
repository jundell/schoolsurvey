<div class="standardQuestions index">
	<h2><?php echo __('Standard Questions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('with_options'); ?></th>
			<th><?php echo $this->Paginator->sort('is_calculating'); ?></th>
			<th><?php echo $this->Paginator->sort('theme_id'); ?></th>
			<th><?php echo $this->Paginator->sort('is_deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($standardQuestions as $standardQuestion): ?>
	<tr>
		<td><?php echo h($standardQuestion['StandardQuestion']['id']); ?>&nbsp;</td>
		<td><?php echo h($standardQuestion['StandardQuestion']['name']); ?>&nbsp;</td>
		<td><?php echo h($standardQuestion['StandardQuestion']['type']); ?>&nbsp;</td>
		<td><?php echo h($standardQuestion['StandardQuestion']['with_options']); ?>&nbsp;</td>
		<td><?php echo h($standardQuestion['StandardQuestion']['is_calculating']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($standardQuestion['StandardTheme']['name'], array('controller' => 'standard_themes', 'action' => 'view', $standardQuestion['StandardTheme']['id'])); ?>
		</td>
		<td><?php echo h($standardQuestion['StandardQuestion']['is_deleted']); ?>&nbsp;</td>
		<td><?php echo h($standardQuestion['StandardQuestion']['created']); ?>&nbsp;</td>
		<td><?php echo h($standardQuestion['StandardQuestion']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $standardQuestion['StandardQuestion']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $standardQuestion['StandardQuestion']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $standardQuestion['StandardQuestion']['id']), null, __('Are you sure you want to delete # %s?', $standardQuestion['StandardQuestion']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Standard Question'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Standard Themes'), array('controller' => 'standard_themes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Standard Theme'), array('controller' => 'standard_themes', 'action' => 'add')); ?> </li>
	</ul>
</div>
