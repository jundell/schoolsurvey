<div class="subscriberSettings view">
<h2><?php echo __('Subscriber Setting'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($subscriberSetting['SubscriberSetting']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($subscriberSetting['SubscriberSetting']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Slug'); ?></dt>
		<dd>
			<?php echo h($subscriberSetting['SubscriberSetting']['slug']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($subscriberSetting['SubscriberSetting']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subscriber Id'); ?></dt>
		<dd>
			<?php echo h($subscriberSetting['SubscriberSetting']['subscriber_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($subscriberSetting['SubscriberSetting']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Font-size'); ?></dt>
		<dd>
			<?php echo h($subscriberSetting['SubscriberSetting']['font-size']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Alignment'); ?></dt>
		<dd>
			<?php echo h($subscriberSetting['SubscriberSetting']['alignment']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Color'); ?></dt>
		<dd>
			<?php echo h($subscriberSetting['SubscriberSetting']['color']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($subscriberSetting['SubscriberSetting']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($subscriberSetting['SubscriberSetting']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Subscriber Setting'), array('action' => 'edit', $subscriberSetting['SubscriberSetting']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Subscriber Setting'), array('action' => 'delete', $subscriberSetting['SubscriberSetting']['id']), null, __('Are you sure you want to delete # %s?', $subscriberSetting['SubscriberSetting']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Subscriber Settings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Subscriber Setting'), array('action' => 'add')); ?> </li>
	</ul>
</div>
