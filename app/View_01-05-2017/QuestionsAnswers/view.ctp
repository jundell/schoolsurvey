<div class="questionsAnswers view">
<h2><?php  echo __('Questions Answer'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($questionsAnswer['QuestionsAnswer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($questionsAnswer['User']['id'], array('controller' => 'users', 'action' => 'view', $questionsAnswer['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($questionsAnswer['Question']['id'], array('controller' => 'questions', 'action' => 'view', $questionsAnswer['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answer Option'); ?></dt>
		<dd>
			<?php echo $this->Html->link($questionsAnswer['AnswerOption']['id'], array('controller' => 'answer_options', 'action' => 'view', $questionsAnswer['AnswerOption']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($questionsAnswer['QuestionsAnswer']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($questionsAnswer['QuestionsAnswer']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Questions Answer'), array('action' => 'edit', $questionsAnswer['QuestionsAnswer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Questions Answer'), array('action' => 'delete', $questionsAnswer['QuestionsAnswer']['id']), null, __('Are you sure you want to delete # %s?', $questionsAnswer['QuestionsAnswer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions Answers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Questions Answer'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Answer Options'), array('controller' => 'answer_options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer Option'), array('controller' => 'answer_options', 'action' => 'add')); ?> </li>
	</ul>
</div>
