<p><span><?php echo $report_settings['table4_title']; ?></span> <?php echo $report_settings['table4_text']; ?></p>

<p class="blue"><?php echo $report_settings['open_ended_questions_title']; ?></p>

<?php foreach($result['openEnded'] as $oe){ ?>

    <p class="blue"><?php echo $oe['num']; ?>. <?php echo $oe['name']; ?></p>

    <?php foreach($oe['groups'] as $oeg){?>
        
        <h5><?php echo $oeg['name']; ?></h5>

        <?php if(sizeof($oeg['resp'])){ ?>
            <p>Answers:</p>

            <div class="table-responsive table_oe">
                <table>
                    <tbody>
                        <?php foreach($oeg['resp'] as $resp){ ?>
                            <tr>
                                <td><?php echo $resp; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <br/>
        <?php } ?>
    <?php } ?>
<?php } ?>