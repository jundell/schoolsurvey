<?php

?>
<p><span><?php echo $report_settings['table1_title']; ?></span> <?php echo $report_settings['table1_text']; ?></p>
<div class="table-responsive">          
    <table class="table-1 table">
        <thead>
            <tr>
                <th colspan="<?php echo sizeof($result['groups'])*2 + 3; ?>" class="theader1"></th>
                <th class="theader1">Admin</th>
                <th class="theader1">Overall</th>
            </tr>
            <tr>
                <th></th>
                <?php foreach($result['groups'] as $key=>$group){ ?>
                    <th colspan="2"><?php echo $group['Group']['name']; ?></th>
                <?php } ?>
                <th colspan="2">Total</th>
                <th>Admin</th>
                <th>Difference</th>
            </tr>
            <tr>
                <th>Dimension</th>
                <?php $groupSize = sizeof($result['groups']); ?>
                <?php while($groupSize>=0){ ?>
                    <th class="tbg-gray">Ave Score</th>
                    <th># of Respondents</th>
                    <?php $groupSize--; ?>
                <?php } ?>
                <th class="tbg-gray"></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($result['dimensions'] as $k=>$d){ ?>
            <?php $dimensionRes = $result['dimensionsResult'][$d['SurveyTheme']['id']]; ?>
                <?php if($dimensionRes['Total']['noOfResp']){?>
                    <tr>
                        <td style="color:<?php echo $d['SurveyTheme']['color'];?>"><?php echo $dimensionRes['name']; ?></td>
                        <?php foreach($dimensionRes['groups'] as $kg=>$g){ ?>
                            <td class="tbg-gray"><?php echo $g['avg']; ?></td>
                            <td><?php echo $g['noOfResp']; ?></td>
                        <?php } ?>
                        <td class="tbg-gray"><?php echo $dimensionRes['Total']['avg']; ?></td>
                        <td><?php echo $dimensionRes['Total']['noOfResp']; ?></td>
                        <td class="tbg-gray"><?php echo $dimensionRes['AdminAvg']; ?></td>
                        <td><?php echo $dimensionRes['AwarenessQuotient']; ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</div>