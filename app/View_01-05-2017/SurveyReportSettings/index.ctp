<div class="surveyReportSettings index">
	<h2><?php echo __('Survey Report Settings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('slug'); ?></th>
			<th><?php echo $this->Paginator->sort('value'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($surveyReportSettings as $surveyReportSetting): ?>
	<tr>
		<td><?php echo h($surveyReportSetting['SurveyReportSetting']['id']); ?>&nbsp;</td>
		<td><?php echo h($surveyReportSetting['SurveyReportSetting']['name']); ?>&nbsp;</td>
		<td><?php echo h($surveyReportSetting['SurveyReportSetting']['slug']); ?>&nbsp;</td>
		<td><?php echo h($surveyReportSetting['SurveyReportSetting']['value']); ?>&nbsp;</td>
		<td><?php echo h($surveyReportSetting['SurveyReportSetting']['created']); ?>&nbsp;</td>
		<td><?php echo h($surveyReportSetting['SurveyReportSetting']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $surveyReportSetting['SurveyReportSetting']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $surveyReportSetting['SurveyReportSetting']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $surveyReportSetting['SurveyReportSetting']['id']), null, __('Are you sure you want to delete # %s?', $surveyReportSetting['SurveyReportSetting']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Survey Report Setting'), array('action' => 'add')); ?></li>
	</ul>
</div>
