<div class="surveyReportSettings view">
<h2><?php  echo __('Survey Report Setting'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($surveyReportSetting['SurveyReportSetting']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($surveyReportSetting['SurveyReportSetting']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Slug'); ?></dt>
		<dd>
			<?php echo h($surveyReportSetting['SurveyReportSetting']['slug']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($surveyReportSetting['SurveyReportSetting']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($surveyReportSetting['SurveyReportSetting']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($surveyReportSetting['SurveyReportSetting']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Survey Report Setting'), array('action' => 'edit', $surveyReportSetting['SurveyReportSetting']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Survey Report Setting'), array('action' => 'delete', $surveyReportSetting['SurveyReportSetting']['id']), null, __('Are you sure you want to delete # %s?', $surveyReportSetting['SurveyReportSetting']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Survey Report Settings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Survey Report Setting'), array('action' => 'add')); ?> </li>
	</ul>
</div>
