<div class="respondentPasscodes form">
<?php echo $this->Form->create('RespondentPasscode'); ?>
    <fieldset>
        <legend><?php echo __('Edit Respondent'); ?></legend>
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-4">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name',array('class'=>'form-control','div'=>array('class'=>'form-group input text')));
		echo $this->Form->input('passcode',array('class'=>'form-control','div'=>array('class'=>'form-group input text')));
                ?>
            </div>
        </div>
    </fieldset>
<?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-default'))); ?>
</div>
