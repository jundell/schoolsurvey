<div class="uqGroups view">
<h2><?php  echo __('Uq Group'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($uqGroup['UqGroup']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($uqGroup['UqGroup']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($uqGroup['UqGroup']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($uqGroup['UqGroup']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Uq Group'), array('action' => 'edit', $uqGroup['UqGroup']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Uq Group'), array('action' => 'delete', $uqGroup['UqGroup']['id']), null, __('Are you sure you want to delete # %s?', $uqGroup['UqGroup']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Uq Groups'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Uq Group'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Question Groups'), array('controller' => 'question_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question Group'), array('controller' => 'question_groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List User Groups'), array('controller' => 'user_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Group'), array('controller' => 'user_groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Question Groups'); ?></h3>
	<?php if (!empty($uqGroup['QuestionGroup'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Question Id'); ?></th>
		<th><?php echo __('Uq Group Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($uqGroup['QuestionGroup'] as $questionGroup): ?>
		<tr>
			<td><?php echo $questionGroup['id']; ?></td>
			<td><?php echo $questionGroup['question_id']; ?></td>
			<td><?php echo $questionGroup['uq_group_id']; ?></td>
			<td><?php echo $questionGroup['created']; ?></td>
			<td><?php echo $questionGroup['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'question_groups', 'action' => 'view', $questionGroup['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'question_groups', 'action' => 'edit', $questionGroup['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'question_groups', 'action' => 'delete', $questionGroup['id']), null, __('Are you sure you want to delete # %s?', $questionGroup['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Question Group'), array('controller' => 'question_groups', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related User Groups'); ?></h3>
	<?php if (!empty($uqGroup['UserGroup'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Uq Group Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($uqGroup['UserGroup'] as $userGroup): ?>
		<tr>
			<td><?php echo $userGroup['id']; ?></td>
			<td><?php echo $userGroup['user_id']; ?></td>
			<td><?php echo $userGroup['uq_group_id']; ?></td>
			<td><?php echo $userGroup['created']; ?></td>
			<td><?php echo $userGroup['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'user_groups', 'action' => 'view', $userGroup['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'user_groups', 'action' => 'edit', $userGroup['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'user_groups', 'action' => 'delete', $userGroup['id']), null, __('Are you sure you want to delete # %s?', $userGroup['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User Group'), array('controller' => 'user_groups', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
