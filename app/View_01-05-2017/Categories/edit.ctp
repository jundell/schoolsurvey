<div class="categories form">
<?php echo $this->Form->create('Category'); ?>
	<fieldset>
		<legend><?php echo __('Edit Category'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
                echo $this->Form->input('admins', 
                        array('type' => 'select',
                            'multiple' => 'checkbox',
                            'label' => 'Choose Admins',
                            'options' => $admins,
                            'selected' => $selected,
                            ));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('user_links'); ?>
