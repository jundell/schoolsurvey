<div class="col-xs-12 col-sm-6 col-sm-offset-3">
    <div class="row">
        <div class="form-forgot-password">
            <h2 class="form-signin-heading">Account Recovery</h2>
            <?php echo $this->Form->create('User'); ?>
            <div class="form-group row">
                <?php echo $this->Form->input('first_name', array('class' => 'form-control','div'=>array('class'=>'col-xs-12 col-sm-6 input text'))) ?> 
                <?php echo $this->Form->input('last_name', array('class' => 'form-control','div'=>array('class'=>'col-xs-12 col-sm-6 input text'))) ?> 
            </div>

                <?php echo $this->Form->input('email_address', array('class' => 'form-control', 'label' => 'Email Address', 'div'=>array('class'=>'form-group input text')))?> 

            <button class="btn btn-lg btn-primary" type="submit">Submit</button>
            <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>
