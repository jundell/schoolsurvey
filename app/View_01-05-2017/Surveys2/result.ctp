<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php if($hasResult && !$hasError){?>
<div id="survey_result">
    <h4><?php echo $result['survey']['Survey']['name'];?></h4>
    <table class="table table-bordered" style="font-size: 10px;  border: 1px solid #C0C0C0;">
        <thead>
            <tr>
                <th></th>
                <th colspan="<?php echo sizeof($result["groups"]) * 2 + 2; ?>"><?php echo isset($settings['report_respondent_header'])?$settings['report_respondent_header']:'Respondent'; ?></th>
                <th><?php echo isset($settings['report_admin_header'])?$settings['report_admin_header']:'Admin'; ?></th>
                <th><?php echo isset($settings['report_overall_header'])?$settings['report_overall_header']:'Overall'; ?></th>
            </tr>
            <tr>
                <th></th>
					<?php foreach ($result["groups"] as $k => $v) { ?>
                <th colspan="2"><?php echo $k; ?></th>
					<?php } ?>
                <th colspan="2"><?php echo isset($settings['report_total_header'])?$settings['report_total_header']:'Total'; ?></th>
                <th><?php echo isset($settings['report_admin_second_row_header'])?$settings['report_admin_second_row_header']:'Admin'; ?></th>
                <th><?php echo isset($settings['report_awareness_quotient_header'])?$settings['report_awareness_quotient_header']:'Awareness Quotient'; ?></th>
            </tr>
            <tr class="heighttoprint">
                <th><p class="vertical-text"><?php echo isset($settings['report_questions_header'])?$settings['report_questions_header']:'Questions'; ?></p></th>
					<?php foreach ($result["groups"] as $k => $v) { ?>
        <th><p class="vertical-text"><?php echo isset($settings['report_average_score_header'])?$settings['report_average_score_header']:'Ave Score'; ?></p></th>
        <th><p class="vertical-text"><?php echo isset($settings['report_number_of_respondents_header'])?$settings['report_number_of_respondents_header']:'# of Respondents'; ?></p></th>
					<?php } ?>
        <th><p class="vertical-text"><?php echo isset($settings['report_average_score_header'])?$settings['report_average_score_header']:'Ave Score'; ?></p></th>
        <th><p class="vertical-text"><?php echo isset($settings['report_number_of_respondents_header'])?$settings['report_number_of_respondents_header']:'# of Respondents'; ?></p></th>
        <th><p class="vertical-text"><?php echo isset($settings['report_score_header'])?$settings['report_score_header']:'Score'; ?></p></th>
        <th><p class="vertical-text"><?php echo isset($settings['report_difference_header'])?$settings['report_difference_header']:'Difference'; ?></p></th>
        </tr>
        </thead>
        <tbody>
				<?php $cnt = 1; ?>
				<?php foreach ($result["questions"] as $k => $v) { ?>
            <tr style="background-color:<?php echo $v['SurveyTheme']['color'];?>;">	
                                                <?php if($v['Question']['is_calculating']=='1' && $v['Question']['type']!='open ended'){ ?>
                <td><?php echo $cnt; ?></td>
							<?php foreach ($result["groups"] as $gk => $gv) { ?>
                <td><?php echo $gv['avgScores'][$v['Question']['id']]; ?></td>
                <td><?php echo $gv['respCount'][$v['Question']['id']]; ?></td>
							<?php } ?>
                <td><?php echo $result['totalAvg']['avgScores'][$v['Question']['id']]; ?></td>
                <td><?php echo $result['totalAvg']['respCount'][$v['Question']['id']]; ?></td>
                <td><?php echo $result['admin']['scores'][$v['Question']['id']]; ?></td>
                <td><?php echo $result['AQ']['aqs'][$v['Question']['id']]; echo !$result['admin']['scores'][$v['Question']['id']]?' *':''; ?></td>
						<?php }else{ ?>
                                                        <?php
                                                        $oeText = isset($settings['report_open_ended_label'])?$settings['report_open_ended_label']:'Open Ended';
                                                        $ncText = isset($settings['report_non_calculating_label'])?$settings['report_non_calculating_label']:'Non-calculating';
                                                        ?>
                <td><?php echo $cnt; ?></td>
                <td colspan="<?php echo (sizeof($result["groups"])*2)+4 ?>" class="open-ended"><?php echo $v['Question']['type']=='open ended'?$oeText:$ncText; ?></td>

						<?php } ?>
            </tr>
					<?php $cnt++; ?>
				<?php } ?>
            <tr>
                <th><?php echo isset($settings['report_overall_total_label'])?$settings['report_overall_total_label']:'Overall Total'; ?></th>
					<?php foreach ($result["groups"] as $gk => $gv) { ?>
                <th><?php echo $gv['totalAvgScore']; ?></th>
                <th><?php echo $gv['totalRespCount']; ?></th>
					<?php } ?>
                <th><?php echo $result['totalAvg']['totalAvgScore']; ?></th>
                <th><?php echo $result['totalAvg']['totalRespCount']; ?></th>
                <th><?php echo $result['admin']['totalAvgScore']; ?></th>
                <th><?php echo $result['AQ']['total']; ?></th>
            </tr>
        </tbody>
    </table>
                <?php if(sizeof($themes)){ ?>
    <div id="report-legend">
        <div id="legend-title"><strong><?php echo isset($settings['report_legend_label'])?$settings['report_legend_label']:'Legend:'; ?></strong></div>
        <table>
                            <?php foreach($themes as $theme){ ?>
            <tr>
                <td style="background-color:<?php echo $theme['SurveyTheme']['color']; ?>; width: 18px; border-bottom:5px #fff solid;"></td>
                <td style="padding-left:5px; font-size: 11px;"><?php echo $theme['SurveyTheme']['name']; ?></td>
            </tr>
                        <?php } ?>
        </table>
    </div>
                <?php } ?>
    <p style="font-style: italic; font-size: 11px;"><?php echo isset($settings['report_admin_answer_note'])?$settings['report_admin_answer_note']:'Please note that questions with * are not yet answered by the Admin.'; ?></p>


		<?php if(sizeof($openEnded)){?>
    <div style="margin-bottom: 50px;">
        <strong><?php echo isset($settings['report_open_ended_questions_label'])?$settings['report_open_ended_questions_label']:'OPEN ENDED QUESTIONS'; ?></strong><br />
			<?php $qCount = 1; ?>
			<?php foreach($result["questions"] as $k => $v){ ?>
				<?php if($v['Question']['type']=='open ended'){ ?>
        <p style="margin:10px 0 0;"><strong><?php echo $qCount.'. '.$openEnded[$v['Question']['id']]['question_name']; ?></strong></p>
					<?php foreach($openEnded[$v['Question']['id']]['groups'] as $g){ ?>
                                                <?php if(sizeof($g['resp'])){ ?>
        <p style="padding-left: 20px; margin:0;"><strong><?php echo $g['name']; ?></strong></p>
        <table class="table table-bordered" style="margin-left: 40px; width: 1130px;">
            <thead>
                <tr>
                    <th style="width: 20%;"><?php echo isset($settings['report_open_ended_email_header'])?$settings['report_open_ended_email_header']:'Email'; ?></th>
                    <th><?php echo isset($settings['report_open_ended_answer_header'])?$settings['report_open_ended_answer_header']:'Answer'; ?></th>
                </tr>
            </thead>
            <tbody>
									<?php foreach($g['resp'] as $r){?>
                <tr>
                    <td><?php echo $r['email'];?></td>
                    <td><?php echo $r['answer']; ?></td>
                </tr>
									<?php }?>
            </tbody>
        </table>
						<?php } ?>
					<?php } ?>
				<?php } ?>
				<?php $qCount++; ?> 
			<?php } ?>
    </div>
		<?php } ?>

    <!--non calculating !-->
                <?php if(sizeof($nonCalculating)){?>

    <div style="margin-bottom: 20px;">
        <strong><?php echo isset($settings['report_non_calculating_questions_label'])?$settings['report_non_calculating_questions_label']:'NON-CALCULATING QUESTIONS'; ?></strong><br />
			<?php $qCount = 1; ?>
			<?php foreach($result["questions"] as $k => $v){ ?>
				<?php if(in_array($v['Question']['type'],array('likert','radio button')) && !$v['Question']['is_calculating']){ ?>
        <p style="margin:10px 0 0;"><strong><?php echo $qCount.'. '.$nonCalculating[$v['Question']['id']]['question_name']; ?></strong></p>
        <table class="table table-bordered" style="margin-left: 40px; width: 1130px;">
            <thead>
                <tr>
                    <th style="width: 20%;"><?php echo isset($settings['report_non_calculating_options_header'])?$settings['report_non_calculating_options_header']:'Options'; ?></th>
                                            <?php $noOfRespText = isset($settings['report_non_calculating_number_of_respondents_header'])?$settings['report_non_calculating_number_of_respondents_header']:'# of respondents'; ?>
                                            <?php foreach($nonCalculating[$v['Question']['id']]['groups'] as $g){ ?>
                    <th><?php echo $g['name']; ?><br><span class="header-subtext"><?php echo $noOfRespText; ?></span></th>
                                            <?php } ?>
                    <th><?php echo isset($settings['report_non_calculating_admin_header'])?$settings['report_non_calculating_admin_header']:'Admin'; ?><br><span class="header-subtext"><?php echo $noOfRespText; ?></span></th>
                    <th><?php echo isset($settings['report_non_calculating_total_header'])?$settings['report_non_calculating_total_header']:'Total'; ?><br><span class="header-subtext"><?php echo $noOfRespText; ?></span></th>
                </tr>
            </thead>
            <tbody>
					<?php foreach($nonCalculating[$v['Question']['id']]['question_options'] as $kqo => $qo){?>
                <tr>
                    <td><?php echo $qo;?></td>
                                            <?php $total = 0; ?>
                                            <?php foreach($nonCalculating[$v['Question']['id']]['groups'] as $g){ ?>
                                                <?php $count = isset($g[$kqo])?$g[$kqo]:0; ?>
                                                <?php $total+= $count;?>
                    <td><?php echo $count; ?></td>

                                            <?php } ?>
                                            <?php $adminResp = intval($nonCalculating[$v['Question']['id']]['admin'][$kqo]); ?>
                    <td><?php echo $adminResp; ?></td>
                    <td><?php echo $total+$adminResp; ?></td>
                </tr>
					<?php }?>
            </tbody>
        </table>
				<?php } ?>
				<?php $qCount++; ?> 
			<?php } ?>
    </div>
		<?php } ?>

</div>

<a href="<?php echo $this->webroot; ?>surveys/export_report/<?php echo $result['survey']['Survey']['id'];?>" class="btn" id="btnExport">Export to Excel</a>
<script type="text/javascript">
//		$(document).ready(function() {
//			$("#btnExport").click(function(e) {
////				var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#survey_result').html()) 
////				location.href=url;
////				return false;
//			});	
//		});
//            var uri = 'data:application/vnd.ms-excel;base64,'
//            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" ><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
//                        , base64 = function(s) {
//                            return window.btoa(unescape(encodeURIComponent(s)))
//                        }
//                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
//    var table = $('#survey_result');
//    var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
//    window.URL = window.URL || window.webkitURL;
//    var blob = new Blob([format(template, ctx)]);
//    var blobURL = window.URL.createObjectURL(blob);
//    var fileName = 'Survey Report';
//    if (blobURL) {
//      $("<a/>")
//        .attr("href", blobURL)
//        .attr("download", fileName)
//        .text("Download "+fileName+" for import")
//        .appendTo('#btnExport');
//    }
//    else {
//      $("#btnExport").html("Please cut and paste from the table below");
//    }  
</script>
<a href="javascript:window.print()"><input type="submit" class="noprint" value="Print"></a>
<?php } ?>

<style type="text/css" media="print">
    @page { size: landscape; margin: 1cm; }
    @media {orientation: landscape}
    .btn {display: none;}
    .noprint {display: none;}
    .navbar-inner {display: none;} 
    body { padding-left: 2px; }
    .vertical-text {
        transform: rotate(-90deg);
        width: 25px;
    }
    .heighttoprint {height: 120px;}
</style>