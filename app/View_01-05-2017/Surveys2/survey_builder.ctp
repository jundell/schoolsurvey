<div class="surveys form">
<?php echo $this->Form->create('Survey'); ?>

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-4">
            <h2><?php echo __('Survey Builder'); ?></h2>
            <br />
	<?php
		echo $this->Form->input('name', array('label'=>'Name<span class="required"> *</span>','class'=>'form-control','div'=>array('class'=>'form-group input text')));
		echo $this->Form->input('description', array('type'=>'textarea','class'=>'form-control','div'=>array('class'=>'form-group input textarea')));
		echo $this->Form->input('open', array(
                    'div'=>array('class'=>'form-group input'),
                    'separator'=> '</label></div><div class="radio"><label>',
                    'before' => '<div class="radio"><label>',
                    'after' => '</label></div>',
                    'type'=>'radio',
                    'value'=>isset($this->request->data['Survey']['open'])?$this->request->data['Survey']['open']:'1',
                    'options' => array(
                        '1'=>'Open System',
                        '0'=>'Close System'
                        ),
					'legend' => false
                ));
                if(!isset($this->request->data['Survey']['open'])||$this->request->data['Survey']['open']){
                    echo $this->Form->input('password', array('required'=>false,'type' => 'text','class'=>'form-control','label'=>'Respondent Password<span class="required"> *</span>','div'=>array('class'=>'form-group input text res-password')));
                }
                echo $this->Form->input('admin_password',array('label'=>'Admin Password<span class="required"> *</span>','class'=>'form-control','div'=>array('class'=>'form-group input text')));
                echo '<p>Email Verification</p>';
                echo $this->Form->input('email_verification_on', array(
                    'div'=>array('class'=>'form-group input'),
                    'separator'=> '</label></div><div class="radio"><label>',
                    'before' => '<div class="radio"><label>',
                    'after' => '</label></div>',
                    'type'=>'radio',
                    'value'=>isset($this->request->data['Survey']['email_verification_on'])?$this->request->data['Survey']['email_verification_on']:'1',
                    'options' => array(
                        '1'=>'on',
                        '0'=>'off'
                        ),
					'legend' => false
                ));
                echo $this->Form->input('welcome_message', array('type'=>'textarea','class'=>'form-control', 'div'=>array('class'=>'form-group input textarea'),'value'=>$this->request->data['Survey']['welcome_message']?$this->request->data['Survey']['welcome_message']:'Please enter the necessary information to take the survey.'));
                echo $this->Form->input('closing_message', array('type'=>'textarea','class'=>'form-control', 'div'=>array('class'=>'form-group input textarea'),'value'=>$this->request->data['Survey']['closing_message']?$this->request->data['Survey']['closing_message']:'Thank you for taking the time to answer this survey!'));
	?>
        </div>
    </div>
    <!-- / survey basic info -->

    <hr />



    <div class="row">
        <div class="col-xs-12 col-sm-8">
            <h3><?php echo __('Add Groups'); ?></h3>
            <br />
            <p>Choose from these standard groups.</p>
            <div class="survey-buttons">
            <?php echo $this->Html->link(__('Check All'), '#' , array('type'=>'button','class'=>'check-all-group btn btn-default', 'data-checkbox'=>'standard-group')); ?>
            <?php echo $this->Html->link(__('Uncheck All'), '#' , array('type'=>'button','class'=>'uncheck-all-group btn btn-default', 'data-checkbox'=>'standard-group')); ?>
            </div>

            <?php $cnt = 0; ?>
            <?php foreach($standardGroups as $group){ ?>
            <div class="checkbox">
                <label for="group[<?php echo $cnt; ?>]">
                    <input type="checkbox" class="standard-group" name="group[<?php echo $cnt; ?>]" value="<?php echo $group['StandardGroup']['name']; ?>" data-groupCnt="<?php echo $cnt; ?>"/>
                        <?php echo $group['StandardGroup']['name']; ?>
                </label>
                
                <?php //if group is not in the request data and is not yet checked ?>
                <div data-resPassCount='<?php echo $cnt; ?>' class="choose-respondent-add add_more_fieldsets" data-fields="resPass[<?php echo $cnt; ?>][name],resPass[<?php echo $cnt; ?>][passcode]" style='display:none;'>
                    <fieldset class="add_more form-respondent">
                                                <?php echo $this->Form->input('name', array('class' => 'form-control','name'=>"resPass[$cnt][name][0]", 'required'=>false)); ?>
                                                <?php echo $this->Form->input('passcode', array('class' => 'form-control','name'=>"resPass[$cnt][passcode][0]", 'required'=>false)); ?>
                        <a href="#" class="btn btn-default add_another">Add another respondent</a>
                    </fieldset>
                </div>
            <?php $cnt++; ?>

            </div>
            <?php } ?>
            <br />
            <p><strong>Add your own groups</strong></p>
            <div>
                <div class="choose-group-add">
                    <fieldset class="form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                    <?php echo $this->Form->input('name', array('label'=>'Group Name','class' => 'form-control','name'=>'ownGroup', 'required'=>false)); ?>
                            </div>
                            </div>
                    </fieldset>
                </div>
                <?php if(!$surveySystem){ ?>
                    <div class="choose-respondent-add add_more_fieldsets" data-fields="ownGroup['resPass'][name],ownGroup['resPass'][passcode]">
                        <fieldset class="form-group add_more form-respondent">
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <?php echo $this->Form->input('name', array('class' => 'form-control','name'=>"ownGroup['resPass'][name][0]", 'required'=>false)); ?>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <?php echo $this->Form->input('passcode', array('class' => 'form-control','name'=>"rownGroup['resPass'][name][0]", 'required'=>false)); ?>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                      <a href="#" class="btn btn-default add_another">Add another respondent</a>
                                </div>
                            </div>
                            
                        </fieldset>
                    </div>
                <?php } ?>
                <a href="#" class="btn btn-default another_group">Add another group</a>
            </div>
            
        </div>
    </div>
    <!-- / add groups -->
    
    
    <hr />
    
    <div class="row">
        <div class="col-xs-12">
            <h3><?php echo __('Add Themes'); ?></h3>
            <br />
            <p>Choose from these standard themes.</p>
            <div class="survey-buttons">
            <?php echo $this->Html->link(__('Check All'), '#' , array('type'=>'button','class'=>'check-all-theme btn btn-default', 'data-checkbox'=>'standard-theme')); ?>
            <?php echo $this->Html->link(__('Uncheck All'), '#' , array('type'=>'button','class'=>'uncheck-all-theme btn btn-default', 'data-checkbox'=>'standard-theme')); ?>
            </div>
            <?php
            $cnt = 0;
            foreach($standardThemes as $theme){ ?>
            <div class="checkbox">

                <label for="theme[<?php echo $cnt; ?>]">
                    <input type="checkbox" class="standard-theme" name="theme[<?php echo $cnt; ?>]" value="<?php echo $theme['StandardTheme']['id']; ?>"/>
                    <img class="standard-theme-icon img-responsive" src="<?php echo $theme['StandardTheme']['icon']?APP_URL.$theme['StandardTheme']['icon']:APP_URL.'files/uploads/standard_theme_icons/icon-default.png'; ?>" /><span class="standard-theme-name" style="color:#<?php echo $theme['StandardTheme']['color'] ?>;"><?php echo $theme['StandardTheme']['name']; ?></span>
                </label>
            </div>
            <?php $cnt++; ?>
        <?php } ?>
            <br />
            <div id="ownTheme">
            <strong>Add Your Own Theme</strong>
            <input name="add_another_theme" class="add_another_theme" value="0" type="hidden">
            <div class="choose-theme-add">
                <fieldset class="form-theme">
                        <div class="row">
                            <div class="col-xs-12 col-md-3">
                                <?php echo $this->Form->input('name', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control', 'name'=>'ownTheme[name]', 'required'=>false)); ?>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <?php echo $this->Form->input('description', array('div'=>array('class'=>'form-group input textarea'),'class'=>'form-control', 'name'=>'ownTheme[description]', 'required'=>false)); ?>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <?php echo $this->Form->input('icon', array('div'=>array('class'=>'form-group input file'), 'type'=>'file', 'name'=>'data[ownTheme][icon]')); ?>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <?php echo $this->Form->input('color', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control color','style'=>'width:80px;', 'default'=>'000000', 'name'=>'ownTheme[color]')); ?>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                      <a href="#" class="btn btn-default add_another">Add another theme</a>
                                </div>
                        <?php
//                                    echo $this->Form->input('name', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control', 'name'=>'ownTheme[name]', 'required'=>false));
//                                    echo $this->Form->input('description', array('div'=>array('class'=>'form-group input textarea'),'class'=>'form-control', 'name'=>'ownTheme[description]', 'required'=>false));
//                                    echo $this->Form->input('icon', array('div'=>array('class'=>'form-group input file'), 'type'=>'file', 'name'=>'data[ownTheme][icon]'));
//                                    echo $this->Form->input('color', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control color','style'=>'width:80px;', 'default'=>'000000', 'name'=>'ownTheme[color]'));
                            ?>
                            </div>
                        </div>
                </fieldset>
            </div>
            </div>
        </div>
    </div>
    <!-- / add groups -->

    <?php echo $this->Form->end(array('label' => 'Build Feedback Survey', 'class' => 'btn btn-lg btn-success', 'div'=>array('class'=>'submit text-center'))); ?>


    </div>









<?php echo $this->Html->script('survey_builder'); ?>
<?php echo $this->Html->css('survey_builder'); ?>