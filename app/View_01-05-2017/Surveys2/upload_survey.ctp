<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php if($success){ ?>
<div class="upload-success-buttons">
<?php echo $this->Html->link(__('View Themes'), array('action' => 'add_theme', $survey['Survey']['id']), array('type'=>'button','class'=>'btn btn-success')); ?>
<?php echo $this->Html->link(__('View Questions'), array('action' => 'add_question', $survey['Survey']['id']), array('type'=>'button','class'=>'btn btn-success')); ?>
<?php echo $this->Html->link(__('Preview Survey'), array('action' => 'verify', $survey['Survey']['slug']), array('type'=>'button','class'=>'btn btn-success')); ?>
</div>
<?php }else{ ?>
<div class="surveys form">
<?php echo $this->Form->create('Survey', array( 'action'=>'upload_survey','type' => 'file','div'=>array('class'=>'form-group col-xs-12 col-sm-8')));?>
    <fieldset>
        <legend><?php echo __('Upload Survey'); ?></legend><br />
<!--<label style="font-weight: bold;">Upload survey questionnaire</label>-->
<div class="row"> 
<?php echo $this->Form->input('survey',array( 'type' => 'file', 'label'=>false, 'div'=>array('class'=>'form-group col-xs-12 col-sm-3'))); ?>
<?php echo $this->Form->submit('Upload', array('class'=>'btn btn-success','div'=>array('class'=>'form-group col-xs-12 col-sm-9'))); ?>
</div>
</fieldset>
</div>
<?php } ?>
