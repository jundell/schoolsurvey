<div class="standardThemes form">
<?php echo $this->Form->create('StandardTheme', array('enctype' => 'multipart/form-data', 'role' => 'form')); ?>
    <fieldset>
        <legend><?php echo __('Add Standard Dimension'); ?></legend>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
	<?php
		echo $this->Form->input('name', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
		echo $this->Form->input('description', array('div'=>array('class'=>'form-group input textarea'),'class'=>'form-control'));
		echo $this->Form->input('icon', array('div'=>array('class'=>'form-group input file'), 'type'=>'file'));
		echo $this->Form->input('color', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control color','style'=>'width:80px;', 'default'=>'000000'));
	?>
            </div>
        </div>
    </fieldset>
<?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-default'))); ?>
</div>
<?php echo $this->Html->script('jscolor/jscolor'); ?>

<div class="standardThemes index">
    <br /><br />
        <?php if(!sizeof($standardThemes)){ ?>
            <p><br/>No standard themes added.</p>
        <?php }else{ ?>
    <p><?php echo __('Standard Dimensions'); ?></p>
    <table class="table table-hover" cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('name'); ?></th>
            <th><?php echo $this->Paginator->sort('description'); ?></th>
            <th><?php echo $this->Paginator->sort('icon'); ?></th>
            <th><?php echo $this->Paginator->sort('color'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
	<?php foreach ($standardThemes as $standardTheme): ?>
            <tr>
                <td><?php echo h($standardTheme['StandardTheme']['name']); ?>&nbsp;</td>
                <td><?php echo h($standardTheme['StandardTheme']['description']); ?>&nbsp;</td>
                <td><img class="img-responsive" src="<?php echo $standardTheme['StandardTheme']['icon']?APP_URL.$standardTheme['StandardTheme']['icon']:APP_URL.'files/uploads/standard_theme_icons/icon-default.png'; ?>" />&nbsp;</td>
                <td style="color:<?php echo $standardTheme['StandardTheme']['color']; ?>"><?php echo h($standardTheme['StandardTheme']['color']); ?>&nbsp;</td>
                <td class="actions">
                            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $standardTheme['StandardTheme']['id'])); ?>
                            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $standardTheme['StandardTheme']['id']), null, __('Are you sure you want to delete this dimension?')); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
                echo $this->Paginator->first('<< first');
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                echo $this->Paginator->last('last >>');
	?>
    </div>
</div>
        <?php }?>
<?php echo $this->Html->link(__('Back to survey builder menu'), array('controller'=>'survey_builder_components'), array('type'=>'button','class'=>'btn btn-default', 'style'=>'margin-top: 30px;')); ?>