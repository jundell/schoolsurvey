<?php

/** This is the report page **/

?>
<!-- page 1 -->

<?php echo $this->element('report/menu_header'); //header menu ?>

<?php if(!$hasResult){ ?>
<header id="report_header">
    <div class="container"> 
        <?php echo $this->Session->flash(); ?>
    </div>
</header>
<?php }else{ ?>
        
<?php echo $this->element('report/header'); //header ?>

<div class="content">
    <div class="container">
        <div>
            <h1><em class="blue"><?php echo $report_settings['report_title_lla']; ?></em> <span class="green report_header"><?php echo $report_settings['report_title_360']; ?></span></h1>
            <h2 class="blue report_header_name"><?php echo $result['survey']['Survey']['name']; ?></h2>
            
            <h3 class="blue"><u><?php echo $report_settings['overview_title']; ?></u></h3>
            <p><?php echo $report_settings['overview_p1']; ?> <strong><i><?php echo $report_settings['overview_p1_dimension']; ?></i></strong> <?php echo $report_settings['overview_p1_2']; ?></p>

            <?php echo $this->element('report/dimensions'); //dimensions ?>

            <p><?php echo $report_settings['overview_p2']; ?> <u><?php echo $report_settings['overview_p2_1']; ?></u> <?php echo $report_settings['overview_p2_2']; ?></p>
            <h3 class="blue"><u><?php echo $report_settings['report_data_title']; ?></u></h3>
            <p><?php echo $report_settings['responses_description_title']; ?></p>
            <p><span><?php echo $report_settings['likert_title']; ?></span> <?php echo $report_settings['likert_text']; ?> <i><?php echo $report_settings['likert_note']; ?></i></p>
            <p><span><?php echo $report_settings['yes_no_title']; ?></span> <?php echo $report_settings['yes_no_text']; ?></p>
            <p><span><?php echo $report_settings['open_ended_title']; ?></span> <?php echo $report_settings['open_ended_text']; ?></p>
        </div><!--end row-->
        <div class="page2">

            <?php echo $this->element('report/groups'); //groups ?>

            <p><?php echo $report_settings['groups_text2']; ?></p>

            <p><span><?php echo $report_settings['self_assessment_title']; ?></span> <?php echo $report_settings['self_assessment_text']; ?></p>

            <p><span><?php echo $report_settings['survey_management_title']; ?></span> <?php echo $report_settings['survey_management_text']; ?>
                <br /><strong><?php echo $report_settings['email_title']; ?></strong> <?php echo $report_settings['email_text']; ?> <strong><?php echo $report_settings['computer_ip_title']; ?></strong> <?php echo $report_settings['computer_ip_text']; ?>
            </p>

            <h3 class="blue"><u><?php echo $report_settings['your_data_title']; ?></u></h3>

            <?php echo $this->element('report/table1'); //table1 ?>

        </div><!--end row-->

        <div class="page3">

            <?php echo $this->element('report/table2'); //table2 ?>

        </div><!--end row-->
        <div class="page4">

            <?php echo $this->element('report/table3'); //table3 ?>

            <?php echo $this->element('report/table4'); //table4 ?>
        </div>

        <div class="page8">

            <?php echo $this->element('report/table5'); //table5 ?>

            <h3 class="blue"><u><?php echo $report_settings['reflecting_feedback_title']; ?></u></h3>

            <p><?php echo $report_settings['reflecting_feedback_text']; ?></p>

            <h5 class="blue"><?php echo $report_settings['few_things_title']; ?></h5>
            <ul>
                <li><?php echo $report_settings['few_things_text1']; ?></li>
                <li><?php echo $report_settings['few_things_text2']; ?></li>
                <li><?php echo $report_settings['few_things_text3']; ?></li>
                <li><?php echo $report_settings['few_things_text4']; ?></li>
            </ul>
            <br/>
            <h5><?php echo $report_settings['positive_feedback_title']; ?></h5>
            <ul>
                <li><?php echo $report_settings['positive_feedback_text1']; ?></li>
                <li><?php echo $report_settings['positive_feedback_text2']; ?></li>
            </ul>
            <br/>
            <h5><?php echo $report_settings['negative_feedback_title']; ?></h5>
            <ul>
                <li><?php echo $report_settings['negative_feedback_text1']; ?></li>
                <li><?php echo $report_settings['negative_feedback_text2']; ?></li>
                <li><?php echo $report_settings['negative_feedback_text3']; ?></li>
            </ul>
            <br />
            <br />
            <p><?php echo $report_settings['further_reading_title'].' <a href="'.$report_settings['further_reading_link'].'">'.$report_settings['further_reading_link'].'</a>';?></p>
            <br />
        </div><!--end row-->
    </div><!--end container-->
</div><!--end content-->

<?php echo $this->element('report/footer'); //footer ?>
<?php } ?>