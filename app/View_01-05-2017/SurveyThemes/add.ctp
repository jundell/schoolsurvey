<?php if(!$surveyStatus){ ?>
<div class="row">
    <div class="col-xs-12">
        <legend>Choose from these standard dimensions.</legend>
<?php echo $this->Form->create('StandardTheme', array('type'=>'file')); ?>
        <div class="survey-buttons">
    <?php echo $this->Html->link(__('Check All'), '#' , array('type'=>'button','class'=>'check-all btn btn-default', 'data-checkbox'=>'standard-theme', 'style'=>'margin-top: 30px;')); ?>
    <?php echo $this->Html->link(__('Uncheck All'), '#' , array('type'=>'button','class'=>'uncheck-all btn btn-default', 'data-checkbox'=>'standard-theme', 'style'=>'margin-top: 30px;')); ?>
        </div>
<?php
    //themes in request data
    $themes = isset($this->request->data['theme'])?$this->request->data['theme']:array();
    $cnt = 0;
    foreach($standardThemes as $theme){ ?>
    <?php
        //exists in $themes
        $tExist = array_key_exists ($cnt , $themes );     
    ?>
        <div class="checkbox">
            <label for="theme[<?php echo $cnt; ?>]">
            <?php if(in_array($theme['StandardTheme']['name'], $themeDatalist)){ ?>
                    <?php //if theme already exist in survey data ?>
                        <input type="checkbox" checked="checked" disabled="disabled" class="standard-theme" name="theme[<?php echo $cnt; ?>]" value="<?php echo $theme['StandardTheme']['id']; ?>"/>
                    <?php }else{ ?>
                        <input type="checkbox" <?php echo $tExist?'checked="checked"':'' ?> class="standard-theme" name="theme[<?php echo $cnt; ?>]" value="<?php echo $theme['StandardTheme']['id']; ?>"/>
                    <?php }?>
                        
                    <img class="standard-theme-icon img-responsive" src="<?php echo $theme['StandardTheme']['icon']?APP_URL.$theme['StandardTheme']['icon']:APP_URL.'files/uploads/standard_theme_icons/icon-default.png'; ?>" /><span class="standard-theme-name" style="color:<?php echo $theme['StandardTheme']['color']; ?>;"><?php echo $theme['StandardTheme']['name']; ?></span> <a href="#" data-toggle="modal" data-target="#tDetails_<?php echo $theme['StandardTheme']['id']; ?>">View Details</a>
            </label>
        </div>

        <!-- Question Details -->
        <div class="modal fade" id="tDetails_<?php echo $theme['StandardTheme']['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $theme['StandardTheme']['name']; ?>">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel" style="color:<?php echo $theme['StandardTheme']['color']; ?>;"><img class="standard-theme-icon img-responsive" src="<?php echo $theme['StandardTheme']['icon']?APP_URL.$theme['StandardTheme']['icon']:APP_URL.'files/uploads/standard_theme_icons/icon-default.png'; ?>" /> <?php echo $theme['StandardTheme']['name']; ?></h4>
                    </div>
                    <div class="modal-body">
                        <?php echo $theme['StandardTheme']['description']; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


    <?php $cnt++; ?>
<?php } ?>
        <br />
    <?php
        //own theme in request data
            $ownTheme = isset($this->request->data['ownTheme'])?$this->request->data['ownTheme']:array();
    ?>
        <div id="ownTheme">
            <strong>Add your own dimension</strong>
            <input name="add_another_theme" class="add_another_theme" value="0" type="hidden">
            <div class="choose-theme-add">
                <fieldset class="form-theme">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                    <?php
                            echo $this->Form->input('name', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control', 'name'=>'ownTheme[name]', 'required'=>false, 'default'=>isset($ownTheme['name'])?$ownTheme['name']:''));
                            if(isset($errorMessage['name'])){ ?>
                                <div class="error-message alert"><?php echo $errorMessage['name'][0]; ?></div>
                            <?php }
                            echo $this->Form->input('description', array('div'=>array('class'=>'form-group input textarea'),'class'=>'form-control', 'name'=>'ownTheme[description]', 'required'=>false, 'default'=>isset($ownTheme['description'])?$ownTheme['description']:''));
                    ?>
                            <div class="form-group input file">
                            <?php echo $this->Form->input('icon', array('div'=>false, 'type'=>'file', 'name'=>'data[ownTheme][icon]')); ?>
                                <span class="icon-def">Image height and width should not be more than 50 pixels.</span>
                                <?php if(isset($errorMessage['icon'])){ ?>
                                <div class="error-message alert"><?php echo $errorMessage['icon'][0]; ?></div>
                            <?php } ?>
                            </div>
                    <?php         echo $this->Form->input('color', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control color','style'=>'width:80px;', 'name'=>'ownTheme[color]', 'default'=>isset($ownTheme['color'])?$ownTheme['color']:'000000'));
                    ?>
                        </div>
                    </div>
                </fieldset>

            </div>
        </div>
        <br />
    <?php if($build){ ?>
        <a href="#" class="btn btn-default another_theme" id="save-and-add">Save and add another dimension</a><br /><br />
            <?php echo $this->Form->end(__(array('label' => 'Finish', 'class' => 'btn btn-success'))); ?>
        <?php }else{ ?>
            <?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-success'))); ?>
        <?php } ?>
    </div>
</div>
<br /><br />
<?php } ?>

<div class="row">
    <div class="col-xs-12">
        <div class="related table-responsive"> 
        <?php if(!sizeof($themeslist)){ ?>
            <p><br />No dimensions added.</p>
        <?php } ?>
	<?php if ($themeslist): ?>
            <p><?php echo __('Dimensions'); ?></p>
            <table class="table table-hover" cellpadding="0" cellspacing="0">
                <tr>
                    <th><?php echo $this->Paginator->sort('name'); ?></th>
                    <th><?php echo $this->Paginator->sort('description'); ?></th>
                    <th><?php echo $this->Paginator->sort('icon'); ?></th>
                    <th><?php echo $this->Paginator->sort('color'); ?></th>
                    <?php if(!$surveyStatus){ ?>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                    <?php } ?>
                </tr>
	<?php foreach ($themeslist as $theme): ?>
                <tr>
                    <td><?php echo h($theme['SurveyTheme']['name']); ?>&nbsp;</td>
                    <td><?php echo h($theme['SurveyTheme']['description']); ?>&nbsp;</td>
                    <td><img class="img-responsive" src="<?php echo $theme['SurveyTheme']['icon']?APP_URL.$theme['SurveyTheme']['icon']:APP_URL.'files/uploads/standard_theme_icons/icon-default.png'; ?>" />&nbsp;</td>
                    <td style="color:<?php echo $theme['SurveyTheme']['color']; ?>"><?php echo h($theme['SurveyTheme']['color']); ?>&nbsp;</td>
                    <?php if(!$surveyStatus){ ?>
                        <td class="actions">
                        <?php echo $this->Html->link(__('Edit'), array('controller' => 'survey_themes', 'action' => 'edit', $theme['SurveyTheme']['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'survey_themes', 'action' => 'delete', $theme['SurveyTheme']['id'],$return), null, __('Are you sure you want to delete this dimension?')); ?>
                        </td>
                    <?php } ?>
                </tr>
        <?php endforeach; ?>
            </table>
            <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
            <div class="paging">
	<?php
                echo $this->Paginator->first('<< first');
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                echo $this->Paginator->last('last >>');
	?>
            </div>
<?php endif; ?>

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        if ($("input#QuestionIsCalculating").is(':checked')) {
            $('div[data-fields="option,score"] div.input.number').show();
        } else {
            $('div[data-fields="option,score"] div.input.number').hide();
        }
        if ($('select[name="data[Question][type]"]').val() === "open ended") {
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }

        $(".option-score").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 49 || e.which > 57)) {
                //display error message
                $("#errmsg").html("Positive numbers only").show().fadeOut("slow");
                return false;
            }
        });
    });
    $('select[name="data[Question][type]"]').change(function () {
        if ($(this).val() === "open ended") {
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }
    })
    $('input#QuestionIncludeNoResponse').click(function () {
        $("#no-response-option").toggle(this.checked);
    })
    $('input#QuestionIsCalculating').click(function () {
        if ($(this).is(':checked')) {
            $('div[data-fields="option,score"] div.input.number').show();
        } else {
            $('div[data-fields="option,score"] div.input.number').hide();
        }
    });
</script>