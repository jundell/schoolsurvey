<div class="standardQuestionOptions form">
<?php echo $this->Form->create('StandardQuestionOption'); ?>
	<fieldset>
		<legend><?php echo __('Edit Standard Question Option'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('question_id');
		echo $this->Form->input('score');
		echo $this->Form->input('is_deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('StandardQuestionOption.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('StandardQuestionOption.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Standard Question Options'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
