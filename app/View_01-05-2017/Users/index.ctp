<div class="users form">
<?php echo $this->Form->create('User'); ?>
    <legend><?php echo __('Add Subscriber'); ?></legend>
    <fieldset>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <div class="form-group input text">
                    <?php echo $this->Form->input('first_name',array('div'=>false,'class'=>'form-control')); ?>
                    <?php if(isset($validationErrors['first_name'])){ ?>
                        <div class="error-message alert"><?php echo $validationErrors['first_name'][0]; ?></div>
                    <?php } ?>
                </div>
                <div class="form-group input text">
                    <?php echo $this->Form->input('last_name',array('div'=>false,'class'=>'form-control'));?>
                    <?php if(isset($validationErrors['last_name'])){ ?>
                        <div class="error-message alert"><?php echo $validationErrors['last_name'][0]; ?></div>
                    <?php } ?>
                </div>
                <div class="form-group input text">
                    <?php echo $this->Form->input('email_address',array('div'=>false,'class'=>'form-control'));?>
                    <?php if(isset($validationErrors['email_address'])){ ?>
                        <div class="error-message alert"><?php echo $validationErrors['email_address'][0]; ?></div>
                    <?php } ?>
                </div>
                <div class="form-group input password">
                    <?php echo $this->Form->input('password',array('div'=>false,'class'=>'form-control'));?>
                    <?php if(isset($validationErrors['password'])){ ?>
                        <div class="error-message alert"><?php echo $validationErrors['password'][0]; ?></div>
                    <?php } ?>
                </div>
                <div class="form-group input password">
                    <?php echo $this->Form->input('password_confirm', array('label'=>'Confirm Password', 'type'=>'password','div'=>false,'class'=>'form-control'));?>
                    <?php if(isset($validationErrors['password_confirm'])){ ?>
                        <div class="error-message alert"><?php echo $validationErrors['password_confirm'][0]; ?></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </fieldset>
<?php   echo $this->Form->submit( __('Submit'), array('class' => 'btn btn-primary')) ?>
<?php   echo $this->Form->end();?>
</div>



<div class="related">
    <br />
    <?php if(!sizeof($users)){ ?>
    <p><br />No users added.</p>
        <?php }else{ ?>
    <p><?php echo __('Subscribers');?></p>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-hover">

            <tr>
                <th><?php echo $this->Paginator->sort('name');?></th>
                <th><?php echo $this->Paginator->sort('email_address');?></th>
                <th class="actions"><?php echo __('Actions');?></th>
            </tr>
            <?php
            if(!empty($users)) :
                foreach ($users as $user):
        ?>
            <tr>		
                <td><?php echo h($user['User']['name']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['email_address']); ?>&nbsp;</td>
                <td class="actions">
                            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['user_id'])); ?> |
                            <?php echo $this->Html->link(__('Change Password'), array('action' => 'change_subscriber_password', $user['User']['user_id'])); ?> |
                            <?php if($user['User']['is_active']){ ?>
                                <?php echo $this->Html->link(__('Deactivate'), array('action' => 'deactivate', $user['User']['user_id'])); ?> |
                            <?php }else{ ?>
                                <?php echo $this->Html->link(__('Activate'), array('action' => 'activate', $user['User']['user_id'])); ?> |
                            <?php } ?>
                                                    <?php echo $this->Html->link(__('Survey Header'), array('controller'=>'subscriber_settings','action' => 'survey_header', $user['User']['user_id'])); ?>
                </td>
            </tr>
    <?php   
            endforeach;
        endif;
    ?>
        </table>
    </div>
<?php   echo $this->element('paginate') ?>
        <?php } ?>
</div>

