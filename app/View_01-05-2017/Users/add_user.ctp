<div class="users form">
    <?php if (sizeof($admins)) { ?>
        <?php echo $this->Form->create('User'); ?>
        <fieldset>
            <legend><?php echo __('Add User'); ?></legend>
            <?php
            echo $this->Form->input('firstname');
            echo $this->Form->input('lastname');
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            echo $this->Form->input('email');
            echo $this->Form->input('address');
            echo $this->Form->input('admin', array('empty' => 'Select...'));
            ?>
        </fieldset>
        <?php echo $this->Form->end(__('Submit')); ?>
    <?php
    } else {
        echo "You have to add an admin to create a user.<br />";
        echo $this->Html->link(__('Add Admin'), array('controller' => 'users', 'action' => 'add_admin'));
    }
    ?>
</div>
<?php echo $this->element('user_links'); ?>