<?php
App::uses('AppController', 'Controller');
/**
 * QuestionGroups Controller
 *
 * @property QuestionGroup $QuestionGroup
 */
class QuestionGroupsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->QuestionGroup->recursive = 0;
		$this->set('questionGroups', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->QuestionGroup->id = $id;
		if (!$this->QuestionGroup->exists()) {
			throw new NotFoundException(__('Invalid question group'));
		}
		$this->set('questionGroup', $this->QuestionGroup->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->QuestionGroup->create();
			if ($this->QuestionGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The question group has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question group could not be saved. Please, try again.'));
			}
		}
		$questions = $this->QuestionGroup->Question->find('list');
		$uqGroups = $this->QuestionGroup->UqGroup->find('list');
		$this->set(compact('questions', 'uqGroups'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->QuestionGroup->id = $id;
		if (!$this->QuestionGroup->exists()) {
			throw new NotFoundException(__('Invalid question group'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->QuestionGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The question group has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question group could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->QuestionGroup->read(null, $id);
		}
		$questions = $this->QuestionGroup->Question->find('list');
		$uqGroups = $this->QuestionGroup->UqGroup->find('list');
		$this->set(compact('questions', 'uqGroups'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->QuestionGroup->id = $id;
		if (!$this->QuestionGroup->exists()) {
			throw new NotFoundException(__('Invalid question group'));
		}
		if ($this->QuestionGroup->delete()) {
			$this->Session->setFlash(__('Question group deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Question group was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
