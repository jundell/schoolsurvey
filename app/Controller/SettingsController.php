<?php
App::uses('AppController', 'Controller');
/**
 * Settings Controller
 *
 * @property Setting $Setting
 */
class SettingsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Setting->recursive = 0;
		$this->set('settings', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Setting->exists($id)) {
			throw new NotFoundException(__('Invalid setting'));
		}
		$options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => $id));
		$this->set('setting', $this->Setting->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Setting->create();
			if ($this->Setting->save($this->request->data)) {
				$this->Session->setFlash(__('The setting has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The setting could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Setting->exists($id)) {
			throw new NotFoundException(__('Invalid setting'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Setting->save($this->request->data)) {
				$this->Session->setFlash(__('The setting has been saved'), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The setting could not be saved. Please, try again.'),'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => $id));
			$this->request->data = $this->Setting->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Setting->id = $id;
		if (!$this->Setting->exists()) {
			throw new NotFoundException(__('Invalid setting'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Setting->delete()) {
			$this->Session->setFlash(__('Setting deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Setting was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function survey_header(){
		$this->loadModel('User');
        
        $toolName = $this->Setting->get_by_slug('survey_default_tool_name_header');
        $companyName = $this->Setting->get_by_slug('survey_default_company_name_header');
        $logo = $this->Setting->get_by_slug('survey_default_logo_header');
        $toolNameSize = $this->Setting->get_by_slug('header_default_tool_name_font_size');
        $toolNameColor = $this->Setting->get_by_slug('header_default_tool_name_font_color');
        $toolNameAlignment = $this->Setting->get_by_slug('header_default_tool_name_alignment');
        $surveyNameSize = $this->Setting->get_by_slug('header_default_survey_name_font_size');
        $surveyNameColor = $this->Setting->get_by_slug('header_default_survey_name_font_color');
        $surveyNameAlignment = $this->Setting->get_by_slug('header_default_survey_name_alignment');
        $companyNameSize = $this->Setting->get_by_slug('header_default_company_name_font_size');
        $companyNameColor = $this->Setting->get_by_slug('header_default_company_name_font_color');
        $companyNameAlignment = $this->Setting->get_by_slug('header_default_company_name_alignment');
        $backgroundColor = $this->Setting->get_by_slug('header_default_background_color');


        $this->set('toolName', $toolName['value']);
        $this->set('companyName', $companyName['value']);
        $this->set('logo', $logo['value']);
        $this->set('toolNameSize', $toolNameSize['value']);
        $this->set('toolNameColor', $toolNameColor['value']);
        $this->set('toolNameAlignment', $toolNameAlignment['value']);
        $this->set('surveyNameSize', $surveyNameSize['value']);
        $this->set('surveyNameColor', $surveyNameColor['value']);
        $this->set('surveyNameAlignment', $surveyNameAlignment['value']);
        $this->set('companyNameSize', $companyNameSize['value']);
        $this->set('companyNameColor', $companyNameColor['value']);
        $this->set('companyNameAlignment', $companyNameAlignment['value']);
        $this->set('backgroundColor', $backgroundColor['value']);

        $success = false;
        if ($this->request->is('post')) {



echo '<pre>';
var_dump($this->request->data);
echo '</pre>';

            $this->save_default_setting($toolName, $this->request->data['Setting']['header_tool_name']); //Header Tool Name
            $this->save_default_setting($companyName, $this->request->data['Setting']['header_company_name']); //Header Company Name
            $this->save_default_setting($toolNameSize, $this->request->data['Setting']['header_tool_name_font_size']); //Header Tool Name Font Size
            $this->save_default_setting($toolNameColor, $this->request->data['Setting']['header_tool_name_font_color']);//Header Tool Name Font Color
            $this->save_default_setting($toolNameAlignment, $this->request->data['Setting']['header_tool_name_alignment']);//Header Tool Name Alignment
            $this->save_default_setting($surveyNameSize, $this->request->data['Setting']['header_survey_name_font_size']);//Header Survey Name Font Size
            $this->save_default_setting($surveyNameColor, $this->request->data['Setting']['header_survey_name_font_color']);//Header Survey Name Font Color
            $this->save_default_setting($surveyNameAlignment, $this->request->data['Setting']['header_survey_name_alignment']);//Header Survey Name Alignment
            $this->save_default_setting($companyNameSize, $this->request->data['Setting']['header_company_name_font_size']);//Header Company Name Font Size
            $this->save_default_setting($companyNameColor, $this->request->data['Setting']['header_company_name_font_color']);//Header Company Name Font Color
            $this->save_default_setting($companyNameAlignment, $this->request->data['Setting']['header_company_name_alignment']);//Header Comapany Name Alignment
            $this->save_default_setting($backgroundColor, $this->request->data['Setting']['header_background_color']);//Header Background Color
            

            //logo
            $logoInput = $this->request->data['Setting']['header_logo'];
            $logoVal = $logo['value'];
            if ($logoInput['name']) {
                $filename = basename($logoInput['name']);
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (in_array($ext, array('gif', 'jpg', 'png', 'jpeg'))) { // image file only
                    if (is_uploaded_file($logoInput['tmp_name'])) {
                        $savedFileName = $id . '_' . $logoInput['name'];
                        $file = 'files/logos/' . $savedFileName;

                        move_uploaded_file(
                                $logoInput['tmp_name'], $file
                        );
                        //$this->resizeImage('resize', $savedFileName, WWW_ROOT . 'files/logos/' . DS, false, false, 75);
                        $logoVal = $savedFileName;
                    }
                } else {
                    $success = false;
                    $this->Session->setFlash("Invalid image file.", "default", array("class" => "alert alert-error"));
                }
            }
            if ($logo['id']) {
                $this->Setting->id = $logo['id'];
                $this->Setting->set(array('value' => $logoVal));
                $success = $this->Setting->save();
            }

            if ($success) {
                $this->Session->setFlash(__('The default survey header has been saved'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('controller' => 'surveys', 'action' => 'index'));
            }
        }
	}

	private function save_default_setting($setting, $input) {
        if ($setting['id']) {
            $this->Setting->id = $setting['id'];
            $this->Setting->set(array('value' => $input));
            $success = $this->Setting->save();
        }
    }
}
