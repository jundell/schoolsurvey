<?php

App::uses('AppController', 'Controller');

/**
 * SubscriberSettings Controller
 *
 * @property SubscriberSetting $SubscriberSetting
 */
class SubscriberSettingsController extends AppController {

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->SubscriberSetting->recursive = 0;
        $this->set('subscriberSettings', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->SubscriberSetting->exists($id)) {
            throw new NotFoundException(__('Invalid subscriber setting'));
        }
        $options = array('conditions' => array('SubscriberSetting.' . $this->SubscriberSetting->primaryKey => $id));
        $this->set('subscriberSetting', $this->SubscriberSetting->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->SubscriberSetting->create();
            if ($this->SubscriberSetting->save($this->request->data)) {
                $this->Session->setFlash(__('The subscriber setting has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The subscriber setting could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->SubscriberSetting->exists($id)) {
            throw new NotFoundException(__('Invalid subscriber setting'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->SubscriberSetting->save($this->request->data)) {
                $this->Session->setFlash(__('The subscriber setting has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The subscriber setting could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('SubscriberSetting.' . $this->SubscriberSetting->primaryKey => $id));
            $this->request->data = $this->SubscriberSetting->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->SubscriberSetting->id = $id;
        if (!$this->SubscriberSetting->exists()) {
            throw new NotFoundException(__('Invalid subscriber setting'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->SubscriberSetting->delete()) {
            $this->Session->setFlash(__('Subscriber setting deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Subscriber setting was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    public function survey_header($id) {
        $this->loadModel('User');
        if (!$this->User->exists($id) || !$this->user_allowed('User', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }

        $toolName = $this->get_subscriber_setting($id, 'header_tool_name', 'survey_default_tool_name_header');
        $companyName = $this->get_subscriber_setting($id, 'header_company_name', 'survey_default_company_name_header');
        $logo = $this->get_subscriber_setting($id, 'header_logo', 'survey_default_logo_header');
        $toolNameSize = $this->get_subscriber_setting($id, 'header_tool_name_font_size', 'header_default_tool_name_font_size');
        $toolNameColor = $this->get_subscriber_setting($id, 'header_tool_name_font_color', 'header_default_tool_name_font_color');
        $toolNameAlignment = $this->get_subscriber_setting($id, 'header_tool_name_alignment', 'header_default_tool_name_alignment');
        $surveyNameSize = $this->get_subscriber_setting($id, 'header_survey_name_font_size', 'header_default_survey_name_font_size');
        $surveyNameColor = $this->get_subscriber_setting($id, 'header_survey_name_font_color', 'header_default_survey_name_font_color');
        $surveyNameAlignment = $this->get_subscriber_setting($id, 'header_survey_name_alignment', 'header_default_survey_name_alignment');
        $companyNameSize = $this->get_subscriber_setting($id, 'header_company_name_font_size', 'header_default_company_name_font_size');
        $companyNameColor = $this->get_subscriber_setting($id, 'header_company_name_font_color', 'header_default_company_name_font_color');
        $companyNameAlignment = $this->get_subscriber_setting($id, 'header_company_name_alignment', 'header_default_company_name_alignment');
        $backgroundColor = $this->get_subscriber_setting($id, 'header_background_color', 'header_default_background_color');


        $this->set('toolName', $toolName['text']);
        $this->set('companyName', $companyName['text']);
        $this->set('logo', $logo['text']);
        $this->set('toolNameSize', $toolNameSize['text']);
        $this->set('toolNameColor', $toolNameColor['text']);
        $this->set('toolNameAlignment', $toolNameAlignment['text']);
        $this->set('surveyNameSize', $surveyNameSize['text']);
        $this->set('surveyNameColor', $surveyNameColor['text']);
        $this->set('surveyNameAlignment', $surveyNameAlignment['text']);
        $this->set('companyNameSize', $companyNameSize['text']);
        $this->set('companyNameColor', $companyNameColor['text']);
        $this->set('companyNameAlignment', $companyNameAlignment['text']);
        $this->set('backgroundColor', $backgroundColor['text']);

        $success = false;
        if ($this->request->is('post')) {
            $this->save_susbscriber_setting($id, $toolName, @$this->request->data['SubscriberSetting']['header_tool_name'], 'Header Tool Name', 'header_tool_name'); //Header Tool Name
            $this->save_susbscriber_setting($id, $companyName, @$this->request->data['SubscriberSetting']['header_company_name'],'Header Company Name', 'header_company_name'); //Header Company Name
            $this->save_susbscriber_setting($id, $toolNameSize, @$this->request->data['SubscriberSetting']['header_tool_name_font_size'],'Header Tool Name Font Size','header_tool_name_font_size'); //Header Tool Name Font Size
            $this->save_susbscriber_setting($id, $toolNameColor, @$this->request->data['SubscriberSetting']['header_tool_name_font_color'],'Header Tool Name Font Color','header_tool_name_font_color');//Header Tool Name Font Color
            $this->save_susbscriber_setting($id, $toolNameAlignment, @$this->request->data['SubscriberSetting']['header_tool_name_alignment'],'Header Tool Name Alignment','header_tool_name_alignment');//Header Tool Name Alignment
            $this->save_susbscriber_setting($id, $surveyNameSize, @$this->request->data['SubscriberSetting']['header_survey_name_font_size'],'Header Survey Name Font Size','header_survey_name_font_size');//Header Survey Name Font Size
            $this->save_susbscriber_setting($id, $surveyNameColor, @$this->request->data['SubscriberSetting']['header_survey_name_font_color'],'Header Survey Name Font Color','header_survey_name_font_color');//Header Survey Name Font Color
            $this->save_susbscriber_setting($id, $surveyNameAlignment, @$this->request->data['SubscriberSetting']['header_survey_name_alignment'],'Header Survey Name Alignment','header_survey_name_alignment');//Header Survey Name Alignment
            $this->save_susbscriber_setting($id, $companyNameSize, @$this->request->data['SubscriberSetting']['header_company_name_font_size'], 'Header Company Name Font Size','header_company_name_font_size');//Header Company Name Font Size
            $this->save_susbscriber_setting($id, $companyNameColor, @$this->request->data['SubscriberSetting']['header_company_name_font_color'],'Header Company Name Font Color','header_company_name_font_color');//Header Company Name Font Color
            $this->save_susbscriber_setting($id, $companyNameAlignment, @$this->request->data['SubscriberSetting']['header_company_name_alignment'], 'Header Comapany Name Alignment','header_company_name_alignment');//Header Comapany Name Alignment
            $this->save_susbscriber_setting($id, $backgroundColor, @$this->request->data['SubscriberSetting']['header_background_color'],'Header Background Color','header_background_color');//Header Background Color
            

            //logo
            $logoInput = $this->request->data['SubscriberSetting']['header_logo'];
            $logoVal = $logo['text'];
            if ($logoInput['name']) {
                $filename = basename($logoInput['name']);
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (in_array($ext, array('gif', 'jpg', 'png', 'jpeg'))) { // image file only
                    if (is_uploaded_file($logoInput['tmp_name'])) {
                        $savedFileName = $id . '_' . $logoInput['name'];
                        $file = 'files/logos/' . $savedFileName;

                        move_uploaded_file(
                                $logoInput['tmp_name'], $file
                        );
                        //$this->resizeImage('resize', $savedFileName, WWW_ROOT . 'files/logos/' . DS, false, false, 75);
                        $logoVal = $savedFileName;
                    }
                } else {
                    $success = false;
                    $this->Session->setFlash("Invalid image file.", "default", array("class" => "alert alert-error"));
                }
            }
            if ($logo['size']) {
                $this->SubscriberSetting->id = $logo['id'];
                $this->SubscriberSetting->set(array('value' => $logoVal));
                $success = $this->SubscriberSetting->save();
            } else {
                $this->SubscriberSetting->create();
                $this->SubscriberSetting->set(array('name' => 'Header Logo', 'slug' => 'header_logo', 'value' => $logoVal, 'subscriber_id' => $id));
                $success = $this->SubscriberSetting->save();
            }
            if ($success) {
                $this->Session->setFlash(__('The subscriber survey header has been saved'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('controller' => 'users', 'action' => 'index'));
            }
        }
    }

    private function save_susbscriber_setting($subscriberId, $setting, $input, $settingName, $settingSlug) {
        if ($setting['size']) {
            $this->SubscriberSetting->id = $setting['id'];
            $this->SubscriberSetting->set(array('value' => $input));
            $success = $this->SubscriberSetting->save();
        } else {
            $this->SubscriberSetting->create();
            $this->SubscriberSetting->set(array('name' => $settingName, 'slug' => $settingSlug, 'value' => $input, 'subscriber_id' => $subscriberId));
            $success = $this->SubscriberSetting->save();
        }
    }

    private function get_subscriber_setting($subscriberId, $slug, $defSlug) {
        $setting = $this->SubscriberSetting->find('first', array('conditions' => array('SubscriberSetting.subscriber_id' => $subscriberId, 'SubscriberSetting.slug' => $slug)));
        $settingTxt = sizeof($setting) ? $setting['SubscriberSetting']['value'] : $this->Setting->get_value($defSlug);
        return array('size' => sizeof($setting), 'id' => isset($setting['SubscriberSetting']['id'])?$setting['SubscriberSetting']['id']:null, 'text' => $settingTxt);
    }

    private function resizeImage($cType = 'resize', $id, $imgFolder, $newName = false, $newWidth = false, $newHeight = false, $quality = 75, $bgcolor = false) {
        $img = $imgFolder . $id;
        list($oldWidth, $oldHeight, $type) = getimagesize($img);
        $ext = $this->image_type_to_extension($type);

        //check to make sure that the file is writeable, if so, create destination image (temp image)
        if (is_writeable($imgFolder)) {
            if ($newName) {
                $dest = $imgFolder . $newName;
            } else {
                $dest = $imgFolder . 'tmp_' . $id;
            }
        } else {
            //if not let developer know
            $imgFolder = substr($imgFolder, 0, strlen($imgFolder) - 1);
            $imgFolder = substr($imgFolder, strrpos($imgFolder, '\\') + 1, 20);
            debug("You must allow proper permissions for image processing. And the folder has to be writable.");
            debug("Run \"chmod 777 on '$imgFolder' folder\"");
            exit();
        }

        //check to make sure that something is requested, otherwise there is nothing to resize.
        //although, could create option for quality only
        if ($newWidth OR $newHeight) {
            /*
             * check to make sure temp file doesn't exist from a mistake or system hang up.
             * If so delete.
             */
            if (file_exists($dest)) {
                unlink($dest);
            } else {
                switch ($cType) {
                    default:
                    case 'resize':
                        # Maintains the aspect ration of the image and makes sure that it fits
                        # within the maxW(newWidth) and maxH(newHeight) (thus some side will be smaller)
                        $widthScale = 2;
                        $heightScale = 2;

                        if ($newWidth)
                            $widthScale = $newWidth / $oldWidth;
                        if ($newHeight)
                            $heightScale = $newHeight / $oldHeight;
                        //debug("W: $widthScale  H: $heightScale<br>");
                        if ($widthScale < $heightScale) {
                            $maxWidth = $newWidth;
                            $maxHeight = false;
                        } elseif ($widthScale > $heightScale) {
                            $maxHeight = $newHeight;
                            $maxWidth = false;
                        } else {
                            $maxHeight = $newHeight;
                            $maxWidth = $newWidth;
                        }

                        if ($maxWidth > $maxHeight) {
                            $applyWidth = $maxWidth;
                            $applyHeight = ($oldHeight * $applyWidth) / $oldWidth;
                        } elseif ($maxHeight > $maxWidth) {
                            $applyHeight = $maxHeight;
                            $applyWidth = ($applyHeight * $oldWidth) / $oldHeight;
                        } else {
                            $applyWidth = $maxWidth;
                            $applyHeight = $maxHeight;
                        }
                        //debug("mW: $maxWidth mH: $maxHeight<br>");
                        //debug("aW: $applyWidth aH: $applyHeight<br>");
                        $startX = 0;
                        $startY = 0;
                        //exit();
                        break;
                    case 'resizeCrop':
                        // -- resize to max, then crop to center
                        $ratioX = $newWidth / $oldWidth;
                        $ratioY = $newHeight / $oldHeight;

                        if ($ratioX < $ratioY) {
                            $startX = round(($oldWidth - ($newWidth / $ratioY)) / 2);
                            $startY = 0;
                            $oldWidth = round($newWidth / $ratioY);
                            $oldHeight = $oldHeight;
                        } else {
                            $startX = 0;
                            $startY = round(($oldHeight - ($newHeight / $ratioX)) / 2);
                            $oldWidth = $oldWidth;
                            $oldHeight = round($newHeight / $ratioX);
                        }
                        $applyWidth = $newWidth;
                        $applyHeight = $newHeight;
                        break;
                    case 'crop':
                        // -- a straight centered crop
                        $startY = ($oldHeight - $newHeight) / 2;
                        $startX = ($oldWidth - $newWidth) / 2;
                        $oldHeight = $newHeight;
                        $applyHeight = $newHeight;
                        $oldWidth = $newWidth;
                        $applyWidth = $newWidth;
                        break;
                }

                switch ($ext) {
                    case 'gif' :
                        $oldImage = imagecreatefromgif($img);
                        break;
                    case 'png' :
                        $oldImage = imagecreatefrompng($img);
                        break;
                    case 'jpg' :
                    case 'jpeg' :
                        $oldImage = imagecreatefromjpeg($img);
                        break;
                    default :
                        //image type is not a possible option
                        return false;
                        break;
                }

                //create new image
                $newImage = imagecreatetruecolor($applyWidth, $applyHeight);

                if ($bgcolor):
                    //set up background color for new image
                    sscanf($bgcolor, "%2x%2x%2x", $red, $green, $blue);
                    $newColor = ImageColorAllocate($newImage, $red, $green, $blue);
                    imagefill($newImage, 0, 0, $newColor);
                endif;

                //put old image on top of new image
                imagecopyresampled($newImage, $oldImage, 0, 0, $startX, $startY, $applyWidth, $applyHeight, $oldWidth, $oldHeight);

                switch ($ext) {
                    case 'gif' :
                        imagegif($newImage, $dest, $quality);
                        break;
                    case 'png' :
                        imagepng($newImage, $dest, $quality);
                        break;
                    case 'jpg' :
                    case 'jpeg' :
                        imagejpeg($newImage, $dest, $quality);
                        break;
                    default :
                        return false;
                        break;
                }

                imagedestroy($newImage);
                imagedestroy($oldImage);

                if (!$newName) {
                    unlink($img);
                    rename($dest, $img);
                }

                return true;
            }
        } else {
            return false;
        }
    }

    private function image_type_to_extension($imagetype) {
        if (empty($imagetype))
            return false;
        switch ($imagetype) {
            case IMAGETYPE_GIF : return 'gif';
            case IMAGETYPE_JPEG : return 'jpg';
            case IMAGETYPE_PNG : return 'png';
            case IMAGETYPE_SWF : return 'swf';
            case IMAGETYPE_PSD : return 'psd';
            case IMAGETYPE_BMP : return 'bmp';
            case IMAGETYPE_TIFF_II : return 'tiff';
            case IMAGETYPE_TIFF_MM : return 'tiff';
            case IMAGETYPE_JPC : return 'jpc';
            case IMAGETYPE_JP2 : return 'jp2';
            case IMAGETYPE_JPX : return 'jpf';
            case IMAGETYPE_JB2 : return 'jb2';
            case IMAGETYPE_SWC : return 'swc';
            case IMAGETYPE_IFF : return 'aiff';
            case IMAGETYPE_WBMP : return 'wbmp';
            case IMAGETYPE_XBM : return 'xbm';
            default : return false;
        }
    }

}
