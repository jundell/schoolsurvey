<?php

App::uses('AppController', 'Controller');

/**
 * SurveySettings Controller
 *
 * @property SurveySetting $SurveySetting
 */
class SurveySettingsController extends AppController {

    public $uses = array('Survey', 'SurveySetting');
    var $helpers = array('Time');
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->SurveySetting->recursive = 0;
        $this->set('surveySettings', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->SurveySetting->exists($id)) {
            throw new NotFoundException(__('Invalid survey setting'));
        }
        $options = array('conditions' => array('SurveySetting.' . $this->SurveySetting->primaryKey => $id));
        $this->set('surveySetting', $this->SurveySetting->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->SurveySetting->create();
            if ($this->SurveySetting->save($this->request->data)) {
                $this->Session->setFlash(__('The survey setting has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The survey setting could not be saved. Please, try again.'));
            }
        }
        $surveys = $this->SurveySetting->Survey->find('list');
        $this->set(compact('surveys'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->SurveySetting->exists($id)) {
            throw new NotFoundException(__('Invalid survey setting'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->SurveySetting->save($this->request->data)) {
                $this->Session->setFlash(__('The survey setting has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The survey setting could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('SurveySetting.' . $this->SurveySetting->primaryKey => $id));
            $this->request->data = $this->SurveySetting->find('first', $options);
        }
        $surveys = $this->SurveySetting->Survey->find('list');
        $this->set(compact('surveys'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->SurveySetting->id = $id;
        if (!$this->SurveySetting->exists()) {
            throw new NotFoundException(__('Invalid survey setting'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->SurveySetting->delete()) {
            $this->Session->setFlash(__('Survey setting deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Survey setting was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    public function group_message($surveyId){
        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey',$surveyId)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $groupMsg = $this->get_survey_setting($surveyId,'group_message','survey_default_group_message');
//        $groupMsg = $this->SurveySetting->find('first', array('conditions'=>array('SurveySetting.survey_id'=>$surveyId, 'SurveySetting.slug'=>'group_message')));
//        $groupMsgTxt = sizeof($groupMsg)?$groupMsg['SurveySetting']['value']:$this->Setting->get_value('survey_default_group_message');
        $success = false;
        if ($this->request->is('post')) {
            $value = $this->request->data['SurveySetting']['value'];
            if($groupMsg['size']){
                $this->SurveySetting->id = $groupMsg['id'];
                $this->SurveySetting->set(array('value'=>$value));
                $success = $this->SurveySetting->save();
            }else{
                $this->SurveySetting->create();
                $this->SurveySetting->set(array('name'=>'Group Message','slug'=>'group_message', 'value'=>$value, 'survey_id'=>$surveyId));
                $success = $this->SurveySetting->save();
            }
            
            if ($success) {
                $this->Session->setFlash(__('The survey setting has been saved'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('controller'=>'surveys','action' => 'index'));
            } else {
                $this->Session->setFlash(__('The survey setting could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-error'));
            }
        }
        $this->set('groupMsgTxt', $groupMsg['text']);
    }
    
    public function footer($surveyId){
        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey',$surveyId)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $footer = $this->get_survey_setting($surveyId,'footer','survey_default_footer');
//        $footerTxt = sizeof($footer)?$footer['SurveySetting']['value']:$this->Setting->get_value('survey_default_footer');
        $success = false;
        if ($this->request->is('post')) {
            $value = $this->request->data['SurveySetting']['value'];
            if($footer['size']){
                $this->SurveySetting->id = $footer['id'];
                $this->SurveySetting->set(array('value'=>$value));
                $success = $this->SurveySetting->save();
            }else{
                $this->SurveySetting->create();
                $this->SurveySetting->set(array('name'=>'Footer','slug'=>'footer', 'value'=>$value, 'survey_id'=>$surveyId));
                $success = $this->SurveySetting->save();
            }
            
            if ($success) {
                $this->Session->setFlash(__('The survey setting has been saved'), 'default', array('class' => 'alert alert-success'));
                // $this->redirect(array('controller'=>'surveys','action' => 'index'));
            } else {
                $this->Session->setFlash(__('The survey setting could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-error'));
            }
        }
        $this->set('footerTxt',$footer['text']);
        $this->set('surveyId', $surveyId);
    }
    
    public function set_header($surveyId){
        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey',$surveyId)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        
        $toolName = $this->get_survey_setting($surveyId,'tool_name','survey_default_tool_name_header');
        $companyName = $this->get_survey_setting($surveyId,'company_name','survey_default_company_name_header');
        $logo = $this->get_survey_setting($surveyId,'logo','survey_default_logo_header');
        
        $success = false;
        if ($this->request->is('post')) {
            $toolInput = $this->request->data['SurveySetting']['tool_name'];
            $companyInput = $this->request->data['SurveySetting']['company_name'];
            $logoInput = $this->request->data['SurveySetting']['logo'];
            
            //tool name
            if($toolName['size']){
                $this->SurveySetting->id = $toolName['id'];
                $this->SurveySetting->set(array('value'=>$toolInput));
                $success = $this->SurveySetting->save();
            }else{
                $this->SurveySetting->create();
                $this->SurveySetting->set(array('name'=>'Tool Name','slug'=>'tool_name', 'value'=>$toolInput, 'survey_id'=>$surveyId));
                $success = $this->SurveySetting->save();
            }
            
            //company name
            if($companyName['size']){
                $this->SurveySetting->id = $companyName['id'];
                $this->SurveySetting->set(array('value'=>$companyInput));
                $success = $this->SurveySetting->save();
            }else{
                $this->SurveySetting->create();
                $this->SurveySetting->set(array('name'=>'Company Name','slug'=>'company_name', 'value'=>$companyInput, 'survey_id'=>$surveyId));
                $success = $this->SurveySetting->save();
            }
            
            //logo
            $logoVal = $logo['text'];
            if($logoInput['name']){
                $filename = basename($logoInput['name']);
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (in_array($ext, array('gif', 'jpg', 'png', 'jpeg'))) { // image file only
                    if (is_uploaded_file($logoInput['tmp_name'])){
                        $savedFileName = $surveyId.'_'.$logoInput['name'];
                        $file = 'files/logos/' . $savedFileName;

                        move_uploaded_file(
                            $logoInput['tmp_name'],
                            $file
                        );
                        $this->resizeImage('resize', $savedFileName, WWW_ROOT . 'files/logos/' . DS, false, false, 75);
                        $logoVal = $savedFileName;
                    }
                }else{
                    $success = false;
                    $this->Session->setFlash("Invalid image file.", "default", array("class" => "alert alert-error"));
                }
            }
            if($logo['size']){
                $this->SurveySetting->id = $logo['id'];
                $this->SurveySetting->set(array('value'=>$logoVal));
                $success = $this->SurveySetting->save();
            }else{
                $this->SurveySetting->create();
                $this->SurveySetting->set(array('name'=>'Logo','slug'=>'logo', 'value'=>$logoVal, 'survey_id'=>$surveyId));
                $success = $this->SurveySetting->save();
            }
            if ($success) {
                $this->Session->setFlash(__('The survey setting has been saved'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('controller'=>'surveys','action' => 'index'));
            }
        }
        
        $this->set('toolName',$toolName['text']);
        $this->set('companyName',$companyName['text']);
        $this->set('logo',$logo['text']);
    }
    
    private function get_survey_setting($surveyId, $slug, $defSlug){
        $setting = $this->SurveySetting->find('first', array('conditions'=>array('SurveySetting.survey_id'=>$surveyId, 'SurveySetting.slug'=>$slug)));
        $settingTxt = sizeof($setting)?$setting['SurveySetting']['value']:$this->Setting->get_value($defSlug);
        return array('size'=>sizeof($setting),'id'=>$setting['SurveySetting']['id'],'text'=>$settingTxt);
    }
	
	private function resizeImage($cType = 'resize', $id, $imgFolder, $newName = false, $newWidth=false, $newHeight=false, $quality = 75, $bgcolor = false)
    {
        $img = $imgFolder . $id;
        list($oldWidth, $oldHeight, $type) = getimagesize($img); 
        $ext = $this->image_type_to_extension($type);
        
        //check to make sure that the file is writeable, if so, create destination image (temp image)
        if (is_writeable($imgFolder))
        {
            if($newName){
                $dest = $imgFolder . $newName;
            } else {
                $dest = $imgFolder . 'tmp_'.$id;
            }
        }
        else
        {
            //if not let developer know
            $imgFolder = substr($imgFolder, 0, strlen($imgFolder) -1);
            $imgFolder = substr($imgFolder, strrpos($imgFolder, '\\') + 1, 20);
            debug("You must allow proper permissions for image processing. And the folder has to be writable.");
            debug("Run \"chmod 777 on '$imgFolder' folder\"");
            exit();
        }
        
        //check to make sure that something is requested, otherwise there is nothing to resize.
        //although, could create option for quality only
        if ($newWidth OR $newHeight)
        {
            /*
             * check to make sure temp file doesn't exist from a mistake or system hang up.
             * If so delete.
             */
            if(file_exists($dest))
            {
                unlink($dest);
            }
            else
            {
                switch ($cType){
                    default:
                    case 'resize':
                        # Maintains the aspect ration of the image and makes sure that it fits
                        # within the maxW(newWidth) and maxH(newHeight) (thus some side will be smaller)
                        $widthScale = 2;
                        $heightScale = 2;
                        
                        if($newWidth) $widthScale =     $newWidth / $oldWidth;
                        if($newHeight) $heightScale = $newHeight / $oldHeight;
                        //debug("W: $widthScale  H: $heightScale<br>");
                        if($widthScale < $heightScale) {
                            $maxWidth = $newWidth;
                            $maxHeight = false;                            
                        } elseif ($widthScale > $heightScale ) {
                            $maxHeight = $newHeight;
                            $maxWidth = false;
                        } else {
                            $maxHeight = $newHeight;
                            $maxWidth = $newWidth;
                        }
                        
                        if($maxWidth > $maxHeight){
                            $applyWidth = $maxWidth;
                            $applyHeight = ($oldHeight*$applyWidth)/$oldWidth;
                        } elseif ($maxHeight > $maxWidth) {
                            $applyHeight = $maxHeight;
                            $applyWidth = ($applyHeight*$oldWidth)/$oldHeight;
                        } else {
                            $applyWidth = $maxWidth; 
                                $applyHeight = $maxHeight;
                        }
                        //debug("mW: $maxWidth mH: $maxHeight<br>");
                        //debug("aW: $applyWidth aH: $applyHeight<br>");
                        $startX = 0;
                        $startY = 0;
                        //exit();
                        break;
                    case 'resizeCrop':
                        // -- resize to max, then crop to center
                        $ratioX = $newWidth / $oldWidth;
                        $ratioY = $newHeight / $oldHeight;
    
                        if ($ratioX < $ratioY) { 
                            $startX = round(($oldWidth - ($newWidth / $ratioY))/2);
                            $startY = 0;
                            $oldWidth = round($newWidth / $ratioY);
                            $oldHeight = $oldHeight;
                        } else { 
                            $startX = 0;
                            $startY = round(($oldHeight - ($newHeight / $ratioX))/2);
                            $oldWidth = $oldWidth;
                            $oldHeight = round($newHeight / $ratioX);
                        }
                        $applyWidth = $newWidth;
                        $applyHeight = $newHeight;
                        break;
                    case 'crop':
                        // -- a straight centered crop
                        $startY = ($oldHeight - $newHeight)/2;
                        $startX = ($oldWidth - $newWidth)/2;
                        $oldHeight = $newHeight;
                        $applyHeight = $newHeight;
                        $oldWidth = $newWidth; 
                        $applyWidth = $newWidth;
                        break;
                }
                
                switch($ext)
                {
                    case 'gif' :
                        $oldImage = imagecreatefromgif($img);
                        break;
                    case 'png' :
                        $oldImage = imagecreatefrompng($img);
                        break;
                    case 'jpg' :
                    case 'jpeg' :
                        $oldImage = imagecreatefromjpeg($img);
                        break;
                    default :
                        //image type is not a possible option
                        return false;
                        break;
                }
                
                //create new image
                $newImage = imagecreatetruecolor($applyWidth, $applyHeight);
                
                if($bgcolor):
                //set up background color for new image
                    sscanf($bgcolor, "%2x%2x%2x", $red, $green, $blue);
                    $newColor = ImageColorAllocate($newImage, $red, $green, $blue); 
                    imagefill($newImage,0,0,$newColor);
                endif;
                
                //put old image on top of new image
                imagecopyresampled($newImage, $oldImage, 0,0 , $startX, $startY, $applyWidth, $applyHeight, $oldWidth, $oldHeight);
                
                    switch($ext)
                    {
                        case 'gif' :
                            imagegif($newImage, $dest, $quality);
                            break;
                        case 'png' :
                            imagepng($newImage, $dest, $quality);
                            break;
                        case 'jpg' :
                        case 'jpeg' :
                            imagejpeg($newImage, $dest, $quality);
                            break;
                        default :
                            return false;
                            break;
                    }
                
                imagedestroy($newImage);
                imagedestroy($oldImage);
                
                if(!$newName){
                    unlink($img);
                    rename($dest, $img);
                }
                
                return true;
            }

        } else {
            return false;
        }
        

    }

    private function image_type_to_extension($imagetype)
    {
    if(empty($imagetype)) return false;
        switch($imagetype)
        {
            case IMAGETYPE_GIF    : return 'gif';
            case IMAGETYPE_JPEG    : return 'jpg';
            case IMAGETYPE_PNG    : return 'png';
            case IMAGETYPE_SWF    : return 'swf';
            case IMAGETYPE_PSD    : return 'psd';
            case IMAGETYPE_BMP    : return 'bmp';
            case IMAGETYPE_TIFF_II : return 'tiff';
            case IMAGETYPE_TIFF_MM : return 'tiff';
            case IMAGETYPE_JPC    : return 'jpc';
            case IMAGETYPE_JP2    : return 'jp2';
            case IMAGETYPE_JPX    : return 'jpf';
            case IMAGETYPE_JB2    : return 'jb2';
            case IMAGETYPE_SWC    : return 'swc';
            case IMAGETYPE_IFF    : return 'aiff';
            case IMAGETYPE_WBMP    : return 'wbmp';
            case IMAGETYPE_XBM    : return 'xbm';
            default                : return false;
        }
    }
    
    /**
     * Downloads respondents' answers
     * 
     * @param int $id
     * @throws NotFoundException
     */
    public function download_data($id){
        set_time_limit(360);
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $this->loadModel('Question');
        $this->loadModel('QuestionsOption');
        $this->loadModel('SurveyResult');
        $this->loadModel('Group');
        $this->loadModel('SurveyAnswer');
        
        //survey
        $survey = $this->Survey->find('first', array(
            'conditions'=>array('id'=>$id),
            'fields'=>array('Survey.name','Survey.description','Survey.open','Survey.created','User.first_name','User.last_name','User.email_address')
            ));
        $survey['Survey']['short_name'] = $this->tokenTruncate($survey['Survey']['name'], 50);
        
        //quesions
        $questions = $this->Question->find('list', array('conditions' => array('Question.survey_id' => $id, 'Question.is_deleted' => 'no'), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));
        foreach($questions as $key=>$question){
            $questions[$key] = array();
            $questions[$key]['name'] = $question;
            $options = $this->QuestionsOption->find('all', array(
                'conditions' => array('QuestionsOption.question_id' => $key, 'QuestionsOption.is_deleted' => 'no'),
                'order' => array('QuestionsOption.id ASC'),
                'fields' => array('QuestionsOption.id','QuestionsOption.name','QuestionsOption.score')
                ));
            foreach($options as $option){
                $questions[$key]['options'][$option['QuestionsOption']['id']] = array();
                $questions[$key]['options'][$option['QuestionsOption']['id']]['name'] = $option['QuestionsOption']['name'];
                $questions[$key]['options'][$option['QuestionsOption']['id']]['score'] = $option['QuestionsOption']['score'];
            }
        }
        
        
        //respondents' answers
        $surveyRespondents = $this->SurveyResult->find('all', array(
            'conditions' => array('SurveyResult.survey_id' => $id),
            'fields' => array('SurveyResult.id','SurveyResult.ipaddress','SurveyResult.group_id','SurveyResult.email','SurveyResult.respondent','SurveyResult.created'),'recursive' => -1
            ));
        
        $queryGroups = $this->Group->find('all', array(
            'conditions' => array('Group.survey_id' => $id),
            'fields' => array('Group.id','Group.name'),
            'recursive' => -1
        ));
        $groups = array();
        if ( $queryGroups ) {
            foreach ($queryGroups as $group) {
                $groups[$group['Group']['id']] = $group['Group']['name'];
            }
        }

        //respondents' answers
        $respondentAnswers = array();
        if ( $surveyRespondents ) {
            foreach ($surveyRespondents as $respondent) {
                $resultID = $respondent['SurveyResult']['id'];
                $respondentAnswers[$resultID] = $this->SurveyAnswer->find('all', array(
                    'conditions' => array('SurveyAnswer.survey_result_id' => $resultID),
                    'recursive' => -1
                ));
            }
        }
        
        $this->set(compact('survey','questions','surveyRespondents','groups','respondentAnswers'));
    }
}
