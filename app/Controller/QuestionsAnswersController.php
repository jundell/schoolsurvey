<?php
App::uses('AppController', 'Controller');
/**
 * QuestionsAnswers Controller
 *
 * @property QuestionsAnswer $QuestionsAnswer
 */
class QuestionsAnswersController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->QuestionsAnswer->recursive = 0;
		$this->set('questionsAnswers', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->QuestionsAnswer->id = $id;
		if (!$this->QuestionsAnswer->exists()) {
			throw new NotFoundException(__('Invalid questions answer'));
		}
		$this->set('questionsAnswer', $this->QuestionsAnswer->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->QuestionsAnswer->create();
			if ($this->QuestionsAnswer->save($this->request->data)) {
				$this->Session->setFlash(__('The questions answer has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The questions answer could not be saved. Please, try again.'));
			}
		}
		$users = $this->QuestionsAnswer->User->find('list');
		$questions = $this->QuestionsAnswer->Question->find('list');
		$answerOptions = $this->QuestionsAnswer->AnswerOption->find('list');
		$this->set(compact('users', 'questions', 'answerOptions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->QuestionsAnswer->id = $id;
		if (!$this->QuestionsAnswer->exists()) {
			throw new NotFoundException(__('Invalid questions answer'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->QuestionsAnswer->save($this->request->data)) {
				$this->Session->setFlash(__('The questions answer has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The questions answer could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->QuestionsAnswer->read(null, $id);
		}
		$users = $this->QuestionsAnswer->User->find('list');
		$questions = $this->QuestionsAnswer->Question->find('list');
		$answerOptions = $this->QuestionsAnswer->AnswerOption->find('list');
		$this->set(compact('users', 'questions', 'answerOptions'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->QuestionsAnswer->id = $id;
		if (!$this->QuestionsAnswer->exists()) {
			throw new NotFoundException(__('Invalid questions answer'));
		}
		if ($this->QuestionsAnswer->delete()) {
			$this->Session->setFlash(__('Questions answer deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Questions answer was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
