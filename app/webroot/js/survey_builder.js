$(document).ready(function(){
     $('input[name="data[Survey][open]"]').change(function(){
         if($(this).val()=='1'){
             $('div.res-password').show();
         }else{
             $('div.res-password').hide();
         }
     });
     
    $('a.add_another_row').on('click',function(e){
        add_row($(this));
        e.preventDefault();
    });
    $('a.remove-row').on('click',function(e){
        var parent = $(this).parents('.add_more_row');
        var btn = parent.find('a.btn.add_another_row').parent();
        if(btn.length){
            parent.prev().find('div.row').find('div.last-col-input').after(btn);
        }
        parent.remove();
        e.preventDefault();
    });
    
//    $('a.remove_row').on('click',function(e){
//        alert('test');
////        var parent = $(this).parents('.add_more_row');
////        var btn = parent.find('a.btn.add_another_row').parent();
////        if(btn.length){
////            parent.prev().find('div.row').find('div.last-col-input').after(btn);
////        }
////        parent.remove();
//        e.preventDefault();
//    });
 });
 
 function add_row($this){
    
    var addmore = $this.parents('.add_more_row'); // row to clone
    var pr = addmore.parent(); //
    var cl = addmore.clone();
    var cnt =  pr.find('.add_more_row').length;
    
//    console.log(addmore)
//    alert(cnt);
    var fields = pr.data('fields').split(',');
    
    var cnt2 = cnt;
    
    if($this.attr('data-lastId')){
        cnt2 = $this.attr('data-lastId');
    }
    
    addmore.find('div.add-row-btn-wrapper').remove();
    cl.find('div.remove-row-btn-wrapper').remove();
    
    //change the names of the input fields
    var count=0;
    cl.find('input,textarea').each(function(){
        var $thisField = $(this);
        $thisField.val('');
        $thisField.attr('name',fields[count]+'['+cnt2+']');
        if($thisField.hasClass('option-score')){
            $thisField.val(cnt+1);
        }
        count++;
    });
    var removeBtn = '<div class="col-xs-4 col-md-1 remove-row-btn-wrapper"><a class="btn btn-default remove-row" href="#">X</a></div>';
    if(cl.find('div.clearfix').length){
        cl.find('div.clearfix').last().before(removeBtn);
    }else{
        cl.find('div.row.row-wrap').append(removeBtn);
    }
    
    pr.append(cl);
    var btnAddAnother = pr.find('.add_more_row').last().find('a.add_another_row');
    if($this.attr('data-lastId')){
        btnAddAnother.attr('data-lastId',parseInt(cnt2)+1);
    }
    
    //color fields
    var colorFields = pr.find('.add_more_row').last().find('input.color');
    if(colorFields.length){
        colorFields.each(function(){
            $(this).val('000000');
        });
        new jscolor.init();
    }
    
    btnAddAnother.on('click',function(e){
        add_row($(this));
        e.preventDefault();
    });
    pr.find('.remove-row').on('click',function(e){
        var parent = $(this).parents('.add_more_row');
        var btn = parent.find('a.btn.add_another_row').parent();
        if(btn.length){
            parent.prev().find('div.row.row-wrap').find('div.last-col-input').after(btn);
//            parent.prev().find('div.input.text').last().after(btn);
        }
        parent.remove();
        e.preventDefault();
    });
}