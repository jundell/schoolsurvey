$(document).ready(function(){
    $('div.table-responsive.table2-table').each(function () {
        $this = $(this);
        width = $this.width();
        scoresWidth = width - (width * .38) - 4;
        scores = $this.find('th.score-th');
        scoreWidth = (scoresWidth / scores.length) - 2;
        scores.each(function () {
            $(this).width(scoreWidth);
        });
    });
});
