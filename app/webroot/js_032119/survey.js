/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    $('a.add_another').on('click',function(e){
        add_field($(this));
        e.preventDefault();
    });
    
    $('a.add_another_fieldset').on('click',function(e){
        add_fieldset($(this));
        e.preventDefault();
    });
    
    $('a.another_group').on('click',function(e){
        var f = $(this).parent('form')
        $('.add_another_group').val('1');
        f.submit();
        e.preventDefault();
    });
    
    $('a.another_question').on('click',function(e){
        var f = $(this).parent('form')
        $('.add_another_question').val('1');
        f.submit();
        e.preventDefault();
    });
    
    $('a.another_theme').on('click',function(e){
        var f = $(this).parent('form')
        $('.add_another_theme').val('1');
        f.submit();
        e.preventDefault();
    });
    
    $('.remove').on('click',function(){
        var parent = $(this).parent();
        var btn = parent.find('a.btn.add_another');
        if(btn.length){
            parent.prev().find('div.input.text').after(btn);
        }
        parent.remove();
    })
    
    //check all
    $('.check-all').click(function(e){
        var $this = $(this);
        var checkbox = $this.attr('data-checkbox');
        $('.'+checkbox).each(function(){
            if(!$(this).is(':checked') && !$(this).is(':disabled')){
                $(this).prop( "checked", true );
            }
        });
        e.preventDefault();
    });
    
    //uncheck all
    $('.uncheck-all').click(function(e){
        var $this = $(this);
        var checkbox = $this.attr('data-checkbox');
        $('.'+checkbox).each(function(){
            if($(this).is(':checked') && !$(this).is(':disabled')){
                $(this).prop( "checked", false );
            }
        });
        e.preventDefault();
    });
});

function add_fieldset($this){
    
    var addmore = $this.parent();
    var pr = addmore.parent();
    var cl = addmore.clone();
    var cnt =  pr.find('.add_more').length;
    var fields = pr.data('fields').split(',');
    console.log(fields);
    var cnt2 = cnt;
    
    if($this.attr('data-lastId')){
        cnt2 = $this.attr('data-lastId');
    }
    
    addmore.find('a.add_another_fieldset').remove();
    cl.find('a.remove').remove();
    
    //change the names of the input fields
    var count=0;
    cl.find('input,textarea').each(function(){
        console.log(count);
        console.log(fields[count]);
        var $thisField = $(this);
        console.log($thisField);
        $thisField.val('');
        $thisField.attr('name',fields[count]+'['+cnt2+']');
        count++;
    });
    cl.append('<a class="btn btn-default remove" href="#">X</a>');

    pr.append(cl);
    var btnAddAnother = pr.find('.add_more').last().find('a.add_another_fieldset');
    if($this.attr('data-lastId')){
        btnAddAnother.attr('data-lastId',parseInt(cnt2)+1);
    }
    btnAddAnother.bind('click',function(e){
        
        add_fieldset($(this));
        e.preventDefault();
    });
    pr.find('.remove').bind('click',function(){
        var parent = $(this).parent();
        var btn = parent.find('a.btn.add_another_fieldset');
        if(btn.length){
            parent.prev().find('div.input.text').after(btn);
        }
        parent.remove();
    });
}

function add_field($this){
    
    var addmore = $this.parent();
    var pr = addmore.parent();
    var cl = addmore.clone();
    var cnt =  pr.find('.add_more').length;
    var fields = pr.data('fields').split(',');
    
    var cnt2 = cnt;
    
    if($this.attr('data-lastId')){
        cnt2 = $this.attr('data-lastId');
    }
    
    addmore.find('a.add_another').remove();
    cl.find('a.remove').remove();
    
    //change the names of the input fields
    var count=0;
    cl.find('input,textarea').each(function(){
        var $thisField = $(this);
        $thisField.val('');
        $thisField.attr('name',fields[count]+'['+cnt2+']');
        if($thisField.hasClass('option-score')){
            $thisField.val(cnt+1);
        }
        count++;
    })
    if(cl.find('div.clearfix').length){
        cl.find('div.clearfix').last().before('<a class="btn btn-default remove" href="#">X</a>');
    }else{
        cl.append('<a class="btn btn-default remove" href="#">X</a>');
    }
    
    pr.append(cl);
    var btnAddAnother = pr.find('.add_more').last().find('a.add_another');
    if($this.attr('data-lastId')){
        btnAddAnother.attr('data-lastId',parseInt(cnt2)+1);
    }
    
    //color fields
    var colorFields = pr.find('.add_more').last().find('input.color');
    if(colorFields.length){
        colorFields.each(function(){
            $(this).val('000000');
        });
        new jscolor.init();
    }
    
    btnAddAnother.bind('click',function(e){
        add_field($(this));
        e.preventDefault();
    });
    pr.find('.remove').bind('click',function(e){
        var parent = $(this).parent();
        var btn = parent.find('a.btn.add_another');
        if(btn.length){
            parent.prev().find('div.input.text').last().after(btn);
        }
        parent.remove();
        e.preventDefault();
    });
}