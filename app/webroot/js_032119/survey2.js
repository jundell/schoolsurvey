/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    $('a.add_another').on('click',function(e){
        add_field($(this));
        e.preventDefault();
    });
    $('a.another_question').on('click',function(e){
        var f = $(this).parent('form')
        $('.add_another_question').val('1');
        f.submit();
        e.preventDefault();
    });
});

function add_field($this){
    var addmore = $this.parent();
    var pr = addmore.parent();
    var cl = addmore.clone();
    var cnt =  pr.find('.add_more').length;
    console.info(cnt);
    var fields = pr.data('fields').split(',');
    addmore.find('a.add_another').remove();
    
    //change the names of the input fields
    var count=0;
    cl.find('input').each(function(){
        $(this).val('');
        $(this).attr('name',fields[count]+'['+cnt+']');
        count++;
    })
    
    pr.append(cl);
    pr.find('.add_more').last().find('a.add_another').bind('click',function(){
        add_field($(this));
    })     
}