<?php
/**
 * SurveyResultFixture
 *
 */
class SurveyResultFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'survey_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'group_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'question_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'question_option_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'respondents' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ipaddress' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'survey_id' => 1,
			'group_id' => 1,
			'question_id' => 1,
			'question_option_id' => 1,
			'respondents' => 'Lorem ipsum dolor sit amet',
			'ipaddress' => 'Lorem ipsum dolor sit amet',
			'created' => '2013-10-30 01:48:48',
			'modified' => '2013-10-30 01:48:48'
		),
	);

}
