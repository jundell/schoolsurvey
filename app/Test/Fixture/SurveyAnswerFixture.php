<?php
/**
 * SurveyAnswerFixture
 *
 */
class SurveyAnswerFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'survey_result_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'question_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'question_option_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'is_deleted' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'survey_result_id' => 1,
			'question_id' => 1,
			'question_option_id' => 1,
			'created' => '2013-11-05 07:51:05',
			'modified' => '2013-11-05 07:51:05',
			'is_deleted' => 1
		),
	);

}
