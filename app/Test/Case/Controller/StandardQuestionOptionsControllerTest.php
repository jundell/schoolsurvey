<?php
App::uses('StandardQuestionOptionsController', 'Controller');

/**
 * StandardQuestionOptionsController Test Case
 *
 */
class StandardQuestionOptionsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.standard_question_option',
		'app.question',
		'app.survey',
		'app.user',
		'app.role',
		'app.survey_theme',
		'app.survey_answer',
		'app.questions_option',
		'app.setting'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
