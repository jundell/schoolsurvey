<?php
App::uses('QuestionsAnswersController', 'Controller');

/**
 * QuestionsAnswersController Test Case
 *
 */
class QuestionsAnswersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.questions_answer',
		'app.user',
		'app.group',
		'app.user_group',
		'app.question',
		'app.category',
		'app.answer_option',
		'app.answer',
		'app.question_group',
		'app.uq_group',
		'app.uq'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
