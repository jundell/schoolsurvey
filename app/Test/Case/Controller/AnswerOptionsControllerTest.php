<?php
App::uses('AnswerOptionsController', 'Controller');

/**
 * AnswerOptionsController Test Case
 *
 */
class AnswerOptionsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.answer_option',
		'app.question',
		'app.category',
		'app.question_group',
		'app.uq_group',
		'app.user_group',
		'app.user',
		'app.group',
		'app.answer',
		'app.questions_answer'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
