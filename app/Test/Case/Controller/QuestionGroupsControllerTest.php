<?php
App::uses('QuestionGroupsController', 'Controller');

/**
 * QuestionGroupsController Test Case
 *
 */
class QuestionGroupsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.question_group',
		'app.question',
		'app.category',
		'app.answer_option',
		'app.answer',
		'app.questions_answer',
		'app.uq_group',
		'app.user_group',
		'app.user',
		'app.group',
		'app.uq'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
