<?php
App::uses('AnswersController', 'Controller');

/**
 * AnswersController Test Case
 *
 */
class AnswersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.answer',
		'app.question',
		'app.category',
		'app.answer_option',
		'app.questions_answer',
		'app.user',
		'app.group',
		'app.user_group',
		'app.uq_group',
		'app.question_group'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
