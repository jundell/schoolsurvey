<?php
App::uses('RespondentPasscodesController', 'Controller');

/**
 * RespondentPasscodesController Test Case
 *
 */
class RespondentPasscodesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.respondent_passcode',
		'app.survey',
		'app.user',
		'app.role',
		'app.group',
		'app.setting'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
