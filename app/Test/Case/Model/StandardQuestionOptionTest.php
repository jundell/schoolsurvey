<?php
App::uses('StandardQuestionOption', 'Model');

/**
 * StandardQuestionOption Test Case
 *
 */
class StandardQuestionOptionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.standard_question_option',
		'app.question',
		'app.survey',
		'app.user',
		'app.role',
		'app.survey_theme',
		'app.survey_answer',
		'app.questions_option'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->StandardQuestionOption = ClassRegistry::init('StandardQuestionOption');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->StandardQuestionOption);

		parent::tearDown();
	}

}
