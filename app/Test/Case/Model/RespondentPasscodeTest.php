<?php
App::uses('RespondentPasscode', 'Model');

/**
 * RespondentPasscode Test Case
 *
 */
class RespondentPasscodeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.respondent_passcode',
		'app.survey',
		'app.user',
		'app.role',
		'app.group'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RespondentPasscode = ClassRegistry::init('RespondentPasscode');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RespondentPasscode);

		parent::tearDown();
	}

}
