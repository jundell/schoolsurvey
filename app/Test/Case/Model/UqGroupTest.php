<?php
App::uses('UqGroup', 'Model');

/**
 * UqGroup Test Case
 *
 */
class UqGroupTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.uq_group',
		'app.question_group',
		'app.question',
		'app.category',
		'app.answer',
		'app.user_group'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UqGroup = ClassRegistry::init('UqGroup');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UqGroup);

		parent::tearDown();
	}

}
