<?php
App::uses('StandardQuestion', 'Model');

/**
 * StandardQuestion Test Case
 *
 */
class StandardQuestionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.standard_question',
		'app.theme'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->StandardQuestion = ClassRegistry::init('StandardQuestion');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->StandardQuestion);

		parent::tearDown();
	}

}
