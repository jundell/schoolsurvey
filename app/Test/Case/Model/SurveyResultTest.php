<?php
App::uses('SurveyResult', 'Model');

/**
 * SurveyResult Test Case
 *
 */
class SurveyResultTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.survey_result',
		'app.survey',
		'app.question',
		'app.group',
		'app.question_option'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SurveyResult = ClassRegistry::init('SurveyResult');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SurveyResult);

		parent::tearDown();
	}

}
