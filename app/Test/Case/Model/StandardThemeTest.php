<?php
App::uses('StandardTheme', 'Model');

/**
 * StandardTheme Test Case
 *
 */
class StandardThemeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.standard_theme'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->StandardTheme = ClassRegistry::init('StandardTheme');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->StandardTheme);

		parent::tearDown();
	}

}
