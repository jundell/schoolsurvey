<?php
App::uses('SurveyReportSetting', 'Model');

/**
 * SurveyReportSetting Test Case
 *
 */
class SurveyReportSettingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.survey_report_setting'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SurveyReportSetting = ClassRegistry::init('SurveyReportSetting');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SurveyReportSetting);

		parent::tearDown();
	}

}
