<?php
App::uses('QuestionsOption', 'Model');

/**
 * QuestionsOption Test Case
 *
 */
class QuestionsOptionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.questions_option',
		'app.question',
		'app.survey',
		'app.survey_result',
		'app.group',
		'app.question_option'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->QuestionsOption = ClassRegistry::init('QuestionsOption');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->QuestionsOption);

		parent::tearDown();
	}

}
