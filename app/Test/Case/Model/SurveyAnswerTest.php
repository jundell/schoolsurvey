<?php
App::uses('SurveyAnswer', 'Model');

/**
 * SurveyAnswer Test Case
 *
 */
class SurveyAnswerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.survey_answer',
		'app.survey_result',
		'app.survey',
		'app.question',
		'app.group',
		'app.question_option'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SurveyAnswer = ClassRegistry::init('SurveyAnswer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SurveyAnswer);

		parent::tearDown();
	}

}
