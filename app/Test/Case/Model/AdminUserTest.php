<?php
App::uses('AdminUser', 'Model');

/**
 * AdminUser Test Case
 *
 */
class AdminUserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.admin_user',
		'app.admin',
		'app.user',
		'app.group',
		'app.user_group',
		'app.uq_group',
		'app.question_group',
		'app.question',
		'app.category',
		'app.answer_option',
		'app.answer',
		'app.questions_answer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AdminUser = ClassRegistry::init('AdminUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AdminUser);

		parent::tearDown();
	}

}
