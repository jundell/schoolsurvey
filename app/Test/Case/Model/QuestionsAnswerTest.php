<?php
App::uses('QuestionsAnswer', 'Model');

/**
 * QuestionsAnswer Test Case
 *
 */
class QuestionsAnswerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.questions_answer',
		'app.user',
		'app.group',
		'app.user_group',
		'app.uq_group',
		'app.question_group',
		'app.question',
		'app.category',
		'app.answer_option',
		'app.answer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->QuestionsAnswer = ClassRegistry::init('QuestionsAnswer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->QuestionsAnswer);

		parent::tearDown();
	}

}
