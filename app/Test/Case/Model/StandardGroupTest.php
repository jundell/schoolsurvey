<?php
App::uses('StandardGroup', 'Model');

/**
 * StandardGroup Test Case
 *
 */
class StandardGroupTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.standard_group'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->StandardGroup = ClassRegistry::init('StandardGroup');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->StandardGroup);

		parent::tearDown();
	}

}
