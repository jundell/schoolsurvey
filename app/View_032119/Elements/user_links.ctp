<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <?php if ($this->Session->read('Auth.User.group_id') == '1') { ?>
            <li><?php echo $this->Html->link(__('List Admins'), array('controller' => 'users', 'action' => 'user_list/admin')); ?></li>
            <li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'user_list/user')); ?> </li>
            <li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('Add Admin'), array('controller' => 'users', 'action' => 'add_admin')); ?> </li>
            <li><?php echo $this->Html->link(__('Add User'), array('controller' => 'users', 'action' => 'add_user')); ?> </li>
            <li><?php echo $this->Html->link(__('Add Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
            <li><?php echo $this->Html->link(__('Add Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
        <?php } ?>
    </ul>
</div>