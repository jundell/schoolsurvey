<?php
 if(count($result['nonCalculating']) > 0) :?>
<p><span><?php echo $report_settings['table5_title']; ?></span> <?php echo $report_settings['table5_text']; ?></p>
<?php endif ;?>

<?php foreach($result['nonCalculating'] as $nc){ ?>
    <p><?php echo $nc['num']; ?>. <?php echo $nc['name']; ?></p>
    <div class="table-responsive">          
        <table class="table">
            <thead>
                <tr>
                    <th>Answers</th>
                    <?php foreach($result['groups'] as $group){ ?>
                        <th><?php echo $group['Group']['name']; ?></th>
                    <?php } ?>
                    <th>Admin</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($nc['options'] as $option){ ?>
                    <tr>
                        <td><?php echo $option['name']; ?></td>
                        <?php foreach($option['groups'] as $optionGroup){ ?>
                            <td><?php echo $optionGroup; ?></td>
                        <?php } ?>
                        <td><?php echo $option['admin']; ?></td>
                        <td><?php echo $option['total']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?>