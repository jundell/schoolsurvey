<?php $qRankDimensionsColors = array(); ?>
<?php foreach($result['dimensions'] as $d){ ?>
    <?php $qRankDimensionsColors[$d['SurveyTheme']['id']] = $d['SurveyTheme']['color']; ?>
<?php } ?>

<p><span><?php echo $report_settings['table3_title']; ?></span> <?php echo $report_settings['table3_text']; ?></p>

<p><?php echo $report_settings['table_of_descriptors_text']; ?></p>
<div class="table-responsive">          
    <table class="table pg4-table1">
        <thead>
            <tr>
                <th></th>
                <th><?php echo $report_settings['table3_dimension']; ?></th>
                <th><?php echo $report_settings['table3_self']; ?></th>
                <th><?php echo $report_settings['table3_overall_average']; ?></th>
                <th><?php echo $report_settings['table3_difference']; ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($result['questionRanks'] as $qRank) { ?>
                <tr>
                    <td><?php echo $qRank['num']; ?> - <?php echo $qRank['name']; ?></td>
                    <td style="color:<?php echo $qRankDimensionsColors[$qRank['dimensionId']] ?>;"><?php echo $qRank['dimension']; ?></td>
                    <td class="tbg-gray"><?php echo $qRank['adminAvg']; ?></td>
                    <td><?php echo $qRank['overallAvg']; ?></td>
                    <td class="tbg-gray"><?php echo $qRank['difference']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>