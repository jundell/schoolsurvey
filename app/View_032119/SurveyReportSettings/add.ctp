<div class="surveyReportSettings form">
<?php echo $this->Form->create('SurveyReportSetting'); ?>
	<fieldset>
		<legend><?php echo __('Add Survey Report Setting'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('slug');
//                echo $this->Wysiwyg->textarea('value');
		echo $this->Form->input('value', array('type'=>'textarea'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Survey Report Settings'), array('action' => 'index')); ?></li>
	</ul>
</div>
