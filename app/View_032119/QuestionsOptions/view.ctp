<div class="questionsOptions view">
<h2><?php echo __('Questions Option'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($questionsOption['QuestionsOption']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($questionsOption['Question']['name'], array('controller' => 'questions', 'action' => 'view', $questionsOption['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($questionsOption['QuestionsOption']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($questionsOption['QuestionsOption']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Score'); ?></dt>
		<dd>
			<?php echo h($questionsOption['QuestionsOption']['score']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($questionsOption['QuestionsOption']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($questionsOption['QuestionsOption']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Deleted'); ?></dt>
		<dd>
			<?php echo h($questionsOption['QuestionsOption']['is_deleted']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Questions Option'), array('action' => 'edit', $questionsOption['QuestionsOption']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Questions Option'), array('action' => 'delete', $questionsOption['QuestionsOption']['id']), null, __('Are you sure you want to delete # %s?', $questionsOption['QuestionsOption']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions Options'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Questions Option'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
