<div class="standardQuestionOptions view">
<h2><?php echo __('Standard Question Option'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($standardQuestionOption['StandardQuestionOption']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($standardQuestionOption['StandardQuestionOption']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($standardQuestionOption['Question']['name'], array('controller' => 'questions', 'action' => 'view', $standardQuestionOption['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Score'); ?></dt>
		<dd>
			<?php echo h($standardQuestionOption['StandardQuestionOption']['score']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Deleted'); ?></dt>
		<dd>
			<?php echo h($standardQuestionOption['StandardQuestionOption']['is_deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($standardQuestionOption['StandardQuestionOption']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($standardQuestionOption['StandardQuestionOption']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Standard Question Option'), array('action' => 'edit', $standardQuestionOption['StandardQuestionOption']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Standard Question Option'), array('action' => 'delete', $standardQuestionOption['StandardQuestionOption']['id']), null, __('Are you sure you want to delete # %s?', $standardQuestionOption['StandardQuestionOption']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Standard Question Options'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Standard Question Option'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
