<?php echo $this->Html->script(array('jquery.numeric.min')); ?>
<?php if(sizeof($themes)){ ?>
<div class="questions form">
<?php echo $this->Form->create('Question'); ?>
    <input name="add_another_question" class="add_another_question" value="0" type="hidden">
    <div class="question_fieldsets">
        <legend><?php echo __('Add Question'); ?></legend>
        <?php echo $this->Form->input('survey_id',array('type'=>'hidden', 'value'=>$survey_id)); ?>
        <fieldset>
        <div class="row">
        <div class="col-xs-12 col-sm-3">

        <?php
            echo $this->Form->input('name', array('label'=>'Question','div'=>array('class'=>'form-group input text'),'class'=>'form-control','maxlength'=>5000));
                        echo $this->Form->input('survey_theme_id', array(
                            'options' => $themes,
                                                'class'=>'form-control',
                                                'div'=>array('class'=>'form-group input text'),
                                                'label' => 'Theme'
                        ));
            echo $this->Form->input('type', array(
                            'options' => $types,
                            'div'=>array('class'=>'form-group input select'),
                            'class'=>'form-control'
                        ));
                        
        ?>
                <div class="input checkbox " id="non-calculating">
                    <?php 
                        $is_calculating = true;
                        if(isset($this->request->data['Question']['is_calculating'])){
                            $is_calculating = $this->request->data['Question']['is_calculating']?true:false;
                        }
                    ?>
                    <?php echo $this->Form->input('is_calculating',array('type'=>'checkbox','div'=>false, 'label'=>'Calculating', 'checked'=>$is_calculating)); ?>
                </div>

                </div>
                </div>
        </fieldset>
        <div class="add_more_fieldsets" data-fields="option,score">
                <?php if($this->request->is('post')){ ?>
                <?php
                    $options = $this->request->data['option'];
                    $scores = $this->request->data['score'];
                ?>
            <?php if(isset($options)&& sizeof($options)){ ?>
                <?php $size = sizeof($options); ?>
                <?php $cnt = 1; ?>
                <?php foreach($options as $k=>$o){ ?>
                    <fieldset class="add_more form-group">
                    <?php
                            echo $this->Form->input('option', array('label'=>'Option','class'=>'form-control','type'=>'text','name'=>'option['.$k.']', 'value'=>$o));
                            echo $this->Form->input('score', array('label'=>'Score','name'=>'score['.$k.']','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>$scores[$k]));
                    ?>
                         <?php if($cnt==$size){ ?>
                            <a href="#" class="btn btn-default add_another" data->Add another option</a>
                        <?php } ?>
                        <?php if($cnt>2){ ?>
                            <a class="btn btn-default remove" href="#">X</a>
                        <?php } ?>
                        <?php $cnt++; ?>
                    </fieldset>
                       
                <?php } ?>
            <?php }}else{ ?>
            <fieldset class="add_more form-group">
            <?php
                    echo $this->Form->input('option', array('label'=>'Option','class'=>'form-control','name'=>'option[0]'));
                    echo $this->Form->input('score', array('label'=>'Score','name'=>'score[0]','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>'1'));
            ?>
            </fieldset>
            <fieldset class="add_more form-group">
            <?php
                    echo $this->Form->input('option', array('label'=>'Option','class'=>'form-control','name'=>'option[1]'));
                    echo $this->Form->input('score', array('label'=>'Score','name'=>'score[1]','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>'2'));
            ?>
                <a href="#" class="btn btn-default add_another" data->Add another option</a>
            </fieldset>
            <?php } ?>
        </div>
    </div>
    <a href="#" class="btn btn-default another_question" id="save-and-add">Save and add another question</a><br /><br />
<?php echo $this->Form->end(__(array('label' => 'Finish', 'class' => 'btn btn-default'))); ?>
</div>
<div class="related"> 
        <br />
        <?php if(!sizeof($questions)){ ?>
            <p><br/>No questions added.</p>
        <?php } ?>
    <?php if ($questions): ?>
    <p><?php echo __('Questions'); ?></p>
    <table class="table table-hover" cellpadding="0" cellspacing="0" >
    
        <tr>
            <th><?php echo __('#'); ?></th>
            <th><?php echo __('Name'); ?></th>

            <th class="actions"><?php echo __('Actions'); ?></th>

        </tr>
    <?php
        $cnt = 1;
        foreach ($questions as $q): ?>
        <tr>
            <td><?php echo $cnt; ?></td>

            <td><?php echo $q['Question']['name']; ?></td>

            <td class="actions">
                                <?php if(sizeof($q['SurveyAnswer'])){ ?>
                                <?php echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $q['Question']['id'], $survey_id), null, __('Someone has already answered this question. Are you sure you want to edit this question?', $q['Question']['id'])); ?>
                                <?php }else{?>
                                <?php echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $q['Question']['id'], $survey_id)); ?>
                                <?php }?>
                                <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions', 'action' => 'delete', $q['Question']['id'], $survey_id), null, __('Are you sure you want to delete # %s?', $q['Question']['id'])); ?>
            </td>
        </tr>
        <?php $cnt++; ?>
    <?php endforeach; ?>
    </table>
<?php endif; ?>

</div>

<script type="text/javascript">
    $(document).ready(function(){
        if($("input#QuestionIsCalculating").is(':checked')){
            $('div[data-fields="option,score"] div.input.number').show();
        }else{
            $('div[data-fields="option,score"] div.input.number').hide();
        }
        if($('select[name="data[Question][type]"]').val()==="open ended"){
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }
        
        $(".option-score").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 49 || e.which > 57)) {
               //display error message
               $("#errmsg").html("Positive numbers only").show().fadeOut("slow");
                      return false;
           }
          });
    });
    $('select[name="data[Question][type]"]').change(function() {
        if ($(this).val() === "open ended") {
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }
    })
    $('input#QuestionIncludeNoResponse').click(function() {
        $("#no-response-option").toggle(this.checked);
    })
    $('input#QuestionIsCalculating').click(function() {
        if($(this).is(':checked')){
            $('div[data-fields="option,score"] div.input.number').show();
        }else{
            $('div[data-fields="option,score"] div.input.number').hide();
        }
    });
</script>
<?php }else{ ?>
    <?php echo $this->Html->link(__('Add themes before adding questions'), array('action' => 'add_theme', $survey_id), array('type'=>'button','class'=>'btn btn-success', 'style'=>"margin-right:10px;")); ?>
<?php } ?>

    
