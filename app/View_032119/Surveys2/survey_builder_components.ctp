<legend><?php echo __('Survey Builder Components'); ?></legend>
<div class="survey-builder-buttons">
    <?php echo $this->Html->link(__('Standard Groups'), array('controller'=>'standard_groups'), array('type'=>'button','class'=>'btn btn-success')); ?>
    <?php echo $this->Html->link(__('Standard Dimensions'), array('controller'=>'standard_themes', 'action'=>'/'), array('type'=>'button','class'=>'btn btn-success')); ?>
    <?php echo $this->Html->link(__('Standard Descriptors-Questions'), array('controller'=>'standard_questions'), array('type'=>'button','class'=>'btn btn-success')); ?>
    <?php echo $this->Html->link(__('Standard Contents'), array('controller'=>'standard_contents'), array('type'=>'button','class'=>'btn btn-success')); ?>
</div>