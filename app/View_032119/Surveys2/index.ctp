<!--<pre>
    <?php var_dump($surveys); ?>
</pre>-->

<?php

if(!$surveys){
?>
<h3> Welcome to Larry Lobert Survey Dashboard</h3>
<a href="<?php echo $this->webroot; ?>surveys/add" class="btn btn-default">Start your first survey</a>
<a href="<?php echo $this->webroot; ?>surveys/upload_survey" class="btn btn-default">Upload your first Survey</a>
<?php } else { ?>
<div class="surveys index">
    <a href="<?php echo $this->webroot; ?>surveys/add" class="btn btn-default">Survey Builder</a>
    <a href="<?php echo $this->webroot; ?>surveys/upload_survey" class="btn btn-default">Upload Survey</a>
    <?php if($profile['User']['role_id']==1){ ?>
    <a href="<?php echo $this->webroot; ?>survey_builder_components" class="btn btn-default">Survey Builder Components</a>
    <a href="<?php echo $this->webroot; ?>users" class="btn btn-default">Subscribers</a>
    
    <a href="<?php echo $this->webroot; ?>settings" class="btn btn-default btn-gen-settings">General Settings</a>
    <a href="<?php echo $this->webroot; ?>settings/survey_header" class="btn btn-default btn-gen-settings survey-header-link">Default Survey Header</a>
    
    <?php } ?>
    <br />
    <br />
    <table class="table table-hover" cellpadding="0" cellspacing="0" >
        <tr>
            <th class="<?php echo $profile['User']['role_id']==1?'col-xs-3':'col-xs-4'; ?>"><?php echo $this->Paginator->sort('name'); ?></th>
            <?php if($profile['User']['role_id']==1){ ?>
            <th class="col-xs-1"><?php echo $this->Paginator->sort('User.first_name', 'Subscriber'); ?></th>
            <?php }?>
            <th class="actions col-xs-5"><?php echo __('Actions'); ?></th>
        </tr>
	<?php foreach ($surveys as $survey): ?>
        <tr>
            <td><?php echo h($survey['Survey']['name']); ?>&nbsp;</td>
            <?php if($profile['User']['role_id']==1){ ?>
            <td><?php echo h($survey['User']['first_name'].' '.$survey['User']['last_name']); ?>&nbsp;</td>
            <?php }?>

            <td class="actions survey-index">
                <?php $alertMessage = 'You have to add '; ?>
                <?php $missing = !sizeof($survey['Group']) || !sizeof($survey['SurveyTheme']) || !sizeof($survey['Question']); ?>
                <?php $missingRespondents = !$survey['Survey']['open'] && !sizeof($survey['RespondentPasscode']); ?>
                <?php // echo 'test = '.$missing; ?>
                
                <?php if($missing){ ?>
                    <?php if(!sizeof($survey['Group'])){ // no groups
                            $alertMessage = $alertMessage.'Groups';
                    }?>

                    <?php if(!sizeof($survey['SurveyTheme'])){ // no dimensions
                            if(!sizeof($survey['Group']))
                                $alertMessage = $alertMessage.', ';
                            $alertMessage = $alertMessage.'Dimensions and Descriptors-Questions';
                    }?>
                    <?php
                    
                        if(sizeof($survey['SurveyTheme']) && !sizeof($survey['Question'])){ // no questions
                            if(!sizeof($survey['Group']))
                                $alertMessage = $alertMessage.' and ';
                            $alertMessage = $alertMessage.'Descriptors-Questions';
                        }
                    ?>
                    <?php $alertMessage = $alertMessage.' to proceed.'; ?>
                <?php }else{
                    $alertMessage = $alertMessage.'Group Respondents to proceed.';
                } ?>
                
                
                
            <?php echo $this->Html->link(__('Groups'), array('controller'=>'groups', 'action' => 'add', $survey['Survey']['id']), array('type'=>'button','class'=>'btn btn-primary')); ?>
                    <?php echo $this->Html->link(__('Dimensions'), array('controller'=>'survey_themes', 'action' => 'add', $survey['Survey']['id']), array('type'=>'button','class'=>'btn btn-primary')); ?>
			
                        <?php if(sizeof($survey['SurveyTheme'])){ ?>
                            <?php echo $this->Html->link(__('Descriptors-Questions'), array('controller'=>'questions', 'action' => 'add', $survey['Survey']['id']), array('type'=>'button','class'=>'btn btn-primary')); ?>
                        <?php }else{ ?>
                            <?php echo $this->Html->link(__('Descriptors-Questions'), '#', array('type'=>'button','class'=>'btn btn-primary', 'onclick'=>'alert("You have to add Dimensions to proceed.");')); ?>
                        <?php } ?>
                            
                        <?php echo $this->Html->link(__('Report'), array('action' => 'report', $survey['Survey']['id']), array('type'=>'button','class'=>'btn btn-primary')); ?>
                        <?php if(!$missing && !$missingRespondents){ ?>
			<?php echo $this->Html->link(__('View Survey'), array('action' => 'verify', $survey['Survey']['slug']), array('target' => '_new','type'=>'button','class'=>'btn btn-primary')); ?>
                        <?php }else{ ?>
                            <?php echo $this->Html->link(__('View Survey'), '#', array('target' => '_new','type'=>'button','class'=>'btn btn-primary','onclick' => 'alert("'.$alertMessage.'");')); ?>
                        <?php } ?>
                
                            <?php if($survey['Survey']['status']=='0'){ ?>
                            <?php if(!$missing && !$missingRespondents){ ?>
                                <?php echo $this->Html->link(__('Start'), array('action' => 'start_stop_survey', $survey['Survey']['id'],'1'), array('type'=>'button','class'=>'btn btn-success','confirm' => 'Are you sure you wish to start this survey?')); ?>
                            <?php }else{ ?>
                                <?php echo $this->Html->link(__('Start'), '#', array('type'=>'button','class'=>'btn btn-success','onclick' => 'alert("'.$alertMessage.'");')); ?>
                            <?php } ?>
                        <?php }else{ ?>
                            <?php echo $this->Html->link(__('Stop'), array('action' => 'start_stop_survey',$survey['Survey']['id'],'0'), array('type'=>'button','class'=>'btn btn-danger','confirm' => 'Are you sure you wish to stop this survey?')); ?>
                        <?php } ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" 
                            data-toggle="dropdown">
                        Settings <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" style="text-align: left;">
                        <li><a href="<?php echo $this->webroot; ?>surveys/view/<?php echo $survey['Survey']['id']; ?>/1">View</a></li>
                        <?php if(!$survey['Survey']['status']){ ?>
                            <li><a href="<?php echo $this->webroot; ?>surveys/edit/<?php echo $survey['Survey']['id']; ?>">Edit</a></li>
                        <?php } ?>
                        <li><a href="#" data-toggle="modal" data-target="#duplicate-survey-<?php echo $survey['Survey']['id'];?>">Duplicate</a></li>
                        <?php if($profile['User']['role_id']==1){ ?>
                        <li><a href="<?php echo $this->webroot; ?>survey_settings/download_data/<?php echo $survey['Survey']['id']; ?>">Download Data</a></li>
                            <?php if(!$survey['Survey']['status']){ ?>
                            <li><a href="<?php echo $this->webroot; ?>survey_settings/footer/<?php echo $survey['Survey']['id']; ?>">Footer</a></li>
                            <?php } ?>
                        <?php } ?>
                        <?php if(!$survey['Survey']['status']){ ?>
                            <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $survey['Survey']['id']), null, __('Are you sure you want to delete this survey?'), array('type'=>'button','class'=>'btn btn-delete')); ?></li>
                        <?php } ?>
                    </ul>
                    <div class="modal fade" id="duplicate-survey-<?php echo $survey['Survey']['id'];?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $survey['Survey']['name']; ?>">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Duplicate Survey</h4>
                                </div>
                                <div class="modal-body">
                                    
                                    <strong>Survey to duplicate:</strong><br />
                                    <h4><?php echo $survey['Survey']['name']; ?></h4>
                                    <br />
                                    <?php echo $this->Form->create('Survey', array('action' => 'duplicate/'.$survey['Survey']['id'])); ?>
                                            <fieldset>
                                                <?php
                                                        echo $this->Form->input('name', array('label'=>'Survey Name:','div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
                                                ?>
                                            </fieldset>
                                        <?php echo $this->Form->end(__(array('label' => 'Duplicate', 'class' => 'btn btn-success'))); ?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
<?php endforeach; ?>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
                echo $this->Paginator->first('<< first');
		echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
                echo $this->Paginator->last('last >>');
	?>
    </div>
</div>
<?php } ?>