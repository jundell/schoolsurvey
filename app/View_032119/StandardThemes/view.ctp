<div class="standardThemes view">
<h2><?php echo __('Standard Theme'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($standardTheme['StandardTheme']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($standardTheme['StandardTheme']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($standardTheme['StandardTheme']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Icon'); ?></dt>
		<dd>
			<?php echo h($standardTheme['StandardTheme']['icon']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Color'); ?></dt>
		<dd>
			<?php echo h($standardTheme['StandardTheme']['color']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Deleted'); ?></dt>
		<dd>
			<?php echo h($standardTheme['StandardTheme']['is_deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($standardTheme['StandardTheme']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($standardTheme['StandardTheme']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Standard Theme'), array('action' => 'edit', $standardTheme['StandardTheme']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Standard Theme'), array('action' => 'delete', $standardTheme['StandardTheme']['id']), null, __('Are you sure you want to delete # %s?', $standardTheme['StandardTheme']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Standard Themes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Standard Theme'), array('action' => 'add')); ?> </li>
	</ul>
</div>
