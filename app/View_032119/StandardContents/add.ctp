<div class="standardContents form">
<?php echo $this->Form->create('StandardContent'); ?>
	<fieldset>
		<legend><?php echo __('Add Standard Content'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('slug');
		echo $this->Form->input('value');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Standard Contents'), array('action' => 'index')); ?></li>
	</ul>
</div>
