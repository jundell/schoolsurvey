<div class="standardContents form">
<?php echo $this->Form->create('StandardContent'); ?>
    <fieldset>
        <legend><?php echo __('Edit Standard Content'); ?></legend>
        <div class="row">
            <div class="col-xs-12 col-sm-4">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
		echo $this->Form->input('value', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
	?>
            </div>
        </div>
    </fieldset>
<?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-default'))); ?>
</div>
