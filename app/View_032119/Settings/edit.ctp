<div class="settings form">
<?php echo $this->Form->create('Setting'); ?>
        <legend><?php echo __('Edit Setting'); ?></legend>
	<fieldset>
            <div class="row">
                <div class="col-xs-12 col-sm-3">

                <?php
                        echo $this->Form->input('id');
                        echo $this->Form->input('name', array('readonly','div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
                        echo $this->Form->input('value', array('type'=>'textarea', 'div'=>array('class'=>'form-group input textarea'),'class'=>'form-control'));
                ?>
                </div>
            </div>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
