<div class="surveyThemes view">
<h2><?php echo __('Survey Theme'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($surveyTheme['SurveyTheme']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($surveyTheme['SurveyTheme']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Survey Id'); ?></dt>
		<dd>
			<?php echo h($surveyTheme['SurveyTheme']['survey_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($surveyTheme['SurveyTheme']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($surveyTheme['SurveyTheme']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Deleted'); ?></dt>
		<dd>
			<?php echo h($surveyTheme['SurveyTheme']['is_deleted']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Survey Theme'), array('action' => 'edit', $surveyTheme['SurveyTheme']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Survey Theme'), array('action' => 'delete', $surveyTheme['SurveyTheme']['id']), null, __('Are you sure you want to delete # %s?', $surveyTheme['SurveyTheme']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Survey Themes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Survey Theme'), array('action' => 'add')); ?> </li>
	</ul>
</div>
