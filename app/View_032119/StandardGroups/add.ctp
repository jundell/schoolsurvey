<div class="standardGroups form">
<?php echo $this->Form->create('StandardGroup'); ?>
    <fieldset>
        <legend><?php echo __('Add Standard Group'); ?></legend>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
	<?php
                echo $this->Form->input('name', array('disabled'=>sizeof($standardGroups) < Configure::read('site_config.max_groups')?false:'disabled', 'div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
            
	?>
            </div>
        </div>
    </fieldset>
    <?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-default','disabled'=>sizeof($standardGroups) < Configure::read('site_config.max_groups')?false:'disabled'))); ?>
    
    <?php if(sizeof($standardGroups) >= Configure::read('site_config.max_groups')){ ?>
        <br />
        <p class="group-exceed-message"><?php echo Configure::read('site_config.max_groups_message'); ?></p>
    <?php } ?>
</div>
<div class="related"> <br /><br />
        <?php if(!sizeof($standardGroups)){ ?>
            <p><br/>No standard groups added.</p>
        <?php }else{ ?>
	<p><?php echo __('Standard Groups'); ?></p>
	<table class="table table-hover" cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($standardGroups as $standardGroup): ?>
	<tr>
		<td><?php echo h($standardGroup['StandardGroup']['name']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $standardGroup['StandardGroup']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $standardGroup['StandardGroup']['id']), null, __('Are you sure you want to delete # %s?', $standardGroup['StandardGroup']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
        <?php }?>
</div>

<?php echo $this->Html->link(__('Back to survey builder menu'), array('controller'=>'survey_builder_components'), array('type'=>'button','class'=>'btn btn-default', 'style'=>'margin-top: 30px;')); ?>