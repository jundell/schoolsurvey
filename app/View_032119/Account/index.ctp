<?php /** user profile **/ ?>
<section id="user_information" class="well">
    <h2>Account Information</h2>
    <ul class="nav nav-tabs" role="tablist" id="myTab">
        <li role="presentation" <?php echo $activeTab=='edit-profile'?' class="active"':''; ?>><a href="#edit-profile" aria-controls="home" role="tab" data-toggle="tab">Edit Profile</a></li>
        <li role="presentation" <?php echo $activeTab=='change-password'?' class="active"':''; ?>><a href="#change-password" aria-controls="home" role="tab" data-toggle="tab">Change Password</a></li>
      <?php if($profile['User']['role_id']==1){ ?>
        <li role="presentation"><?php echo $this->Html->link(__('Survey Header'), array('controller'=>'subscriber_settings','action' => 'survey_header',$profile['User']['user_id'])); ?></li>
      <?php } ?>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane<?php echo $activeTab=='edit-profile'?' active':''; ?>" id="edit-profile">
            <?php echo $this->Form->create('User');?>
            <input type="hidden" name="change_password" value="0" />
            <fieldset>
                <div class="row">
                    <div class="col-xs-12 col-sm-3">
                            <?php
                                echo $this->Form->input('user_id');
                                echo $this->Form->input('first_name', array('label' => 'First Name','div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
                                echo $this->Form->input('last_name', array('label' => 'Last Name','div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
                                echo $this->Form->input('email_address', array('label' => 'Email Address','div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
                                echo $this->Form->input('email_address_confirm', array('type' => 'hidden'));
                            ?>
                    </div>
                </div>
            </fieldset>

            <hr class="main" />
            <div class="row-fluid">
                <h3>Contact Details</h3>
                <fieldset>
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                    <?php
                        echo $this->Form->input('phone', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control','error' => array( 'attributes' => array( 'class' => 'label label-important' ) )));
                        echo $this->Form->input('address', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control','error' => array( 'attributes' => array( 'class' => 'label label-important' ) )));
                        echo $this->Form->input('city', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control','error' => array( 'attributes' => array( 'class' => 'label label-important' ) )));
                        echo $this->Form->input('state', array('div'=>array('class'=>'form-group input select'),'class'=>'form-control','options' => $this->Geography->stateList(array('outside' => 'Outside US')), 'empty' => 'Select State', 'error' => array( 'attributes' => array( 'class' => 'label label-important' ) )));
                        echo $this->Form->input('zip', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control','error' => array( 'attributes' => array( 'class' => 'label label-important' ) )));
                        echo $this->Form->input('country', array('div'=>array('class'=>'form-group input select'),'class'=>'form-control','options' => $this->Geography->countryList(), 'empty' => 'Select Country', 'default' => 'US', 'error' => array( 'attributes' => array( 'class' => 'label label-important' ) )));
                    ?>
                        </div>
                    </div>
                </fieldset>

            </div>

        <?php echo $this->Form->end(array('class' => 'btn btn-primary', 'label' => 'Update My Account'));?>
        </div>
        <div role="tabpanel" class="tab-pane<?php echo $activeTab=='change-password'?' active':''; ?>" id="change-password">
                <?php echo $this->Form->create('User', array('url' => '/account/index', 'autocomplete' => 'off'));?>
                <fieldset>
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <input type="hidden" name="change_password" value="1" />
                            <?php
                                echo $this->Form->input('current_password',array('div'=>array('class'=>'form-group input password'),'class'=>'form-control','label' => 'Current Password','type' => 'password'));
                                echo $this->Form->input('password', array('div'=>array('class'=>'form-group input password'),'class'=>'form-control','value' => '', 'label' => 'New Password','type' => 'password'));
                                echo $this->Form->input('password_confirm',array('div'=>array('class'=>'form-group input password'),'class'=>'form-control','label' => 'Confirm New Password','type' => 'password'));
                            ?>
                        </div>
                    </div>
                </fieldset>
                <?php echo $this->Form->end(array('class'=>'btn btn-primary', 'label' => 'Change Password')); ?>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $('i[rel=tooltip]').tooltip();

//        if (window.location.href.indexOf('change-password') >= 0) {
//            $('#myTab a:last').tab('show');
//        } else {
//            $('#myTab a:first').tab('show');
//        }
    });
</script>
