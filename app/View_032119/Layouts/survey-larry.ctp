<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Larry Lobert Survey</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<script type="text/javascript">
		 $(document).ready(function(){
		  $('ul#topNav li:nth-child(2n)').addClass('alt');
		  $('.pageProductsPostWrap:first').addClass('background0');
		  $('.pageSupportBoxCol:last-child').addClass('background0');
		  $('div.pageSupportCol1:nth-child(2)').addClass('pageSupportCol2').removeClass('pageSupportCol1');
		  $('#pageCompanyRightContact ul.post-meta li:last-child').addClass('display0');
		 });
		</script>


                	<?php

                            echo $this->Html->css(array('style-survey'));

                            echo $this->fetch('css');

                            echo $this->Html->script(array('jquery-1.10.2', 'bootstrap.min','survey'));

                            echo $this->fetch('script');
                    ?>


	</head>
	<body>
		<div id="headerWrap">
			<div class="container">
				<div id="logomarkWrap">
					<div id="logomark">
					</div>
				</div><!-- end of logomarkWrap -->
				<div id="header">
					<div id="logo">
						<a href="index.html"><img src="   <?php echo $this->webroot; ?>images/logo.png" /></a>
					</div>
					<ul id="topNav">
                                            <li><a href="http://mydevwebsites.com/LarryLobert/" class="active" target="_blank">Home</a></li>
						<li><a href="http://mydevwebsites.com/LarryLobert/experience/" target="_blank">Experience</a></li>
						<li><a href="http://mydevwebsites.com/LarryLobert/testimonials/" target="_blank">Testimonials</a></li>
						<li><a href="http://mydevwebsites.com/LarryLobert/affiliation/#" target="_blank">Services</a></li>
						<li><a href="http://mydevwebsites.com/LarryLobert/affiliation/" target="_blank">Affiliation</a></li>
						<li><a href="http://mydevwebsites.com/LarryLobert/resources/" target="_blank">Resources</a></li>
					</ul>
				</div>
				<div class="clear"></div>
				
                                <div id="contentPage" class="generic" style="padding: 50px 0 50px 0; height: 100%">

                                                <?php
                                                    echo $this->Session->flash('auth');
                                                    echo $this->Session->flash();
                                                ?>
                                                    <?php echo $content_for_layout ?>
				</div>
					   
				<div class="clear"></div>
                         </div>
		</div><!-- end of headerWrap -->
	
		<div id="footer">
			<div class="container">
				<p><?php echo $footer; ?></p>
			</div>
		</div>
                <?php echo $this->element('footer'); //footer element ?>
	</body>
</html>


