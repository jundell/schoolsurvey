<div class="answerOptions index">
	<h2><?php echo __('Answer Options'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('option'); ?></th>
			<th><?php echo $this->Paginator->sort('value'); ?></th>
			<th><?php echo $this->Paginator->sort('question_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($answerOptions as $answerOption): ?>
	<tr>
		<td><?php echo h($answerOption['AnswerOption']['id']); ?>&nbsp;</td>
		<td><?php echo h($answerOption['AnswerOption']['option']); ?>&nbsp;</td>
		<td><?php echo h($answerOption['AnswerOption']['value']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($answerOption['Question']['id'], array('controller' => 'questions', 'action' => 'view', $answerOption['Question']['id'])); ?>
		</td>
		<td><?php echo h($answerOption['AnswerOption']['created']); ?>&nbsp;</td>
		<td><?php echo h($answerOption['AnswerOption']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $answerOption['AnswerOption']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $answerOption['AnswerOption']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $answerOption['AnswerOption']['id']), null, __('Are you sure you want to delete # %s?', $answerOption['AnswerOption']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Answer Option'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions Answers'), array('controller' => 'questions_answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Questions Answer'), array('controller' => 'questions_answers', 'action' => 'add')); ?> </li>
	</ul>
</div>
