<div class="answerOptions view">
<h2><?php  echo __('Answer Option'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($answerOption['AnswerOption']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Option'); ?></dt>
		<dd>
			<?php echo h($answerOption['AnswerOption']['option']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($answerOption['AnswerOption']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($answerOption['Question']['id'], array('controller' => 'questions', 'action' => 'view', $answerOption['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($answerOption['AnswerOption']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($answerOption['AnswerOption']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Answer Option'), array('action' => 'edit', $answerOption['AnswerOption']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Answer Option'), array('action' => 'delete', $answerOption['AnswerOption']['id']), null, __('Are you sure you want to delete # %s?', $answerOption['AnswerOption']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Answer Options'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer Option'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions Answers'), array('controller' => 'questions_answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Questions Answer'), array('controller' => 'questions_answers', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Answers'); ?></h3>
	<?php if (!empty($answerOption['Answer'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Question Id'); ?></th>
		<th><?php echo __('Answer Option Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($answerOption['Answer'] as $answer): ?>
		<tr>
			<td><?php echo $answer['id']; ?></td>
			<td><?php echo $answer['question_id']; ?></td>
			<td><?php echo $answer['answer_option_id']; ?></td>
			<td><?php echo $answer['created']; ?></td>
			<td><?php echo $answer['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'answers', 'action' => 'view', $answer['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'answers', 'action' => 'edit', $answer['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'answers', 'action' => 'delete', $answer['id']), null, __('Are you sure you want to delete # %s?', $answer['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Questions Answers'); ?></h3>
	<?php if (!empty($answerOption['QuestionsAnswer'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Question Id'); ?></th>
		<th><?php echo __('Answer Option Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($answerOption['QuestionsAnswer'] as $questionsAnswer): ?>
		<tr>
			<td><?php echo $questionsAnswer['id']; ?></td>
			<td><?php echo $questionsAnswer['user_id']; ?></td>
			<td><?php echo $questionsAnswer['question_id']; ?></td>
			<td><?php echo $questionsAnswer['answer_option_id']; ?></td>
			<td><?php echo $questionsAnswer['created']; ?></td>
			<td><?php echo $questionsAnswer['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'questions_answers', 'action' => 'view', $questionsAnswer['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'questions_answers', 'action' => 'edit', $questionsAnswer['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions_answers', 'action' => 'delete', $questionsAnswer['id']), null, __('Are you sure you want to delete # %s?', $questionsAnswer['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Questions Answer'), array('controller' => 'questions_answers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
