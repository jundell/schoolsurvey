<div class="surveys form">
<?php echo $this->Form->create('Survey'); ?>
    <legend><?php echo __('Add Survey'); ?></legend>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-4">
	<?php   
		echo $this->Form->input('name', array('label'=>'Name<span class="required"> *</span>','class'=>'form-control','div'=>array('class'=>'form-group input text')));
		echo $this->Form->input('description', array('type'=>'textarea','class'=>'form-control','div'=>array('class'=>'form-group input textarea'), 'default'=>$standard_contents['survey_description']));
		echo $this->Form->input('open', array(
                    'div'=>array('class'=>'form-group input'),
                    'separator'=> '</label></div><div class="radio"><label>',
                    'before' => '<div class="radio"><label>',
                    'after' => '</label></div>',
                    'type'=>'radio',
                    'value'=>isset($this->request->data['Survey']['open'])?$this->request->data['Survey']['open']:'1',
                    'options' => array(
                        '1'=>'Open System<img class="tooltip-img" src="'.$this->webroot.'images/questionmark.png"/ data-toggle="tooltip" data-placement="right" title="'.$standard_contents['open_system_description'].'" />',
                        '0'=>'Closed System<img class="tooltip-img" src="'.$this->webroot.'images/questionmark.png"/ data-toggle="tooltip" data-placement="right" title="'.$standard_contents['close_system_description'].'" />'
                        ),
					'legend' => false
                ));
                $displayStyle = !isset($this->request->data['Survey']['open'])||$this->request->data['Survey']['open']?'block':'none';

                echo $this->Form->input('password', array('required'=>false,'type' => 'text','class'=>'form-control','label'=>'Respondent Password<span class="required"> *</span>','div'=>array('class'=>'form-group input text res-password', 'style'=>'display: '.$displayStyle.';')));
                
                echo $this->Form->input('admin_password',array('label'=>'Admin Password<span class="required"> *</span>','class'=>'form-control','div'=>array('class'=>'form-group input text')));

                echo '<div id="email-verification-wrap" style="display: '.$displayStyle.';">';
                    echo '<p>Email Verification</p>';
                    echo $this->Form->input('email_verification_on', array(
                        'div'=>array('class'=>'form-group input'),
                        'separator'=> '</label></div><div class="radio"><label>',
                        'before' => '<div class="radio"><label>',
                        'after' => '</label></div>',
                        'type'=>'radio',
                        'value'=>isset($this->request->data['Survey']['email_verification_on'])?$this->request->data['Survey']['email_verification_on']:'1',
                        'options' => array(
                            '1'=>'on',
                            '0'=>'off'
                            ),
    					'legend' => false
                    ));
                echo '</div>';

                echo $this->Form->input('welcome_message', array('type'=>'textarea','class'=>'form-control', 'div'=>array('class'=>'form-group input textarea'),'value'=>$this->request->data['Survey']['welcome_message']?$this->request->data['Survey']['welcome_message']:$standard_contents['survey_welcome_message']));
                echo $this->Form->input('group_message', array('type'=>'textarea','class'=>'form-control', 'div'=>array('class'=>'form-group input textarea'),'value'=>$this->request->data['Survey']['group_message']?$this->request->data['Survey']['group_message']:$standard_contents['survey_group_message']));
                echo $this->Form->input('closing_message', array('type'=>'textarea','class'=>'form-control', 'div'=>array('class'=>'form-group input textarea'),'value'=>$this->request->data['Survey']['closing_message']?$this->request->data['Survey']['closing_message']:$standard_contents['survey_closing_message']));
	?>
        </div>
    </div>
    <?php echo $this->Form->end(array('label' => 'Save', 'class' => 'btn btn-default')); ?>
</div>

<script type="text/javascript">
 
 $(document).ready(function(){
     $('input[name="data[Survey][open]"]').change(function(){
         hide_show_fields($(this).val());
     });
    $('[data-toggle="tooltip"]').tooltip();
 });

 function hide_show_fields(system){
    if(system == '1'){
        $('div.res-password').show();
        $('#email-verification-wrap').show();
    }else{
        $('div.res-password').hide();
        $('#email-verification-wrap').hide();
    }
 }
 
</script>