<div class="users form">
<?php echo $this->Form->create('User');?>
        <legend><?php echo __('Edit User'); ?></legend>
	<fieldset>
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group input text">
                        <?php echo $this->Form->input('first_name',array('div'=>false,'class'=>'form-control')); ?>
                        <?php if(isset($validationErrors['first_name'])){ ?>
                            <div class="error-message alert"><?php echo $validationErrors['first_name'][0]; ?></div>
                        <?php } ?>
                    </div>
                    <div class="form-group input text">
                        <?php echo $this->Form->input('last_name',array('div'=>false,'class'=>'form-control')); ?>
                        <?php if(isset($validationErrors['last_name'])){ ?>
                            <div class="error-message alert"><?php echo $validationErrors['last_name'][0]; ?></div>
                        <?php } ?>
                    </div>
                    <div class="form-group input text">    
                        <?php echo $this->Form->input('email_address',array('div'=>false,'class'=>'form-control')); ?>
                        <?php if(isset($validationErrors['email_address'])){ ?>
                            <div class="error-message alert"><?php echo $validationErrors['email_address'][0]; ?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
	</fieldset>
<?php   echo $this->Form->submit( __('Update'), array('class' => 'btn btn-primary')) ?>
<?php   echo $this->Form->end();?>
</div>
