<?php echo $this->Form->create('User');?>
<legend><?php echo __('Change Password'); ?></legend>
<fieldset>
    <div class="row">
        <div class="col-xs-12 col-sm-3">
            <?php
            echo $this->Form->input('password', array('value' => '', 'label' => 'New Password','type' => 'password','div'=>array('class'=>'form-group input password'),'class'=>'form-control'));
            echo $this->Form->input('password_confirm',array('label' => 'Confirm New Password','type' => 'password','div'=>array('class'=>'form-group input password'),'class'=>'form-control'));
            ?>
        </div>
    </div>
</fieldset>
<?php echo $this->Form->end(array('class'=>'btn btn-primary', 'label' => 'Change Password')); ?>