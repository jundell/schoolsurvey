<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $uses = array('Setting','User');
    public $components = array('Session',
        'Acl',
        'Auth' => array(
            'loginAction' => array('controller' => 'account', 'action' => 'login'),
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'email_address', 'password' => 'password'),
                    'scope' => array('User.is_active' => 1)
                )
            ),
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            ),
            'authError' => 'Invalid username and/or password entered.',
            'loginRedirect' => array('controller' => 'surveys', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'account', 'action' => 'login'),
        ),
        //'DebugKit.Toolbar' => array('panels' => array('history' => false)),
        'Access',
        'RequestHandler',
            //'PImage',
            //'Security'
    );
    public $helpers = array('Session', 'Form', 'Html', 'Number', 'Time', 'Geography');

    /** public function beforeFilter() {
      $this->Auth->allow();

      if(!$this->Access->check($this->params['controller'] . '/'. $this->params['action'])) {
      $this->Session->setFlash('Access denied.', 'default', array('class' => 'alert alert-error'));
      $this->redirect('/');
      }
      } * */
    public function beforeFilter() {
        //   if(0 === Configure::read('Auth.enabled')) {
        //       $this->Auth->allow();
        //   }
    }

    function beforeRender() {
        if ($this->Auth->user()) {
            //$this->loadModel('User'); 
            $this->set('profile', $this->User->read(null, $this->Auth->user('user_id')));
        }
    }

    protected function user_allowed($model, $id) {
        if ($this->Auth->login()) {
            $group = $this->Auth->user('role_id');
            if ($group == '1') {
                return true;
            } else {
                $this->loadModel($model);
                $data = $this->$model->find('first', array('conditions' => array($model . '.id' => $id, $model . '.user_id' => $this->Auth->user('user_id'))));
                if (sizeof($data)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function user_allowed_conditions($model, $id, $conditions) {
        if ($this->Auth->login()) {
            $group = $this->Auth->user('role_id');
            if ($group == '1') {
                return true;
            } else {
                $this->loadModel($model);
                $conditions[$model . '.id'] = $id;
                $data = $this->$model->find('first', array('conditions' => $conditions));
                if (sizeof($data)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function send_mail($emailData) {
        $Email = new CakeEmail();
        if (isset($emailData['attachments']) && sizeof($emailData['attachments'])) {
            $Email->attachments($emailData['attachments']);
        }
        $from = array('noreply@larrylobert.com' => 'Larry Lobert and Associates');
        if (isset($emailData['from'])) {
            $from = $emailData['from'];
        }
        $viewVars = $emailData['viewVars'];
        $viewVars['signature'] = $this->Setting->get_value('new_subscriber_email_signature');
        $sent = $Email->emailFormat('html')->template(isset($emailData['template']) ? $emailData['template'] : 'larry_email')
                ->to($emailData['to'] ? $emailData['to'] : 'larry@larrylobert.com') //temporary email
                //->to(array('noreply@larrylobert.com' => 'Larry Lobert and Associates'))
                ->from($from)
                ->subject($emailData['subject'])
                ->viewVars($viewVars)
                ->send();
        return $sent ? true : false;
    }

    protected function tokenTruncate($string, $width) {
        $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
        $parts_count = count($parts);

        $length = 0;
        $last_part = 0;
        for (; $last_part < $parts_count; ++$last_part) {
            $length += strlen($parts[$last_part]);
            if ($length > $width) {
                break;
            }
        }

        return implode(array_slice($parts, 0, $last_part));
    }

}
