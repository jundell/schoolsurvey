<?php

App::uses('AppController', 'Controller');

/**
 * RespondentPasscodes Controller
 *
 * @property RespondentPasscode $RespondentPasscode
 */
class RespondentPasscodesController extends AppController {

    public $uses = array('Survey', 'Group', 'RespondentPasscode');
    
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->RespondentPasscode->recursive = 0;
        $this->set('respondentPasscodes', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->RespondentPasscode->exists($id)) {
            throw new NotFoundException(__('Invalid respondent passcode'));
        }
        $options = array('conditions' => array('RespondentPasscode.' . $this->RespondentPasscode->primaryKey => $id));
        $this->set('respondentPasscode', $this->RespondentPasscode->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->RespondentPasscode->create();
            if ($this->RespondentPasscode->save($this->request->data)) {
                $this->Session->setFlash(__('The respondent passcode has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The respondent passcode could not be saved. Please, try again.'));
            }
        }
        $surveys = $this->RespondentPasscode->Survey->find('list');
        $groups = $this->RespondentPasscode->Group->find('list');
        $this->set(compact('surveys', 'groups'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->RespondentPasscode->exists($id)) {
            throw new NotFoundException(__('Invalid respondent'));
        }

        $surveyId = $this->RespondentPasscode->field('survey_id', array('id' => $id));
        $groupId = $this->RespondentPasscode->field('group_id', array('id' => $id));

        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey', $surveyId)) {
            throw new NotFoundException(__('Invalid respondent'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->RespondentPasscode->save($this->request->data)) {
                $this->Session->setFlash(__('The respondent has been saved.'), 'default', array('class' => 'alert alert-success'));
                //$this->redirect(array('controller' => 'groups', 'action' => 'respondents/' . $groupId));
            } else {
                $this->Session->setFlash(__('The respondent could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('RespondentPasscode.' . $this->RespondentPasscode->primaryKey => $id));
            $this->request->data = $this->RespondentPasscode->find('first', $options);
        }
        $surveys = $this->RespondentPasscode->Survey->find('list');
        $groups = $this->RespondentPasscode->Group->find('list');
        $this->set(compact('surveys', 'groups'));
        $this->set('surveyId', $surveyId);
        $this->set('groupId', $groupId);
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->RespondentPasscode->id = $id;
        if (!$this->RespondentPasscode->exists()) {
            throw new NotFoundException(__('Invalid respondent'));
        }
        
        $surveyId = $this->RespondentPasscode->field('survey_id', array('id' => $id));
        $groupId = $this->RespondentPasscode->field('group_id', array('id' => $id));
        
        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey', $surveyId)) {
            throw new NotFoundException(__('Invalid respondent'));
        }
        
        $this->request->onlyAllow('post', 'delete');
        if ($this->RespondentPasscode->delete()) {
            $this->Session->setFlash(__('Respondent deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('controller' => 'groups', 'action' => 'respondents/' . $groupId));
        }
        $this->Session->setFlash(__('Respondent was not deleted.'), 'default', array('class' => 'alert alert-danger'));
        $this->redirect(array('controller' => 'groups', 'action' => 'respondents/' . $groupId));
    }

}
