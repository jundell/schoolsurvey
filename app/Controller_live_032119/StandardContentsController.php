<?php

App::uses('AppController', 'Controller');

/**
 * StandardContents Controller
 *
 * @property StandardContent $StandardContent
 */
class StandardContentsController extends AppController {

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->StandardContent->recursive = 0;
        $this->set('standardContents', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->StandardContent->exists($id)) {
            throw new NotFoundException(__('Invalid standard content'));
        }
        $options = array('conditions' => array('StandardContent.' . $this->StandardContent->primaryKey => $id));
        $this->set('standardContent', $this->StandardContent->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->StandardContent->create();
            if ($this->StandardContent->save($this->request->data)) {
                $this->Session->setFlash(__('The standard content has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The standard content could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->StandardContent->exists($id)) {
            throw new NotFoundException(__('Invalid standard content'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->StandardContent->save($this->request->data)) {
                $this->Session->setFlash(__('The standard content has been saved'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The standard content could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('StandardContent.' . $this->StandardContent->primaryKey => $id));
            $this->request->data = $this->StandardContent->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->StandardContent->id = $id;
        if (!$this->StandardContent->exists()) {
            throw new NotFoundException(__('Invalid standard content'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->StandardContent->delete()) {
            $this->Session->setFlash(__('Standard content deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Standard content was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
