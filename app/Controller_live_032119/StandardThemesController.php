<?php

App::uses('AppController', 'Controller');

/**
 * StandardThemes Controller
 *
 * @property StandardTheme $StandardTheme
 */
class StandardThemesController extends AppController {
    
    public $paginate = array(
        'StandardTheme' => array(
            'limit' => 10,
            'conditions'=>array('StandardTheme.is_deleted'=>'0')
        )
    );

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->StandardTheme->recursive = 0;
        $this->set('standardThemes', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->StandardTheme->exists($id)) {
            throw new NotFoundException(__('Invalid standard dimension'));
        }
        $options = array('conditions' => array('StandardTheme.' . $this->StandardTheme->primaryKey => $id));
        $this->set('standardTheme', $this->StandardTheme->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->StandardTheme->create();
            if (substr($this->request->data['StandardTheme']['color'], 0, 1) != '#') {
                $this->request->data['StandardTheme']['color'] = '#' . $this->request->data['StandardTheme']['color'];
            }
            if ($this->StandardTheme->save($this->request->data)) {
                $this->Session->setFlash(__('The standard dimension has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('The standard dimension could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }

        $this->StandardTheme->recursive = 0;
        $this->set('standardThemes', $this->paginate());
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->StandardTheme->exists($id)) {
            throw new NotFoundException(__('Invalid standard dimension'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if (substr($this->request->data['StandardTheme']['color'], 0, 1) != '#') {
                $this->request->data['StandardTheme']['color'] = '#' . $this->request->data['StandardTheme']['color'];
            }
            if ($this->StandardTheme->save($this->request->data)) {
                $this->Session->setFlash(__('The standard dimension has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => '/'));
            } else {
                $this->Session->setFlash(__('The standard dimension could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('StandardTheme.' . $this->StandardTheme->primaryKey => $id));
            $this->request->data = $this->StandardTheme->find('first', $options);
        }
        $iconPath = $this->StandardTheme->field('icon', array('id' => $id));
        $this->set('icon', $iconPath);
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->StandardTheme->id = $id;
        if (!$this->StandardTheme->exists()) {
            throw new NotFoundException(__('Invalid standard dimension'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->StandardTheme->delete($id,true)) {
            $this->Session->setFlash(__('Standard dimension deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => '/'));
        }
        $this->Session->setFlash(__('Standard dimension was not deleted.'), 'default', array('class' => 'alert alert-danger'));
        $this->redirect(array('action' => '/'));
    }

//    /**
//     * choose method
//     *
//     * @throws NotFoundException
//     * @param string $id
//     * @return void
//     */
//    public function choose($surveyId) {
//        $this->loadModel('Survey');
//        $this->loadModel('SurveyTheme');
//        $this->Survey->id = $surveyId;
//        if (!$this->Survey->exists()) {
//            throw new NotFoundException(__('Invalid survey.'));
//        }
//
//        if ($this->request->is('post')) {
//            $add_another_theme = $this->request->data['add_another_theme'];
//            $themes = $this->request->data['theme'];
//            $ownTheme = $this->request->data['ownTheme'];
//
//            if (sizeof($themes) || ($ownTheme['name'] && strlen(trim($ownTheme['name'])) != 0)) {
//
//                $errors = 0;
//
//                //chosen standard themes
//                foreach ($themes as $theme) {
//                    $themeData = $this->StandardTheme->read(null, $theme);
//                    $this->SurveyTheme->create();
//                    if ($themeData) {
//                        $icon = '';
//                        if ($themeData['StandardTheme']['icon']) {
////                            echo $themeData['StandardTheme']['icon'];
//                            $file = new File(substr($themeData['StandardTheme']['icon'], 1));
//                            if ($file) {
//                                $dir = new Folder('files/uploads/theme_icons');
//                                $file->name = $file->name() . '-survey' . $surveyId . '.' . $file->ext();
//                                $file->copy($dir->path . DS . $file->name, true);
//
//                                $this->SurveyTheme->Behaviors->detach('Uploader.Attachment');
//                                $this->SurveyTheme->Behaviors->detach('Uploader.FileValidation');
//                                $icon = '/files/uploads/theme_icons/' . $file->name;
//                            }
//                        }
//                        $color = $themeData['StandardTheme']['color'];
//                        if (substr($color, 0, 1) != '#') {
//                            $color = '#' . $color;
//                        }
//                        if (!$this->SurveyTheme->save(array(
//                                    'name' => $themeData['StandardTheme']['name'],
//                                    'description' => $themeData['StandardTheme']['description'],
//                                    'icon' => $icon,
//                                    'color' => $color,
//                                    'survey_id' => $surveyId
//                                ))) {
//                            $errors++;
//                        }
//                    }
//                }
//                $validationErrors = array();
//                
//                //own theme
//                if ($ownTheme['name'] && strlen(trim($ownTheme['name'])) != 0) {
////                    echo '<pre>';
////                    var_dump($this->request->data);
////                    echo '</pre>';
//                    $this->SurveyTheme->Behaviors->attach('Uploader.Attachment');
//                    $this->SurveyTheme->Behaviors->attach('Uploader.FileValidation');
//                    $this->SurveyTheme->create();
//                    if (substr($ownTheme['color'], 0, 1) != '#') {
//                            $ownTheme['color'] = '#' . $ownTheme['color'];
//                        }
//                    $ownTheme['survey_id'] = $surveyId;
//                    
//                    if(!$this->SurveyTheme->save($ownTheme)){
//                        $errors++;
//                        $validationErrors = $this->SurveyTheme->validationErrors;
//                    }
//                }
//                 $errorMessage = '';
//                 if($errors){
//                     foreach($errors as $error){
//                         $errorMessage = $errorMessage.'<br />Error: '.$error[0];
//                     }
//                 }
//                 if($add_another_theme){
//                     if($errors){
//                         $this->Session->setFlash(__('Saving own theme failed.'.$errorMessage), 'default', array('class' => 'alert alert-danger'));
//                         if(!sizeof($themes)){
//                             $this->redirect(array('controller'=>'surveys', 'action' => 'add_group_message/' . $surveyId));
//                         }
//                     }else{
//                         $this->Session->setFlash(__('Theme(s) successfully added'), 'default', array('class' => 'alert alert-success'));
//                     }
//                     $this->redirect(array('controller'=>'surveys', 'action' => 'add_theme/' . $surveyId.'/1'));
//                 }else{
//                     $this->Session->setFlash(__('Theme(s) successfully added'), 'default', array('class' => 'alert alert-success'));
//                     $this->redirect(array('controller'=>'surveys', 'action' => 'add_theme_message/' . $surveyId));
//                 }
//            } else {
//                
//                if($add_another_theme){
//                    $this->Session->setFlash(__('You haven\'t added any theme.'), 'default', array('class' => 'alert alert-danger'));
//                    $this->redirect(array('controller'=>'surveys', 'action' => 'add_theme/' . $surveyId.'/1'));
//                }else{
//                    $this->Session->setFlash(__('You haven\'t added any theme.'), 'default', array('class' => 'alert alert-danger'));
//                    $this->redirect(array('controller'=>'surveys', 'action' => 'add_group_message/' . $surveyId));
//                }
//            }
//        }
//
//        $this->set('standardThemes', $this->StandardTheme->find('all'));
//    }

}
