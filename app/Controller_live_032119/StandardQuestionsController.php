<?php

App::uses('AppController', 'Controller');

/**
 * StandardQuestions Controller
 *
 * @property StandardQuestion $StandardQuestion
 */
class StandardQuestionsController extends AppController {
    
    
    public $uses = array('StandardQuestion', 'StandardQuestionOption');
    public $paginate = array(
        'StandardQuestion' => array(
            'order' => array('StandardTheme.id' => 'asc','StandardQuestion.id' => 'asc'),
            'conditions'=>array('StandardQuestion.is_deleted'=>0),
            'limit' => 10,
        )
    );
    
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->StandardQuestion->recursive = 0;
        $this->set('standardQuestions', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->StandardQuestion->exists($id)) {
            throw new NotFoundException(__('Invalid standard question'));
        }
        $options = array('conditions' => array('StandardQuestion.' . $this->StandardQuestion->primaryKey => $id));
        $this->set('standardQuestion', $this->StandardQuestion->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {

            $options = array_filter($this->request->data['option']);
            $scores = $this->request->data['score'];

            if ($this->request->data['StandardQuestion']['type'] == 'open ended') {  //if question is open ended
                $this->StandardQuestion->create();
                $this->save_question($id, $options, $scores);
            } else { //question is either likert or radio button
                if (sizeof($options) >= 2) { // checks if number of options is equal or greater than 2
                    $this->StandardQuestion->create();
                    $this->save_question($id, $options, $scores);
                } else {
                    $this->Session->setFlash(__('You should provide at least two options.'), 'default', array('class' => 'alert alert-danger'));
                }
            }
        }
        $themes = $this->StandardQuestion->StandardTheme->find('list');
        $this->set(compact('themes'));
        $types = $this->StandardQuestion->get_question_type_array(); // returns question types array (radio button, likert, open ended) 
        $this->set(compact('types'));
        $this->StandardQuestion->recursive = 0;
        
        $this->set('standardQuestions', $this->paginate());
    }
    
    /**
     * save_question method
     *
     * @throws NotFoundException
     * @param string $id
     * @param array $options
     * @param array $scores
     * @return void
     */
    private function save_question($id, $options, $scores) {
        if ($this->StandardQuestion->save($this->request->data)) {

            //save options
            foreach ($options as $k => $option) {
                $this->StandardQuestionOption->create();
                $optionData = array('question_id' => $this->StandardQuestion->getLastInsertId(), 'name' => $option, 'score' => $scores[$k]);
                if ($option) {
                    $this->StandardQuestionOption->save($optionData);
                }
            }
            $this->Session->setFlash(__('The question has been saved'), 'default', array('class' => 'alert alert-success'));
            $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The question could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
        }
    }
    

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->StandardQuestion->exists($id)) {
            throw new NotFoundException(__('Invalid standard question'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            
            
            $savedOptions = $this->StandardQuestionOption->find('list', array('conditions' => array('StandardQuestionOption.question_id' => $id), 'order' => 'StandardQuestionOption.id ASC'));
            $options = array_filter($this->request->data['option']);
            $scores = $this->request->data['score'];
            if ($this->request->data['StandardQuestion']['type'] == 'open ended') {  //if question is open ended
                if ($this->StandardQuestion->save($this->request->data)) {
                    $this->Session->setFlash(__('The question has been saved'), 'default', array('class' => 'alert alert-success'));
                    $this->redirect(array('controller'=>'standard_questions', 'action' => 'index'));
                }else{
                    $this->Session->setFlash(__('The question could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-error'));
                }
            }else{ //question is either likert or radio button
                if (sizeof($options) >= 2) {
                    if ($this->StandardQuestion->save($this->request->data)) {
                        foreach ($options as $k => $option) {
                            if(array_key_exists($k, $savedOptions)){
                               $this->StandardQuestionOption->read(null, $k);
                               $this->StandardQuestionOption->set(array(
                                    'name' => $option,
                                    'score' => $scores[$k]
                                ));
                                $this->StandardQuestionOption->save();
                            }else{
                                $this->StandardQuestionOption->create();
                                $optionData = array('question_id' => $id, 'name' => $option, 'score' => $scores[$k]);
                                $this->StandardQuestionOption->save($optionData);
                            }
                        }

                        //delete options
                        $delOptions = array_diff_key($savedOptions, $options);
                        foreach($delOptions as $kd=>$od){
                            $this->StandardQuestionOption->delete($kd);
                        }
                        $this->Session->setFlash(__('The question has been saved'), 'default', array('class' => 'alert alert-success'));
                        $this->redirect(array('controller'=>'standard_questions', 'action' => 'index'));
                    } else {
                        $this->Session->setFlash(__('The question could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
                    }
                } else {
                    $this->Session->setFlash(__('You should provide at least two options.'), 'default', array('class' => 'alert alert-danger'));
                }
            }
            
            
//            if ($this->StandardQuestion->save($this->request->data)) {
//                $this->Session->setFlash(__('The standard question has been saved'));
//                $this->redirect(array('action' => 'index'));
//            } else {
//                $this->Session->setFlash(__('The standard question could not be saved. Please, try again.'));
//            }
        } else {
            $options = array('conditions' => array('StandardQuestion.' . $this->StandardQuestion->primaryKey => $id));
            $this->request->data = $this->StandardQuestion->find('first', $options);
        }
        $themes = $this->StandardQuestion->StandardTheme->find('list');
        $this->set(compact('themes'));
        $types = $this->StandardQuestion->get_question_type_array(); // returns question types array (radio button, likert, open ended) 
        $this->set(compact('types'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->StandardQuestion->id = $id;
        if (!$this->StandardQuestion->exists()) {
            throw new NotFoundException(__('Invalid standard question'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->StandardQuestion->delete($id,true)) {
            $this->Session->setFlash(__('Standard question deleted'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Standard question was not deleted'), 'default', array('class' => 'alert alert-danger'));
        $this->redirect(array('action' => 'index'));
    }

}
