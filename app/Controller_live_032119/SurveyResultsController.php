<?php
App::uses('AppController', 'Controller');
/**
 * SurveyResults Controller
 *
 * @property SurveyResult $SurveyResult
 */
class SurveyResultsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SurveyResult->recursive = 0;
		$this->set('surveyResults', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->SurveyResult->id = $id;
		if (!$this->SurveyResult->exists()) {
			throw new NotFoundException(__('Invalid survey result'));
		}
		$this->set('surveyResult', $this->SurveyResult->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SurveyResult->create();
			if ($this->SurveyResult->save($this->request->data)) {
				$this->Session->setFlash(__('The survey result has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The survey result could not be saved. Please, try again.'));
			}
		}
		$surveys = $this->SurveyResult->Survey->find('list');
		$groups = $this->SurveyResult->Group->find('list');
		$questions = $this->SurveyResult->Question->find('list');
		$questionOptions = $this->SurveyResult->QuestionOption->find('list');
		$this->set(compact('surveys', 'groups', 'questions', 'questionOptions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->SurveyResult->id = $id;
		if (!$this->SurveyResult->exists()) {
			throw new NotFoundException(__('Invalid survey result'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->SurveyResult->save($this->request->data)) {
				$this->Session->setFlash(__('The survey result has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The survey result could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->SurveyResult->read(null, $id);
		}
		$surveys = $this->SurveyResult->Survey->find('list');
		$groups = $this->SurveyResult->Group->find('list');
		$questions = $this->SurveyResult->Question->find('list');
		$questionOptions = $this->SurveyResult->QuestionOption->find('list');
		$this->set(compact('surveys', 'groups', 'questions', 'questionOptions'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->SurveyResult->id = $id;
		if (!$this->SurveyResult->exists()) {
			throw new NotFoundException(__('Invalid survey result'));
		}
		if ($this->SurveyResult->delete()) {
			$this->Session->setFlash(__('Survey result deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Survey result was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
