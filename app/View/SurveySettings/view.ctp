<div class="surveySettings view">
<h2><?php echo __('Survey Setting'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($surveySetting['SurveySetting']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($surveySetting['SurveySetting']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Slug'); ?></dt>
		<dd>
			<?php echo h($surveySetting['SurveySetting']['slug']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($surveySetting['SurveySetting']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Survey'); ?></dt>
		<dd>
			<?php echo $this->Html->link($surveySetting['Survey']['name'], array('controller' => 'surveys', 'action' => 'view', $surveySetting['Survey']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($surveySetting['SurveySetting']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($surveySetting['SurveySetting']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Survey Setting'), array('action' => 'edit', $surveySetting['SurveySetting']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Survey Setting'), array('action' => 'delete', $surveySetting['SurveySetting']['id']), null, __('Are you sure you want to delete # %s?', $surveySetting['SurveySetting']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Survey Settings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Survey Setting'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Surveys'), array('controller' => 'surveys', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Survey'), array('controller' => 'surveys', 'action' => 'add')); ?> </li>
	</ul>
</div>
