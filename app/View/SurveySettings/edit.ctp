<div class="surveySettings form">
<?php echo $this->Form->create('SurveySetting'); ?>
	<fieldset>
		<legend><?php echo __('Edit Survey Setting'); ?></legend>
		<div class="row">
            <div class="col-xs-12 col-sm-8 col-md-4">
				<?php
					echo $this->Form->input('id');
					echo $this->Form->input('name');
					echo $this->Form->input('slug');
					echo $this->Form->input('value');
					echo $this->Form->input('survey_id');
				?>
			</div>
		</div>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SurveySetting.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('SurveySetting.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Survey Settings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Surveys'), array('controller' => 'surveys', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Survey'), array('controller' => 'surveys', 'action' => 'add')); ?> </li>
	</ul>
</div>
