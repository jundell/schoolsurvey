<div class="categories view">
<h2><?php  echo __('Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($category['Category']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($category['Category']['name']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Admins'); ?></dt>
                 
                
                <dd>
                   <?php foreach ($admins as $admin){ ?>
                        <?= h($admin['User']['username']).'<br />'; ?>
                    <?php } ?>
                </dd>
                
<!--		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($category['Category']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($category['Category']['modified']); ?>
			&nbsp;
		</dd>-->
	</dl>
</div>
<?php echo $this->element('user_links'); ?>