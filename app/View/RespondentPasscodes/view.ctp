<div class="respondentPasscodes view">
<h2><?php echo __('Respondent Passcode'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($respondentPasscode['RespondentPasscode']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Survey'); ?></dt>
		<dd>
			<?php echo $this->Html->link($respondentPasscode['Survey']['name'], array('controller' => 'surveys', 'action' => 'view', $respondentPasscode['Survey']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Group'); ?></dt>
		<dd>
			<?php echo $this->Html->link($respondentPasscode['Group']['name'], array('controller' => 'groups', 'action' => 'view', $respondentPasscode['Group']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($respondentPasscode['RespondentPasscode']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Passcode'); ?></dt>
		<dd>
			<?php echo h($respondentPasscode['RespondentPasscode']['passcode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($respondentPasscode['RespondentPasscode']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($respondentPasscode['RespondentPasscode']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Respondent Passcode'), array('action' => 'edit', $respondentPasscode['RespondentPasscode']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Respondent Passcode'), array('action' => 'delete', $respondentPasscode['RespondentPasscode']['id']), null, __('Are you sure you want to delete # %s?', $respondentPasscode['RespondentPasscode']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Respondent Passcodes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Respondent Passcode'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Surveys'), array('controller' => 'surveys', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Survey'), array('controller' => 'surveys', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
