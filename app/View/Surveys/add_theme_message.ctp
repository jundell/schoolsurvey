<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<?php echo $this->Html->link(__('Click here to add descriptors-questions'), array('controller'=>'questions','action' => 'add', $survey_id,1), array('type'=>'button','class'=>'btn btn-success')); ?>


<div class="survey-build">
    <?php //survey details ?>
    <h1><?php echo $survey['Survey']['name']; ?></h1>
    <h4>Description</h4>
    <p><?php echo $survey['Survey']['description']; ?></p>
    <h4>Welcome Message</h4>
    <p><?php echo $survey['Survey']['welcome_message']; ?></p>
    <h4>Group Message</h4>
    <p><?php echo $group_message; ?></p>
    <h4>Closing Message</h4>
    <p><?php echo $survey['Survey']['closing_message']; ?></p>
    
    <?php //groups ?>
    <div class="groups">
        <h3>Groups</h3>
        <ul class="list-unstyled">
        <?php foreach($groups as $group){ ?>
            <li><?php echo $group; ?></li>
        <?php } ?>
        </ul>
    </div>

    <?php //dimensions ?>
    <div class="dimensions">
        <h3>Dimensions</h3>
        <ul class="list-unstyled">
        <?php foreach($themes as $theme){ ?>
            <li><img class="standard-theme-icon img-responsive" src="<?php echo $theme['SurveyTheme']['icon']?APP_URL.$theme['SurveyTheme']['icon']:APP_URL.'files/uploads/theme_icons/icon-default.png'; ?>" /><span class="standard-theme-name" style="color:#<?php echo $theme['SurveyTheme']['color'] ?>;"><?php echo $theme['SurveyTheme']['name']; ?></span></li>
        <?php } ?>
        </ul>
    </div>
</div>