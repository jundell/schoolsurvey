<?php if(!isset($status) || $status){ ?>
<div class="surveys view take-survey">
    <?php echo $this->Form->create('Question'); ?>

    <?php
    /** 
        added function to format questions alternately by dimension 
        06/07/2018
    **/

        $theme_list = [];
        foreach($questions as $question){
            array_push($theme_list, $question["SurveyTheme"]["name"]);
        }
        $unique_theme = array_unique($theme_list);
        $unique_theme = array_values($unique_theme);
        $new_questions = [];
        $x =0;
        $temp = [];
        $current = '';
        $totalCount = count($unique_theme)-1;
        foreach ($questions as $question){ 
            if(($question['SurveyTheme']['name'] == $unique_theme[$x]) && ($question['Question']['type'] != 'open ended')) {
                array_push($new_questions, $question);
                $current = $unique_theme[$x];
                $x++;
            }else{
                array_push($temp, $question);
             
            }
            if($x > $totalCount){
                $x = 0;
            }
            //echo $x;
        }
    $n = array_merge($new_questions, $temp);
   /* foreach ($n as $nq){
        $questionId = $nq['Question']['id'];
        echo "</h6>"; echo $nq['Question']['name'];  echo $nq['SurveyTheme']['name']; echo $questionId; echo "</br></h6>";
    }
*/
    ?>

    <?php foreach($n as $question){ ?>
        <?php $questionId = $question['Question']['id']; ?>
        <?php $answers = $this->request->data['Question']['answer']; ?>
	<?php if(in_array($question['Question']['type'],array('radio button', 'likert'))){?>
                <h3> <?php echo $question['Question']['name']; ?></h3>
                <?php $options = array(
                            'div'=>array('class'=>'form-group input'),
                    'separator'=> '</label></div><div class="radio"><label>',
                    'before' => '<div class="radio"><label>',
                    'after' => '</label></div>',
                            'options' => $options,
                            'type' => 'radio',
                            'legend' => false,
                    );
                    
                    ?>
                <?php if($question['Question']['type'] == 'likert'){
                    $cnt=0;
                    foreach($question['QuestionsOption'] as $k=>$optionVal){
                        
                        if($optionVal['is_deleted'] == 'no'){
                            if($optionVal['score']>0){
                                if($cnt==0){?>
                                    <div class="radio inline">
                                        <input name="data[Question][answer][<?php echo $questionId; ?>]" id="QuestionAnswer_" value="" type="hidden">
                                        <input name="data[Question][answer][<?php echo $questionId; ?>]" id="QuestionAnswer<?php echo $optionVal['id'];?>" value="<?php echo $optionVal['id'];?>" type="radio" <?php echo $answers[$questionId] == $optionVal['id']? 'checked': ''; ?>>
                                        <label for="QuestionAnswer<?php echo $optionVal['id'];?>"><?php echo $optionVal['name'];?></label>
                                        <span class="vertical-line"></span>
                                    </div>
                                <?php }else{ ?>
                                    <div class="radio inline">
                                        <hr class="likert-connect">
                                        <input name="data[Question][answer][<?php echo $questionId; ?>]" id="QuestionAnswer<?php echo $optionVal['id']; ?>" value="<?php echo $optionVal['id'];?>" type="radio" <?php echo $answers[$questionId] == $optionVal['id']? 'checked': ''; ?>>
                                        <label for="QuestionAnswer<?php echo $optionVal['id']; ?>"><?php echo $optionVal['name'];?></label>
                                        <?php if(!isset($question['QuestionsOption'][$k+1]) || $question['QuestionsOption'][$k+1]['score']!=0){ ?>
                                            <span class="vertical-line"></span>
                                        <?php } ?>
                                    </div>
                                <?php }
                                $cnt++;
                            }else{ ?>
                                <div class="input radio likert_no_response">
                                    <input name="data[Question][answer][<?php echo $questionId; ?>]" id="QuestionAnswer<?php echo $optionVal['id']; ?>" value="<?php echo $optionVal['id']; ?>" type="radio" <?php echo $answers[$questionId] == $optionVal['id']? 'checked': ''; ?>>
                                    <label for="QuestionAnswer<?php echo $optionVal['id']; ?>"><?php echo $optionVal['name']; ?></label>
                                </div>
                            <?php }
                        }
                    }
                }else{ ?>
                    
                    <div class="form-group input">
                        <?php foreach($question['QuestionsOption'] as $k=>$optionVal){ ?>
                            <div class="radio radio-button-option">
                                <label>
                                    <input type="radio" name="data[Question][answer][<?php echo $questionId; ?>]" id="QuestionAnswer<?php echo $optionVal['id']; ?>" value="<?php echo $optionVal['id']; ?>" <?php echo $answers[$questionId] == $optionVal['id']? 'checked': ''; ?>>
                                    <label for="QuestionAnswer<?php echo $optionVal['id']; ?>"><?php echo $optionVal['name']; ?></label>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                    
                <?php } ?>
                
	<?php }else{ ?>
        <legend> <?php echo $question['Question']['name'];?> </legend>
    	<div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6">
            <?php 	echo $this->Form->input('answer', array('name'=>"data[Question][answer][$questionId]",'value'=>isset($answers[$questionId])?$answers[$questionId]:'','class'=>'form-control','div'=>array('class'=>'form-group input text'))); ?>
            </div>
        </div>
	<?php }
    ?>

    <hr />

    <?php } ?>

    <?php echo $this->Form->end(array('label' => 'Next >>>', 'class' => 'btn btn-success')); ?>
</div>

<div class="progress progress-striped active">
    <div class="progress-bar"  role="progressbar" aria-valuenow="<?php echo $percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percent; ?>%">
        <span class="sr-only"><?php echo $percent; ?>% Complete</span>
    </div>
</div>

<script type="text/javascript">
    // $(document).ready(function(){
    //     $('span.vertical-line:last').hide();
    // });
</script>
<?php } ?>