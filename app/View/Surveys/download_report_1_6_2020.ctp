<?php
//global $report_name, $report_date, $r_settings;

ob_end_clean(); // Just in case, to be sure


// Initialize the TBS instance
$TBS = $this->Tbs->getOpenTbs();; // new instance of TBS

$TBS->NoErr = true;
//Survey name
$TBS->VarRef['report_name'] = $result['survey']['Survey']['name'];

$TBS->VarRef['subscriber_name'] = $result['survey']['User']['subscriber_name'];

//Report date
$TBS->VarRef['report_date'] = date("F j, Y");

//Report settings
//$TBS->VarRef['report_settings'] = $report_settings;

// -----------------
// Load the template
// -----------------
//
$template = dirname(__FILE__).'/new_report_template.docx';
$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

//Dimensions
$dimensionsDivs = array_chunk($result['dimensions'], ceil(count($result['dimensions']) / 2));
$cnt = 1;
$dimensions = array();
$dimensions1 = array();
$dimensions2 = array();
$dimensions_table1 = array();
for($i=1; $i<=50; $i++){
    $dimensions_table1[] = array('exist'=>0);
}
foreach($dimensionsDivs as $dimensionsDiv){
    foreach($dimensionsDiv as $dimension){
        $basename = explode('/', strtolower($dimension['SurveyTheme']['name']));
        $basename = preg_replace('/[\W]+/', '-', $basename[0]);

        if( $dimension['SurveyTheme']['icon'] ) {
            if( file_exists( APP.'webroot'.$dimension['SurveyTheme']['icon'] ) ){
                $path = $dimension['SurveyTheme']['icon'];
            } elseif ( file_exists(APP.'webroot'.preg_replace('/-survey[\d]*\./', '.', $dimension['SurveyTheme']['icon'])) ) {
                $path = preg_replace('/-survey[\d]*\./', '.', $dimension['SurveyTheme']['icon']);
            } elseif ( file_exists(APP.'webroot'.dirname($dimension['SurveyTheme']['icon']).'/icon-'.$basename.'.png') ) {
                $path = dirname($dimension['SurveyTheme']['icon']).'/icon-'.$basename.'.png';
            } else {
                $path = '/files/uploads/theme_icons/icon-default.png';
            }
        } else {
            $path = '/files/uploads/theme_icons/icon-default.png';
        }
        
        $path = trim($path,"/");

        ${"dimensions" . $cnt}[] = $dimensions[] = array(
//            'path'=>str_replace(array("\\/","/"), array("\\","\\"), $path),
            'path'=>$path,
            'color'=>substr($dimension['SurveyTheme']['color'],1),
            'name'=>$dimension['SurveyTheme']['name'].':',
            'desc'=>$dimension['SurveyTheme']['description']?$dimension['SurveyTheme']['description']:''
        );
    }
    $cnt++;
}


foreach($dimensions_table1 as $k=>$v){
    $dimensionRes = $result['dimensionsResult'][$result['dimensions'][$k]['SurveyTheme']['id']];
    if($dimensionRes['Total']['noOfResp']){
        $v = array_merge(array('exist'=>1), $dimensions[$k]);
        
        $ChartNameOrNum = 'graph'.$k; // Title of the shape that embeds the chart
        $SeriesNameOrNum = 'Series 1';
        $labels = array('Self','Overall');
        $values = array($dimensionRes['AdminAvg'],$dimensionRes['Total']['avg']);
        foreach($dimensionRes['groups'] as $kg=>$g){
            if(!empty($g['name'])) {
                $labels[] = $g['name'];
                $values[] = $g['avg'];
            }
        }
        $NewValues = array( $labels, $values );
        $NewLegend = "Updated series 1";
        $TBS->PlugIn(OPENTBS_CHART, $ChartNameOrNum, $SeriesNameOrNum, $NewValues, $NewLegend);
    }
    $TBS->VarRef['d'.$k] = $v;
}


//Table 2 - Bar Graphs\
//foreach($result['dimensions'] as $k=>$d){
//    $dimensionRes = $result['dimensionsResult'][$d['SurveyTheme']['id']];
//    if($dimensionRes['Total']['noOfResp']){
//        $d = array_merge(array('exist'=>1), $dimensions[$k]);
//        $TBS->VarRef['d'.$k] = $d;
//    }
//}
// Change chart series

//$ChartNameOrNum = 'graph0'; // Title of the shape that embeds the chart
//$SeriesNameOrNum = 'Series 1';
//$NewValues = array( array('Self','Overall','Teachers','Support staff', 'Building and District Administrators', 'Parents'), array(3, 1.1, 4.0, 3.3, 3, 6) );
//$NewLegend = "Updated series 1";
//$TBS->PlugIn(OPENTBS_CHART, $ChartNameOrNum, $SeriesNameOrNum, $NewValues, $NewLegend);
//
//$ChartNameOrNum = 'graph1'; // Title of the shape that embeds the chart
//$SeriesNameOrNum = 'Series 1';
//$NewValues = array( array('Self','Overall','Teachers','Support staff', 'Building and District Administrators', 'Parents'), array(3, 1.1, 4.0, 3.3, 3, 6) );
//$NewLegend = "Updated series 1";
//$TBS->PlugIn(OPENTBS_CHART, $ChartNameOrNum, $SeriesNameOrNum, $NewValues, $NewLegend);

// dimensions merge block
$TBS->MergeBlock('dimensions', $dimensions);
$TBS->MergeBlock('dimensions1', $dimensions1);
$TBS->MergeBlock('dimensions2', $dimensions2);

//Groups
$groups = $groups2 = array();
$aveN = array();
$aveN[] = 'Dimension';
foreach($result['groups'] as $key=>$group){
    $groups[] = array('name'=>$group['Group']['name']);
    $groups2[$group['Group']['id']] = array('name'=>$group['Group']['name']);
}
$aveN[] = 'Ave Score';
$aveN[] = 'N'; 
$aveN[] = 'Ave Score';
$aveN[] = '';
$table1_labels =["a","b","c","d"];

$TBS->MergeBlock('groups,groups3', $groups);

$TBS->MergeBlock('aveN', $aveN);

$table1=array();
$table1_2 = [];
// implement multi year
$years = array_keys($result['dimensionResultsYearly']);
$TBS->VarRef['table1_2_exist'] = 0;
$TBS->VarRef['table1_3_exist'] = 0;
$TBS->VarRef['table1_4_exist'] = 0;
$TBS->VarRef['table1_header'] = '';
foreach($result['dimensions'] as $k=>$d){
    $dimensionRes = ($result['reportType'] == 1) ? $result['dimensionsResult'][$d['SurveyTheme']['id']] : $result['dimensionResultsYearly'][0][$d['SurveyTheme']['id']];

    if($dimensionRes['Total']['noOfResp']){
        $arr=array(
            'color'=>substr($d['SurveyTheme']['color'],1),
            'name'=>$dimensionRes['name'],
            'totalAvg'=>$dimensionRes['Total']['avg'],
            'totalN'=>$dimensionRes['Total']['noOfResp'],
            'adminAvg'=>$dimensionRes['AdminAvg'],
            'awarenessQ'=>$dimensionRes['AwarenessQuotient']
        );
        foreach($dimensionRes['groups'] as $kg=>$g){
            if(!empty($g['name'])) {
                $groups2[$kg]['avgs'][] = array('avg'=>$g['avg'],'n'=>$g['noOfResp']);
                $arr['groups'][] = array('avg'=>$g['avg'],'n'=>$g['noOfResp']);
            }
        }
        $table1[]=$arr;
    }

    // has multi data
    if (count($result['dimensionResultsYearly']) > 1) {
        $dateRanges = $result['reportByRanges'];
        $TBS->VarRef['table1_header'] = "Table 1-a - ".date('F d, Y', strtotime($dateRanges[0]['from'])) .'-'. date('F d, Y', strtotime($dateRanges[0]['to']));
        for ($dataX = 0; $dataX < count($dateRanges); $dataX++) {

            if (isset($result['dimensionResultsYearly'][$dataX+1])) {
                $tmpResult1 = $result['dimensionResultsYearly'][$dataX][$d['SurveyTheme']['id']];
                $tmpResult2 = $result['dimensionResultsYearly'][$dataX+1][$d['SurveyTheme']['id']];

                if (isset($tmpResult1['name'])
                    || isset($tmpResult2['name'])) {

                        $TBS->VarRef['table1_'.($dataX+2).'_exist'] = 1;
                        $TBS->VarRef['table1_'.($dataX+2).'_header'] = "Table 1-".$table1_labels[$dataX+1]." - ".date('F d, Y', strtotime($dateRanges[$dataX]['from'])) .'-'. date('F d, Y', strtotime($dateRanges[$dataX]['to'])) .' (Table 1' . $table1_labels[$dataX].') | ' . date('F d, Y', strtotime($dateRanges[$dataX + 1]['from'])) .'-'. date('F d, Y', strtotime($dateRanges[$dataX + 1]['to'])) . ' (Table 1'.$table1_labels[$dataX+1].')';
                        $diffTotalAvg = $tmpResult1['Total']['avg'] - $tmpResult2['Total']['avg'];
                        $diffAdmingAvg = $tmpResult1['AdminAvg'] - $tmpResult2['AdminAvg'];
                        $tmpArr = [
                            'exist' => 1,
                            'header' => '',
                            'color' => substr($d['SurveyTheme']['color'],1),
                            'name' => (isset($tmpResult2['name'])) ? $tmpResult2['name'] : $tmpResult1['name'],
                            'total_avg1' => ($tmpResult1['Total']['avg']) ? $tmpResult1['Total']['avg'] : 0,
                            'total_avg2' => ($tmpResult2['Total']['avg']) ? $tmpResult2['Total']['avg'] : 0,
                            'total_change' => $diffTotalAvg,
                            'admin_avg1' => ($tmpResult1['AdminAvg']) ? $tmpResult1['AdminAvg'] : 0,
                            'admin_avg2' => ($tmpResult2['AdminAvg']) ? $tmpResult2['AdminAvg'] : 0,
                            'admin_change' => $diffAdmingAvg,
                        ];

                        // attemp to get data from first data
                        if ($tmpResult1) {
                            foreach($tmpResult1['groups'] as $kg=>$g){
                                
                                if(!empty($g['name'])) {
                                    $tmpArr['tables'][] = [
                                        'table1' => 'Table 1' . $table1_labels[$dataX],
                                        'table2' => 'Table 1' . $table1_labels[$dataX+1]
                                    ];

                                    $avg2 = isset($tmpResult2 ['groups'][$kg]['avg']) ? $tmpResult2 ['groups'][$kg]['avg'] : 0;
                                    $tmpArr['groups'][] = [
                                        'avg1' => $g['avg'],
                                        'avg2' => $avg2,
                                        'change' => $g['avg'] - $avg2
                                    ];
                                }
                            }
                        }
                        else {
                            foreach($tmpResult2['groups'] as $kg=>$g){
                               
                                if(!empty($g['name'])) {
                                    $tmpArr['tables'][] = [
                                        'table1' => 'Table 1' . $table1_labels[$dataX],
                                        'table2' => 'Table 1' . $table1_labels[$dataX+1]
                                    ];

                                    $avg2 = isset($tmpResult2 ['groups'][$kg]['avg']) ? $tmpResult2 ['groups'][$kg]['avg'] : 0;
                                    $tmpArr['groups'][] = [
                                        'avg1' => 0,
                                        'avg2' => $avg2,
                                        'change' => $avg2
                                    ];
                                }
                            }
                        }
        
                        if (count($tmpArr)) {
                            $table1_2[$dataX][] = $tmpArr;
                            $tmpArr = [];
                        }
                }
                
            }
        }
    }
}
$TBS->MergeBlock('groups2', $groups2);
$TBS->MergeBlock('table1',$table1);
$TBS->MergeBlock('table1_2', $table1_2[0]);
$TBS->MergeBlock('table1_2_table', $table1_2[0][0]['tables']);
$TBS->MergeBlock('groups1_2', $groups2);
$TBS->MergeBlock('table1_3', $table1_2[1]);
$TBS->MergeBlock('groups1_3', $groups2);
$TBS->MergeBlock('table1_3_table', $table1_2[1][0]['tables']);
$TBS->MergeBlock('table1_4', $table1_2[2]);
$TBS->MergeBlock('groups1_4', $groups2);
$TBS->MergeBlock('table1_4_table', $table1_2[2][0]['tables']);

//Table 3 - Table of Descriptors
//echo '<pre>';
//var_dump($result['questionRanks']);
//echo '</pre>';

$qRankDimensionsColors = array();
foreach($result['dimensions'] as $d){
    $qRankDimensionsColors[$d['SurveyTheme']['id']] = $d['SurveyTheme']['color'];
}
$table3 = array();
foreach($result['questionRanks'] as $qRank) {

    if ($qRank['num']) {
        $table3[] = array(
            'num'=>$qRank['num'],
            'name'=>$qRank['name'],
            'color'=>substr($qRankDimensionsColors[$qRank['dimensionId']],1),
            'dimension'=>$qRank['dimension'],
            'adminAvg'=>$qRank['adminAvg'],
            'overallAvg'=>$qRank['overallAvg'],
            'difference'=>$qRank['difference']
        );
    }
    
}
$TBS->MergeBlock('table3',$table3);

//Table 4 - Table of Open-ended Answers
$TBS->MergeBlock('table4',$result['openEnded']);

//Report settings
if(count($result['nonCalculating']) == 0){
    unset($report_settings['table5_title']);
    unset($report_settings['table5_text']);
}

$TBS->VarRef['report_settings'] = $report_settings;


$TBS->MergeBlock('table5',$result['nonCalculating']);


if (isset($_POST['debug']) && ($_POST['debug']=='current')) $TBS->Plugin(OPENTBS_DEBUG_XML_CURRENT, true); // Display the intented XML of the current sub-file, and exit.
if (isset($_POST['debug']) && ($_POST['debug']=='info'))    $TBS->Plugin(OPENTBS_DEBUG_INFO, true); // Display information about the document, and exit.
if (isset($_POST['debug']) && ($_POST['debug']=='show'))    $TBS->Plugin(OPENTBS_DEBUG_XML_SHOW); // Tells TBS to display information when the document is merged. No exit.

// --------------------------------------------
// Merging and other operations on the template
// --------------------------------------------

// Delete comments
$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);


// -----------------
// Output the result
// -----------------
//
// Define the name of the output file
$save_as = (isset($_POST['save_as']) && (trim($_POST['save_as'])!=='') && ($_SERVER['SERVER_NAME']=='localhost')) ? trim($_POST['save_as']) : '';
$output_file_name = str_replace('.', '_'.date('Y-m-d').$save_as.'.', $template);
// Define the name of the output file
if ($save_as==='') {
    // Output the result as a downloadable file (only streaming, no data saved in the server)
    $TBS->Show(OPENTBS_DOWNLOAD, $result['survey']['Survey']['short_name'].'.docx'); // Also merges all [onshow] automatic fields.
    // Be sure that no more output is done, otherwise the download file is corrupted with extra data.
    exit();
} else {
    // Output the result as a file on the server.
    $TBS->Show(OPENTBS_FILE, $output_file_name); // Also merges all [onshow] automatic fields.
    // The script can continue.
    exit("File [$output_file_name] has been created.");
}
