<div class="survey-build">
    <?php //survey details ?>
    <h1><?php echo $survey['Survey']['name']; ?></h1>
    <h4>Description</h4>
    <p><?php echo $survey['Survey']['description']; ?></p>
    <h4>Welcome Message</h4>
    <p><?php echo $survey['Survey']['welcome_message']; ?></p>
    <h4>Closing Message</h4>
    <p><?php echo $survey['Survey']['closing_message']; ?></p>
    
    <?php //groups ?>
    <div class="groups">
        <h3>Groups</h3>
        <ul class="list-unstyled">
        <?php foreach($groups as $group){ ?>
            <li><?php echo $group; ?></li>
        <?php } ?>
        </ul>
    </div>

    <?php //dimensions ?>
    <div class="dimensions">
        <h3>Dimensions</h3>
        <ul class="list-unstyled">
        <?php foreach($themes as $theme){ ?>
            <li><img class="standard-theme-icon img-responsive" src="<?php echo $theme['SurveyTheme']['icon']?APP_URL.$theme['SurveyTheme']['icon']:APP_URL.'files/uploads/theme_icons/icon-default.png'; ?>" /><span class="standard-theme-name" style="color:#<?php echo $theme['SurveyTheme']['color'] ?>;"><?php echo $theme['SurveyTheme']['name']; ?></span></li>
        <?php } ?>
        </ul>
    </div>

    <?php //questions ?>
    <div class="questions">
        <h3>Questions</h3>
        <?php foreach($themes as $theme){ ?>
            <?php if(sizeof($theme['Question'])){ ?>
                <div>
                    <img class="standard-theme-icon img-responsive" src="<?php echo $theme['SurveyTheme']['icon']?APP_URL.$theme['SurveyTheme']['icon']:APP_URL.'files/uploads/theme_icons/icon-default.png'; ?>" /><span class="standard-theme-name" style="color:#<?php echo $theme['SurveyTheme']['color'] ?>;"><?php echo $theme['SurveyTheme']['name']; ?></span>
                    <ul class="list">
                <?php foreach($theme['Question'] as $question){ ?>
                        <li><?php echo $question['name']; ?></li>
                <?php } ?>
                    </ul>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
    <br />
    <div class="text-center">
        <h2>Build Feedback Survey? <a href="<?php echo APP_URL; ?>surveys/start_stop_survey/<?php echo $survey['Survey']['id'] ?>/1/1" class="btn btn-lg btn-success">Yes</a> <a href="<?php echo APP_URL; ?>surveys" class="btn btn-lg btn-danger">No</a></h2>
    </div>
</div>