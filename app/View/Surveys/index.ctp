<?php

if(!$surveys){
?>
<h3> Welcome to Larry Lobert Survey Dashboard</h3>
<a href="<?php echo $this->webroot; ?>surveys/add" class="btn btn-default">Start your first survey</a>
<a href="<?php echo $this->webroot; ?>surveys/upload_survey" class="btn btn-default">Upload your first Survey</a>
<?php } else { ?>
<div class="surveys index">
    <a href="<?php echo $this->webroot; ?>surveys/add" class="btn btn-default">Survey Builder</a>
    <a href="<?php echo $this->webroot; ?>surveys/upload_survey" class="btn btn-default">Upload Survey</a>
    <?php if($profile['User']['role_id']==1){ ?>
    <a href="<?php echo $this->webroot; ?>survey_builder_components" class="btn btn-default">Survey Builder Components</a>
    <a href="<?php echo $this->webroot; ?>users" class="btn btn-default">Subscribers</a>
    
    <a href="<?php echo $this->webroot; ?>settings" class="btn btn-default btn-gen-settings">General Settings</a>
    <a href="<?php echo $this->webroot; ?>settings/survey_header" class="btn btn-default btn-gen-settings survey-header-link">Default Survey Header</a>
    
    <?php } ?>

    <br />
    <br />
	<form class="form-inline" method="GET">
		<div class="col-lg-3">
			<label>Name</label>
			<input type="text" class="form-control" name="survey_name" />
		</div>
		<div class="col-lg-5">
			<input type="submit" value="Filter" class="btn btn-primary" />
		</div>
	</form>

	<br />
	<br />
    <table class="table table-hover" cellpadding="0" cellspacing="0" >
        <tr>
            <th class="<?php echo $profile['User']['role_id']==1?'col-xs-3':'col-xs-4'; ?>"><?php echo $this->Paginator->sort('name'); ?></th>
            <?php if($profile['User']['role_id']==1){ ?>
            <th class="col-xs-1"><?php echo $this->Paginator->sort('User.first_name', 'Subscriber'); ?></th>
            <?php }?>
            <th><?php echo __('No. Years') ?></th>
            <th class="actions col-xs-5"><?php echo __('Actions'); ?></th>
        </tr>
	<?php foreach ($surveys as $survey): ?>
        <tr id="survey_<?php echo h($survey['Survey']['id']); ?>">
            <td><?php echo h($survey['Survey']['name']); ?>&nbsp;</td>
            <?php if($profile['User']['role_id']==1){ ?>
            <td><?php echo h($survey['User']['first_name'].' '.$survey['User']['last_name']); ?>&nbsp;</td>
            <?php }?>
            <td>
                <?php
                    $years = [];
                    // TODO 12-06-2018
                    // foreach ($survey['SurveyRunHistories'] as $history) {
                    //     $year = date('Y', strtotime($history['created']));
                    //     if (!in_array($year, $years)) {
                    //         $years[] = $year;
                    //     }
                    // }
                    foreach ($survey['SurveyResults'] as $ans) {
                        // group by year
                        $year = date('Y', strtotime($ans['created']));
        
                        if (!in_array($year, $years)) {
                            $years[] = $year;
                        }
                    }
                    echo ((count($years) > 0) ? count($years) : 1); 
                ?>
            </td>
            <td class="actions survey-index">
                <?php $alertMessage = 'You have to add '; ?>
                <?php $missing = !sizeof($survey['Group']) || !sizeof($survey['SurveyTheme']) || !sizeof($survey['Question']); ?>
                <?php $missingRespondents = !$survey['Survey']['open'] && !sizeof($survey['RespondentPasscode']); ?>
                <?php // echo 'test = '.$missing; ?>
                
                <?php if($missing){ ?>
                    <?php if(!sizeof($survey['Group'])){ // no groups
                            $alertMessage = $alertMessage.'Groups';
                    }?>

                    <?php if(!sizeof($survey['SurveyTheme'])){ // no dimensions
                            if(!sizeof($survey['Group']))
                                $alertMessage = $alertMessage.', ';
                            $alertMessage = $alertMessage.'Dimensions and Descriptors-Questions';
                    }?>
                    <?php
                    
                        if(sizeof($survey['SurveyTheme']) && !sizeof($survey['Question'])){ // no questions
                            if(!sizeof($survey['Group']))
                                $alertMessage = $alertMessage.' and ';
                            $alertMessage = $alertMessage.'Descriptors-Questions';
                        }
                    ?>
                    <?php $alertMessage = $alertMessage.' to proceed.'; ?>
                <?php }else{
                    $alertMessage = $alertMessage.'Group Respondents to proceed.';
                } ?>
                
                
                
            <?php echo $this->Html->link(__('Groups'), array('controller'=>'groups', 'action' => 'add', $survey['Survey']['id']), array('type'=>'button','class'=>'btn btn-primary')); ?>
                    <?php echo $this->Html->link(__('Dimensions'), array('controller'=>'survey_themes', 'action' => 'add', $survey['Survey']['id']), array('type'=>'button','class'=>'btn btn-primary')); ?>
			
                        <?php if(sizeof($survey['SurveyTheme'])){ ?>
                            <?php echo $this->Html->link(__('Descriptors-Questions'), array('controller'=>'questions', 'action' => 'add', $survey['Survey']['id']), array('type'=>'button','class'=>'btn btn-primary')); ?>
                        <?php }else{ ?>
                            <?php echo $this->Html->link(__('Descriptors-Questions'), '#', array('type'=>'button','class'=>'btn btn-primary', 'onclick'=>'alert("You have to add Dimensions to proceed.");')); ?>
                        <?php } ?>
                            <!-- report -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" 
                                    data-toggle="dropdown">
                                Report <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" style="text-align: left;">
                                <li>
                                <?php echo $this->Html->link(__('Single'), array('action' => 'report', $survey['Survey']['id'])); ?>
                                <li>
                                    <a class="generate-report-btn" href="javascript:void(0);" data-toggle="modal" data-target="#modal-report" data-id="<?php echo $survey['Survey']['id'];?>">Cumulative</a>
                                </li>
                            </ul>
                        </div>
                        <!-- end report -->
                        <?php if(!$missing && !$missingRespondents){ ?>
			<?php echo $this->Html->link(__('View Survey'), array('action' => 'verify', $survey['Survey']['slug']), array('target' => '_blank','type'=>'button','class'=>'btn btn-primary')); ?>
                        <?php }else{ ?>
                            <?php echo $this->Html->link(__('View Survey'), '#', array('target' => '_new','type'=>'button','class'=>'btn btn-primary','onclick' => 'alert("'.$alertMessage.'");')); ?>
                        <?php } ?>
                
                            <?php if($survey['Survey']['status']=='0'){ ?>
                            <?php if(!$missing && !$missingRespondents){ ?>
                                <?php echo $this->Html->link(__('Start'), array('action' => 'start_stop_survey', $survey['Survey']['id'],'1', $this->Paginator->current()), array('type'=>'button','class'=>'btn btn-success','confirm' => 'Are you sure you wish to start this survey?')); ?>
                            <?php }else{ ?>
                                <?php echo $this->Html->link(__('Start'), '#', array('type'=>'button','class'=>'btn btn-success','onclick' => 'alert("'.$alertMessage.'");')); ?>
                            <?php } ?>
                        <?php }else{ ?>
                            <?php echo $this->Html->link(__('Stop'), array('action' => 'start_stop_survey',$survey['Survey']['id'],'0', $this->Paginator->current()), array('type'=>'button','class'=>'btn btn-danger','confirm' => 'Are you sure you wish to stop this survey?')); ?>
                        <?php } ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" 
                            data-toggle="dropdown">
                        Settings <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" style="text-align: left;">
                        <li><a href="<?php echo $this->webroot; ?>surveys/view/<?php echo $survey['Survey']['id']; ?>/1">View</a></li>
                        <?php if(!$survey['Survey']['status']){ ?>
                            <li><a href="<?php echo $this->webroot; ?>surveys/edit/<?php echo $survey['Survey']['id']; ?>">Edit</a></li>
                        <?php } ?>
                        <li><a href="#" data-toggle="modal" data-target="#duplicate-survey-<?php echo $survey['Survey']['id'];?>">Duplicate</a></li>
                        <?php if($profile['User']['role_id']==1){ ?>
                        <li><a href="<?php echo $this->webroot; ?>survey_settings/download_data/<?php echo $survey['Survey']['id']; ?>">Download Data</a></li>
                            <?php if(!$survey['Survey']['status']){ ?>
                            <li><a href="<?php echo $this->webroot; ?>survey_settings/footer/<?php echo $survey['Survey']['id']; ?>">Footer</a></li>
                            <?php } ?>
                        <?php } ?>
                        <?php if(!$survey['Survey']['status']){ ?>
                            <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $survey['Survey']['id']), null, __('Are you sure you want to delete this survey?'), array('type'=>'button','class'=>'btn btn-delete')); ?></li>
                        <?php } ?>
                    </ul>
                    <div class="modal fade" id="duplicate-survey-<?php echo $survey['Survey']['id'];?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $survey['Survey']['name']; ?>">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Duplicate Survey</h4>
                                </div>
                                <div class="modal-body">
                                    
                                    <strong>Survey to duplicate:</strong><br />
                                    <h4><?php echo $survey['Survey']['name']; ?></h4>
                                    <br />
                                    <?php echo $this->Form->create('Survey', array('action' => 'duplicate/'.$survey['Survey']['id'])); ?>
                                            <fieldset>
                                                <?php
                                                        echo $this->Form->input('name', array('label'=>'Survey Name:','div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
                                                ?>
                                            </fieldset>
                                        <?php echo $this->Form->end(__(array('label' => 'Duplicate', 'class' => 'btn btn-success'))); ?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
<?php endforeach; ?>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
			$this->Paginator->options(array('url' => $args));
			echo $this->Paginator->first('<< first');
			echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
			/*echo $this->Paginator->numbers(array('separator' => ''));*/
            echo $this->Paginator->numbers(array('separator' => '', 'class' => 'next_num'));
			echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
            /*echo $this->Paginator->last('last >>');*/
            echo $this->Paginator->last('last >>', array(), null, array('class' => 'last'));
	?>
    </div>
</div>
<?php } ?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-report">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Report (Table 1 will show as main report)</h4>
      </div>
      <?php echo $this->Form->create(null, array('url' => '/surveys/report/', 'class' => 'form-inline')); ?>
      <div class="modal-body">
            <div class="report-table" style="margin-bottom: 20px;">
                <p>
                    <strong>Table 1</strong>
                </p>
                <div class="form-group">
                    <label for="from">From</label>
                    <input type="text" name="from[]" autocomplete="off" class="form-control from" required>
                </div>
                <div class="form-group">
                    <label for="to">to</label>
                    <input type="text" name="to[]" autocomplete="off" class="form-control to" required>
                </div>
                <hr>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger report-remove-date-btn" style="display: none">Remove Compare Date</button>
        <button type="button" class="btn btn-success report-add-date-btn">Add Compare Date</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Generate</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
   $(document).ready(function () {
        var url_path = location.pathname;
        if(url_path.indexOf('id:') != -1){
            $id= (url_path.substr(url_path.lastIndexOf('/') + 1)).replace('id:', '');
            $('html, body').animate({
                scrollTop: $('#survey_'+$id).offset().top - 50
            }, 2000);
        }

         $('.next_num, .next, .prev').on('click',function(){
            url_path = $(this).find('a').attr('href');
            $id= (url_path.substr(url_path.lastIndexOf('/') + 1));
             url_path = url_path.replace($id, '');
            if($id.indexOf('id:') != -1){
                url_path = url_path.replace($id, '');
                window.location = url_path;
                return false;
            }

        });

        var $surveyId = 0;
        var $url = '<?php  echo $this->Html->url(array(
                "controller" => "surveys",
                "action" => "report")); ?>';
        var $rangeForm = '';
        var $rangeFormCtr = 1;

        jQuery('a.generate-report-btn').on('click', function (){
           
            $surveyId = jQuery(this).attr('data-id');
            $rangeFormCtr = 1;

            if ($rangeForm != '') {
                $rangeForm.find('strong').html('Table ' + $rangeFormCtr);
                $rangeForm.find('input').val('').removeClass('hasDatepicker');
                jQuery('div#modal-report').find('div.modal-body').html($rangeForm);
            }
            
            jQuery('div#modal-report').find('form').attr('action', $url + '/' + $surveyId + '/2');
            jQuery('button.report-add-date-btn').show();
            jQuery('button.report-remove-date-btn').hide();
        });

        jQuery('button.report-add-date-btn').on('click', function() {

            if ($rangeFormCtr <=3 ) {
                $rangeFormCtr++;
                $rangeForm = jQuery('div#modal-report').find('div.report-table:eq(0)').clone();
                $rangeForm.find('strong').html('Table ' + $rangeFormCtr);
                $rangeForm.find('input').val('').removeClass('hasDatepicker').removeAttr('id');
                jQuery('div#modal-report').find('div.modal-body').append($rangeForm);

                if ($rangeFormCtr > 1) {
                    jQuery('button.report-remove-date-btn').show();
                }

                if ($rangeFormCtr == 4) {
                    jQuery('button.report-add-date-btn').hide();
                }
            }
        });

        jQuery('button.report-remove-date-btn').on('click', function () {
            if ($rangeFormCtr > 1) {
                $rangeFormCtr--;

                jQuery('div#modal-report').find('div.modal-body > div.report-table:eq('+$rangeFormCtr+')').remove();

                if ($rangeFormCtr == 1) {
                    jQuery('button.report-remove-date-btn').hide();
                }

                if ($rangeFormCtr < 4) {
                    jQuery('button.report-add-date-btn').show();
                }
            }
        });

        // fix for firefox
        $('#modal-report').on('show.bs.modal', function() {
            $.fn.modal.Constructor.prototype.enforceFocus = function () {
            };
        });


        // modal report
        var $dateFormat = "yy-mm-dd";
        var $from, $to;
        jQuery(document).on('focus', 'input.from',function(e) {
            e.preventDefault();
            jQuery(this)
                .datepicker({
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dateFormat: $dateFormat
                })
                .on( "change", function() {
                    jQuery(this).next().find('input.to').datepicker( "option", "minDate", getDate( this ) );
                });
        });
        
        jQuery(document).on('focus', 'input.to', function (e){
            e.preventDefault();
            jQuery(this).datepicker({
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
                dateFormat: $dateFormat
            })
            .on( "change", function() {
                jQuery(this).prev().find('input.from').datepicker( "option", "maxDate", getDate( this ) );
            });
        });
        
        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }
        
            return date;
        }
    });
</script>
