<?php if(!isset($status) || $status){ ?>
<div class="take-survey form" >

<?php echo $this->Form->create('Survey', array('action' => 'verify/'.h($survey['Survey']['slug']))); ?>
    
	
		<p><?php echo $survey['Survey']['welcome_message']; ?></p>
        <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-3">
	<?php
                echo $this->Form->input('slug', array('type'=>'hidden', 'value'=>$survey['Survey']['slug']));
		if($survey['Survey']['email_verification_on']){
			echo $this->Form->input('email', array('required'=>'required','class'=>'form-control','div'=>array('class'=>'form-group input email'), 'label'=>'<span style="margin-right: 5px;">Email</span><a href="#" data-toggle="tooltip" title="'.$tooltipHover.'" class="email-popupLink"><img src="'.$this->webroot.'images/questionmark.png"/></a>'));			
		}
		echo $this->Form->input('password', array('type' => 'password','class'=>'form-control','div'=>array('class'=>'form-group input password')));
	?>
	
        </div>
    </div>
       
<?php echo $this->Form->end(array('label' => 'Validate', 'class' => 'btn btn-success')); ?>
</div>
<?php if($survey['Survey']['email_verification_on']){ ?>
<div class="tooltip">
   <div class="tooltip-inner">
     Tooltip!
   </div>
   <div class="tooltip-arrow"></div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $tooltipTitle; ?></h4>
      </div>
      <div class="modal-body">
        <?php echo $tooltipContent; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">I understand</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$('.email-popupLink').click(function(){
		$('#myModal').modal('show');
	});
</script>

<?php } ?>

<?php }?>