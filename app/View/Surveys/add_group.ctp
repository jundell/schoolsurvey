<div class="groups form">
<?php echo $this->Form->create('Group'); ?>
    <legend><?php echo __('Add Group'); ?></legend>
    <?php echo $this->Form->input('survey_id', array('type'=>'hidden', 'value'=>$survey_id)); ?>
    <div class="add_more_fieldsets" data-fields="name">
        <fieldset class="add_more form-group">
			<?php echo $this->Form->input('name', array('label'=>'Name','class' => 'form-control','name'=>'name[0]')); ?>
            <a href="#" class="btn btn-default add_another">Add another group</a>
        </fieldset>
    </div>
<?php echo $this->Form->end(__(array('label' => 'Save', 'class' => 'btn btn-default'))); ?>
</div>

<div class="related">
    <br /> 
    <?php if(!sizeof($grouplist)){ ?>
    <p>No groups added.</p>
    <?php } ?>

	<?php if ($grouplist): ?>
    <table class="table table-hover" cellpadding="0" cellspacing="0" >
        <caption><?php echo __('Groups'); ?></caption>
        <tr>
            <th><?php echo __('Id'); ?></th>
            <th><?php echo __('Name'); ?></th>

            <th class="actions"><?php echo __('Actions'); ?></th>

        </tr>
	<?php
        foreach ($grouplist as $k=>$v): ?>
            <tr>
                <td><?php echo $k; ?></td>

                <td><?php echo $v; ?></td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Edit'), array('controller' => 'groups', 'action' => 'edit', $k,$survey_id)); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'groups', 'action' => 'delete', $k,$survey_id), null, __('Are you sure you want to delete # %s?', $k)); ?>
                </td>
            </tr>
	<?php endforeach; ?>
    </table>
<?php endif; ?>

</div>