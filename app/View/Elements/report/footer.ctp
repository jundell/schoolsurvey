<footer>
    <div class="container">
        <div class="row">
<!--            <div class="col-md-1">
                <div class="row">
                    <img src="<?php echo FULL_BASE_URL.$report_settings['report_logo'];?>" alt="" class="logo" />
                </div>end row
            </div>end col-md-12 -->
            <div class="col-md-12"> 
                <div class="footer-line-left"><p><span><?php echo $result['survey']['Survey']['name'];?></span></p></div>
                <div class="footer-line"></div><!--end line-->
            </div>
        </div><!--end row-->
         
            <?php 
                
                $queryReportRange = '';
                if (count($result['reportByRanges'])) {
                    foreach ($result['reportByRanges'] as $reportRange) {
                        $queryReportRange .= 'from[]=' . $reportRange['from'] . '&';
                        $queryReportRange .= 'to[]=' . $reportRange['to'] . '&';
                    }
                }
            ?>

             <a href="<?php echo $this->webroot; ?>surveys/download_report/<?php echo $result['survey']['Survey']['id'];?>/<?php echo $result['reportType'];?>?<?php echo $queryReportRange;?>" class="btn btn-success" id="export_word">Export to MS Word Document</a>
    </div><!--end container-->
</footer><!--end header-->