<?php

?>
<p><span><?php echo $report_settings['table1_title']; ?></span> <?php echo $report_settings['table1_text']; ?></p>
<div class="table-responsive">
    
    <?php 
        // get years
        $years = array_keys($result['dimensionResultsYearly']);
        sort($years);
        $currentYear = end($years);
        $reportByRanges = $result['reportByRanges'];
        $table1_labels = ["a","b","c","d"];
    ?>
    <!-- show first year survey -->
    <table class="table-1 table">
        <thead>
            <tr>
                <th colspan="<?php echo sizeof($result['groups'])*2 + 3; ?>" class="theader1">Table 1-<?php echo $table1_labels[0]; ?> - <?php echo date('F d, Y', strtotime($reportByRanges[0]['from'])) . ' - ' . date('F d, Y', strtotime($reportByRanges[0]['to'])) . ' (Table 1a)'; ?></th>
                <th class="theader1">Admin</th>
                <th class="theader1">Overall</th>
            </tr>
            <tr>
                <th></th>
                <?php foreach($result['groups'] as $key=>$group){ ?>
                    <th colspan="2"><?php echo $group['Group']['name']; ?></th>
                <?php } ?>
                <th colspan="2">Total</th>
                <th>Admin</th>
                <th>Difference</th>
            </tr>
            <tr>
                <th>Dimension</th>
                <?php $groupSize = sizeof($result['groups']); ?>
                <?php while($groupSize>=0){ ?>
                    <th class="tbg-gray">Ave Score</th>
                    <th># of Respondents</th>
                    <?php $groupSize--; ?>
                <?php } ?>
                <th class="tbg-gray"></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($result['dimensionResultsYearly'][0])) { ?>
            <?php foreach($result['dimensions'] as $k=>$d){ ?>
            <?php $dimensionRes = $result['dimensionResultsYearly'][0][$d['SurveyTheme']['id']];?>
                <?php if($dimensionRes['Total']['noOfResp']){?>
                    <tr>
                        <td style="color:<?php echo $d['SurveyTheme']['color'];?>"><?php echo $dimensionRes['name']; ?></td>
                        <?php foreach($dimensionRes['groups'] as $kg=>$g){ ?>
                            <?php if(!empty($g['name'])) { ?>
                                <td class="tbg-gray"><?php echo $g['avg']; ?></td>
                                <td><?php echo $g['noOfResp']; ?></td>
                            <?php } ?>    
                        <?php } ?>
                        <td class="tbg-gray"><?php echo $dimensionRes['Total']['avg']; ?></td>
                        <td><?php echo $dimensionRes['Total']['noOfResp']; ?></td>
                        <td class="tbg-gray"><?php echo $dimensionRes['AdminAvg']; ?></td>
                        <td><?php echo $dimensionRes['AwarenessQuotient']; ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="13"><span class="color-red">No records found.</span></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <!-- end -->

    <!-- start multi year -->
    <?php
        foreach ($reportByRanges as $rangeKey => $date) { 
            $resultData = $result['dimensionResultsYearly'][$rangeKey];
            
            // only show table if we have other year to compare
            if (isset($reportByRanges[$rangeKey + 1])) {
    ?>
    <table class="table-1 table">
        <thead>
            <tr>
                <th colspan="<?php echo sizeof($result['groups'])*3 + 7; ?>" class="theader1">
                    Table 1-<?php echo $table1_labels[$rangeKey+1]; ?> - <?php echo date('F d, Y', strtotime($date['from'])) .'-'. date('F d, Y', strtotime($date['to'])) .' (Table 1' . $table1_labels[$rangeKey].') | ' . date('F d, Y', strtotime($reportByRanges[$rangeKey + 1]['from'])) .'-'. date('F d, Y', strtotime($reportByRanges[$rangeKey + 1]['to'])) . ' (Table 1'.$table1_labels[$rangeKey+1].')'; ?>
                </th>
                <!-- <th class="theader1">Admin</th>
                <th class="theader1">Overall</th> -->
            </tr>
            <tr>
                <th></th>
                <?php foreach($result['groups'] as $key=>$group){ ?>
                    <th colspan="3"><?php echo $group['Group']['name']; ?></th>
                <?php } ?>
                <th colspan="3">Total</th>
            </tr>
            <tr>
                <th>Dimension</th>
                <?php $groupSize = sizeof($result['groups']); ?>
                <?php while($groupSize>=0){ ?>
                    <th class="tbg-gray">
                        Ave Table 1<?php echo $table1_labels[$rangeKey]; ?>
                    </th>
                    <th>
                        <?php // # of Respondents ?>
                        <?php echo 'Ave Table 1' . $table1_labels[$rangeKey+1]; ?>
                    </th>
                    <th>
                        Change
                    </th>
                    <?php $groupSize--; ?>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php if (count($resultData) || count($result['dimensionResultsYearly'][$rangeKey + 1])) { ?>
            <?php foreach($result['dimensions'] as $k => $d){ ?>
            <?php 
                $dimensionRes = $resultData[$d['SurveyTheme']['id']]; 
                $currentYearDimensions = $result['dimensionResultsYearly'][$rangeKey + 1][$d['SurveyTheme']['id']];
            ?>
                <?php if($dimensionRes || $currentYearDimensions){ ?>
                    <tr>
                        <td style="color:<?php echo $d['SurveyTheme']['color'];?>">
                            <?php //dimension name ?>
                            <?php echo ($currentYearDimensions['name']) ? $currentYearDimensions['name'] : $dimensionRes['name']; ?>
                        </td>
                        <?php if ($dimensionRes) { // check if the previous data has no dimension answers ?>
                            <?php foreach($dimensionRes['groups'] as $kg => $g){ ?>
                                <?php if(!empty($g['name'])) { ?>
                                    <td class="tbg-gray">
                                        <?php echo $g['avg']; ?>
                                    </td>
                                    <td>
                                        <?php //echo $g['noOfResp']; ?>
                                        <?php 
                                            echo (isset($currentYearDimensions['groups'][$kg]['avg'])) ? $currentYearDimensions['groups'][$kg]['avg'] : 0;
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            echo (isset($currentYearDimensions['groups'][$kg]['avg'])) ? $g['avg'] - $currentYearDimensions['groups'][$kg]['avg'] : 0;
                                        ?>
                                    </td>
                                <?php } ?>    
                            <?php } ?>
                        <?php }else{ // then make all data to 0 ?>
                            <?php foreach($currentYearDimensions['groups'] as $kg => $g){ ?>
                                <?php if(!empty($g['name'])) { ?>
                                    <td class="tbg-gray">
                                        <?php echo 0; ?>
                                    </td>
                                    <td>
                                        <?php //echo $g['noOfResp']; ?>
                                        <?php 
                                            echo (isset($currentYearDimensions['groups'][$kg]['avg'])) ? $currentYearDimensions['groups'][$kg]['avg'] : 0;
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            echo (isset($currentYearDimensions['groups'][$kg]['avg'])) ? 0 - $currentYearDimensions['groups'][$kg]['avg'] : 0;
                                        ?>
                                    </td>
                                <?php } ?>    
                            <?php } ?>
                        <?php } ?>
                        <td class="tbg-gray">
                            <?php
                                $tmpTotalAvg = 0;
                                if (isset($dimensionRes['Total']['avg'])) {
                                    $tmpTotalAvg = $dimensionRes['Total']['avg']; 
                                }

                                echo $tmpTotalAvg;
                            ?>
                        </td>
                        <td>
                            <?php //echo $dimensionRes['Total']['noOfResp']; ?>
                            <?php echo (isset($currentYearDimensions['Total']['avg'])) ? $currentYearDimensions['Total']['avg'] : 0; ?>
                        </td>
                        <td>
                            <?php echo (isset($currentYearDimensions['Total']['avg'])) ? $tmpTotalAvg - $currentYearDimensions['Total']['avg'] : 0; ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="16"><span>No records found.</span></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <?php } } ?>
</div>