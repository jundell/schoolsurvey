<p><span><?php echo $report_settings['table2_title']; ?></span> <?php echo $report_settings['table2_text']; ?></p>

<?php foreach($result['dimensions'] as $k=>$d){ ?>
    <?php $dimensionRes = $result['dimensionsResult'][$d['SurveyTheme']['id']]; ?>
    <?php if($dimensionRes['Total']['noOfResp']){?>
<div class="icon-holder">
            <?php if($d['SurveyTheme']['icon']){ ?>
    <img src="<?php echo APP_URL.$d['SurveyTheme']['icon'];?>" alt="" />
                <?php }else{ ?>
    <img src="<?php echo APP_URL.'files/uploads/theme_icons/icon-default.png';?>" alt="" />
                <?php } ?>
</div>
<div class="text-holder">
    <h4 style="color:<?php echo $d['SurveyTheme']['color'];?>"><?php echo $d['SurveyTheme']['name'];?>:</h4>
</div>
<div class="clearfix"></div>
<div class="table-responsive table2-table">
    <table class="table">
        <thead>
            <tr>
                <th class="tbg-gray">Respondent</th>
                <th class="tbg-gray"></th>
                        <?php $i=1; ?>
                        <?php while($i<=$dimensionRes['ResCeiling']){ ?>
                <th class="tbg-gray score-th"><?php echo $i; ?></th>
                            <?php $i++; ?>
                        <?php } ?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Self</td>
                <td><?php echo $dimensionRes['AdminAvg']; ?></td>
                <td colspan="<?php echo $i; ?>">
                    <div class="progress">
                        <div class="progress-bar progress-bar-darkgray" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $dimensionRes['AdminAvg']/$dimensionRes['ResCeiling']*100; ?>%">
                            <!--<span class="sr-only">80% Complete (danger)</span>-->
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Overall Average</td>
                <td><?php echo $dimensionRes['Total']['avg']; ?></td>
                <td colspan="<?php echo $i; ?>">
                    <div class="progress">
                        <div class="progress-bar progress-bar-darkgray" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $dimensionRes['Total']['avg']/$dimensionRes['ResCeiling']*100; ?>%">
                            <!--<span class="sr-only">45% Complete (success)</span>-->
                        </div>
                    </div>
                </td>
            </tr>
                    <?php foreach($dimensionRes['groups'] as $kg=>$g){ ?>
            <tr>
                <td><?php echo $g['name']; ?></td>
                <td><?php echo $g['avg']; ?></td>
                <td colspan="<?php echo $i; ?>">
                    <div class="progress">
                        <div class="progress-bar progress-bar-lightgray" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $g['avg']/$dimensionRes['ResCeiling']*100; ?>%">
                            <!--<span class="sr-only">20% Complete</span>-->
                        </div>
                    </div>
                </td>
            </tr>
                        <?php } ?>

        </tbody>
    </table>
</div><!--end first table -->
    <?php } ?>
<?php } ?>