<nav class="navbar navbar-fixed-top navbar-inverse report-navbar">
    <div class="navbar-inner">
        <div class="container">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button> 
            </div>
            <?php $surveyAction = $this->request->params['action']; ?>
            <?php $surveyController = $this->request->params['controller']; ?>
            <div class="navbar-collapse collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <li<?php echo $surveyController=='surveys' && $surveyAction=='index'?' class="active"':'';?>><a href="<?php echo $this->webroot; ?>surveys">Surveys</a></li>
            <?php if ($access->isLoggedin()) { ?>
                    <li<?php echo $surveyController=='surveys' && $surveyAction=='add'?' class="active"':'';?>> <a href="<?php echo $this->webroot; ?>surveys/add">Add</a></li>
                    <li<?php echo $surveyController=='surveys' && $surveyAction=='upload_survey'?' class="active"':'';?>> <a href="<?php echo $this->webroot; ?>surveys/upload_survey">Upload</a></li>
            <?php } ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
            <?php if ($access->isLoggedin()) { ?>
                    <li<?php echo $surveyController=='account' && $surveyAction=='index'?' class="active"':'';?>><a href="<?php echo $this->webroot; ?>account/">Account</a></li>
                    <li<?php echo $surveyController=='account' && $surveyAction=='logout'?' class="active"':'';?>><a href="<?php echo $this->webroot; ?>account/logout">Logout</a></li>
            <?php } ?>
                </ul>
            </div><!-- /.nav-collapse -->
        </div><!-- /.container -->
    </div>
</nav>