<header id="report_header">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-sm-1 col-xs-12 logo-wrap"> 
                        <img src="<?php echo FULL_BASE_URL.$report_settings['report_logo'];?>" alt="" class="logo" />
                    </div>
                    <div class="line col-sm-11 col-xs-12">
                        <p><span><?php echo date("F j, Y"); ?></span></p>
                    </div><!--end line-->
                </div><!--end row-->
            </div><!--end col-md-12-->
            <div class="col-md-12"> 
                <p class="preport-text"><span><?php echo $result['survey']['Survey']['name'];?></span></p> 
            </div><!--end col-md-12-->    
        </div><!--end row-->
    </div><!--end container-->
</header><!--end header-->