<h6><?php echo $report_settings['dimensions_text']; ?></h6>
            
<?php $dimensionsDivs = array_chunk($result['dimensions'], ceil(count($result['dimensions']) / 2)); ?>
<?php foreach($dimensionsDivs as $dimensions){ ?>
    <div class="col-md-6">
        <div class="row">
        <?php foreach($dimensions as $dimension){ ?>
            <div class="icon-holder">
                <?php 
                    $basename = explode('/', strtolower($dimension['SurveyTheme']['name']));
                    $basename = preg_replace('/[\W]+/', '-', $basename[0]);
                ?>
                <?php if($dimension['SurveyTheme']['icon']){ ?>
                    <?php if( file_exists( APP.'webroot'.$dimension['SurveyTheme']['icon'] ) ){ ?>
                        <img src="<?php echo APP_URL.$dimension['SurveyTheme']['icon'];?>" alt="" />

                    <?php }elseif ( file_exists(APP.'webroot'.preg_replace('/-survey[\d]*\./', '.', $dimension['SurveyTheme']['icon'])) ) { ?>
                        <img src="<?php echo APP_URL.preg_replace('/-survey[\d]*\./', '.', $dimension['SurveyTheme']['icon']);?>" alt="" />

                    <?php }elseif ( file_exists(APP.'webroot'.dirname($dimension['SurveyTheme']['icon']).'/icon-'.$basename.'.png') ) { ?>
                        <img src="<?php echo APP_URL.dirname($dimension['SurveyTheme']['icon']).'/icon-'.$basename.'.png';?>" alt="" />

                    <?php }else{ ?>
                        <img src="<?php echo APP_URL.'files/uploads/theme_icons/icon-default.png';?>" alt="" />
                    <?php } ?>
                <?php }else{ ?>
                    <img src="<?php echo APP_URL.'files/uploads/theme_icons/icon-default.png';?>" alt="" />
                <?php } ?>
            </div>
            <div class="text-holder">
                <h4 style="color:<?php echo $dimension['SurveyTheme']['color'];?>"><?php echo $dimension['SurveyTheme']['name'] ?><?php echo $dimension['SurveyTheme']['description']?':':'';?></h4>
                <p><?php echo $dimension['SurveyTheme']['description'] ?></p>
            </div>

            <div class="clearfix"></div>

        <?php } ?>
        </div><!--end row-->
    </div><!--end col-md-6-->
<?php } ?>
<div class="clearfix"></div>