<div class="uqGroups form">
<?php echo $this->Form->create('UqGroup'); ?>
	<fieldset>
		<legend><?php echo __('Add Uq Group'); ?></legend>
	<?php
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Uq Groups'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Question Groups'), array('controller' => 'question_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question Group'), array('controller' => 'question_groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List User Groups'), array('controller' => 'user_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Group'), array('controller' => 'user_groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
