<div class="questionsOptions form">
<?php echo $this->Form->create('QuestionsOption'); ?>
    <legend><?php echo __('Add Question Option(s)'); ?></legend>
    <div class="add_more_fieldsets" data-fields="name">
	<fieldset class="add_more">
		
	<?php
                echo $this->Form->input('name', array('label'=>'Option','name'=>'name[0]'));
	?>
            <a href="#" class="btn add_another" data->Add another option</a>
	</fieldset>
    </div>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="related">
        <?php echo __('Options'); ?>
	
	<table class="table table-hover" cellpadding="0" cellspacing="0" >
	<tr>
		<th><?php echo __('#'); ?></th>
		<th><?php echo __('Name'); ?></th>
                <th><?php echo __('Score'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>

	</tr>
        <?php if ($options): ?>
	<?php
		$cnt=1;
		foreach ($options as $k=>$v): ?>
		<tr>
			<td><?php echo $cnt; ?></td>

			<td><?php echo $v['QuestionsOption']['name']; ?></td>
                        
                        <td class="option-points"><?php echo $v['QuestionsOption']['score']; ?></td>
                        
                        <td class="actions">
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'questions_options', 'action' => 'edit', $v['QuestionsOption']['id'], $question['Question']['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions_options', 'action' => 'delete', $v['QuestionsOption']['id'], $question['Question']['id']), null, __('Are you sure you want to delete # %s?', $v['QuestionsOption']['id'])); ?>
			</td>
		</tr>
                <?php $cnt++; ?>
	<?php endforeach; ?>
                <?php endif; ?>
                    <tr>
			<td><?php echo $cnt; ?></td>
			<td><?php if($question['Question']['include_no_response']){ echo $question['Question']['no_response_option']; } ?></td>
                        <td class="option-points-na"><?php if($question['Question']['include_no_response']){ ?> N.A. <?php } ?></td>
                        <td>
                            <?php echo $this->Form->create('QuestionsOption', array('action' => 'include_no_response/'.$question['Question']['id'])); ?>
                                <div class="input checkbox option-no-response" id="question-noresponse" style="margin-top: 0;">
                                    <?php echo $this->Form->input('include_no_response',array('type'=>'checkbox','div'=>false, 'label'=>'include no response option', 'checked' => $question['Question']['include_no_response'])); ?>
                                    <div id="edit-nr-option" style="display:none;">
                                        <a href="#" id="edit-noresponseoption">Edit Option</a>
                                        <div id="no-response-option" style="display:none;">
                                            <?php echo $this->Form->input('no_response_option', array('label'=>false, 'value'=>$question['Question']['no_response_option']?$question['Question']['no_response_option']:'Have not observed/I don’t know')); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php echo $this->Form->end(__('Submit')); ?>
                        </td>
                    </tr>
	</table>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        if($("input#QuestionsOptionIncludeNoResponse").is(':checked')){
            $("#edit-nr-option").show();
        }else{
            $("#edit-nr-option").hide();
        }
    });
    $('input#QuestionsOptionIncludeNoResponse').click(function() {
       $("#edit-nr-option").toggle(this.checked);
    });
    $('#edit-noresponseoption').click(function(e){
        e.preventDefault();
        $('#no-response-option').toggle();
    });
</script>
