<div class="questions form">
<?php echo $this->Form->create('Question'); ?>
    <fieldset>
        <legend><?php echo __('Edit Question'); ?></legend>
        <div class="row">
        <div class="col-xs-12 col-sm-4">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('survey_id', array('type'=>'hidden'));
		echo $this->Form->input('name', array('type'=>'textarea', 'label'=>'Question','div'=>array('class'=>'form-group input textarea'),'class'=>'form-control','maxlength'=>5000));
                echo $this->Form->input('survey_theme_id', array(
                            'options' => $themes,
                            'class'=>'form-control',
                            'div'=>array('class'=>'form-group input text'),
                                                'label' => 'Theme'
                        ));
                echo $this->Form->input('type', array(
                            'div'=>array('class'=>'form-group input text'),
                            'options' => $types,'class'=>'form-control'
                        ));
	?>
        <div class="input checkbox " id="non-calculating">
                    <?php 
                        $is_calculating = true;
                        if(isset($this->request->data['Question']['is_calculating'])){
                            $is_calculating = $this->request->data['Question']['is_calculating']?true:false;
                        }
                    ?>
                    <?php echo $this->Form->input('is_calculating',array('type'=>'checkbox','div'=>false, 'label'=>'Calculating', 'checked'=>$is_calculating)); ?>
        </div>

        </div>
        </div>
    </fieldset>
    <div class="add_more_fieldsets" data-fields="option,score">
                <?php if($this->request->is('post') || $this->request->is('put')){ ?>
                <?php
                    $options = $this->request->data['option'];
                    $scores = $this->request->data['score'];
                ?>
                <?php if(isset($options)&& sizeof($options)){ ?>
                    <?php $size = sizeof($options); ?>
                    <?php $cnt = 1; ?>
                    <?php foreach($options as $k=>$o){ ?>
                        <fieldset class="add_more form-group">
                        <?php
                                echo $this->Form->input('option', array('label'=>'Option','type'=>'text','class'=>'form-control','name'=>'option['.$k.']', 'value'=>$o));
                                echo $this->Form->input('score', array('label'=>'Score','name'=>'score['.$k.']','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>$scores[$k]));
                        ?>
                            
                            <?php if($optionEditable){ ?>
                             <?php if($cnt==$size){ ?>
                                    <a href="#" class="btn btn-default add_another" data-lastId="<?php echo $k+1; ?>">Add another option</a>
                                <?php } ?>
                                <?php if($cnt>2){ ?>
                                    <a class="btn btn-default remove" href="#">X</a>
                                <?php } ?>
                                <?php $cnt++; ?>
                            <?php } ?>
                        </fieldset>

                    <?php } ?>
                <?php }}else{ ?>
                    <?php if(isset($options_data)&& sizeof($options_data)){ ?>
                        <?php $size = sizeof($options_data); ?>
                        <?php $cnt = 1; ?>
                        <?php foreach($options_data as $k=>$o){ ?>
                            <fieldset class="add_more form-group">
                            <?php
                                    echo $this->Form->input('option', array('label'=>'Option','type'=>'text','class'=>'form-control','name'=>'option['.$o['QuestionsOption']['id'].']', 'value'=>$o['QuestionsOption']['name']));
                                    echo $this->Form->input('score', array('label'=>'Score','name'=>'score['.$o['QuestionsOption']['id'].']','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>$o['QuestionsOption']['score']));
                            ?>
                                <?php if(!sizeof($this->request->data['SurveyAnswer'])){ ?>
                                 <?php if($cnt==$size){ ?>
                                    <a href="#" class="btn btn-default add_another" data-lastId="<?php echo $o['QuestionsOption']['id']+1; ?>">Add another option</a>
                                <?php } ?>
                                <?php if($cnt>2){ ?>
                                    <a class="btn btn-default remove" href="#">X</a>
                                <?php } ?>
                                <?php $cnt++; ?>
                                <?php } ?>
                            </fieldset>

                        <?php } ?>
                    <?php } ?>
            <?php } ?>
        </div>
        <!-- groups -->
        <div>
            <button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#group-modal-own">Groups</button>
            <div class="modal fade" id="group-modal-own" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Select Groups</h4>
                        </div>
                        <div class="modal-body">
                            <?php foreach ($groups as $groupId => $groupName) { ?>  
                                    <label>
                                        <?php
                                            if (count($questionGroups)) {  
                                        ?>
                                        <input <?php echo (in_array($groupId, $questionGroups)) ? 'checked="checked"' : ''; ?>  type="checkbox" value="<?php echo $groupId; ?>" name="checkbox_groups[]" /> <?php echo $groupName ?>  
                                        <?php }  else { ?>
                                        <input type="checkbox" value="<?php echo $groupId; ?>" name="checkbox_groups[]" checked="checked" /> <?php echo $groupName ?>
                                        <?php } ?>
                                    </label>
                                    <br />
                            <?php } ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div><br />
<?php echo $this->Form->end(__(array('label' => 'Save', 'class' => 'btn btn-success'))); ?>
</div>
<br /><br />
<?php echo $this->Html->link(__('Go back to Questions List'), array('controller' => 'questions', 'action' => 'add/'.$surveyId), array('class'=>'btn btn-default')); ?>
<br /><br />
<?php echo $this->element('view_manage_survey_btn'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        if($("input#QuestionIsCalculating").is(':checked')){
            $('div[data-fields="option,score"] div.input.number').show();
        }else{
            $('div[data-fields="option,score"] div.input.number').hide();
        }
        if($('select[name="data[Question][type]"]').val()==="open ended"){
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }
        
        $(".option-score").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               //display error message
               $("#errmsg").html("Positive numbers only").show().fadeOut("slow");
                      return false;
           }
          });
    });
    $('select[name="data[Question][type]"]').change(function() {
        if ($(this).val() === "open ended") {
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }
    })
    $('input#QuestionIncludeNoResponse').click(function() {
        $("#no-response-option").toggle(this.checked);
    })
    $('input#QuestionIsCalculating').click(function() {
        if($(this).is(':checked')){
            $('div[data-fields="option,score"] div.input.number').show();
        }else{
            $('div[data-fields="option,score"] div.input.number').hide();
        }
    });
</script>
