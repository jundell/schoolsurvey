<?php $page = $this->params->params['named']['page'];
$page = $page ? "/prev:".$page : "/prev:1";
?>
<?php if(!$surveyStatus){ ?>
<div class="row">
    <div class="col-xs-12">
        <?php if(sizeof($standardThemes)){ ?>
        <legend>Choose from these standard descriptors-questions.</legend>
        <?php echo $this->Form->create('Question', array('type'=>'file')); ?>
        <div class="survey-buttons">
            <?php echo $this->Html->link(__('Check All'), '#' , array('type'=>'button','class'=>'check-all btn btn-default', 'data-checkbox'=>'standard-question', 'style'=>'margin-top: 30px;')); ?>
            <?php echo $this->Html->link(__('Uncheck All'), '#' , array('type'=>'button','class'=>'uncheck-all btn btn-default', 'data-checkbox'=>'standard-question', 'style'=>'margin-top: 30px;')); ?>
        </div>
        <br />
        <?php }else{ ?>
        <legend>Add Question</legend>
                 <?php echo $this->Form->create('Question', array('type'=>'file')); ?>
        <?php }?>

        <?php
            //questions in request data
            $questions = isset($this->request->data['question'])?$this->request->data['question']:array();
            $cnt = 0; 
        ?>
        <?php foreach($standardThemes as $standardTheme) { ?>
        <div>
                <?php $icon = isset($themeIcons[$standardTheme['StandardTheme']['name']])&&$themeIcons[$standardTheme['StandardTheme']['name']]?APP_URL.$themeIcons[$standardTheme['StandardTheme']['name']]:APP_URL.'files/uploads/theme_icons/icon-default.png'; ?>
            <img class="standard-theme-icon img-responsive" src="<?php echo $icon; ?>" /><span class="standard-theme-name" style="color:#<?php echo $standardTheme['StandardTheme']['color'] ?>;"><?php echo $standardTheme['StandardTheme']['name']; ?></span>
        </div>
            <?php foreach($standardTheme['StandardQuestion'] as $question){ ?>
            <?php
                //exists in $questions
                $qExist = array_key_exists ($cnt , $questions );     
            ?>
            <?php // var_dump($question); ?>
        <div class="checkbox">
            <label>
                    <?php if(in_array($question['StandardQuestion']['name'], $questionDatalist)){ ?>
                            <?php //if question already exist in survey data ?>
                <input type="checkbox" checked="checked" disabled="disabled" class="standard-question" name="question[<?php echo $cnt; ?>]" value="<?php echo $question['StandardQuestion']['id']; ?>"/>
                            <?php }else{ ?>

                <input type="checkbox" <?php echo $qExist?'checked="checked"':'' ?> class="standard-question" name="question[<?php echo $cnt; ?>]" value="<?php echo $question['StandardQuestion']['id']; ?>"/>
                            <?php  }?>
                            <?php echo $question['StandardQuestion']['name']; ?> <a href="#" data-toggle="modal" data-target="#qDetails_<?php echo $question['StandardQuestion']['id']; ?>">View Details</a>
            </label>
        </div>
        <!-- Question Details -->
        <div class="modal fade" id="qDetails_<?php echo $question['StandardQuestion']['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $question['StandardQuestion']['name']; ?>">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo $question['StandardQuestion']['name']; ?></h4>
                    </div>
                    <div class="modal-body">
                        <p><strong>Type: </strong><?php echo $question['StandardQuestion']['type']; ?></p>
                        <p><strong>Calculating: </strong><?php echo $question['StandardQuestion']['is_calculating']?'yes':'no'; ?></p>
                            <?php if(in_array($question['StandardQuestion']['type'], array('likert','radio button')) && sizeof($question['StandardQuestionOption'])){ ?>
                        <p><strong>Options:</strong></p>
                        <div class="table-responsive">
                            <table class="table table-hover" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Option</th>
                                            <?php if($question['StandardQuestion']['is_calculating']){  ?>
                                    <th>Score</th>
                                            <?php } ?>
                                </tr>
                                        <?php foreach($question['StandardQuestionOption'] as $option){ ?>
                                <tr>
                                    <td><?php echo $option['name'] ?></td>
                                                <?php if($question['StandardQuestion']['is_calculating']){  ?>
                                    <td><?php echo $option['score'] ?></td>
                                                <?php } ?>
                                </tr>
                                        <?php } ?>
                            </table>
                        </div>
                            <?php } ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
                <?php $cnt++; ?>
                <?php } ?>
        <hr />
        <?php } ?>

        <br />
    <?php
        //own theme in request data
            $ownQuestion = isset($this->request->data['ownQuestion'])?$this->request->data['ownQuestion']:array();
    ?>
        <div id="ownQuestion">
            <?php if(sizeof($standardThemes)){ ?>
            <strong>Add your own descriptor-question</strong>
            <?php } ?>
            <input name="add_another_question" class="add_another_question" value="0" type="hidden">
            <div class="choose-theme-add">
                <fieldset class="form-theme">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                        <?php
                            echo $this->Form->input('name', array('type'=>'textarea', 'required'=>false,'label'=>'Descriptor-Question','name'=>'ownQuestion[name]', 'div'=>array('class'=>'form-group input textarea'),'class'=>'form-control','maxlength'=>5000,'default'=>isset($ownQuestion['name'])?$ownQuestion['name']:''));
                                        echo $this->Form->input('survey_theme_id', array(
                                            'options' => $themes,
                                                                'class'=>'form-control',
                                                                'div'=>array('class'=>'form-group input text'),
                                                                'label' => 'Dimension',
                                            'name'=>'ownQuestion[survey_theme_id]',
                                            'default'=>isset($ownQuestion['survey_theme_id'])?$ownQuestion['survey_theme_id']:''
                                        ));
                            echo $this->Form->input('type', array(
                                            'options' => $types,
                                            'div'=>array('class'=>'form-group input select'),
                                            'class'=>'form-control',
                                            'name'=>'ownQuestion[type]',
                                            'default'=>isset($ownQuestion['type'])?$ownQuestion['type']:''
                                        ));

                        ?>
                            <div class="input checkbox " id="non-calculating">
                                    <?php 
                                        $is_calculating = true;
                                          if(isset($ownQuestion['is_calculating'])){
                                            $is_calculating = $ownQuestion['is_calculating']?true:false;
                                        }
                                    ?>
                                    <?php echo $this->Form->input('is_calculating',
                                            array(
                                                'type'=>'checkbox',
                                                'div'=>false, 
                                                'label'=>'Calculating', 
                                                'checked'=>$is_calculating, 
                                                'name'=>'ownQuestion[is_calculating]',
//                                                'default'=>isset($ownQuestion['is_calculating'])?$ownQuestion['is_calculating']:''
                                                )); ?>
                            </div>

                        </div>
                    </div>
                </fieldset>
                <div class="add_more_fieldsets" data-fields="ownQuestion[option],ownQuestion[score]">
                <?php if($this->request->is('post')){ ?>
                <?php
                    $options = $this->request->data['ownQuestion']['option'];
                    $scores = $this->request->data['ownQuestion']['score'];
                ?>
            <?php if(isset($options)&& sizeof($options)){ ?>
                <?php $size = sizeof($options); ?>
                <?php $cnt = 1; ?>
                <?php foreach($options as $k=>$o){ ?>
                    <fieldset class="add_more form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-8 col-md-6">
                                        <?php echo $this->Form->input('option', array('label'=>'Option','class'=>'form-control','type'=>'text','name'=>'ownQuestion[option]['.$k.']', 'value'=>$o)); ?>
                                    </div>
                                    <div class="col-xs-4 col-md-2">
                                        <?php echo $this->Form->input('score', array('label'=>'Score','name'=>'ownQuestion[score]['.$k.']','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>$scores[$k])); ?>
                                    </div>
                                    <?php if($cnt==$size){ ?>
                                    <div class="col-xs-8 col-md-3 add-row-btn-wrapper">
                                        <a href="#" class="btn btn-default add_another_row" >Add another option</a>
                                    </div>
                                    <?php } ?>
                                    <?php if($cnt>2){ ?>
                                    <div class="col-xs-2 col-md-1 remove-row-btn-wrapper"><a class="btn btn-default remove-row" href="#">X</a></div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>

                        <?php $cnt++; ?>
                    </fieldset>

                <?php } ?>
            <?php }}else{ ?>
                    <fieldset class="add_more_row form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-8 col-md-6">
                                    <?php echo $this->Form->input('option', array('label'=>'Option','class'=>'form-control','name'=>'ownQuestion[option][0]'));?>
                                    </div>
                                    <div class="col-xs-4 col-md-2">
                                        <?php echo $this->Form->input('score', array('label'=>'Score','name'=>'ownQuestion[score][0]','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>'1'));?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="add_more_row form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-wrap">
                                    <div class="col-xs-8 col-md-6">
                                        <?php echo $this->Form->input('option', array('label'=>'Option','class'=>'form-control','name'=>'ownQuestion[option][1]')); ?>
                                    </div>
                                    <div class="col-xs-4 col-md-2 last-col-input">
                                        <?php echo $this->Form->input('score', array('label'=>'Score','name'=>'ownQuestion[score][1]','class'=>'option-score form-control', 'min'=>'0', 'step'=>'1', 'type'=>'number', 'value'=>'2')); ?>
                                    </div>
                                    <div class="col-xs-8 col-md-3 add-row-btn-wrapper">
                                        <a href="#" class="btn btn-default add_another_row" >Add another option</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
            <?php } ?>
                </div>
            </div>
        </div>
        <br />
        <?php if($build){ ?>
        <a href="#" class="btn btn-default another_question" id="save-and-add">Save and add another descriptor-question</a><br /><br />
            <?php echo $this->Form->end(__(array('label' => 'Finish', 'class' => 'btn btn-success'))); ?>
        <?php }else{ ?>
            <?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-success'))); ?>
        <?php } ?>
    </div>
</div>
<br /><br />
<?php } ?>

<div class="row">
    <div class="col-xs-12">
        <div class="related table-responsive descriptor-tbl"> 
        <?php if(!sizeof($questionslist)){ ?>
            <p><br />No descriptors-questions added.</p>
        <?php } ?>
	<?php if ($questionslist): ?>
            <p><?php echo __('Descriptors-Questions'); ?></p>
            <table class="table table-hover" cellpadding="0" cellspacing="0">
                <tr>
                    <th><?php echo __('#'); ?></th>
                    <th><?php echo __('Descriptor-Question'); ?></th>
                    <th><?php echo __('Type'); ?></th>
                    <th><?php echo __('Dimension'); ?></th>
                    <?php if(!$surveyStatus){ ?>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                    <?php } ?>
                </tr>
                <?php $paginatorParams = $this->Paginator->params(); ?>
                <?php $cnt = ($paginatorParams['page']-1)*$paginatorParams['limit']+1; ?>
	<?php foreach ($questionslist as $question): ?>
                <tr>
                    <td><?php echo $cnt; ?>&nbsp;</td>
                    <td><?php echo h($question['Question']['name']); ?>&nbsp;</td>
                    <td><?php echo h($question['Question']['type']); ?>&nbsp;</td>
                    <td style="color: <?php echo $question['SurveyTheme']['color'] ?>">
                        <?php echo h($question['SurveyTheme']['name']); ?>&nbsp;
                    </td>
                    <?php if(!$surveyStatus){ ?>
                        <td class="actions">
                            <?php if(sizeof($question['SurveyAnswer'])){ ?>
                                <?php /*echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $question['Question']['id']), null, __('Someone has already answered this question. Are you sure you want to edit this descriptor-question?'));*/ ?>  <!-- commented out on 1/15/2018 -->
                                <a href="javascript:void(0);" onclick="if(confirm('Someone has already answered this question. Are you sure you want to edit this descriptor-question?')){window.location='/survey/questions/edit/<?php echo $question["Question"]["id"].$page;?>'}">Edit</a>

                            <?php }else{?>
                                <?php /*echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $question['Question']['id']));*/ ?> <!-- commented out on 1/15/2018 -->
                                <a href="/survey/questions/edit/<?php echo $question['Question']['id'].$page;?>">Edit</a>
                            <?php }?>
                            <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions', 'action' => 'delete', $question['Question']['id']), null, __('Are you sure you want to delete this descriptor-question?')); ?>
                        </td>
                    <?php } ?>
                </tr>
                <?php $cnt++; ?>
        <?php endforeach; ?>
            </table>
            <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
            <div class="paging">
	<?php
                echo $this->Paginator->first('<< first');
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                echo $this->Paginator->last('last >>');
	?>
            </div>
<?php endif; ?>

        </div>
    </div>
</div>

<?php echo $this->element('view_manage_survey_btn'); ?>

<script type="text/javascript">
    $(document).ready(function () {
        var url_path = location.pathname;
        if(url_path.indexOf('page:') != -1){
            $('html, body').animate({
                scrollTop: $(".descriptor-tbl").offset().top
            }, 2000);
        }
        if ($("input#QuestionIsCalculating").is(':checked')) {
            $('div[data-fields="ownQuestion[option],ownQuestion[score]"] div.input.number').parent().show();
        } else {
            $('div[data-fields="ownQuestion[option],ownQuestion[score]"] div.input.number').parent().hide();
        }
        if ($('select[name="ownQuestion[type]"]').val() === "open ended") {
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }

        $(".option-score").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                $("#errmsg").html("Positive numbers only").show().fadeOut("slow");
                return false;
            }
        });
    });
    $('select[name="ownQuestion[type]"]').change(function () {
        if ($(this).val() === "open ended") {
            $('div#non-calculating').hide();
            $('div.add_more_fieldsets').hide();
            $('div#question-noresponse').hide();
        } else {
            $('div#non-calculating').show();
            $('div.add_more_fieldsets').show();
            $('div#question-noresponse').show();
        }
    });
    $('input#QuestionIncludeNoResponse').click(function () {
        $("#no-response-option").toggle(this.checked);
    });
    $('input#QuestionIsCalculating').click(function () {
        if ($(this).is(':checked')) {
            $('div[data-fields="ownQuestion[option],ownQuestion[score]"] div.input.number').parent().show();
        } else {
            $('div[data-fields="ownQuestion[option],ownQuestion[score]"] div.input.number').parent().hide();
        }
    });
</script>

