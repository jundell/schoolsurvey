
<div class="survey-build">
    <?php //survey details ?>
    <h1><?php echo $survey['Survey']['name']; ?></h1>
    <h4>Description</h4>
    <p><?php echo $survey['Survey']['description']; ?></p>
    <h4>Welcome Message</h4>
    <p><?php echo $survey['Survey']['welcome_message']; ?></p>
    <h4>Closing Message</h4>
    <p><?php echo $survey['Survey']['closing_message']; ?></p>

    <?php //groups ?>
    <div class="groups">
        <h3>Groups</h3>
        <ul class="list-unstyled">
        <?php foreach($groups as $group){ ?>
            <li><?php echo $group; ?></li>
        <?php } ?>
        </ul>
    </div>

    <?php //dimensions ?>
    <div class="dimensions">
        <h3>Dimensions</h3>
        <ul class="list-unstyled">
        <?php foreach($themes as $theme){ ?>
            <li><img class="standard-theme-icon img-responsive" src="<?php echo $theme['SurveyTheme']['icon']?APP_URL.$theme['SurveyTheme']['icon']:APP_URL.'files/uploads/theme_icons/icon-default.png'; ?>" /><span class="standard-theme-name" style="color:#<?php echo $theme['SurveyTheme']['color'] ?>;"><?php echo $theme['SurveyTheme']['name']; ?></span></li>
        <?php } ?>
        </ul>
    </div>


    <?php //questions ?>
    <div class="questions">
        <h3>Questions</h3>
        <?php foreach($themes as $theme){ ?>
            <?php if(sizeof($theme['Question'])){ ?>
        <div>
            <img class="standard-theme-icon img-responsive" src="<?php echo $theme['SurveyTheme']['icon']?APP_URL.$theme['SurveyTheme']['icon']:APP_URL.'files/uploads/theme_icons/icon-default.png'; ?>" /><span class="standard-theme-name" style="color:#<?php echo $theme['SurveyTheme']['color'] ?>;"><?php echo $theme['SurveyTheme']['name']; ?></span>
            <ul class="list">
                <?php foreach($theme['Question'] as $question){ ?>
                <li><?php echo $question['name']; ?></li>
                <?php } ?>
            </ul>
        </div>
            <?php } ?>
        <?php } ?>
    </div>

    <?php //respondents ?>
    <div class="respondents table-responsive">

	<?php  if (!empty($respondents)): ?>
        <h3>Respondents</h3>
        <table class="table table-hover" cellpadding="0" cellspacing="0" >
            <tr>
                <th><?php  echo __('Email'); ?></th>
                <th><?php  echo __('IP address'); ?></th>
                <th><?php  echo __('Admin?'); ?></th>
                <th><?php  echo __('Date Taken'); ?></th>


            </tr>
	<?php
		 $i = 0;
		 foreach ($respondents as $respondent): ?>
            <tr>

                <td><?php echo $respondent['SurveyResult']['email']; ?></td>
                <td><?php echo $respondent['SurveyResult']['ipaddress']; ?></td>
                <td><?php echo $respondent['SurveyResult']['is_admin']; ?></td>
                <td><?php echo date('M j, Y h:i A', strtotime(h($respondent['SurveyResult']['created'])));  ?></td>

                <td class="actions">
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'surveys', 'action' => 'delete_respondent', $respondent['SurveyResult']['id'],$survey['Survey']['id']), null, __('Are you sure you want to delete # %s?', $respondent['SurveyResult']['id'])); ?>
                </td>
            </tr>
	<?php  endforeach; ?>
        </table>
<?php endif; ?>

    </div>
    <br />
    <br />
   <?php echo $this->Html->link(__('Back to Survey List'), array('action' => 'index'), array('type'=>'button','class'=>'btn btn-success')); ?>
</div>