<!DOCTYPE html>
<html lang="en">
    <head>
	<?php echo $this->Html->charset(); ?>
        <title>
		<?php echo $title_for_layout; ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
	<?php
		echo $this->Html->meta('icon');
                
                echo $this->fetch('meta');

		echo $this->Html->css(array('bootstrap.min', 'style'));
		
		echo $this->fetch('css');

                echo $this->Html->script(array('jquery-1.10.2', 'bootstrap.min','survey'));
        
		echo $this->fetch('script');
	?>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src=".js/html5shiv.js"></script>
        <![endif]-->
        <script type="text/javascript">
            window.history.forward();
            function noBack() {
                window.history.forward();
            }
        </script>
    </head>
    <body class="take-survey" onload="noBack();"
        onpageshow="if (event.persisted) noBack();" onunload="">

        <div class="navbar navbar-inverse">
            <div class="navbar-inner" style="background-color:#<?php echo $backgroundColor ?>;background-image: none;">
                <div class="container">
                    <div class="row survey-header-row">
                        <div class="col-md-6 col-sm-12">
                            <h1 class="survey-name" style="font-size: <?php echo $surveyNameSize.'px' ?>; color:#<?php echo $surveyNameColor ?>; "><span><?php echo $feedbackFor; ?></span><br /><?php echo $surveyData['Survey']['name']; ?></h1>
                        </div>
                        <div class="col-md-6 col-sm-12 image-col">
                            <div>
                                <img class="img-responsive survey-logo survey-header-logo" src="<?php echo $this->webroot;?>files/logos/<?php echo $logo; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
    <?php
        //echo $this->Session->flash('auth');
        echo $this->Session->flash();
    ?>
	<?php echo $content_for_layout ?>

            <hr>
        </div>
        <footer>
            <?php echo $footer; ?>
            <!--&copy; CopyRight 2015 - Larry Lobert and Associates - All Rights Reserved-->
        </footer>
        <?php echo $this->element('footer'); //footer element ?>
    </body>
</html>
