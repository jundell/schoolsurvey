<div class="related">
    <h2><?php echo __('Settings'); ?></h2>
    <?php if(!sizeof($settings)){ ?>
        <br />No themes added.
    <?php } ?>
    <?php if ($settings): ?>
        <div class="table-responsive">
                <table class="table table-hover" cellpadding="0" cellspacing="0" >
                    <tr>
                        <th><?php echo __('Id'); ?></th>
                        <th><?php echo __('Name'); ?></th>
                        <th><?php echo __('Slug'); ?></th>
                        <th><?php echo __('Value'); ?></th>

                        <th class="actions" style="text-align: center;"><?php echo __('Actions'); ?></th>

                    </tr>
                    <?php

                            foreach ($settings as $setting): ?>
                    <tr>
                        <td><?php echo $setting['Setting']['id']; ?></td>

                        <td><?php echo $setting['Setting']['name']; ?></td>

                        <td><?php echo $setting['Setting']['slug']; ?></td>

                        <td><?php echo $setting['Setting']['value']; ?></td>

                        <td class="actions" style="text-align: center;">
                                           <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $setting['Setting']['id'])); ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
                echo $this->Paginator->first('<< first');
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                echo $this->Paginator->last('last >>');
	?>
	</div>
    <?php endif; ?>
</div>
