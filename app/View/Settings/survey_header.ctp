<?php echo $this->Html->script('jscolor/jscolor'); ?>
<div class="surveySettings form">
<?php echo $this->Form->create('Setting', array('type' => 'file')); ?>
    <fieldset>
        <legend><?php echo __('Default Survey Header'); ?></legend>
        
        <p style="margin-top: 15px; font-weight: bold; line-height: 0;">Survey Name</p>
        <div id="surveyname-wrapper">
            
            <?php echo $this->Form->input('header_survey_name_font_size', array('div'=>array('class'=>'form-group input number'),'style'=>'width: 100px;', 'value'=>$surveyNameSize, 'type'=>'number', 'label'=>'Size','class'=>'form-control'));?>
            <?php echo $this->Form->input('header_survey_name_font_color', array('div'=>array('class'=>'form-group input text'),'style'=>'width: 100px;','value'=>$surveyNameColor, 'class'=>'color form-control', 'label'=>'Color'));?>
            <?php //echo $this->Form->input('header_survey_name_alignment', array('div'=>array('class'=>'form-group input select'),'style'=>'width: 100px;','value'=>$surveyNameAlignment,'options' => array('center'=>'center','left'=>'left','right'=>'right'), 'label'=>'Alignment','class'=>'form-control')); ?>
            <div class="clear"></div>
        </div>
        
        <p style="margin-top: 15px; font-weight: bold; line-height: 0;">Logo</p>
        <div>
        <img class="survey-header-logo img-responsive" src="<?php echo $this->webroot.'files/logos/'.$logo; ?>" style="margin-bottom:10px;"/>
         <?php echo $this->Form->input('header_logo', array('type'=>'file', 'label'=>'Change Logo'));?>
        </div>
               
        <div id="background-wrapper" style="margin-top: 20px;">
            <?php echo $this->Form->input('header_background_color', array('style'=>'width: 100px;', 'value'=>$backgroundColor, 'class'=>'color form-control', 'label'=>'Background Color'));?>
        </div>
    </fieldset>
    <br />
        <?php echo $this->Form->submit( __('Submit'), array('class' => 'btn btn-primary')) ?>
</div>
