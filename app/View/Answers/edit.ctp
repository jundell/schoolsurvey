<div class="answers form">
<?php echo $this->Form->create('Answer'); ?>
	<fieldset>
		<legend><?php echo __('Edit Answer'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('question_id',array('class'=>'form-control','div'=>array('class'=>'form-group input text')));
		echo $this->Form->input('answer_option_id',array('class'=>'form-control','div'=>array('class'=>'form-group input text')));
	?>
	</fieldset>
<?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-default'))); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Answer.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Answer.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Answers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Answer Options'), array('controller' => 'answer_options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer Option'), array('controller' => 'answer_options', 'action' => 'add')); ?> </li>
	</ul>
</div>
