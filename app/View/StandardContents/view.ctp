<div class="standardContents view">
<h2><?php echo __('Standard Content'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($standardContent['StandardContent']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($standardContent['StandardContent']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Slug'); ?></dt>
		<dd>
			<?php echo h($standardContent['StandardContent']['slug']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($standardContent['StandardContent']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($standardContent['StandardContent']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($standardContent['StandardContent']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Standard Content'), array('action' => 'edit', $standardContent['StandardContent']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Standard Content'), array('action' => 'delete', $standardContent['StandardContent']['id']), null, __('Are you sure you want to delete # %s?', $standardContent['StandardContent']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Standard Contents'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Standard Content'), array('action' => 'add')); ?> </li>
	</ul>
</div>
