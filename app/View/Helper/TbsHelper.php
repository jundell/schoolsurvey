<?php

App::uses('AppHelper', 'View/Helper');

class TbsHelper extends AppHelper {

    function getOpenTbs() {
        App::import('Vendor', 'tbs/tbs_class');
        App::import('Vendor', 'tbs/tbs_plugin_opentbs');

        $tbs  = new clsTinyButStrong; // new instance of TBS
        $tbs->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load OpenTBS plugin   
        return $tbs;
    }

}