<div class="surveyThemes form">
<?php echo $this->Form->create('SurveyTheme', array('enctype' => 'multipart/form-data', 'role' => 'form')); ?>
    <fieldset>
        <legend><?php echo __('Edit Survey Dimension'); ?></legend>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control'));
		echo $this->Form->input('description', array('div'=>array('class'=>'form-group input textarea'),'class'=>'form-control'));
        ?>
                <div class="form-group input file">
                    <label for="SurveyThemeIcon">Icon</label>
                    <?php if($icon){  ?>
                    <p><img src = "<?php echo APP_URL.$icon; ?>" /></p>
                    <?php } ?>
		<?php echo $this->Form->input('icon', array('type'=>'file', 'div'=>false, 'label'=>false));?>
                </div>
	<?php
                echo $this->Form->input('color', array('div'=>array('class'=>'form-group input text'),'class'=>'form-control color','style'=>'width:80px;'));
	?>
            </div>
        </div>
    </fieldset>
<?php echo $this->Form->end(__(array('label' => 'Submit', 'class' => 'btn btn-success'))); ?>
</div>

<br /><br />

<?php echo $this->Html->link(__('Go back to Dimensions List'), array('controller' => 'dimensions', 'action' => 'add/'.$surveyId), array('class'=>'btn btn-default')); ?>
<br /></br />
<?php echo $this->element('view_manage_survey_btn'); ?>