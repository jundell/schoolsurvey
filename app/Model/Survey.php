<?php

App::uses('AppModel', 'Model');

/**
 * Survey Model
 *
 * @property Question $Question
 * @property SurveyResult $SurveyResult
 */
class Survey extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Survey Name is required',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'unique' => array(
                'rule' => array('checkUnique', array('name', 'user_id'), false), 
                'message' => 'Survey name already taken.',
            ),
            
            'maxLength' => array(
                'rule' => array('maxLength', 255),
                'message' => 'Name must be no larger than 255 characters long.'
            )
        ),
        'slug' => array(
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Survey name already taken.',
            ),
        ),
        'password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Respondent Password is required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'admin_password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Admin Password is required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'email_verification_on' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Email Verification is required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'welcome_message' => array(
            'rule' => array('maxLength', 2000),
            'message' => 'Welcome Message must be no larger than 2000 characters long.'
        ),
        'closing_message' => array(
            'rule' => array('maxLength', 2000),
            'message' => 'Closing Message must be no larger than 2000 characters long.'
        ),
        'group_message' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Group Message is required.'
            ),
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    /* public $hasMany = array(
      'Question' => array(
      'className' => 'Question',
      'foreignKey' => 'survey_id',
      'dependent' => false,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
      ),
      'SurveyResult' => array(
      'className' => 'SurveyResult',
      'foreignKey' => 'survey_id',
      'dependent' => false,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
      )
      ); */
    /* public $actsAs = array(
      'Uploader.Attachment' => array(
      'questionnaire' => array(
      'tempDir' => TMP,
      'allowEmpty' => false,
      'overwrite' => false,
      'uploadDir' => 'files/uploads/surveys/',
      'finalPath' => 'files/uploads/surveys/',
      )
      ),
      'Uploader.FileValidation' => array(
      'questionnaire' => array(
      'extension' => array('xls'),
      'required' => array(
      'value' => true,
      'error' => 'File required'
      )
      ),
      )
      ); */

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    public $hasMany = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'survey_id',
            'conditions' => array('Group.is_deleted' => 'no'),
//            'order' => 'Comment.created DESC',
//            'limit' => '5',
            'dependent' => true
        ),
        'SurveyTheme' => array(
            'className' => 'SurveyTheme',
            'foreignKey' => 'survey_id',
            'conditions' => array('SurveyTheme.is_deleted' => '0'),
//            'order' => 'Comment.created DESC',
//            'limit' => '5',
            'dependent' => true
        ),
        'Question' => array(
            'className' => 'Question',
            'foreignKey' => 'survey_id',
            'conditions' => array('Question.is_deleted' => 'no'),
//            'order' => 'Comment.created DESC',
//            'limit' => '5',
            'dependent' => true
        ),
        'RespondentPasscode' => array(
            'className' => 'RespondentPasscode',
            'foreignKey' => 'survey_id',
//            'conditions' => array('Question.is_deleted' => 'no'),
//            'order' => 'Comment.created DESC',
//            'limit' => '5',
            'dependent' => true
        ),
        'SurveyRunHistories' => array(
            'className' => 'SurveyRunHistory',
            'foreignKey' => 'survey_id',
            'dependent' => true
        ),
        'SurveyResults' => array(
            'className' => 'SurveyResult',
            'foreignKey' => 'survey_id',
            'dependent' => true
        ),
    );
    

    public function checkUnique($ignoredData, $fields, $or = true) {
        return $this->isUnique($fields, $or);
    }
}
