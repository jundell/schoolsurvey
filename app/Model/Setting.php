<?php

App::uses('AppModel', 'Model');

/**
 * Setting Model
 *
 */
class Setting extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'slug' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
//        'description' => array(
//            'notempty' => array(
//                'rule' => array('notempty'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
    );

    public function get_value($slug) {
        $setting = $this->find('first', array('conditions' => array('Setting.slug' => $slug)));
        return isset($setting['Setting']['value'])?$setting['Setting']['value']:false;
    }

    public function get_by_slug($slug) {
        $setting = $this->find('first', array('conditions' => array('Setting.slug' => $slug)));
        $settingRet['value'] = isset($setting['Setting']['value'])?$setting['Setting']['value']:false;
        $settingRet['id'] = isset($setting['Setting']['id'])?$setting['Setting']['id']:false;
        return $settingRet;
    }
}
