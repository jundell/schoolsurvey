<?php

App::uses('AppModel', 'Model');

/**
 * SurveyRunHistory Model
 *
 */
class SurveyRunHistory extends AppModel {

    var $useTable = 'survey_run_histories';

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Survey' => array(
            'className' => 'Survey',
            'foreignKey' => 'survey_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}