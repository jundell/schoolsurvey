<?php

App::uses('AppModel', 'Model');

/**
 * StandardTheme Model
 *
 */
class StandardTheme extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Theme name already taken.',
            ),
        ),
        /* 'description' => array(
          'notempty' => array(
          'rule' => array('notempty'),
          //'message' => 'Your custom message here',
          //'allowEmpty' => false,
          //'required' => false,
          //'last' => false, // Stop validation after this rule
          //'on' => 'create', // Limit validation to 'create' or 'update' operations
          ),
          ), */
//        'icon' => array(
//            'notempty' => array(
//                'rule' => array('notempty'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
        'color' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'is_deleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
    public $actsAs = array(
        'Uploader.Attachment' => array(
            // Do not copy all these settings, it's merely an example
            'icon' => array(
                'tempDir' => TMP,
                'uploadDir' => 'files/uploads/standard_theme_icons/',
                'finalPath' => 'files/uploads/standard_theme_icons/',
                'dbColumn' => 'icon'
            )
        ),
        'Uploader.FileValidation' => array(
            'icon' => array(
                'maxWidth' => 50,
                'maxHeight' => 50,
                'extension' => array('gif', 'jpg', 'png', 'jpeg'),
                'type' => 'image',
                'required' => false
            )
        )
    );
    public $hasMany = array(
        'StandardQuestion' => array(
            'className' => 'StandardQuestion',
            'foreignKey' => 'theme_id',
            'conditions' => array('StandardQuestion.is_deleted' => '0'),
//            'order' => 'Comment.created DESC',
//            'limit' => '5',
            'dependent' => true
        )
    );

}
