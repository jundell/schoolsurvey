<?php

App::uses('AppController', 'Controller');

/**
 * QuestionsOptions Controller
 *
 * @property QuestionsOption $QuestionsOption
 */
class QuestionsOptionsController extends AppController {

    public $uses = array('Question', 'QuestionsOption');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->QuestionsOption->recursive = 0;
        $this->set('questionsOptions', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->QuestionsOption->exists($id)) {
            throw new NotFoundException(__('Invalid questions option'));
        }
        $options = array('conditions' => array('QuestionsOption.' . $this->QuestionsOption->primaryKey => $id));
        $this->set('questionsOption', $this->QuestionsOption->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($id = null) {
        if (!$this->Question->exists($id)) {
            throw new NotFoundException(__('Invalid Question'));
        }
        if ($this->request->is('post')) {
            $names = $this->request->data['name'];
            foreach ($names as $option) {
                $this->QuestionsOption->create();
                $optionData = array('question_id' => $id, 'name' => $option);
                if ($option) {
                    $this->QuestionsOption->save($optionData);
                }
            }
            
            $this->save_new_option_score($id);
            $this->Session->setFlash(__('Option(s) has been saved'), 'default', array('class' => 'alert alert-success'));
        }
        $options = $this->QuestionsOption->find('all', array('fields'=>array('QuestionsOption.id', 'QuestionsOption.name', 'QuestionsOption.score'),'conditions' => array('QuestionsOption.question_id' => $id, 'QuestionsOption.is_deleted' => 'no'), 'order' => array('QuestionsOption.id ASC')));
        $question = $this->Question->read(array('Question.id','Question.include_no_response','Question.no_response_option'), $id);
        $this->set(compact('options'));
        $this->set(compact('question'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null, $qId = null) {
        if (!$this->QuestionsOption->exists($id) || !$this->Question->exists($qId)) {
            throw new NotFoundException(__('Invalid questions option'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->QuestionsOption->save($this->request->data)) {
                $this->Session->setFlash(__('The questions option has been saved'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'add/' . $qId));
            } else {
                $this->Session->setFlash(__('The questions option could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-error'));
            }
        } else {
            $options = array('conditions' => array('QuestionsOption.' . $this->QuestionsOption->primaryKey => $id));
            $this->request->data = $this->QuestionsOption->find('first', $options);
        }
//        $questions = $this->QuestionsOption->Question->find('list');
//        $this->set(compact('questions'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null, $qId = null) {
        $this->QuestionsOption->id = $id;
        if (!$this->QuestionsOption->exists() || !$this->Question->exists($qId)) {
            throw new NotFoundException(__('Invalid questions option'));
        }
        $this->request->onlyAllow('post', 'delete');
        //$this->QuestionsOption->set('is_deleted', 'yes');
        //if ($this->QuestionsOption->save()) {
        if ($this->QuestionsOption->delete()) {
            $this->save_new_option_score($qId);
            $this->Session->setFlash(__('Questions option deleted'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'add/' . $qId));
        }
        $this->Session->setFlash(__('Questions option was not deleted'), 'default', array('class' => 'alert alert-error'));
        $this->redirect(array('action' => 'index'));
    }
    
    public function delete_no_response($qid){
        $this->Question->read(null, $qid);
        $this->Question->set('include_no_response', 0);
        $this->Question->save();
    }
    
    public function include_no_response($qid){
        $this->Question->read(null, $qid);
        $this->Question->set('include_no_response', $this->request->data['QuestionsOption']['include_no_response']);
        $this->Question->set('no_response_option', $this->request->data['QuestionsOption']['no_response_option']);
        $this->Question->save();
        $this->redirect(array('action' => 'add/' . $qid));
    }
    
    private function save_new_option_score($id){
        //save new option score
            $optionsData = $this->QuestionsOption->find('list', array('conditions' => array('QuestionsOption.question_id' => $id, 'QuestionsOption.is_deleted' => 'no'), 'order'=>array('QuestionsOption.id ASC')));
            $cnt = 1;
            foreach($optionsData as $k=>$v){
                $this->QuestionsOption->id = $k;
                $this->QuestionsOption->set('score', $cnt);
                $this->QuestionsOption->save();
                $cnt++;
            }
    }

}
