<?php

App::uses('AppController', 'Controller');

/**
 * SurveyReportSettings Controller
 *
 * @property SurveyReportSetting $SurveyReportSetting
 */
class SurveyReportSettingsController extends AppController {

//    public $helpers = array('Wysiwyg.Wysiwyg' => array('editor' => 'Ck'));

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->SurveyReportSetting->recursive = 0;
        $this->set('surveyReportSettings', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->SurveyReportSetting->exists($id)) {
            throw new NotFoundException(__('Invalid survey report setting'));
        }
        $options = array('conditions' => array('SurveyReportSetting.' . $this->SurveyReportSetting->primaryKey => $id));
        $this->set('surveyReportSetting', $this->SurveyReportSetting->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->SurveyReportSetting->create();
            if ($this->SurveyReportSetting->save($this->request->data)) {
                $this->Session->setFlash(__('The survey report setting has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The survey report setting could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->SurveyReportSetting->exists($id)) {
            throw new NotFoundException(__('Invalid survey report setting'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->SurveyReportSetting->save($this->request->data)) {
                $this->Session->setFlash(__('The survey report setting has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The survey report setting could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('SurveyReportSetting.' . $this->SurveyReportSetting->primaryKey => $id));
            $this->request->data = $this->SurveyReportSetting->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->SurveyReportSetting->id = $id;
        if (!$this->SurveyReportSetting->exists()) {
            throw new NotFoundException(__('Invalid survey report setting'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->SurveyReportSetting->delete()) {
            $this->Session->setFlash(__('Survey report setting deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Survey report setting was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
