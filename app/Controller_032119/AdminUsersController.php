<?php
App::uses('AppController', 'Controller');
/**
 * AdminUsers Controller
 *
 * @property AdminUser $AdminUser
 */
class AdminUsersController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AdminUser->recursive = 0;
		$this->set('adminUsers', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->AdminUser->id = $id;
		if (!$this->AdminUser->exists()) {
			throw new NotFoundException(__('Invalid admin user'));
		}
		$this->set('adminUser', $this->AdminUser->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AdminUser->create();
			if ($this->AdminUser->save($this->request->data)) {
				$this->Session->setFlash(__('The admin user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The admin user could not be saved. Please, try again.'));
			}
		}
		$admins = $this->AdminUser->Admin->find('list');
		$users = $this->AdminUser->User->find('list');
		$this->set(compact('admins', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->AdminUser->id = $id;
		if (!$this->AdminUser->exists()) {
			throw new NotFoundException(__('Invalid admin user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AdminUser->save($this->request->data)) {
				$this->Session->setFlash(__('The admin user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The admin user could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->AdminUser->read(null, $id);
		}
		$admins = $this->AdminUser->Admin->find('list');
		$users = $this->AdminUser->User->find('list');
		$this->set(compact('admins', 'users'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->AdminUser->id = $id;
		if (!$this->AdminUser->exists()) {
			throw new NotFoundException(__('Invalid admin user'));
		}
		if ($this->AdminUser->delete()) {
			$this->Session->setFlash(__('Admin user deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Admin user was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
