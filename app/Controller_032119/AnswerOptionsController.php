<?php
App::uses('AppController', 'Controller');
/**
 * AnswerOptions Controller
 *
 * @property AnswerOption $AnswerOption
 */
class AnswerOptionsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AnswerOption->recursive = 0;
		$this->set('answerOptions', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->AnswerOption->id = $id;
		if (!$this->AnswerOption->exists()) {
			throw new NotFoundException(__('Invalid answer option'));
		}
		$this->set('answerOption', $this->AnswerOption->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AnswerOption->create();
			if ($this->AnswerOption->save($this->request->data)) {
				$this->Session->setFlash(__('The answer option has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The answer option could not be saved. Please, try again.'));
			}
		}
		$questions = $this->AnswerOption->Question->find('list');
		$this->set(compact('questions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->AnswerOption->id = $id;
		if (!$this->AnswerOption->exists()) {
			throw new NotFoundException(__('Invalid answer option'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AnswerOption->save($this->request->data)) {
				$this->Session->setFlash(__('The answer option has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The answer option could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->AnswerOption->read(null, $id);
		}
		$questions = $this->AnswerOption->Question->find('list');
		$this->set(compact('questions'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->AnswerOption->id = $id;
		if (!$this->AnswerOption->exists()) {
			throw new NotFoundException(__('Invalid answer option'));
		}
		if ($this->AnswerOption->delete()) {
			$this->Session->setFlash(__('Answer option deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Answer option was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
