<?php
App::uses('AppController', 'Controller');
/**
 * UqGroups Controller
 *
 * @property UqGroup $UqGroup
 */
class UqGroupsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UqGroup->recursive = 0;
		$this->set('uqGroups', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->UqGroup->id = $id;
		if (!$this->UqGroup->exists()) {
			throw new NotFoundException(__('Invalid uq group'));
		}
		$this->set('uqGroup', $this->UqGroup->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UqGroup->create();
			if ($this->UqGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The uq group has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The uq group could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->UqGroup->id = $id;
		if (!$this->UqGroup->exists()) {
			throw new NotFoundException(__('Invalid uq group'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->UqGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The uq group has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The uq group could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->UqGroup->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->UqGroup->id = $id;
		if (!$this->UqGroup->exists()) {
			throw new NotFoundException(__('Invalid uq group'));
		}
		if ($this->UqGroup->delete()) {
			$this->Session->setFlash(__('Uq group deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Uq group was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
