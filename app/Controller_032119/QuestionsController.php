<?php

App::uses('AppController', 'Controller');

/**
 * Questions Controller
 *
 * @property Question $Question
 */
class QuestionsController extends AppController {
    /* public function beforeFilter() {
      //parent::beforeFilter();

      $this->Auth->allow();
      } */

    public $uses = array('Survey', 'SurveyTheme', 'Question', 'QuestionsOption', 'StandardTheme', 'StandardQuestion');
//    public $components = array('Paginator');
    public $paginate = array(
        'Question' => array(
            /*'order' => array('SurveyTheme.id' => 'asc','Question.id' => 'asc'),*/
            'order' => array('Question.id' => 'asc','SurveyTheme.id' => 'asc'),
            'limit' => 10,
//            'conditions'=>array('Question.is_deleted'=>'no')
        )
    );

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Question->recursive = 0;
        $this->set('questions', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Question->exists($id)) {
            throw new NotFoundException(__('Invalid question'));
        }
        $options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
        $this->set('question', $this->Question->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($surveyId, $build = 0) {

        $this->layout = 'survey_builder';
        $this->Survey->id = $surveyId;
        if (!$this->Survey->exists()) {
            throw new NotFoundException(__('Invalid survey.'));
        }

        if ($this->request->is('post')) {

            $add_another_question = $this->request->data['add_another_question'];

            $questions = $this->request->data['question'];
            $ownQuestion = $this->request->data['ownQuestion'];

            if (sizeof($questions) || ($ownQuestion['name'] && strlen(trim($ownQuestion['name'])) != 0)) {

                $errors = 0;

                //validate chosen standard questions
                $chosenQuestions = $this->chosen_standard_questions($surveyId, $questions);

                $question = array();
                if ($ownQuestion['name'] && strlen(trim($ownQuestion['name'])) != 0) {
                    //validate own question
                    $question = $this->own_question($surveyId, $ownQuestion, $questions);
                    $errors = $errors + $question['errors'];
                }

                if (!$errors) {
                    //save chosen standard questions
                    $this->Question->saveAll($chosenQuestions['chosenQuestions'], array('deep' => true));
                    
                    if(sizeof($question)){
                        //save own question
                        $this->Question->saveAll(array($question['question']), array('deep' => true));
                    }
                    
                    $this->Session->setFlash(__('Your survey question(s) have been saved.'), 'default', array('class' => 'alert alert-success'));

                    if ($add_another_question) {
                        $this->redirect($this->referer() . '/#ownQuestion');
                    } else {
                        if ($build) {
                            $this->redirect(array('controller' => 'surveys', 'action' => 'build/' . $surveyId));
                        } else {
                           $this->redirect( Router::url( $this->referer(), true ) );
                        }
                    }
                } else {
                    $this->Session->setFlash(__($question['validationErrors']), 'default', array('class' => 'alert alert-danger'));
                }
            } else {
                $this->Session->setFlash(__('You haven\'t added any question.'), 'default', array('class' => 'alert alert-danger'));
            }
        }


        $themes = $this->SurveyTheme->find('list', array('conditions' => array('SurveyTheme.survey_id' => $surveyId, 'SurveyTheme.is_deleted' => 0)));

        $standardThemes = $this->StandardTheme->find('all', array('conditions' => array('StandardTheme.name' => $themes)));

        $themeIcons = $this->SurveyTheme->find('list', array('fields' => array('SurveyTheme.name', 'SurveyTheme.icon'), 'conditions' => array('SurveyTheme.survey_id' => $surveyId, 'SurveyTheme.is_deleted' => 0)));

        foreach ($standardThemes as $k => $sTheme) {
            $standardThemes[$k]['StandardQuestion'] = $this->StandardQuestion->find('all', array('conditions' => array('StandardQuestion.theme_id' => $sTheme['StandardTheme']['id'])));
        }
        
        $questionslist = $this->paginate('Question', array('Question.survey_id' => $surveyId, 'Question.is_deleted' => 'no'));

        $this->set('questionslist', $questionslist);

        $questionDatalist = $this->Question->find('list', array('conditions' => array('Question.survey_id' => $surveyId, 'Question.is_deleted' => 'no'), 'order' => array('Question.id ASC')));
        $this->set('questionDatalist', $questionDatalist);

        //survey status
        $surveyStatusData = $this->Survey->read('status', $surveyId);
        
        $surveyStatus = $surveyStatusData['Survey']['status'];
        if($surveyStatus){
            $this->Session->setFlash(__('The survey has already started. You should stop the survey to edit/delete any descriptor-question information.'), 'default', array('class' => 'alert alert-danger'));
        }
        $this->set('surveyStatus', $surveyStatus);
        

        $types = $this->Question->get_question_type_array(); // returns question types array (radio button, likert, open ended) 
        $this->set(compact('types'));

        $this->set('themes', $themes);

        $this->set('surveyId', $surveyId);

        $this->set('build', $build);
        $this->set('standardThemes', $standardThemes);
        $this->set('themeIcons', $themeIcons);
    }

    private function chosen_standard_questions($surveyId, $questions) {
        $chosenQuestions = array();
        $errors = 0;
        $chosenStandardQuestions = $this->StandardQuestion->find('all', array('conditions' => array('StandardQuestion.id' => $questions)));

        foreach ($chosenStandardQuestions as $question) {
            $theme = $this->SurveyTheme->find('first', array('conditions' => array('SurveyTheme.survey_id' => $surveyId, 'SurveyTheme.name' => $question['StandardTheme']['name'], 'SurveyTheme.is_deleted' => 0)));
            $questionData = array(
                'Question' => array(
                    'name' => $question['StandardQuestion']['name'],
                    'type' => $question['StandardQuestion']['type'],
                    'survey_theme_id' => $theme['SurveyTheme']['id'],
                    'survey_id' => $surveyId,
                    'is_calculating' => $question['StandardQuestion']['is_calculating'],
            ));
            $optionsScores = array();
            if (sizeof($question['StandardQuestionOption'])) {
                foreach ($question['StandardQuestionOption'] as $option) {
                    $optionsScores[] = array('name' => $option['name'], 'score' => $option['score']);
                }
            }
            $questionData['QuestionsOption'] = $optionsScores;
            $chosenQuestions[] = $questionData;
        }

        return array('chosenQuestions' => $chosenQuestions);

    }

    private function own_question($surveyId, $ownQuestion, $questions) {
        $question = array();
        $errors = 0;
        $validationErrors = '';

        //chosen standard questions
        $chosenStandardQuestions = $this->StandardQuestion->find('list', array('conditions' => array('StandardQuestion.id' => $questions)));
        
        $options = array_filter($ownQuestion['option']);
        $scores = $ownQuestion['score'];

        //duplicate own question and standard quesion
        if(in_array($ownQuestion['name'], $chosenStandardQuestions)){
            $errors++;
                $validationErrors = 'Duplicate question(s).';
        }else{
            if ($ownQuestion['type'] == 'open ended') {  //if question is open ended
                //$this->save_question($id, $options, $scores);
                $question = array(
                    'Question' => array(
                        'name' => $ownQuestion['name'],
                        'survey_theme_id' => $ownQuestion['survey_theme_id'],
                        'type' => $ownQuestion['type'],
                        'is_calculating' => $ownQuestion['is_calculating'],
                        'survey_id'=>$surveyId
                    )
                );
                $this->Question->set($question);
                if (!$this->Question->validates()) {
                    $errors++;
                }
            } else { //question is either likert or radio button
                if (sizeof($options) >= 2) { // checks if number of options is equal or greater than 2
                    $question = array(
                        'Question' => array(
                            'name' => $ownQuestion['name'],
                            'survey_theme_id' => $ownQuestion['survey_theme_id'],
                            'type' => $ownQuestion['type'],
                            'is_calculating' => $ownQuestion['is_calculating'],
                            'survey_id'=>$surveyId
                        )
                    );
                    //options
                    $optionsScores = array();
                    foreach ($options as $k => $option) {
                        $optionsScores[] = array('name' => $option, 'score' => $scores[$k]);
                    }
                    $question['QuestionsOption'] = $optionsScores;
                    $this->Question->set($question);
                    if (!$this->Question->validates()) {
                        $errors++;
                    }
                } else {
                    $errors++;
                    $validationErrors = 'You should provide at least two options.';
                }
            }
        }

//        echo $errors;
        return array('question' => $question, 'options' => $options, 'scores' => $scores, 'errors' => $errors, 'validationErrors' => $validationErrors);
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Question->exists($id)) {
            throw new NotFoundException(__('Invalid question'));
        }
        
        
        $surveyId = $this->Question->field('survey_id', array('id' => $id));
        
        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey', $surveyId)) {
            throw new NotFoundException(__('Invalid question'));
        }
        
        $conditions = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
        $question = $this->Question->find('first', $conditions);
        if ($this->request->is('post') || $this->request->is('put')) {
            /** Added for redirect purpose
            * Date: 1/15/2018 By: Mary Gale Jabagat
            **/
            $prev = $this->params->params['named']['prev'];
            $prev_url = (!empty($prev)) ?  $surveyId.'/page:'.$prev : $surveyId;

            $savedOptions = $this->QuestionsOption->find('list', array('conditions' => array('QuestionsOption.question_id' => $id), 'order' => 'QuestionsOption.id ASC'));
            $options = array_filter($this->request->data['option']);
            $scores = $this->request->data['score'];
            if ($this->request->data['Question']['type'] == 'open ended') {  //if question is open ended
                if ($this->Question->save($this->request->data)) {
                    $this->Session->setFlash(__('The question has been saved'), 'default', array('class' => 'alert alert-success'));
                    //$this->redirect(array('controller' => 'surveys', 'action' => 'add_question/' . $surveyId));//commented out on 1/15/2018
                    $this->redirect(array('controller' => 'questions', 'action' => 'add/' . $prev_url));//added on 1/15/2018
                } else {
                    $this->Session->setFlash(__('The question could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
                }
            } else { //question is either likert or radio button
                if (sizeof($options) >= 2) {
                    if ($this->Question->save($this->request->data)) {
                        foreach ($options as $k => $option) {
                            if (array_key_exists($k, $savedOptions)) {
                                $this->QuestionsOption->read(null, $k);
                                $this->QuestionsOption->set(array(
                                    'name' => $option,
                                    'score' => $scores[$k]
                                ));
                                $this->QuestionsOption->save();
                            } else {
                                $this->QuestionsOption->create();
                                $optionData = array('question_id' => $id, 'name' => $option, 'score' => $scores[$k]);
                                $this->QuestionsOption->save($optionData);
                            }
                        }

                        //delete options
                        $delOptions = array_diff_key($savedOptions, $options);
                        foreach ($delOptions as $kd => $od) {
                            $this->QuestionsOption->delete($kd);
                        }
                        $this->Session->setFlash(__('The question has been saved'), 'default', array('class' => 'alert alert-success'));
                        // $this->redirect(array('action' => 'add/' . $surveyId));
                        $this->redirect(array('controller' => 'questions', 'action' => 'add/' . $prev_url));//added on 1/15/2018
                    } else {
                        $this->Session->setFlash(__('The question could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-error'));
                    }
                } else {
                    $this->Session->setFlash(__('You should provide at least two options.'), 'default', array('class' => 'alert alert-danger'));
                }
            }
            $this->set('optionEditable', !sizeof($question['SurveyAnswer']));
        } else {

            $this->request->data = $question;
            $options_data = $this->QuestionsOption->find('all', array('fields' => array('QuestionsOption.id', 'QuestionsOption.name', 'QuestionsOption.score'), 'conditions' => array('QuestionsOption.question_id' => $id), 'order' => 'QuestionsOption.id ASC'));
//                        var_dump($options);
            $this->set(compact('options_data'));
        }
        $types = $this->Question->get_question_type_array();
        $this->set(compact('types'));
        $this->loadModel('SurveyTheme');
        $themes = $this->SurveyTheme->find('list', array('conditions' => array('SurveyTheme.survey_id' => $surveyId, 'SurveyTheme.is_deleted' => 0)));
        $this->set('themes', $themes);
        $surveys = $this->Question->Survey->find('list');
        $this->set(compact('surveys'));
        $this->set('surveyId', $surveyId);
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Question->id = $id;
        if (!$this->Question->exists()) {
            throw new NotFoundException(__('Invalid question'));
        }
        
        $surveyId = $this->Question->field('survey_id', array('id' => $id));
        
        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey', $surveyId)) {
            throw new NotFoundException(__('Invalid question'));
        }
        
        $this->request->onlyAllow('post', 'delete');
        // $this->Question->set('is_deleted', 'yes');
        //if ($this->Question->save()) {
        if ($this->Question->delete($id, true)) {
            $this->Session->setFlash(__('Question deleted.'), 'default', array('class' => 'alert alert-success'));
//			$this->redirect(array('action' => 'index'));
            $this->redirect( Router::url( $this->referer(), true ) );
        }
        $this->Session->setFlash(__('Question was not deleted.'), 'default', array('class' => 'alert alert-danger'));
        $this->redirect( Router::url( $this->referer(), true ) );
    }

}
