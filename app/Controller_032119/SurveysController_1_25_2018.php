<?php

App::uses('AppController', 'Controller');
CakePlugin::load('ExcelReader');

/**
 * Surveys Controller
 *
 * @property Survey $Survey
 */
class SurveysController extends AppController {

    public $uses = array('Survey', 'Group', 'Question', 'QuestionsOption', 'SurveyResult', 'SurveyAnswer', 'SurveyTheme', 'SurveySetting', 'SubscriberSetting', 'RespondentPasscode','StandardContent');
    public $paginate = array('order' => array('Survey.id' => 'desc'));
    public $components = array(
        'ExcelReader.ExcelReader',
        'Paginator'
    );
    public $helpers = array('PhpExcel');

    public function beforeFilter() {
        $this->Auth->allow('verify', 'take', 'start', 'answer', 'thank_you');
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if ($this->Auth->user('role_id') == 3) {
            $this->set('surveys', $this->Paginator->paginate('Survey', array('Survey.user_id' => $this->Auth->user('user_id'))));
        } else {
            $this->Paginator->settings = array(
                'Survey' => array(
                    'order' => array('Survey.id' => 'desc'),
                )
            );
            $this->set('surveys', $this->Paginator->paginate('Survey'));
        }
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $this->survey_data($id);
        $this->set('respondents', $this->SurveyResult->find('all', array('conditions' => array('SurveyResult.survey_id' => $id), 'order' => array('SurveyResult.id' => 'DESC'))));
    }

    public function build($id = null) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $this->survey_data($id);
    }

    private function survey_data($id) {

        $options = array('conditions' => array('Survey.' . $this->Survey->primaryKey => $id));

        //groups
        $this->set('groups', $this->Group->find('list', array('conditions' => array('Group.survey_id' => $id, 'Group.is_deleted' => 'no'), 'order' => array('Group.id ASC'))));

        //themes
        $this->set('themes', $this->SurveyTheme->find('all', array('conditions' => array('SurveyTheme.survey_id' => $id, 'SurveyTheme.is_deleted' => '0'))));

        //questions
        $this->set('survey', $this->Survey->find('first', $options));
        $this->set('respondents', $this->SurveyResult->find('all', array('conditions' => array('SurveyResult.survey_id' => $id), 'order' => array('SurveyResult.id' => 'DESC'))));
    
        //group message
        $this->set('group_message', $this->SurveySetting->field('value',array('slug' => 'group_message', 'survey_id'=>$id)));
    }

    public function start_stop_survey($id, $status, $build = 0) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $this->autoRender = false;
        $this->Survey->read(null, $id);
        $this->Survey->set('status', $status);
        unset($this->Survey->validate['password']);
        unset($this->Survey->validate['email_verification_on']);
        if ($this->Survey->save()) {
            if ($build) {
                $this->Session->setFlash(__('Build feedback survey successful.'), 'default', array('class' => 'alert alert-success'));
            } else {
                $statusText = $status == 1 ? 'started' : 'stopped';
                $this->Session->setFlash(__('Successfully ' . $statusText . ' the survey.'), 'default', array('class' => 'alert alert-success'));
            }
        } else {
            $statusText = $status == 1 ? 'Starting' : 'Stopping';
            $this->Session->setFlash(__($statusText . ' the survey failed.'), 'default', array('class' => 'alert alert-success'));
        }

        $this->redirect( Router::url( $this->referer(), true ) );
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Survey->create();
            $this->request->data['Survey']['slug'] = $this->Auth->user('user_id') . '_' . Inflector::slug($this->request->data['Survey']['name']);
            $this->request->data['Survey']['user_id'] = $this->Auth->user('user_id');
            
            //if survey is closed, disable validation of password and email verification
            if (!$this->request->data['Survey']['open']) {
                //password
                $this->request->data['Survey']['password'] = '';
                $this->Survey->validator()->remove('password', 'notempty');

                //email verification
                $this->request->data['Survey']['email_verification_on'] = '';
                $this->Survey->validator()->remove('email_verification_on', 'notempty');
            }

            
            if (!$this->request->data['Survey']['open'] || $this->request->data['Survey']['password'] != $this->request->data['Survey']['admin_password']) {

                if ($this->Survey->save($this->request->data)) {
                        $surveyLastInsertedId = $this->Survey->getLastInsertId();
                        $this->SurveySetting->create();
                        $this->SurveySetting->save(array(
                            'name'=>'Group Message',
                            'slug'=>'group_message',
                            'value'=>$this->request->data['Survey']['group_message'],
                            'survey_id'=>$surveyLastInsertedId
                                ));
                        $this->Session->setFlash(__('The survey has been saved. Please add groups.'), 'default', array('class' => 'alert alert-success'));
                        $this->redirect(array('controller' => 'surveys', 'action' => 'add_group_option/' . $surveyLastInsertedId));
                } else {
                        $this->Session->setFlash(__('The survey could not be saved. Please try again.'), 'default', array('class' => 'alert alert-danger'));
                }
            } else {
                $this->Session->setFlash(__('Respondent Password and Admin Password should not be the same.'), 'default', array('class' => 'alert alert-danger'));
            }
            
        }
        $this->set('standard_contents', $this->StandardContent->find('list', array('fields'=>array('StandardContent.slug','StandardContent.value'))));
    }

    /**
     * add_survey_message method
     * This method is for the page display after the survey has been successfully added
     * 
     * @param string $survey_id
     * @return void
     * 
     */
    public function add_survey_message($survey_id = null) {
        if (!$this->Survey->exists($survey_id) || !$this->user_allowed('Survey', $survey_id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $this->set('survey_id', $survey_id);
    }

    /**
     * 
     * @param type $survey_id
     */
    public function add_group_option($survey_id) {
        $this->add_survey_message($survey_id);
//        $this->survey_data($suvrey_id)
         $this->survey_data($survey_id);
//        $this->set('survey', $this->Survey->find('first', array('conditions'=>array('Survey.id'))));
        $this->render('add_survey_message');
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $openSurvey = $this->Survey->field('open', array('id' => $id));
        $activeSurvey = $this->Survey->field('status', array('id' => $id));
        $groupMessage = $this->SurveySetting->find('first',array('conditions'=>array('SurveySetting.survey_id' => $id,'SurveySetting.slug'=>'group_message')));
        if ($this->request->is('post') || $this->request->is('put')) {

            if (!$this->request->data['Survey']['open']) {
                //password
                $this->request->data['Survey']['password'] = '';
                $this->Survey->validator()->remove('password', 'notempty');
                
                //email verification
                $this->request->data['Survey']['email_verification_on'] = '';
                $this->Survey->validator()->remove('email_verification_on', 'notempty');
            }
            if ($this->request->data['Survey']['password'] != $this->request->data['Survey']['admin_password']) {
                if ($this->Survey->save($this->request->data)) {
                    
                    //save group message
                    if(!sizeof($groupMessage)){
                        $this->SurveySetting->create();
                        $this->SurveySetting->save(array(
                            'name'=>'Group Message',
                            'slug'=>'group_message',
                            'value'=>$this->request->data['Survey']['group_message'],
                            'survey_id'=>$id
                                ));
                    }else{
                        $this->SurveySetting->id = $groupMessage['SurveySetting']['id'];
                        $this->SurveySetting->save(array('value'=>$this->request->data['Survey']['group_message']));
                    }
                    
                    
                    if ($openSurvey && !$this->request->data['Survey']['open']) { // if the survey was an open system and then changed to close system
                        if ($activeSurvey) { //if the survey has already started, stop the survey
                            $this->Survey->read(null, $id);
                            $this->Survey->set('status', '0');
                            $this->Survey->save();
                            $this->Session->setFlash(__('The survey has been saved and stopped. Please add group respondents before starting the survey.'), 'default', array('class' => 'alert alert-success'));
                        } else {
                            $this->Session->setFlash(__('The survey has been saved. Please add group respondents.'), 'default', array('class' => 'alert alert-success'));
                        }
                        $this->redirect(array('controller' => 'groups', 'action' => 'add/' . $id)); //redirect to groups to add respondents
                    } else {

                        if (!$openSurvey && $this->request->data['Survey']['open']) { // if the survey was a close system and then changed to open system
                            $this->RespondentPasscode->deleteAll(array('RespondentPasscode.survey_id' => $id)); //delete all respondents of that survey
                        }

                        $this->Session->setFlash(__('The survey has been saved'), 'default', array('class' => 'alert alert-success'));
                        // $this->redirect(array('action' => 'index'));
                    }
                    
                } else {
                    $this->Session->setFlash(__('The survey could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
                }
            } else {
                $this->Session->setFlash(__('Respondent Password and Admin Password should not be the same.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('Survey.' . $this->Survey->primaryKey => $id));
            $this->request->data = $this->Survey->find('first', $options);
            $groupMessageVal = sizeof($groupMessage)?$groupMessage['SurveySetting']['value']:$this->StandardContent->field('value',array('slug'=>'survey_group_message'));
            $this->request->data['Survey']['group_message'] = $groupMessageVal;
            $this->set('standard_contents', $this->StandardContent->find('list', array('fields'=>array('StandardContent.slug','StandardContent.value'))));
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Survey->id = $id;
        if (!$this->Survey->exists() || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }

        //delete all data related to the survey to be deleted
        $this->Group->deleteAll(array('Group.survey_id' => $id), false);
        $qList = $this->Question->find('list', array('conditions' => array('Question.survey_id' => $id), 'fields' => array('Question.id', 'Question.id')));

        $this->QuestionsOption->deleteAll(array('QuestionsOption.question_id' => $qList), false);
        $this->Question->deleteAll(array('Question.survey_id' => $id), false);
        $rList = $this->SurveyResult->find('list', array('conditions' => array('SurveyResult.survey_id' => $id), 'fields' => array('SurveyResult.id', 'SurveyResult.id')));
        $this->SurveyAnswer->deleteAll(array('SurveyAnswer.survey_result_id' => $rList), false);
        $this->SurveyResult->deleteAll(array('SurveyResult.survey_id' => $id), false);
        $this->SurveySetting->deleteAll(array('SurveySetting.survey_id' => $id), false);

        if ($this->Survey->delete()) {
            $this->Session->setFlash(__('Survey deleted'), 'default', array('class' => 'alert alert-success'));
            $this->redirect( Router::url( $this->referer(), true ) );
        }
        $this->Session->setFlash(__('Survey was not deleted'), 'default', array('class' => 'alert alert-danger'));
        
        $this->redirect( Router::url( $this->referer(), true ) );
    }

    /**
     * survey_themes method
     * 
     * @param type $id
     * @throws NotFoundException
     */
    public function survey_themes($id) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $themeslist = $this->SurveyTheme->find('list', array('conditions' => array('SurveyTheme.survey_id' => $id, 'SurveyTheme.is_deleted' => '0')));
        if (!sizeof($themeslist)) {
            $this->redirect(array('action' => 'add_theme_option/' . $id));
        } else {
            $this->redirect(array('action' => 'add_theme/' . $id));
        }
    }

    /**
     * add_theme method
     *
     * @throws NotFoundException
     * @param string $id
     * @param String $return
     * @return void
     */
    public function add_theme($id, $return = 0) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        if ($this->request->is('post')) {

            $names = $this->request->data['name'];

            $surveyId = $this->request->data['SurveyTheme']['survey_id'];
            $themeCnt = $this->SurveyTheme->find('count', array('conditions' => array('SurveyTheme.survey_id' => $id, 'SurveyTheme.is_deleted' => 0)));
            $cnt = $themeCnt;
            $notUnique = array();
            $colors = Configure::read('site_config.theme_colors');

            //checks if themes inputted are all unique 
            foreach ($names as $name) {
                if ($cnt == 10) {
                    $cnt = 0;
                }
                $this->SurveyTheme->create();
                $themeData = array('survey_id' => $surveyId, 'name' => $name, 'is_deleted' => '0', 'color' => $colors[$cnt]);

                if ($name) {
                    if ($this->SurveyTheme->is_unique_name($name, $id)) {
                        $this->SurveyTheme->save($themeData);
                    } else {
                        $notUnique[] = $name;
                    }
                }
                $cnt++;
            }

            if (sizeof($notUnique)) { //if there are duplicate theme names
                $notUniqueTxt = '';
                foreach ($notUnique as $key => $nu) {
                    $notUniqueTxt.= $key + 1 == sizeof($notUnique) ? $nu : ($key == sizeof($notUnique) - 2 ? $nu . ' and ' : $nu . ', ');
                }
                $notUniqueTxt2 = sizeof($notUnique) > 1 ? 'Error saving themes ' . $notUniqueTxt . '. Theme names are not unique.' : 'Error saving theme ' . $notUniqueTxt . '. Theme name is not unique.';
                $this->Session->setFlash(__($notUniqueTxt2), 'default', array('class' => 'alert alert-danger'));
            } else {
                if (!$return) {
                    $this->redirect(array('controller' => 'surveys', 'action' => 'add_theme_message/' . $id));
                } else {
                    $this->redirect(array('controller' => 'surveys', 'action' => 'index/' . $id));
                }
            }
        }
        $this->set('survey_id', $id);
        $themeslist = $this->Paginator->paginate(
                'SurveyTheme', array(
            'SurveyTheme.survey_id' => $id,
            'SurveyTheme.is_deleted' => '0'
                )
        );
        $this->set('themeslist', $themeslist);
        $this->set('return', $return);
    }

    /**
     * add_theme_message method
     * 
     * @param string $survey_id
     * @return void
     * 
     * This method is for the page display after the themes have been successfully added
     */
    public function add_theme_message($survey_id = null) {
        if (!$this->Survey->exists($survey_id) || !$this->user_allowed('Survey', $survey_id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $this->set('survey_id', $survey_id);
    }

    /**
     * 
     * 
     * @param type $id
     * @throws NotFoundException
     */
    public function survey_groups($id) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $groupslist = $this->Group->find('list', array('conditions' => array('Group.survey_id' => $id, 'Group.is_deleted' => 'no'), 'order' => array('Group.id ASC')));
        if (!sizeof($groupslist)) {
            $this->redirect(array('action' => 'add_group_options/' . $id));
        } else {
            $this->redirect(array('controller' => 'standard_groups', 'action' => 'choose/' . $id));
        }
    }

    /**
     * add_group method
     *
     * @throws NotFoundException
     * @param string $id
     * @param String $return
     * @return void
     */
    public function add_group($id, $return = 0) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }

        $groupslist = $this->Group->find('list', array('conditions' => array('Group.survey_id' => $id, 'Group.is_deleted' => 'no'), 'order' => array('Group.id ASC')));

        if ($this->request->is('post')) {

            $names = $this->request->data['name'];

            $surveyId = $this->request->data['Group']['survey_id'];
            $cnt = 0;
            $notUnique = array();

            //checks if groups inputted are all unique 
            foreach ($names as $name) {
                $this->Group->create();
                $groupData = array('survey_id' => $surveyId, 'name' => $name, 'status' => 'active', 'is_deleted' => 'no',);

                if ($name) {
                    if ($this->Group->is_unique_name($name, $id)) {
                        $this->Group->save($groupData);
                    } else {
                        $notUnique[] = $name;
                    }
                }
                $cnt++;
            }
            if (sizeof($notUnique)) { //if there are duplicate group names
                $notUniqueTxt = '';
                foreach ($notUnique as $key => $nu) {
                    $notUniqueTxt.= $key + 1 == sizeof($notUnique) ? $nu : ($key == sizeof($notUnique) - 2 ? $nu . ' and ' : $nu . ', ');
                }
                $notUniqueTxt2 = sizeof($notUnique) > 1 ? 'Error saving groups ' . $notUniqueTxt . '. Group names are not unique.' : 'Error saving group ' . $notUniqueTxt . '. Group name is not unique.';
                $this->Session->setFlash(__($notUniqueTxt2), 'default', array('class' => 'alert alert-danger'));
            } else {
                if (!$return) {
                    $this->redirect(array('controller' => 'surveys', 'action' => 'add_group_message/' . $id));
                } else {
                    $this->redirect(array('controller' => 'surveys', 'action' => 'index/' . $id));
                }
            }
        }
        $this->set('survey_id', $id);

        $this->set('grouplist', $groupslist);
    }

    /**
     * add_group_message method
     * This method is for the page display after the groups have been successfully added
     * 
     * @param string $survey_id
     * @return void
     */
    public function add_group_message($survey_id = null) {
        if (!$this->Survey->exists($survey_id) || !$this->user_allowed('Survey', $survey_id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $this->set('survey_id', $survey_id);
    }

    /**
     * add_theme_option method
     * 
     * @param type $survey_id
     */
    public function add_theme_option($survey_id) {

        $this->add_group_message($survey_id);

//        $grouplist = $this->Group->find('list', array('conditions' => array('Group.survey_id' => $survey_id, 'Group.is_deleted' => 'no'), 'order' => array('Group.id ASC')));
//        $this->set('grouplist', $grouplist);
        $this->survey_data($survey_id);
        $this->render('add_group_message');
    }

    /**
     * add_question_option method
     * 
     * @param type $survey_id
     */
    public function add_question_option($survey_id) {
        $this->add_theme_message($survey_id);
//        $themelist = $this->SurveyTheme->find('all', array('conditions' => array('SurveyTheme.survey_id' => $survey_id, 'SurveyTheme.is_deleted' => '0'), 'order' => array('SurveyTheme.id ASC')));
//        $this->set('themelist', $themelist);
        $this->survey_data($survey_id);
        $this->render('add_theme_message');
    }

    /**
     * add_question method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function add_question($id) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        if ($this->request->is('post')) {
            $options = array_filter($this->request->data['option']);
            $scores = $this->request->data['score'];

            if ($this->request->data['Question']['type'] == 'open ended') {  //if question is open ended
                $this->Question->create();
                $this->save_question($id, $options, $scores);
            } else { //question is either likert or radio button
                if (sizeof($options) >= 2) { // checks if number of options is equal or greater than 2
                    $this->Question->create();
                    $this->save_question($id, $options, $scores);
                } else {
                    $this->Session->setFlash(__('You should provide at least two options.'), 'default', array('class' => 'alert alert-danger'));
                }
            }
        }
        $types = $this->Question->get_question_type_array(); // returns question types array (radio button, likert, open ended) 
        $this->set(compact('types'));
        $themes = $this->SurveyTheme->find('list', array('conditions' => array('SurveyTheme.survey_id' => $id, 'SurveyTheme.is_deleted' => 0)));
        $this->set('themes', $themes);
        $this->set('survey_id', $id);
        $questions = $this->Question->find('all', array('conditions' => array('Question.survey_id' => $id, 'Question.is_deleted' => 'no'), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));
        $this->set(compact('questions'));
    }

    /**
     * save_question method
     *
     * @throws NotFoundException
     * @param string $id
     * @param array $options
     * @param array $scores
     * @return void
     */
    private function save_question($id, $options, $scores) {
        if ($this->Question->save($this->request->data)) {
            $anotherQuestion = $this->request->data['add_another_question'];

            //save options
            foreach ($options as $k => $option) {
                $this->QuestionsOption->create();
                $score++;
                $optionData = array('question_id' => $this->Question->getLastInsertId(), 'name' => $option, 'score' => $scores[$k]);
                if ($option) {
                    $this->QuestionsOption->save($optionData);
                }
            }

            $this->Session->setFlash(__('The question has been saved'), 'default', array('class' => 'alert alert-success'));
            if ($anotherQuestion) {
                $this->redirect(array('action' => 'add_question/' . $id));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        } else {
            $this->Session->setFlash(__('The question could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
        }
    }

    /**
     * 
     * @param type $status
     */
    private function check_status($status) {
        if (!$status) {
            $this->Session->setFlash(__($this->Setting->get_value('survey_not_started')), 'default', array('class' => 'alert alert-danger'));
            $this->set('status', $status);
        }
    }

    /**
     * verify method
     *
     * @throws NotFoundException
     * @param string $slug
     * @return void
     * 
     * Verifies respondent before taking the survey
     */
    public function verify($slug = null) {
        $this->layout = 'survey';
        $emailPopup = 0;
        $post = $this->Survey->findBySlug($slug);
        if (is_null($slug) || !sizeof($post)) {
            throw new NotFoundException(__('Invalid survey'));
        } else {
            $this->check_status($post['Survey']['status']);
            if ($this->request->is('post')) {
                $emailPopup = 1;
                if ($post['Survey']['email_verification_on']) { //if email verification is on
                    if (!empty($this->request->data['Survey']['email'])) { //validate email
                        if ($this->SurveyResult->find('count', array('conditions' => array('SurveyResult.email' => $this->request->data['Survey']['email'], 'SurveyResult.survey_id' => $post['Survey']['id'])))) {

                            $this->Session->setFlash(__($this->Setting->get_value('survey_cannot_take_survey_twice_message')), 'default', array('class' => 'alert alert-danger'));
                        } else {
                            if (filter_var($this->request->data['Survey']['email'], FILTER_VALIDATE_EMAIL)) {
                                $this->verify_password($post, $slug);
                            } else {
                                $this->Session->setFlash(__($this->Setting->get_value('survey_invalid_email_message')), 'default', array('class' => 'alert alert-danger'));
                            }
                        }
                    } else {
                        $this->Session->setFlash(__($this->Setting->get_value('survey_email_required_message')), 'default', array('class' => 'alert alert-danger'));
                    }
                } else {
                    $this->verify_password($post, $slug); //verify password
                }
            }
        }

        $tooltipHover = $this->Setting->get_value('survey_email_tooltip_hover'); //email tooltip hover
        $tooltipTitle = $this->Setting->get_value('survey_email_tooltip_title'); //email tooltip title
        $tooltipContent = $this->Setting->get_value('survey_email_tooltip_content'); //email tooltip content

        $this->set('emailPopup', $emailPopup);
        $this->set('survey', $post);
        $this->set(compact('tooltipHover', 'tooltipTitle', 'tooltipContent'));
        $this->set_survey_header($post['Survey']['id']);
        $this->set('footer', $this->get_survey_setting($post['Survey']['id'], 'footer', 'survey_default_footer'));
    }

    /**
     * verify_password method
     *
     * @param array $post
     * @param string $slug
     * @return void
     */
    private function verify_password($post, $slug = null) {

        $surveyId = $post['Survey']['id'];

        if ($post['Survey']['open']) { //open survey
            if ($post['Survey']['password'] == $this->request->data['Survey']['password'] || $post['Survey']['admin_password'] == $this->request->data['Survey']['password']) {
                $this->start_survey_session($surveyId, $slug, 1, $post); //start survey session
            } else {
                $this->Session->setFlash(__($this->Setting->get_value('survey_incorrect_password_message')), 'default', array('class' => 'alert alert-danger'));
            }
        } else { //closed survey
            $respondent = $this->RespondentPasscode->find('first', array('conditions' => array('RespondentPasscode.survey_id' => $surveyId, 'RespondentPasscode.passcode' => $this->request->data['Survey']['password'])));

            $hasAnswered = $this->SurveyResult->find('count', array('conditions' => array('SurveyResult.respondent' => $respondent['RespondentPasscode']['name'], 'SurveyResult.survey_id' => $surveyId, 'SurveyResult.is_admin' => 'no')));

            if ($post['Survey']['admin_password'] == $this->request->data['Survey']['password'] || sizeof($respondent)) {
                if($hasAnswered){
                   $this->Session->setFlash(__($this->Setting->get_value('survey_cannot_take_survey_twice_message')), 'default', array('class' => 'alert alert-danger'));
                }else{
                    $this->start_survey_session($surveyId, $slug, 0, $post, $respondent); //start survey session
                }
            } else {
                $this->Session->setFlash(__($this->Setting->get_value('survey_incorrect_password_message')), 'default', array('class' => 'alert alert-danger'));
            }
        }
    }

    /**
     * start_survey_session method verifies respondent password
     * 
     * @param int $surveyId
     * @param string $slug
     * @param int $open
     * @param array $respondent
     * @return void
     */
    private function start_survey_session($surveyId, $slug, $open, $post, $respondent = null) {
        $this->Session->setFlash(__($this->Setting->get_value('survey_password_correct_message')), 'default', array('class' => 'alert alert-success'));
        $sessionName = 'Survey_' . $surveyId; // survey session name
        $this->Session->delete($sessionName);
        $this->Session->write($sessionName . '.email', isset($this->request->data['Survey']['email']) ? $this->request->data['Survey']['email'] : '');
        $this->Session->write($sessionName . '.open', $open);
        $this->Session->write($sessionName . '.respondent', '');
        if ($post['Survey']['admin_password'] == $this->request->data['Survey']['password']) {
            $this->Session->write($sessionName . '.is_admin', 1);

            $this->redirect(array('action' => 'take/' . $slug));
        } else {
            $this->Session->write($sessionName . '.is_admin', 0);
            if (!$open) {
                $this->Session->write($sessionName . '.respondent', $respondent['RespondentPasscode']['name']);
                $this->Session->write($sessionName . '.group', $respondent['RespondentPasscode']['group_id']);
            }
            $this->redirect(array('action' => 'take/' . $slug));
        }
    }

    /**
     * take method
     *
     * @throws NotFoundException
     * @param string $slug
     * @return void
     */
    public function take($slug = null) {
        $this->layout = 'survey';
        $hasError = 0;
        $post = $this->Survey->findBySlug($slug);
        if (is_null($slug) || !sizeof($post)) {
            throw new NotFoundException(__($this->Setting->get_value('survey_invalid_survey_message')));
        }
        $this->check_status($post['Survey']['status']);
        $sessionName = 'Survey_' . $post['Survey']['id']; //survey session name
        //check session data
        if (!$this->Session->check($sessionName)) {
            $this->redirect(array('action' => 'verify/' . $slug));
        }

        if ($this->Session->read($sessionName . '.is_admin')) { // if respondent is admin
            $questions = $this->Question->find('list', array('conditions' => array('Question.survey_id' => $post['Survey']['id'], 'Question.is_deleted' => 'no', 'Question.type' => array('radio button', 'likert')), 'fields' => array('Question.id'), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));
        } else {
            $questions = $this->Question->find('list', array('conditions' => array('Question.survey_id' => $post['Survey']['id'], 'Question.is_deleted' => 'no'), 'fields' => array('Question.id'), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));
        }
        $groups = $this->Group->find('list', array('conditions' => array('Group.survey_id' => $post['Survey']['id'], 'Group.is_deleted' => 'no',), 'order' => array('Group.id ASC')));

        if (sizeof($groups) && sizeof($questions)) {
            $question_list = array();
            foreach ($questions as $k => $v) {
                $question_list[] = array($k => 0);
            }
            $this->Session->write($sessionName . '.id', $post['Survey']['id']); // survey id
            $this->Session->write($sessionName . '.ip', $this->request->clientIp()); // survey respondent's ip
            $this->Session->write($sessionName . '.questions', $question_list); // survey questions
            $this->Session->write($sessionName . '.questionAnswers', array()); // survey answers
            $this->set('survey', $post);
        } else {
            if (!sizeof($groups) && !sizeof($questions)) {
                $this->Session->setFlash(__($this->Setting->get_value('survey_no_groups_and_questions_message')), 'default', array('class' => 'alert alert-danger'));
            } elseif (!sizeof($questions)) {
                $this->Session->setFlash(__($this->Setting->get_value('survey_no_groups_message')), 'default', array('class' => 'alert alert-danger'));
            } else {
                $this->Session->setFlash(__($this->Setting->get_value('survey_no_questions_message')), 'default', array('class' => 'alert alert-danger'));
            }
            $hasError = 1;
        }

        $this->set('hasError', $hasError);
        $this->set_survey_header($post['Survey']['id']);
        $this->set('footer', $this->get_survey_setting($post['Survey']['id'], 'footer', 'survey_default_footer'));
    }

    /**
     * start method
     *
     * @throws NotFoundException
     * @return void
     */
    public function start($takenSurveyId) {

        if (!$this->Survey->exists($takenSurveyId)) {
            throw new NotFoundException(__('Invalid survey'));
        }

        $this->layout = 'survey';
        $sessionName = 'Survey_' . $takenSurveyId; //survey session name

        $dataSurvey = $this->Survey->read(null, $takenSurveyId);
        
        $this->check_status($dataSurvey['Survey']['status']);

        //check session data
        if (!$this->Session->check($sessionName)) {
            $this->redirect(array('action' => 'verify/' . $dataSurvey['Survey']['slug']));
        }

        $surveyId = $this->Session->read($sessionName . '.id');


        $groupMsg = $this->get_survey_setting($surveyId, 'group_message', 'survey_default_group_message');

        if ($surveyId) {
            $questionCount = $this->Question->find('count', array('conditions'=>array('Question.survey_id'=>$takenSurveyId,'Question.is_deleted' => 'no')));
            $pages = range(1,ceil($questionCount/10));
            $this->Session->write($sessionName . '.pages', $pages);
            if ($this->Session->read($sessionName . '.is_admin')) { // if respondent is admin
                $this->Session->write($sessionName . '.group', 0);
                
                $this->Session->write($sessionName . '.current_page', 1);
                $this->redirect(array('action' => 'answer/1/' . $surveyId));
                //$this->Session->write($sessionName . '.current_question', 0);
                //$this->redirect(array('action' => 'answer/' . array_shift(array_keys(array_shift(array_values($this->Session->read($sessionName . '.questions'))))) . '/' . $surveyId));
            }
            if (!$this->Session->read($sessionName . '.open')) { // if close survey, continue to questions
                $this->Session->write($sessionName . '.current_page', 1);
                $this->redirect(array('action' => 'answer/1/' . $surveyId));
                //$this->Session->write($sessionName . '.current_question', 0);
                //$this->redirect(array('action' => 'answer/' . array_shift(array_keys(array_shift(array_values($this->Session->read($sessionName . '.questions'))))) . '/' . $surveyId));
            }
            if ($this->request->is('post')) {
                if ($this->request->data['Question']['group']) {
                    $this->Session->write($sessionName . '.group', $this->request->data['Question']['group']);
                    $this->Session->write($sessionName . '.current_page', 1);
                    $this->redirect(array('action' => 'answer/1/' . $surveyId));
                    // $this->Session->write($sessionName . '.current_question', 0);
                    // $this->redirect(array('action' => 'answer/' . array_shift(array_keys(array_shift(array_values($this->Session->read($sessionName . '.questions'))))) . '/' . $surveyId));
                } else {
                    $this->Session->setFlash(__($this->Setting->get_value('survey_need_to_choose_group_message')), 'default', array('class' => 'alert alert-danger'));
                }
            }
            $groups = $this->Group->find('list', array('conditions' => array('Group.survey_id' => $surveyId, 'Group.is_deleted' => 'no'), 'order' => array('Group.id ASC')));
            $this->set('groups', $groups);
            $this->set('group_msg', $groupMsg);
            $this->set_survey_header($surveyId);
            $this->set('footer', $this->get_survey_setting($surveyId, 'footer', 'survey_default_footer'));
        } else {
            throw new NotFoundException(__($this->Setting->get_value('survey_invalid_survey_message')));
        }
    }

    /**
     * answer method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function answer($page, $takenSurveyId = null) {

        if (!$this->Survey->exists($takenSurveyId)) {
            throw new NotFoundException(__('Invalid survey'));
        }


        $this->layout = 'survey';

        $sessionName = 'Survey_' . $takenSurveyId;


        $dataSurvey = $this->Survey->read(null, $takenSurveyId);

        $this->check_status($dataSurvey['Survey']['status']);
        
        //check session data
        if (!$this->Session->check($sessionName)) {
            $this->redirect(array('action' => 'verify/' . $dataSurvey['Survey']['slug']));
        }


        $survey = $this->Session->read($sessionName);
        
        $surveyId = $survey['id']; //session survey id

        $answers = isset($survey['questionAnswers'])?$survey['questionAnswers']:array();
        
        if ($surveyId) {

            if (!$survey['is_admin'] && !$survey['group']) { // if admin
                $this->redirect(array('action' => 'start/' . $surveyId)); //skip group question
            }

            if(!in_array($page, $survey['pages']) || $page != $survey['current_page']){ //current page checking
                $this->redirect(array('action' => 'answer/' . $survey['current_page'].'/'.$surveyId)); //redirect to current page
            }


            /**Change on 07/31/2017 by Gale
             * questions of survey
             * $questions = $this->Question->find('all', array('conditions'=>array('Question.survey_id'=>$takenSurveyId,'Question.is_deleted' => 'no'),'page'=>$page, 'limit'=>10, 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));
             * $questions = $this->Question->find('all', array('conditions'=>array('Question.survey_id'=>$takenSurveyId,'Question.is_deleted' => 'no'),'page'=>$page, 'limit'=>10, 'order' => array('FIELD(Question.type, "likert", "radio button", "open ended") ASC')));
             */
            $questions = $this->Question->find('all', array('conditions'=>array('Question.survey_id'=>$takenSurveyId,'Question.is_deleted' => 'no'),'page'=>$page, 'limit'=>10, 'order' => array('Question.type ASC', 'Question.id ASC')));



            /*$questions = $this->Question->find('all', array('conditions'=>array('Question.survey_id'=>$takenSurveyId,'Question.is_deleted' => 'no'),'page'=>$page, 'limit'=>10, 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));*/

            if ($this->request->is('post')) {
                if(in_array("",$this->request->data['Question']['answer'])){ //if there are unanswered questions
                    //show error
                    $this->Session->setFlash(__($this->Setting->get_value('answer_all_items_error')), 'default', array('class' => 'alert alert-danger'));
                }else{

                    $errorMessages = array();
                    $answers = array();
                    
                    foreach($questions as $k=>$question){
                        $qId = $question['Question']['id']; //question id
                        $qAnswer = $this->request->data['Question']['answer'][$qId]; //respondent's answer to the question
                        if ($question['Question']['type'] == 'open ended' && strlen($qAnswer) > 1000) { //if open ended question and answer has more than 1000 characters
                            $errorMessages[] = $this->Setting->get_value('survey_open_ended_answer_maximum_characters_message'); //show error
                            break;
                        } else {
                            if($question['Question']['type'] != 'open ended'){ //if likert or radio button
                                
                                if(array_search($qAnswer, array_column($question['QuestionsOption'], 'id')) === false){ 
                                    $errorMessages[] = $this->Setting->get_value('answer_all_items_error'); //show error

                                    break; //break loop
                                }
                            }
                            $answers[$qId] = $qAnswer; //store answer
                        }
                    }
                    if(sizeof($errorMessages)){
                        $this->Session->setFlash(__(implode('<br />', $errorMessages)), 'default', array('class' => 'alert alert-danger'));
                    }else{
                        
                        $answers = isset($survey['questionAnswers'])?$survey['questionAnswers'] + $answers:$answers;
                     
                        $this->Session->write($sessionName . '.questionAnswers', $answers);
                        
                        $currentPage = $page+1; //current page is next page
                        
                        if(in_array($currentPage, $survey['pages'])){ //if there's a next page
                            $this->Session->write($sessionName . '.current_page', $page+1);
                            $this->redirect(array('action' => 'answer/'.$currentPage.'/' . $takenSurveyId));  
                        }else{
                           $this->save_answers($takenSurveyId);
                        }
                        
                    }
                }
            }

            $this->set('questions', $questions);
            $this->set_survey_header($surveyId);
            $this->set('percent', round((sizeof($answers) / sizeof($survey['questions'])) * 100, 2));
            $this->set('footer', $this->get_survey_setting($surveyId, 'footer', 'survey_default_footer'));
        } else {
            throw new NotFoundException(__($this->Setting->get_value('survey_invalid_survey_message')));
        }

        
    }


    /**
     * save_answers method
     *
     * @param string $surveyId
     * @return void
     */
    private function save_answers($surveyId) {
        
        $latestSurvey = $this->Session->read('Survey_' . $surveyId);
        $this->SurveyResult->create();
        $surveyData = array('survey_id' => $surveyId, 'respondent' => $latestSurvey['respondent'], 'group_id' => $latestSurvey['group'], 'is_admin' => $latestSurvey['is_admin'] ? 'yes' : 'no', 'ipaddress' => $latestSurvey['ip'], 'email' => isset($latestSurvey['email']) ? $latestSurvey['email'] : '');
        //save survey result data
        if ($this->SurveyResult->save($surveyData)) {

            foreach ($latestSurvey['questionAnswers'] as $q=>$ans) {
                $this->SurveyAnswer->create();
                $que = $this->Question->read('type', $q);

                if ($ans) {
                    $surveyAns = array(
                        'survey_result_id' => $this->SurveyResult->getLastInsertId(),
                        'question_id' => $q,
                    );
                    if (in_array($que['Question']['type'], array('likert', 'radio button'))) {
                        $surveyAns['question_option_id'] = $ans;
                    } else {
                        $surveyAns['question_answer'] = $ans;
                    }

                    $this->SurveyAnswer->save($surveyAns);
                }
            }
            $this->Session->delete('Survey_' . $surveyId);
            $this->redirect(array('action' => 'thank_you/' . $surveyId));
        }
    }

    /**
     * thank_you method
     *
     * @param string $surveyId
     * @return void
     */
    public function thank_you($surveyId) {
        $this->layout = 'survey';
        $this->set('closing_message', $this->Survey->read('closing_message', $surveyId));
        $this->set_survey_header($surveyId);
        $this->set('footer', $this->get_survey_setting($surveyId, 'footer', 'survey_default_footer'));
    }

    /**
     * result method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function result($id = null) {
        $this->get_result($id);
    }

    /**
     * 
     * @param type $id
     */
    private function get_result($id = null) {
        //init result
        $resInit = $this->init_result($id);

        //for open ended questions
        $openEndedArray = $this->result_open_ended($id, $resInit['groups']);

        //for non-calculating questions
        $nonCalculatingArray = $this->result_non_calculating($id, $resInit['groups']);

        !$resInit['hasResult'] ? $this->Session->setFlash(__('No report to be displayed yet.'), 'default', array('class' => 'alert alert-danger')) : '';
        $this->set('hasResult', $resInit['hasResult']);
        $this->set('hasError', $resInit['hasError']);
        $this->set('openEnded', $openEndedArray);
        $this->set('nonCalculating', $nonCalculatingArray);
        $this->set('settings', $this->Setting->find('list', array('fields' => array('Setting.slug', 'Setting.value'), 'conditions' => array('Setting.slug LIKE' => 'report_%'))));
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws NotFoundException
     */
    private function init_result($id = null) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }

        $result = array();
        $groups = $this->Group->find('list', array('conditions' => array('Group.survey_id' => $id, 'Group.is_deleted' => 'no'), 'order' => array('Group.id ASC')));
        $questions = $this->Question->find('list', array('conditions' => array('Question.survey_id' => $id, 'Question.is_deleted' => 'no'), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));

        $groupArray = array();
        $qAvgSums = array();
        $totalRespCnt = 0;
        $hasResult = 0;
        $hasError = 0;
        $surveyResultCnt = $this->SurveyResult->find('count', array('conditions' => array('SurveyResult.survey_id' => $id)));
        //if results found
        if ($surveyResultCnt) {
            $hasResult = 1;
            // if there are already questions and answers
            if (sizeof($groups) && sizeof($questions)) {

                $calculatingQ = $this->Question->find('list', array('conditions' => array('Question.survey_id' => $id, 'Question.is_calculating' => '1', 'Question.type' => array('likert', 'radio button'), 'Question.is_deleted' => 'no'), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));
                $groupScore = $this->result_score_by_group($id, $groups, $calculatingQ, $totalRespCnt);
                $groupArray = $groupScore['groupArray'];
                $totalRespCnt = $groupScore['totalRespCnt'];
                $qAvgSums = $groupScore['qAvgSums'];

                $result['groups'] = $groupArray;

                // for total average score
                foreach ($qAvgSums as $qAvgK => $qAvgV) {
                    $result['totalAvg']['avgScores'][$qAvgK] = array_sum($qAvgV['totalRespCount']) ? round(array_sum($qAvgV['totalScores']) / array_sum($qAvgV['totalRespCount']), 2) : 0;
                    $result['totalAvg']['respCount'][$qAvgK] = array_sum($qAvgV['totalRespCount']);
                }
                $result['totalAvg']['totalAvgScore'] = sizeof($result['totalAvg']['avgScores']) ? round(array_sum($result['totalAvg']['avgScores']) / sizeof($result['totalAvg']['avgScores']), 2) : 0;
                $result['totalAvg']['totalRespCount'] = max($result['totalAvg']['respCount']);
                $result['questions'] = $this->Question->find('all', array('conditions' => array('Question.survey_id' => $id, 'Question.is_deleted' => 'no'), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));

                //for admin
                $result['admin'] = $this->result_admin($id, $calculatingQ, $result);

                //for awareness quotient
                foreach ($result['totalAvg']['avgScores'] as $key => $val) {
                    $result['AQ']['aqs'][$key] = $val - $result['admin']['scores'][$key];
                }
                $result['AQ']['total'] = sizeof($result['AQ']['aqs']) ? round(array_sum($result['AQ']['aqs']) / sizeof($result['AQ']['aqs']), 2) : 0;

                $result['survey'] = $this->Survey->read(array('Survey.name', 'Survey.id'), $id);
                $this->set('result', $result);
                $this->set('themes', $this->SurveyTheme->find('all', array('conditions' => array('SurveyTheme.survey_id' => $id), 'fields' => array('SurveyTheme.name', 'SurveyTheme.color'))));
            } else {
                if (!sizeof($groups) && !sizeof($questions)) {
                    $this->Session->setFlash(__('You haven\'t added survey groups and questions yet.'), 'default', array('class' => 'alert alert-danger'));
                } elseif (!sizeof($questions)) {
                    $this->Session->setFlash(__('You haven\'t added questions yet.'), 'default', array('class' => 'alert alert-danger'));
                } else {
                    $this->Session->setFlash(__('You haven\'t added groups yet.'), 'default', array('class' => 'alert alert-danger'));
                }
                if (!$hasError) {
                    $hasError = 1;
                }
            }
        } else {
            if (!$hasError) {
                $hasError = 1;
            }
        }
        return array('hasError' => $hasError, 'hasResult' => $hasResult, 'groups' => $groups);
    }

    /**
     * result_score_by_group method
     *
     * @param string $id
     * @param array $groups
     * @param array $questions
     * $param integer $totalRespCnt
     * @return array
     */
    private function result_score_by_group($id, $groups, $questions, $totalRespCnt) {
        foreach ($groups as $k => $v) {
            $surveyResults = $this->SurveyResult->find('list', array('conditions' => array('SurveyResult.group_id' => $k, 'SurveyResult.is_admin' => 'no', 'SurveyResult.survey_id' => $id)));
            $groupArray[$v]['avgScores'] = array();
            $groupArray[$v]['respCount'] = array();

            foreach ($questions as $qk => $qv) {
                $ans = $this->SurveyAnswer->find('all', array(
                    'conditions' => array('SurveyAnswer.question_id' => $qk, 'SurveyAnswer.survey_result_id' => $surveyResults),
                    'fields' => array('SurveyAnswer.question_option_id', 'SurveyAnswer.question_id')));
                $totalScores = 0;
                $totalResp = 0;
                if ($qv['Question']['is_calculating']) {
                    foreach ($ans as $a) {

                        if (intval($a['SurveyAnswer']['question_option_id'])) { // for no response -  non-caculating
                            $score = $this->QuestionsOption->read('QuestionsOption.score', intval($a['SurveyAnswer']['question_option_id']));

                            $totalScores += sizeof($score) ? $score['QuestionsOption']['score'] : 0;
                        }
                    }
                    $groupArray[$v]['respCount'][$qk] = sizeof($ans); //number of respondents per question per group
                    $groupArray[$v]['avgScores'][$qk] = $groupArray[$v]['respCount'][$qk] ? round($totalScores / $groupArray[$v]['respCount'][$qk], 2) : 0; // average score per question per group

                    $totalRespCnt += $groupArray[$v]['respCount'][$qk];
                    $qAvgSums[$qk]['totalRespCount'][$k] = sizeof($ans);
                    $qAvgSums[$qk]['totalScores'][$k] = $totalScores; //total scores of respondents per question per group
                }
            }
            $groupArray[$v]['totalRespCount'] = max($groupArray[$v]['respCount']);
            $groupArray[$v]['totalAvgScore'] = $groupArray[$v]['avgScores'] ? round(array_sum($groupArray[$v]['avgScores']) / sizeof($groupArray[$v]['avgScores']), 2) : 0;
        }
        return array('groupArray' => $groupArray, 'totalRespCnt' => $totalRespCnt, 'qAvgSums' => $qAvgSums);
    }

    /**
     * result_open_ended method
     *
     * @param string $id
     * @param array $groups
     * @return array
     */
    private function result_open_ended($id, $groups) {
        $openEndedArray = array();
        $oeQuestions = $this->Question->find('list', array('conditions' => array('Question.survey_id' => $id, 'Question.type' => 'open ended'), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));
        if (sizeof($groups) && sizeof($oeQuestions)) {
            foreach ($oeQuestions as $qk => $qv) {
                $openEndedArray[$qk]['question_name'] = $qv;
                $openEndedArray[$qk]['groups'] = array();
                foreach ($groups as $gk => $gv) {
                    //non-admin answers
                    $surveyResults = $this->SurveyResult->find('all', array('conditions' => array('SurveyResult.group_id' => $gk, 'SurveyResult.is_admin' => 'no', 'SurveyResult.survey_id' => $id, 'SurveyResult.is_deleted' => 'no')));

                    $openEndedArray[$qk]['groups'][$gk]['name'] = $gv;
                    $openEndedArray[$qk]['groups'][$gk]['resp'] = array();

                    $cnt = 1;
                    foreach ($surveyResults as $sk => $sv) {

                        $oeAns = $this->SurveyAnswer->find('first', array(
                            'conditions' => array('SurveyAnswer.question_id' => $qk, 'SurveyAnswer.survey_result_id' => $sv['SurveyResult']['id'], 'SurveyAnswer.is_deleted' => 0),
                            'fields' => array('SurveyAnswer.question_answer')));
                        if (sizeof($oeAns)) {
                            $openEndedArray[$qk]['groups'][$gk]['resp'][$sv['SurveyResult']['id']]['email'] = isset($sv['SurveyResult']['email']) && $sv['SurveyResult']['email'] ? $sv['SurveyResult']['email'] : $cnt;
                            $openEndedArray[$qk]['groups'][$gk]['resp'][$sv['SurveyResult']['id']]['answer'] = isset($oeAns['SurveyAnswer']['question_answer']) ? $oeAns['SurveyAnswer']['question_answer'] : '';
                            $cnt++;
                        }
                    }
                }
            }
        }
        return $openEndedArray;
    }

    /**
     * result_non_calculating method
     *
     * @param string $id
     * @param array $groups
     * @return array
     */
    private function result_non_calculating($id, $groups) {
        $ncArray = array();
        $ncQuestions = $this->Question->find('list', array('conditions' => array('Question.survey_id' => $id, 'Question.type' => array('radio button', 'likert'), 'Question.is_calculating' => 0), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));
        if (sizeof($groups) && sizeof($ncQuestions)) {
            foreach ($ncQuestions as $qk => $qv) {
                $ncArray[$qk]['question_name'] = $qv;
                $ncArray[$qk]['question_options'] = $this->QuestionsOption->find('list', array('conditions' => array('QuestionsOption.question_id' => $qk)));

                //non-admin answers
                foreach ($groups as $gk => $gv) {
                    foreach ($ncArray[$qk]['question_options'] as $ko => $o) {
                        if (!isset($ncArray[$qk]['groups'][$gk][$ko])) {
                            $ncArray[$qk]['groups'][$gk][$ko] = 0;
                        }
                    }
                    $surveyResults = $this->SurveyResult->find('all', array('conditions' => array('SurveyResult.group_id' => $gk, 'SurveyResult.is_admin' => 'no', 'SurveyResult.survey_id' => $id, 'SurveyResult.is_deleted' => 'no')));
                    $ncArray[$qk]['groups'][$gk]['name'] = $gv;
                    $ncArray[$qk]['groups'][$gk]['resp'] = array();
                    foreach ($surveyResults as $sk => $sv) {

                        $oeAns = $this->SurveyAnswer->find('first', array(
                            'conditions' => array('SurveyAnswer.question_id' => $qk, 'SurveyAnswer.survey_result_id' => $sv['SurveyResult']['id'], 'SurveyAnswer.is_deleted' => 0),
                            'fields' => array('SurveyAnswer.question_option_id')));
                        if (sizeof($oeAns)) {
                            $ncArray[$qk]['groups'][$gk][$oeAns['SurveyAnswer']['question_option_id']] = $ncArray[$qk]['groups'][$gk][$oeAns['SurveyAnswer']['question_option_id']] + 1;
                        }
                    }
                }

                //for admin
                $surveyResultsAdmin = $this->SurveyResult->find('all', array('conditions' => array('SurveyResult.is_admin' => 'yes', 'SurveyResult.survey_id' => $id, 'SurveyResult.is_deleted' => 'no')));
                foreach ($surveyResultsAdmin as $sak => $sav) {
                    foreach ($ncArray[$qk]['question_options'] as $ko => $o) {
                        if (!isset($ncArray[$qk]['admin'][$ko])) {
                            $ncArray[$qk]['admin'][$ko] = 0;
                        }
                    }
                    $oeAdminAns = $this->SurveyAnswer->find('first', array(
                        'conditions' => array('SurveyAnswer.question_id' => $qk, 'SurveyAnswer.survey_result_id' => $sav['SurveyResult']['id'], 'SurveyAnswer.is_deleted' => 0),
                        'fields' => array('SurveyAnswer.question_option_id')));
                    if (sizeof($oeAdminAns)) {
                        $ncArray[$qk]['admin'][$oeAdminAns['SurveyAnswer']['question_option_id']] = $ncArray[$qk]['admin'][$oeAdminAns['SurveyAnswer']['question_option_id']] + 1;
                    }
                }
            }
        }
        return $ncArray;
    }

    /**
     * result_admin method
     *
     * @param string $id
     * @param array $questions
     * @param array $result
     * @return array
     */
    private function result_admin($id, $questions, $result) {
        $adminSurveyRes = $this->SurveyResult->find('list', array('conditions' => array('SurveyResult.is_admin' => 'yes', 'SurveyResult.survey_id' => $id)));
        $result['admin']['scores'] = array();
        foreach ($questions as $qk => $qv) {
            $qAdminScore = 0;
            $ans = $this->SurveyAnswer->find('all', array(
                'conditions' => array('SurveyAnswer.question_id' => $qk, 'SurveyAnswer.survey_result_id' => $adminSurveyRes),
                'fields' => array('SurveyAnswer.question_option_id')));
            foreach ($ans as $a) {
                $score = $this->QuestionsOption->read('QuestionsOption.score', $a['SurveyAnswer']['question_option_id']);
                $qAdminScore += sizeof($score) ? $score['QuestionsOption']['score'] : 0;
            }
            if (sizeof($ans)) {
                $result['admin']['scores'][$qk] = sizeof($ans) ? round($qAdminScore / sizeof($ans), 2) : '';
            }
        }
        $result['admin']['totalAvgScore'] = sizeof($result['admin']['scores']) ? round(array_sum($result['admin']['scores']) / sizeof($result['admin']['scores']), 2) : '';
        return $result['admin'];
    }

    /**
     * email_verification method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function email_verification($id) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Survey->save($this->request->data)) {
                $this->Session->setFlash(__('The survey has been saved'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The survey could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('Survey.' . $this->Survey->primaryKey => $id));
            $this->request->data = $this->Survey->find('first', $options);
        }
    }

    /**
     * welcome_message method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function welcome_message($id = null) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Survey->save($this->request->data)) {
                $this->Session->setFlash(__('The survey has been updated'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The survey could not be updated. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $this->request->data = $this->Survey->read(null, $id);
        }
    }

    /**
     * closing_message method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function closing_message($id = null) {
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Survey->save($this->request->data)) {
                $this->Session->setFlash(__('The survey has been updated'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The survey could not be updated. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $this->request->data = $this->Survey->read(null, $id);
        }
    }

    /**
     * delete_respondent method
     *
     * @throws NotFoundException
     * @param string $id
     * @param string $surveyId
     * @return void
     */
    public function delete_respondent($id = null, $surveyId = null) {
        $this->SurveyResult->id = $id;
        if (!$this->SurveyResult->exists() || !$this->Survey->exists($surveyId) || !$this->user_allowed('Survey', $surveyId)) {
            throw new NotFoundException(__('Invalid respondent'));
        }

        $this->SurveyAnswer->deleteAll(array('SurveyAnswer.survey_result_id' => $id), false);

        if ($this->SurveyResult->delete()) {
            $this->Session->setFlash(__('Respondent deleted'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'view/' . $surveyId));
        }
        $this->Session->setFlash(__('Respondent was not deleted'), 'default', array('class' => 'alert alert-danger'));
        $this->redirect(array('action' => 'view/' . $surveyId));
    }

    /**
     * upload_survey method
     *
     * @return void
     */
    public function upload_survey() {
        $success = false;
        if (isset($this->request->data['Survey'])) {
            $filename = null;
            if (!empty($this->request->data['Survey']['survey']['tmp_name']) && is_uploaded_file($this->request->data['Survey']['survey']['tmp_name'])) {
                // Strip path information
                $filename = basename($this->request->data['Survey']['survey']['name']);
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (in_array($ext, array('xls'))) {
                    move_uploaded_file(
                            $this->data['Survey']['survey']['tmp_name'], WWW_ROOT . DS . 'files' . DS . $filename
                    );
                } else {
                    $this->Session->setFlash("Survey should be in excel format.", "default", array("class" => "alert alert-danger"));
                    $this->redirect(array('action' => 'upload_survey'));
                }
            } else {
                $this->Session->setFlash("No readable file chosen!", "default", array("class" => "alert alert-danger"));
                $this->redirect(array('action' => 'upload_survey'));
            }

            $read = $this->ExcelReader->Spreadsheet_Excel_Reader('files/' . $filename);
            if (false === $read) {
                $this->Session->setFlash("No readable file chosen!", "default", array("class" => "alert alert-danger"));
                $this->redirect(array('action' => 'upload_survey'));
            }

            $data = $this->ExcelReader->sheets[0]['cells'];

            unlink('files/' . $filename); //delete the xls file

            $surveyData = array('name' => $data[1][2], 'slug' => $this->Auth->user('user_id') . '_' . Inflector::slug($data[1][2]), 'description' => $data[2][2], 'password' => $data[3][2], 'admin_password' => $data[4][2], 'email_verification_on' => $data[5][2] == 'on' ? 1 : 0, 'welcome_message' => $data[6][2], 'closing_message' => $data[7][2], 'user_id' => $this->Auth->user('user_id'));
            $this->Survey->set($surveyData);
            if (!$this->Survey->validates()) {
                $surveyError = array_shift(array_values($this->Survey->validationErrors));
                $this->Session->setFlash($surveyError[0], "default", array("class" => "alert alert-danger"));
                $this->redirect(array('action' => 'upload_survey'));
            } else {
                $groupsData = $this->groups_data($data);
                if ($groupsData['notUnique'] || !sizeof($groupsData['groups'])) {
                    if ($groupsData['notUnique']) {
                        $this->Session->setFlash(__('Error saving survey. Groups are not unique.'), 'default', array('class' => 'alert alert-danger'));
                        $this->redirect(array('action' => 'upload_survey'));
                    }
                    if (!sizeof($groupsData['groups'])) {
                        $this->Session->setFlash(__('You should enter at least one group.'), 'default', array('class' => 'alert alert-danger'));
                        $this->redirect(array('action' => 'upload_survey'));
                    }
                } else {
                    $themesData = $this->themes_data($data, $groupsData['index']);
                    if ($themesData['notUnique'] || !sizeof($themesData['themes'])) {
                        if ($themesData['notUnique']) {
                            $this->Session->setFlash(__('Error saving survey. Themes are not unique.'), 'default', array('class' => 'alert alert-danger'));
                            $this->redirect(array('action' => 'upload_survey'));
                        }
                        if (!sizeof($themesData['themes'])) {
                            $this->Session->setFlash(__('You should enter at least one theme.'), 'default', array('class' => 'alert alert-danger'));
                            $this->redirect(array('action' => 'upload_survey'));
                        }
                    } else {
                        $surveyValidData = $this->save_uploaded_data($data, $surveyData, $groupsData['groups'], $groupsData['index']);
                        if ($surveyValidData['success']) {
                            $survey = $this->Survey->read(array('Survey.slug', 'Survey.id'), $surveyValidData['surveyId']);
                            $this->set(compact('survey'));
                            $success = true;
                            $this->Session->setFlash(__('Successfully uploaded questionnaire.'), 'default', array('class' => 'alert alert-success'));
                        }
                    }
                }
            }
        }
        $this->set('success', $success);
    }

    private function groups_data($data) {
        $groupArray = array();
        if ($data[9][2] != '') {
            $groupArray[] = $data[9][2];
        }
        $cnt = 10;
        while ($data[$cnt][1] == '') {
            if ($data[$cnt][2] != '') {
                $groupArray[] = $data[$cnt][2];
            }
            $cnt++;
        }
        $notUnique = count($groupArray) !== count(array_unique($groupArray));
        return array('notUnique' => $notUnique, 'index' => $cnt, 'groups' => $groupArray);
    }

    private function themes_data($data, $grpIndex) {
//        unset($data[$grpIndex]); // unsetting the headers
        $themeArray = array();
        $cnt = $grpIndex;
        if (strtolower($data[$grpIndex][1]) == 'theme' && strtolower($data[$grpIndex][2]) == 'question') {
            $cnt = $grpIndex + 1;
        }
        while ($data[$cnt]) {
            if ($data[$cnt][1] != '') {
                $themeArray[] = $data[$cnt][1];
            }
            $cnt++;
        }
        $notUnique = count($themeArray) !== count(array_unique($themeArray));
        return array('notUnique' => $notUnique, 'themes' => $themeArray);
    }

    /**
     * save_uploaded_data method
     *
     * @param string $surveyId
     * @param array $data
     * @return void
     */
    private function save_uploaded_data($data, $surveyData, $groups, $index) {
        $themecnt = $optionCnt = 0;
        $colors = Configure::read('site_config.theme_colors');
        $currentTheme = '';
        $currentQuestion = '';
        $cnt = $index;
        if (strtolower($data[$index][1]) == 'theme' && strtolower($data[$index][2]) == 'question') {
            $cnt = $index + 1;
        }

        //validating data under survey
        if ($this->validate_uploaded_data($data, $surveyData, $groups, $index)) {

//            save survey details
            $this->Survey->create();
            if ($this->Survey->save($surveyData)) {
                $surveyId = $this->Survey->getLastInsertId();

                //save group message
                $this->SurveySetting->create();
                $this->SurveySetting->save(array('survey_id' => $surveyId, 'name' => 'Group Message', 'slug' => 'group_message', 'value' => $data[8][2]));

                //save groups
                foreach ($groups as $group) {
                    $this->Group->create();
                    $this->Group->save(array('survey_id' => $surveyId, 'name' => $group));
                }

                //save questionnaire
                while ($data[$cnt]) {
                    $v = $data[$cnt];
                    //theme
                    if (isset($v[1]) && $v[1]) {
                        if ($themecnt == 10) {
                            $themecnt = 0;
                        }
                        $this->SurveyTheme->create();
                        $themeData = array('survey_id' => $surveyId, 'name' => $v[1], 'is_deleted' => '0', 'color' => $colors[$themecnt]);

                        if ($this->SurveyTheme->save($themeData)) {
                            $currentTheme = $this->SurveyTheme->getLastInsertId();
                        } else {
                            continue;
                        }
                        $themecnt++;
                    }
                    //question
                    if (isset($v[2]) && $v[2]) {
                        $qData = array(
                            'survey_id' => $surveyId,
                            'name' => $v[2],
                            'type' => isset($v[4]) && in_array($v[4], array('likert', 'radio button', 'open ended')) ? $v[4] : 'open ended',
                            'is_calculating' => isset($v[3]) && $v[3] == 'y' ? '1' : '0',
                            'survey_theme_id' => $currentTheme
                        );
                        $this->Question->create();
                        if ($this->Question->save($qData)) {
                            $currentQuestion = $this->Question->getLastInsertId();
                        } else {
                            continue;
                        }
                        $optionCnt = 0;
                    }
                    //option
                    if (isset($v[5]) && $v[5]) {
                        $optionCnt++;
                        $oData = array(
                            'question_id' => $currentQuestion,
                            'name' => $v[5],
                            'score' => isset($v[6]) && $v[6] && is_int($v[6]) ? $v[6] : 0
                        );
                        $this->QuestionsOption->create();
                        if (!$this->QuestionsOption->save($oData)) {
                            continue;
                        }
                    }
                    $cnt++;
                }
                return array('success' => true, 'surveyId' => $surveyId);
            }
        } else {
            return array('success' => false, 'surveyId' => 0);
        }
    }

    private function validate_uploaded_data($data, $surveyData, $groups, $index) {
        $themecnt = $optionCnt = 0;
        $cnt = $index;
        if (strtolower($data[$index][1]) == 'theme' && strtolower($data[$index][2]) == 'question') {
            $cnt = $index + 1;
        }
        while ($data[$cnt]) {
            $v = $data[$cnt];
            //theme
            if (isset($v[1]) && $v[1]) {
                if ($themecnt == 10) {
                    $themecnt = 0;
                }
                $themecnt++;
            }

            //validating questions
            if (isset($v[2]) && $v[2]) {
                if (!isset($v[4]) || !in_array($v[4], array('likert', 'radio button', 'open ended'))) {
                    $this->Session->setFlash(__("Question Type value should only be either 'likert', 'radio button' or 'open ended'. Please check your excel sheet for possible errors."), 'default', array('class' => 'alert alert-danger'));
                    return false;
                } else if (!(isset($v[3]) && ($v[3] == 'y' || $v[3] == 'n'))) {
                    $this->Session->setFlash(__("Calculating value should only be either 'y' or 'n'. Please check your excel sheet for possible errors."), 'default', array('class' => 'alert alert-danger'));
                    return false;
                } else {
                    $optionCnt = 0;
                }
            }
            //validating options
            if (isset($v[5]) && $v[5]) {
                if (isset($v[6]) && $v[6]) {
                    $optionCnt++;
                    if (!is_int($v[6]) || $v[6] < 0) {
                        $this->Session->setFlash(__("Options Scoring value should be an integer and should be greater than or equal to zero. Please check your excel sheet for possible errors."), 'default', array('class' => 'alert alert-danger'));
                        return false;
                    }
                }
            }
            $cnt++;
        }
        return true;
//            return $surveyId;
    }

    private function set_survey_header($surveyId) {
        $surveyData = $this->Survey->read(null, $surveyId);
        $surveySetting = $this->SubscriberSetting->find('list', array('fields' => array('SubscriberSetting.slug', 'SubscriberSetting.value'), 'conditions' => array('SubscriberSetting.subscriber_id' => $surveyData['Survey']['user_id'])));

        $toolName = isset($surveySetting['header_tool_name']) ? $surveySetting['header_tool_name'] : $this->Setting->get_value('survey_default_tool_name_header');
        $companyName = isset($surveySetting['header_company_name']) ? $surveySetting['header_company_name'] : $this->Setting->get_value('survey_default_company_name_header');
        $logo = isset($surveySetting['header_logo']) ? $surveySetting['header_logo'] : $this->Setting->get_value('survey_default_logo_header');

        $toolNameSize = isset($surveySetting['header_tool_name_font_size']) ? $surveySetting['header_tool_name_font_size'] : $this->Setting->get_value('header_default_tool_name_font_size');
        $toolNameColor = isset($surveySetting['header_tool_name_font_color']) ? $surveySetting['header_tool_name_font_color'] : $this->Setting->get_value('header_default_tool_name_font_color');
        $toolNameAlignment = isset($surveySetting['header_tool_name_alignment']) ? $surveySetting['header_tool_name_alignment'] : $this->Setting->get_value('header_default_tool_name_alignment');
        $surveyNameSize = isset($surveySetting['header_survey_name_font_size']) ? $surveySetting['header_survey_name_font_size'] : $this->Setting->get_value('header_default_survey_name_font_size');
        $surveyNameColor = isset($surveySetting['header_survey_name_font_color']) ? $surveySetting['header_survey_name_font_color'] : $this->Setting->get_value('header_default_survey_name_font_color');
        $surveyNameAlignment = isset($surveySetting['header_survey_name_alignment']) ? $surveySetting['header_survey_name_alignment'] : $this->Setting->get_value('header_default_survey_name_alignment');
        $companyNameSize = isset($surveySetting['header_company_name_font_size']) ? $surveySetting['header_company_name_font_size'] : $this->Setting->get_value('header_default_company_name_font_size');
        $companyNameColor = isset($surveySetting['header_company_name_font_color']) ? $surveySetting['header_company_name_font_color'] : $this->Setting->get_value('header_default_company_name_font_color');
        $companyNameAlignment = isset($surveySetting['header_company_name_alignment']) ? $surveySetting['header_company_name_alignment'] : $this->Setting->get_value('header_default_company_name_alignment');
        $backgroundColor = isset($surveySetting['header_background_color']) ? $surveySetting['header_background_color'] : $this->Setting->get_value('header_default_background_color');

        //feedback for
        $feedbackFor = $this->Setting->get_value('survey_header_feedback_for');        

        $this->set(compact('surveyData', 'toolName', 'companyName', 'logo', 'toolNameSize', 'toolNameColor', 'toolNameAlignment', 'surveyNameSize', 'surveyNameColor', 'surveyNameAlignment', 'companyNameSize', 'companyNameColor', 'companyNameAlignment', 'backgroundColor', 'feedbackFor'));
    }

    private function get_survey_setting($surveyId, $slug, $defSlug) {
        $setting = $this->SurveySetting->find('first', array('conditions' => array('SurveySetting.survey_id' => $surveyId, 'SurveySetting.slug' => $slug)));
        $settingTxt = sizeof($setting) ? $setting['SurveySetting']['value'] : $this->Setting->get_value($defSlug);
        return $settingTxt;
    }

    public function export_report($id) {
//        $this->autoRender = false;
        $this->layout = false;

        $this->helpers[] = 'Tbs';
        $this->set('report_data', $this->report_data($id));
    }

    public function download_report($id) {
        $this->helpers[] = 'Tbs';
        $this->set('report_data', $this->report_data($id));
    }

    public function sample_tbs() {
        // Stop Cake from displaying action's execution time, this can corrupt the exported file
        // Re-ativate in order to see bugs
        Configure::write('debug', 0);
        // Make the Tbs helper available in the view
        $this->helpers[] = 'Tbs';

        // Set available data in the view
        $this->set('records', 'test');
    }

    public function report($id) {
        $this->layout = 'report';
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }

        $this->report_data($id);
    }

    private function report_data($id) {
        $result = array();
        $result['survey'] = $this->Survey->read(array('Survey.name', 'Survey.id'), $id);
        $result['survey']['Survey']['short_name'] = $this->tokenTruncate($result['survey']['Survey']['name'], 50);
        $result['dimensions'] = $this->SurveyTheme->find('all', array('conditions' => array('SurveyTheme.survey_id' => $id, 'SurveyTheme.is_deleted' => 0), 'fields' => array('SurveyTheme.id', 'SurveyTheme.name', 'SurveyTheme.description', 'SurveyTheme.icon', 'SurveyTheme.color')));
        $result['groups'] = $this->Group->find('all', array('conditions' => array('Group.survey_id' => $id, 'Group.is_deleted' => 'no'), 'fields' => array('Group.id', 'Group.name'), 'order' => array('Group.id ASC')));

        $surveyResults = $this->SurveyResult->find('all', array('fields' => array('Group.id'), 'conditions' => array('SurveyResult.is_admin' => 'no', 'SurveyResult.survey_id' => $id)));

        $questionNames = $this->Question->find('list', array('fields' => array('Question.id', 'Question.name'), 'conditions' => array('Question.survey_id' => $id, 'Question.is_deleted' => 'no'), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));

        if (sizeof($surveyResults) && sizeof($result['groups']) && sizeof($questionNames)) {

            $surveyAdminResults = $this->SurveyResult->find('all', array('conditions' => array('SurveyResult.is_admin' => 'yes', 'SurveyResult.survey_id' => $id)));

            $dimensionResults = $this->result_by_dimension($id, $surveyResults, $questionNames, $surveyAdminResults);

            $result['dimensionsResult'] = $dimensionResults['dimensionResults'];
            $result['questionRanks'] = $dimensionResults['questionRanks'];

            $result['openEnded'] = $this->report_open_ended_question($id, $result['groups'], $surveyResults, $questionNames);

            $result['nonCalculating'] = $this->report_non_calculating($id, $result['groups'], $surveyResults, $questionNames, $surveyAdminResults);

            //Report settings
            $this->loadModel('SurveyReportSetting');
            $this->set('report_settings', $this->SurveyReportSetting->find('list', array('fields' => array('SurveyReportSetting.slug', 'SurveyReportSetting.value'))));

            $this->set('result', $result);
            $this->set('hasResult', 1);
        } else {
            $this->set('hasResult', 0);
            $this->Session->setFlash(__('No report to be displayed yet.'), 'default', array('class' => 'alert alert-danger'));
        }
    }

    private function result_by_dimension($id, $surveyResults, $questionNames, $surveyAdminResults) {
        $dimensions = $this->SurveyTheme->find('list', array('conditions' => array('SurveyTheme.survey_id' => $id, 'SurveyTheme.is_deleted' => 0), 'fields' => array('SurveyTheme.id', 'SurveyTheme.name')));
        $groups = $this->Group->find('list', array('conditions' => array('Group.survey_id' => $id, 'Group.is_deleted' => 'no'), 'fields' => array('Group.id', 'Group.name'), 'order' => array('Group.id ASC')));
        $questions = $this->Question->find('list', array('fields' => array('Question.id', 'Question.survey_theme_id'), 'conditions' => array('Question.survey_id' => $id, 'Question.is_calculating' => '1', 'Question.type' => array('likert', 'radio button'), 'Question.is_deleted' => 'no'), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));
        $questionKeys = array_keys($questions);
        $questionOptions = $this->QuestionsOption->find('list', array('fields' => array('QuestionsOption.id', 'QuestionsOption.score'), 'conditions' => array('QuestionsOption.question_id' => $questionKeys)));

        $questionRanks = array();

        //initialize dimensions array
        $dimensionsRes = array();
        foreach ($dimensions as $keyD => $dimension) {
            $dimensionsRes[$keyD]['name'] = $dimension;
            foreach ($groups as $keyG => $group) {
                $dimensionsRes[$keyD]['groups'][$keyG] = array('name' => $group, 'avg' => '', 'noOfResp' => '', 'points' => array(), 'points2' => array());
            }
            $dimensionsRes[$keyD]['Total'] = array('avg' => '', 'noOfResp' => '');
            $dimensionsRes[$keyD]['AdminPoints'] = array();
            $dimensionsRes[$keyD]['AdminAvg'] = '';
            $dimensionsRes[$keyD]['AwarenessQuotient'] = '';
            $dimensionsRes[$keyD]['ResCeiling'] = ceil(max($questionOptions));
        }

        //initialize question ranks
        $num = 1;
        foreach ($questionNames as $k => $q) {
            if (isset($questions[$k])) {
                $questionRanks[$k] = array();
                $questionRanks[$k]['name'] = $q;
                $questionRanks[$k]['num'] = $num;
                $questionRanks[$k]['dimension'] = $dimensions[$questions[$k]];
                $questionRanks[$k]['dimensionId'] = $questions[$k];
                $questionRanks[$k]['points'] = array();
                $questionRanks[$k]['adminPoints'] = array();
            }
            $num++;
        }

        //add question points to array
        foreach ($surveyResults as $res) {
            foreach ($res['SurveyAnswer'] as $ans) {
                if (isset($questions[$ans['question_id']])) {
                    $pointS = $questionOptions[$ans['question_option_id']];

                    // add points only if it is greater than 0;
                    if ($pointS) {
                        $dimensionsRes[$questions[$ans['question_id']]]['groups'][$res['Group']['id']]['points'][$ans['question_id']][] = $pointS;

                        //question ranks
                        $questionRanks[$ans['question_id']]['points'][] = $pointS;
                    }

                    $dimensionsRes[$questions[$ans['question_id']]]['groups'][$res['Group']['id']]['points2'][$ans['question_id']][] = $pointS;
                }
            }
        }

        //add admin question points to array
        foreach ($surveyAdminResults as $resAdmin) {
            foreach ($resAdmin['SurveyAnswer'] as $ansAdmin) {
                if ($questions[$ansAdmin['question_id']]) {
                    $adminPointS = $questionOptions[$ansAdmin['question_option_id']];

                    // add points only if it is greater than 0;
                    if ($adminPointS) {
                        $dimensionsRes[$questions[$ansAdmin['question_id']]]['AdminPoints'][$ansAdmin['question_id']][] = $adminPointS;

                        //admin question ranks
                        $questionRanks[$ansAdmin['question_id']]['adminPoints'][] = $adminPointS;
                    }
                }
            }
        }


        //question rank values
        foreach ($questionNames as $k => $q) {
            if (isset($questions[$k])) {
                $questionRanks[$k]['overallAvg'] = round(array_sum($questionRanks[$k]['points']) / count($questionRanks[$k]['points']), 2);
                $questionRanks[$k]['adminAvg'] = round(array_sum($questionRanks[$k]['adminPoints']) / count($questionRanks[$k]['adminPoints']), 2);
                $questionRanks[$k]['difference'] = round($questionRanks[$k]['overallAvg'] - $questionRanks[$k]['adminAvg'], 2);
                unset($questionRanks[$k]['points']);
                unset($questionRanks[$k]['adminPoints']);
            }
        }

        //sort question ranks by overall average 
        foreach ($questionRanks as $k => $r) {
            $overallAvg[$k] = $r['overallAvg'];
        }
        array_multisort($overallAvg, SORT_DESC, $questionRanks);


        //add values to dimension arrays
        foreach ($dimensionsRes as $kR => $res) {
            $dRespTotal = $dPointsTotal = 0;
            foreach ($res['groups'] as $k => $g) {
                $resPoints = $dimensionsRes[$kR]['groups'][$k]['points'];
                $resPoints2 = $dimensionsRes[$kR]['groups'][$k]['points2'];
                $noOfResp = $resPoints ? max(array_map('count', $resPoints)) : 0;
                $noOfResp2 = $resPoints2 ? max(array_map('count', $resPoints2)) : 0;
                $pointsMerge = $resPoints ? call_user_func_array('array_merge', $resPoints) : 0;
                $dGroupAvg = count($pointsMerge) && is_array($pointsMerge) ? round(array_sum($pointsMerge) / count($pointsMerge), 2) : 0;
                $dimensionsRes[$kR]['groups'][$k]['avg'] = $dGroupAvg;
                $dimensionsRes[$kR]['groups'][$k]['noOfResp'] = $noOfResp2;
                $dRespTotal = $dRespTotal + $noOfResp;
                $dPointsTotal = $dPointsTotal + $dGroupAvg * $noOfResp;
                unset($dimensionsRes[$kR]['groups'][$k]['points']);
            }

            if ($dRespTotal) {
                $dimensionsRes[$kR]['Total']['noOfResp'] = $dRespTotal; // total respondents per dimension
                $dimensionsRes[$kR]['Total']['avg'] = round($dPointsTotal / $dRespTotal, 2); //total average per dimension
                //admin
                $resAdminPoints = $dimensionsRes[$kR]['AdminPoints'];
                $pointsAdminMerge = call_user_func_array('array_merge', $resAdminPoints);
                $dimensionsRes[$kR]['AdminAvg'] = round(array_sum($pointsAdminMerge) / count($pointsAdminMerge), 2);

                $dimensionsRes[$kR]['AwarenessQuotient'] = round($dimensionsRes[$kR]['Total']['avg'] - $dimensionsRes[$kR]['AdminAvg'], 2);

                unset($dimensionsRes[$kR]['AdminPoints']);
            } else {
                unset($dimensionsRes[$kR]);
            }
        }

        return array('questionRanks' => $questionRanks, 'dimensionResults' => $dimensionsRes);
    }

    private function report_open_ended_question($id, $groups, $surveyResults, $questionNames) {

        $openEndedArray = array();
        $oeQuestions = $this->Question->find('list', array('conditions' => array('Question.survey_id' => $id, 'Question.type' => 'open ended'), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));

        if (sizeof($groups) && sizeof($oeQuestions)) {
            $num = 1;
            foreach ($questionNames as $qk => $qv) {
                if (isset($oeQuestions[$qk])) {
                    $openEndedArray[$qk]['name'] = $qv;
                    $openEndedArray[$qk]['num'] = $num;
                    $openEndedArray[$qk]['groups'] = array();
                    foreach ($groups as $gk => $gv) {
                        $openEndedArray[$qk]['groups'][$gv['Group']['id']]['name'] = $gv['Group']['name'];
                        $openEndedArray[$qk]['groups'][$gv['Group']['id']]['resp'] = array();
                    }
                }
                $num++;
            }
        }

        foreach ($surveyResults as $key => $res) {
            foreach ($res['SurveyAnswer'] as $ans) {
                if (isset($openEndedArray[$ans['question_id']])) {
                    if ($ans['question_answer'] && !ctype_space($ans['question_answer'])) {
                        $openEndedArray[$ans['question_id']]['groups'][$res['Group']['id']]['resp'][] = $ans['question_answer'];
                    }
                }
            }
        }

        return $openEndedArray;
    }

    private function report_non_calculating($id, $groups, $surveyResults, $questionNames, $surveyAdminResults) {

        $ncArray = array();
        $ncQuestions = $this->Question->find('list', array('conditions' => array('Question.survey_id' => $id, 'Question.type' => array('radio button', 'likert'), 'Question.is_calculating' => 0), 'order' => array('Question.survey_theme_id ASC', 'Question.id ASC')));


        if (sizeof($groups) && sizeof($ncQuestions)) {
            $num = 1;
            foreach ($questionNames as $qk => $qv) {
                if (isset($ncQuestions[$qk])) {

                    $ncArray[$qk]['name'] = $qv;
                    $ncArray[$qk]['num'] = $num;
                    $ncArray[$qk]['options'] = array();
                    $questionOptions = $this->QuestionsOption->find('list', array('conditions' => array('QuestionsOption.question_id' => $qk)));
                    foreach ($questionOptions as $kOptions => $option) {
                        $ncArray[$qk]['options'][$kOptions]['name'] = $option;
                        $ncArray[$qk]['options'][$kOptions]['groups'] = array();
                        foreach ($groups as $group) {
                            $ncArray[$qk]['options'][$kOptions]['groups'][$group['Group']['id']] = 0;
                        }
                        $ncArray[$qk]['options'][$kOptions]['admin'] = 0;
                        $ncArray[$qk]['options'][$kOptions]['total'] = 0;
                    }
                }
                $num++;
            }
        }

        //non-admin
        foreach ($surveyResults as $res) {
            foreach ($res['SurveyAnswer'] as $ans) {
                if (isset($ncArray[$ans['question_id']])) {
                    if ($ans['question_option_id']) {
                        $countAns = $ncArray[$ans['question_id']]['options'][$ans['question_option_id']]['groups'][$res['Group']['id']];
                        $ncArray[$ans['question_id']]['options'][$ans['question_option_id']]['groups'][$res['Group']['id']] = $countAns + 1;

                        //total
                        $totalAns = $ncArray[$ans['question_id']]['options'][$ans['question_option_id']]['total'];
                        $ncArray[$ans['question_id']]['options'][$ans['question_option_id']]['total'] = $totalAns + 1;
                    }
                }
            }
        }

        //admin
        foreach ($surveyAdminResults as $resAdmin) {
            foreach ($resAdmin['SurveyAnswer'] as $ansAdmin) {
                if (isset($ncArray[$ansAdmin['question_id']])) {
                    if ($ansAdmin['question_option_id']) {
                        $countAnsAdmin = $ncArray[$ansAdmin['question_id']]['options'][$ansAdmin['question_option_id']]['admin'];
                        $ncArray[$ansAdmin['question_id']]['options'][$ansAdmin['question_option_id']]['admin'] = $countAnsAdmin + 1;

                        //total
                        $totalAns = $ncArray[$ansAdmin['question_id']]['options'][$ansAdmin['question_option_id']]['total'];
                        $ncArray[$ansAdmin['question_id']]['options'][$ansAdmin['question_option_id']]['total'] = $totalAns + 1;
                    }
                }
            }
        }

        return $ncArray;
    }

    /**
     * survey_builder_components method
     * 
     * Shows button options for the survey builder.
     * 
     * @return void
     */
    public function survey_builder_components() {
        
    }

    /**
     * survey_builder_components method
     * 
     * Shows button options for the survey builder.
     * 
     * @return void
     */
    public function survey_builder() {

        //survey basic info
        //groups
        $this->loadModel('StandardGroup');
        $this->set('standardGroups', $this->StandardGroup->find('all'));

        //themes and theme questions
        $this->loadModel('StandardTheme');
        $this->set('standardThemes', $this->StandardTheme->find('all'));
    }
    
    public function duplicate($id){
        if (!$this->Survey->exists($id) || !$this->user_allowed('Survey', $id)) {
            throw new NotFoundException(__('Invalid survey'));
        }
        $this->autoRender = false;
        if($this->request->data['Survey']['name']){
            
            if ($this->Survey->hasAny(array('Survey.name'=>$this->request->data['Survey']['name']))){
                $this->Session->setFlash(__('Duplicating survey failed. Survey Name is already taken.'), 'default', array('class' => 'alert alert-danger'));
                $this->redirect(array('action' => 'index'));
            }
            
            //data to duplicate
            $this->Survey->Behaviors->load('Containable');
            $this->Survey->contain('SurveyTheme.Question.QuestionsOption', 'Group.RespondentPasscode');
            $surveyData = $this->Survey->find('first', array('conditions'=>array('Survey.id'=>$id)));

            //survey
            $survey = $surveyData['Survey'];
            unset($survey['id']);
            unset($survey['created']);
            unset($survey['modified']);
            $survey['name'] = $this->request->data['Survey']['name'];
            $survey['slug'] = $survey['user_id'] . '_' . Inflector::slug($this->request->data['Survey']['name']);
            
            if(!$survey['open']){
                //disable password validation
                $this->request->data['Survey']['password'] = '';
                $this->Survey->validator()->remove('password', 'notempty');
                //disable email verification validation
                $this->request->data['Survey']['email_verification_on'] = '';
                $this->Survey->validator()->remove('email_verification_on', 'notempty');
            }
            //save survey
            $this->Survey->create();
            if($this->Survey->save($survey)){
                $surveyId = $this->Survey->getLastInsertId();
                //groups
                $groups = $surveyData['Group'];
                foreach($groups as $key=>$group){
                    unset($group['id']);
                    unset($group['created']);
                    unset($group['modified']);
                    $group['survey_id'] = $surveyId;
                    //save group
                    $this->Group->create();
                    $this->Group->save($group);
                    
                    $groupId = $this->Group->getLastInsertId();
                    if(!$survey['open']){
                        //respondents
                         foreach($group['RespondentPasscode'] as $respondent){
                            unset($respondent['id']);
                            unset($respondent['created']);
                            unset($respondent['modified']);
                            $respondent['group_id'] = $groupId;
                            $respondent['survey_id'] = $surveyId;
                            
                            //save respondent
                            $this->RespondentPasscode->create();
                            $this->RespondentPasscode->save($respondent);
                         }
                    }
                }
                
                //dimensions
                $dimensions = $surveyData['SurveyTheme'];
                foreach($dimensions as $key=>$dimension){
                    unset($dimension['id']);
                    unset($dimension['created']);
                    unset($dimension['modified']);
                    $dimension['survey_id'] = $surveyId;
                    
                    //dimension icon
                    $icon = '';
                    if ($dimension['icon']) {
                        $file = new File(substr($dimension['icon'], 1));
                        if ($file) {
                            $dir = new Folder('files/uploads/theme_icons');
                            $expFileName = explode('-', $file->name());
                            array_pop($expFileName);
                            $file->name = implode('-',$expFileName) . '-survey' . $surveyId . '.' . $file->ext();
                            $file->copy($dir->path . DS . $file->name, true);


                            $icon = '/files/uploads/theme_icons/' . $file->name;
                        }
                    }
                    $dimension['icon'] = $icon;
                    //save dimension
                    $this->SurveyTheme->Behaviors->detach('Uploader.Attachment');
                    $this->SurveyTheme->Behaviors->detach('Uploader.FileValidation');
                    $this->SurveyTheme->validator()->remove('icon');
                    $this->SurveyTheme->create();
                    if($this->SurveyTheme->save($dimension)){
                        $dimensionId = $this->SurveyTheme->getLastInsertId();
                        //questions
                        foreach($dimension['Question'] as $qKey => $question){
                            unset($question['id']);
                            unset($question['created']);
                            unset($question['modified']);
                            $question['survey_id'] = $surveyId;
                            $question['survey_theme_id'] = $dimensionId;
                            //question options
                            foreach($question['QuestionsOption'] as $oKey => $option){
                                unset($option['id']);
                                unset($option['question_id']);
                                unset($option['created']);
                                unset($option['modified']);
                                $question['QuestionsOption'][$oKey] = $option;
                            }
                            //save question and question options
                            $this->Question->create();
                            $this->Question->saveAssociated($question, array('deep' => true));
                        }
                    }
                }
                //settings
                $settings = $this->SurveySetting->find('all', array('conditions'=>array('SurveySetting.survey_id'=>$id)));
                foreach($settings as $setting){
                    unset($setting['SurveySetting']['id']);
                    unset($setting['SurveySetting']['created']);
                    unset($setting['SurveySetting']['modified']);
                    $setting['SurveySetting']['survey_id'] = $surveyId;
                    //save setting
                    $this->SurveySetting->create();
                    $this->SurveySetting->save($setting['SurveySetting']);
                }
                $this->Session->setFlash(__('Successfully duplicated survey.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            }
        }else{
            $this->Session->setFlash(__('Duplicating survey failed. Survey Name is required.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => 'index'));
        }
    }

}
