<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('initDB');
    }

    
    public function initDB() {
        $group = $this->User->Role;
        // Allow admins to everything

        $group->id = 1;
        $this->Acl->allow($group, 'controllers');

//        // allow managers to posts and widgets
        $group->id = 3;
//        $this->Acl->deny($group, 'controllers');
//
//        // Users
        $this->Acl->allow($group, 'controllers/Surveys/report');
        $this->Acl->allow($group, 'controllers/Surveys/download_report');
        
        //groups
        $this->Acl->allow($group, 'controllers/Surveys/add_group_option');
        
        $this->Acl->allow($group, 'controllers/Groups/add');
        $this->Acl->allow($group, 'controllers/Groups/edit');
        $this->Acl->allow($group, 'controllers/Groups/delete');
        $this->Acl->allow($group, 'controllers/Groups/respondents');
        
        //themes
        $this->Acl->allow($group, 'controllers/Surveys/add_theme_option');
        
        $this->Acl->allow($group, 'controllers/SurveyThemes/add');
        $this->Acl->allow($group, 'controllers/SurveyThemes/edit');
        $this->Acl->allow($group, 'controllers/SurveyThemes/delete');
        
        //questions
        $this->Acl->allow($group, 'controllers/Surveys/add_question_option');
        
        $this->Acl->allow($group, 'controllers/Questions/add');
        $this->Acl->allow($group, 'controllers/Questions/edit');
        $this->Acl->allow($group, 'controllers/Questions/delete');
        
        //respondent_passcodes
        $this->Acl->allow($group, 'controllers/RespondentPasscodes/edit');
        $this->Acl->allow($group, 'controllers/RespondentPasscodes/delete');
        
        
        //surveys
        $this->Acl->allow($group, 'controllers/Surveys/start_stop_survey');
        $this->Acl->allow($group, 'controllers/Surveys/build');
        $this->Acl->allow($group, 'controllers/Surveys/duplicate');

        echo "all done";
        exit;
    }
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if ($this->request->is('post')) {
            $this->User->validator()->remove('email_address', 'identicalFieldValues');

            $this->User->create();

            $this->request->data['User']['is_email_confirmed'] = true;
            $this->request->data['User']['role_id'] = 3;

            if ($this->User->save($this->request->data)) {
                $emailData = array(
                    'to' => $this->request->data['User']['email_address'],
                    'from' => array($this->Setting->get_value('new_subscriber_email_from') => $this->Setting->get_value('new_subscriber_email_from_text')),
                    'subject' => $this->Setting->get_value('new_subscriber_email_subject'),
                    'viewVars' => array(
                        'emailContent' => $this->Setting->get_value('new_subscriber_email_content') . '\nEmail Address: ' . $this->request->data['User']['email_address'] . '\nPassword: ' . $this->request->data['User']['password'],
                    )
                );
                if ($this->send_mail($emailData)) {
                    $this->Session->setFlash(__('The subscriber has been saved'), 'default', array('class' => 'alert alert-success'));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->set('validationErrors', $this->User->validationErrors);
                $this->Session->setFlash(__('The subscriber could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-error'));
            }
        }
        $this->set('users', $this->paginate('User', array('User.role_id' => '3')));
    }

    /**
     * view method
     *
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid User'));
        }
        $this->set('user', $this->User->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->User->validator()->remove('email_address', 'identicalFieldValues');

            $this->User->create();

            $this->request->data['User']['is_email_confirmed'] = true;
            $this->request->data['User']['role_id'] = 2;

            if ($this->User->save($this->request->data)) {
                $emailData = array(
                    'to' => $this->request->data['User']['email_address'],
                    'from' => 'noreply@larrylobert.com',
                    'subject' => 'Welcome to LLA 360',
                    'viewVars' => array(
                        'emailContent' => 'Welcome to LLA 360. Your subscriber account has now been activated. Click this link <a href="http://larrylobert2.mydevwebsites.info/account/login">http://larrylobert2.mydevwebsites.info/account/login</a> to login\nEmail Address: ' . $this->request->data['User']['email_address'] . '\nPassword: ' . $this->request->data['User']['password'],
                    )
                );
                if ($this->send_mail($emailData)) {
                    $this->Session->setFlash(__('The user has been saved'), 'default', array('class' => 'alert alert-success'));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-error'));
            }
        }

        $this->set('roles', $this->User->Role->find('list'));
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->set('validationErrors', $this->User->validationErrors);
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $this->request->data = $this->User->read(null, $id);

            $this->set('roles', $this->User->Role->find('list'));
        }
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Team->id = $id;
        if (!$this->Team->exists()) {
            throw new NotFoundException(__('Invalid User'));
        }
        if ($this->Team->delete()) {
            $this->Session->setFlash(__('User deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    public function deactivate($id) {
        $this->activate_deactivate($id, 'deactivate');
    }

    public function activate($id) {
        $this->activate_deactivate($id, 'activate');
    }

    private function activate_deactivate($id, $action) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $isActive = $action == 'activate' ? 1 : 0;
        $this->User->set(array('is_active' => $isActive));
        if ($this->User->save()) {
            $actText = $action == 'activate' ? 'activated' : 'deactivated';
            $this->Session->setFlash(__('Successfully ' . $actText . ' the user.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        } else {
            $actText = $action == 'activate' ? 'activating' : 'deactivating';
            $this->Session->setFlash(__('Error ' . $actText . ' user.'), 'default', array('class' => 'alert alert-error'));
        }
    }

    public function change_subscriber_password($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->User->set($this->request->data);
            if ($this->User->validates(array('fieldList' => array('password', 'password_confirm')))) {

                if ($this->User->save()) {
                    $user = $this->User->read(null,$id);
                    $emailData = array(
                        'to' => $user['User']['email_address'],
                        'from' => array($this->Setting->get_value('default_email_from') => $this->Setting->get_value('default_email_from_text')),
                        'subject' => $this->Setting->get_value('change_password_subscriber_email_subject'),
                        'viewVars' => array(
                            'emailContent' => $this->Setting->get_value('change_password_subscriber_email_content') . '\nEmail Address: ' . $user['User']['email_address'] . '\nNew Password: ' . $this->request->data['User']['password'],
                        )
                    );
                    if ($this->send_mail($emailData)) {
                        $this->Session->setFlash(__('Subscriber password successfully updated.'), 'default', array('class' => 'alert alert-success'));
                    }
                    
                }
            } else {
                $this->Session->setFlash(__('The new password could not be saved. Please try again.'), 'default', array('class' => 'alert alert-error'));

                $this->request->data = $user;
            }
        }
    }

}
