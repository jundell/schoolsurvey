<?php

App::uses('AppController', 'Controller');

/**
 * SurveyThemes Controller
 *
 * @property SurveyTheme $SurveyTheme
 */
class SurveyThemesController extends AppController {

    public $uses = array('Survey', 'SurveyTheme', 'StandardTheme');

    public $paginate = array(
        'SurveyTheme' => array(
            'limit' => 10,
       )
    );
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->SurveyTheme->recursive = 0;
        $this->set('surveyThemes', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->SurveyTheme->exists($id)) {
            throw new NotFoundException(__('Invalid survey theme'));
        }
        $options = array('conditions' => array('SurveyTheme.' . $this->SurveyTheme->primaryKey => $id));
        $this->set('surveyTheme', $this->SurveyTheme->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($surveyId, $build = 0) {
        $this->layout = 'survey_builder';

        $this->Survey->id = $surveyId;
        if (!$this->Survey->exists()) {
            throw new NotFoundException(__('Invalid survey.'));
        }
        $themeslist = $this->paginate(
                'SurveyTheme', array(
            'SurveyTheme.survey_id' => $surveyId,
            'SurveyTheme.is_deleted' => '0'
                )
        );

        $themeDatalist = $this->SurveyTheme->find('list', array('conditions' => array('SurveyTheme.survey_id' => $surveyId, 'SurveyTheme.is_deleted' => '0'), 'order' => array('SurveyTheme.id ASC')));

        $this->set('themeslist', $themeslist);
        $this->set('themeDatalist', $themeDatalist);

        if ($this->request->is('post')) {


            $add_another_theme = $this->request->data['add_another_theme'];

            $themes = $this->request->data['theme'];
            $ownTheme = $this->request->data['ownTheme'];


            if (sizeof($themes) || ($ownTheme['name'] && strlen(trim($ownTheme['name'])) != 0)) {

                $errors = 0;

                //validate chosen standard themes
                $chosenThemes = $this->chosen_standard_themes($surveyId, $themes);
                $errors = $errors + $chosenThemes['errors'];

                //validate own theme
                $theme = $this->own_theme($surveyId, $ownTheme, $themes);
                $errors = $errors + $theme['errors'];

                if (!$errors) {
                    foreach ($chosenThemes['chosenThemes'] as $chosenTheme) {
                        $this->SurveyTheme->Behaviors->detach('Uploader.Attachment');
                        $this->SurveyTheme->Behaviors->detach('Uploader.FileValidation');
                        $this->SurveyTheme->validator()->remove('icon');
                        $this->SurveyTheme->create();
                        $this->SurveyTheme->save($chosenTheme);
                    }

                    //save own theme
                    if(sizeof($theme['theme'])){
                        $this->SurveyTheme->Behaviors->attach('Uploader.Attachment');
                        $this->SurveyTheme->Behaviors->attach('Uploader.FileValidation');
                        $this->SurveyTheme->create();
                        $this->SurveyTheme->save($theme['theme']);
                    }

                    $this->Session->setFlash(__('Your survey theme(s) have been saved.'), 'default', array('class' => 'alert alert-success'));

                    if ($add_another_theme) {
                        $this->redirect($this->referer() . '/#ownTheme');
                    } else {
                        if ($build) {
                            $this->redirect(array('controller' => 'surveys', 'action' => 'add_question_option/' . $surveyId));
                        } else {
                            $this->redirect(array('action' => 'add/' . $surveyId));
                        }
                    }
                } else {
                    $errorMessage = '';
                    if ($theme['validationErrors']) {
                        $errorMessage = $theme['validationErrors'];
                    }
                    $this->Session->setFlash(__('Saving survey theme(s) failed.'), 'default', array('class' => 'alert alert-danger'));
                    if ($errorMessage) {
                        $this->set('errorMessage', $errorMessage);
                    }
                }
            } else {
                $this->Session->setFlash(__('You haven\'t added any group.'), 'default', array('class' => 'alert alert-danger'));
            }
        }

        //survey status
        $surveyStatusData = $this->Survey->read('status', $surveyId);
        
        $surveyStatus = $surveyStatusData['Survey']['status'];
        if($surveyStatus){
            $this->Session->setFlash(__('The survey has already started. You should stop the survey to edit/delete any dimension information.'), 'default', array('class' => 'alert alert-danger'));
        }
        $this->set('surveyStatus', $surveyStatus);
        
        $this->set('build', $build);
        $this->set('standardThemes', $this->StandardTheme->find('all'));
    }

    private function chosen_standard_themes($surveyId, $themes) {

        $chosenThemes = array();
        $errors = 0;
        foreach ($themes as $theme) {
            $themeData = $this->StandardTheme->read(null, $theme);
            if ($themeData) {
                $icon = '';
                if ($themeData['StandardTheme']['icon']) {
                    $file = new File(substr($themeData['StandardTheme']['icon'], 1));
                    if ($file) {
                        $dir = new Folder('files/uploads/theme_icons');
                        $file->name = $file->name() . '-survey' . $surveyId . '.' . $file->ext();
                        $file->copy($dir->path . DS . $file->name, true);


                        $icon = '/files/uploads/theme_icons/' . $file->name;
                    }
                }
                $color = $themeData['StandardTheme']['color'];
                if (substr($color, 0, 1) != '#') {
                    $color = '#' . $color;
                }

                $chosenThemeData = array(
                    'name' => $themeData['StandardTheme']['name'],
                    'description' => $themeData['StandardTheme']['description'],
                    'icon' => $icon,
                    'color' => $color,
                    'survey_id' => $surveyId
                );
                $this->SurveyTheme->Behaviors->detach('Uploader.Attachment');
                $this->SurveyTheme->Behaviors->detach('Uploader.FileValidation');
                $this->SurveyTheme->set($chosenThemeData);
                if ($this->SurveyTheme->validates()) {
                    $chosenThemes[] = array(
                        'name' => $themeData['StandardTheme']['name'],
                        'description' => $themeData['StandardTheme']['description'],
                        'icon' => $icon,
                        'color' => $color,
                        'survey_id' => $surveyId
                    );
                } else {
                    $errors++;
                }
            }
        }
        return array('chosenThemes' => $chosenThemes, 'errors' => $errors);
    }

    private function own_theme($surveyId, $ownTheme, $themes) {

        $theme = array();
        $errors = 0;
        $validationErrors = array();
        
        //chosen standard themes names
        $chosenStandardThemes = $this->StandardTheme->find('list', array('conditions'=>array('StandardTheme.id'=>$themes))); 
        
        if ($ownTheme['name'] && strlen(trim($ownTheme['name'])) != 0) {
            
            //duplicate own theme and standard theme
            if(in_array($ownTheme['name'], $chosenStandardThemes)){
                $errors++;
                $validationErrors['name'] = array('Theme name already taken.');
            }else{
            
                $this->SurveyTheme->Behaviors->attach('Uploader.Attachment');
                $this->SurveyTheme->Behaviors->attach('Uploader.FileValidation');
                if (substr($ownTheme['color'], 0, 1) != '#') {
                    $ownTheme['color'] = '#' . $ownTheme['color'];
                }
                $ownTheme['survey_id'] = $surveyId;


                $this->SurveyTheme->set($ownTheme);

                if ($this->SurveyTheme->validates()) {
                    $theme = $ownTheme;
                } else {
                    $errors++;

                    $validationErrors = $this->SurveyTheme->validationErrors;
                }
            }
        }
        return array('theme' => $theme, 'errors' => $errors, 'validationErrors' => $validationErrors);
    }


    public function edit($id = null) {
        if (!$this->SurveyTheme->exists($id)) {
            throw new NotFoundException(__('Invalid dimension'));
        }
        
        $surveyId = $this->SurveyTheme->field('survey_id', array('id' => $id));
        
        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey', $surveyId)) {
            throw new NotFoundException(__('Invalid dimension'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if (substr($this->request->data['SurveyTheme']['color'], 0, 1) != '#') {
                $this->request->data['SurveyTheme']['color'] = '#' . $this->request->data['SurveyTheme']['color'];;
            }
            
            if ($this->SurveyTheme->save($this->request->data)) {
                $this->Session->setFlash(__('The dimension has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'add/' . $surveyId));
            } else {
                $this->Session->setFlash(__('The dimension could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('SurveyTheme.' . $this->SurveyTheme->primaryKey => $id));
            $this->request->data = $this->SurveyTheme->find('first', $options);
        }
        $iconPath = $this->SurveyTheme->field('icon', array('id' => $id));
        $this->set('icon', $iconPath);
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null, $return = 0) {
        $this->SurveyTheme->id = $id;
        if (!$this->SurveyTheme->exists()) {
            throw new NotFoundException(__('Invalid dimension'));
        }
        
        $surveyId = $this->SurveyTheme->field('survey_id', array('id' => $id));
        
        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey', $surveyId)) {
            throw new NotFoundException(__('Invalid dimension'));
        }
        
        $this->request->onlyAllow('post', 'delete');
        if ($this->SurveyTheme->delete($id, true)) {
            $this->Session->setFlash(__('Survey dimension deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'add/' . $surveyId . '/' . $return));
            //$this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Survey dimension was not deleted.'), 'default', array('class' => 'alert alert-danger'));
        $this->redirect(array('action' => 'index'));
    }

}
