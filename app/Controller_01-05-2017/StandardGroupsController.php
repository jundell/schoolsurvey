<?php

App::uses('AppController', 'Controller');

/**
 * StandardGroups Controller
 *
 * @property StandardGroup $StandardGroup
 */
class StandardGroupsController extends AppController {

    public $paginate = array(
        'StandardGroup' => array(
            'limit' => 10,
        )
    );
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->StandardGroup->recursive = 0;
        $this->set('standardGroups', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->StandardGroup->exists($id)) {
            throw new NotFoundException(__('Invalid standard group'));
        }
        $options = array('conditions' => array('StandardGroup.' . $this->StandardGroup->primaryKey => $id));
        $this->set('standardGroup', $this->StandardGroup->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $groupCount = $this->StandardGroup->find('count');
            if($groupCount < Configure::read('site_config.max_groups')){
                $this->StandardGroup->create();
                if ($this->StandardGroup->save($this->request->data)) {
                    $this->Session->setFlash(__('The standard group has been saved.'), 'default', array('class' => 'alert alert-success'));
                    $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash(__('The standard group could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
                }
            }
        }
        $this->StandardGroup->recursive = 0;
        $this->set('standardGroups', $this->paginate());
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->StandardGroup->exists($id)) {
            throw new NotFoundException(__('Invalid standard group'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->StandardGroup->save($this->request->data)) {
                $this->Session->setFlash(__('The standard group has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The standard group could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('StandardGroup.' . $this->StandardGroup->primaryKey => $id));
            $this->request->data = $this->StandardGroup->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->StandardGroup->id = $id;
        if (!$this->StandardGroup->exists()) {
            throw new NotFoundException(__('Invalid standard group'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->StandardGroup->delete()) {
            $this->Session->setFlash(__('Standard group deleted'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Standard group was not deleted'), 'default', array('class' => 'alert alert-danger'));
        $this->redirect(array('action' => 'index'));
    }

    /**
     * choose method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function choose($surveyId) {
        $this->loadModel('Survey');
        $this->loadModel('Group');
        $this->Survey->id = $surveyId;
        if (!$this->Survey->exists()) {
            throw new NotFoundException(__('Invalid survey.'));
        }
        
        
        
        
        if ($this->request->is('post')) {
            $groups = $this->request->data['group'];
            $ownGroups = $this->request->data['ownGroup'];
            
            $saveSize = 0;
            if(sizeof($groups) || sizeof($ownGroups)){
                //chosen standard groups
                foreach($groups as $group){
                    $this->Group->create();
                    if($group){
                        if($this->Group->save(array('survey_id'=>$surveyId, 'name'=>$group))){
                            $saveSize++;
                        }
                    }
                }

                //own groups
                foreach($ownGroups as $ownGroup){
                    $this->Group->create();
                    if($ownGroup){
                        if($this->Group->save(array('survey_id'=>$surveyId, 'name'=>$ownGroup))){
                            $saveSize++;
                        }
                    }
                }
                if($saveSize){
                    $this->Session->setFlash(__('Your survey group(s) has been saved.'), 'default', array('class' => 'alert alert-success'));
                    $this->redirect(array('controller'=>'surveys', 'action' => 'add_group_message/' . $surveyId));
                }else{
                    $this->Session->setFlash(__('You haven\'t added any group.'), 'default', array('class' => 'alert alert-danger'));
                    $this->redirect(array('controller'=>'surveys', 'action' => 'add_survey_message/' . $surveyId));
                }
            }else{
//                $this->Session->setFlash(__('You haven\'t added any group. You can still add your groups after completing the steps by clicking on the Groups button.'), 'default', array('class' => 'alert alert-danger'));
                $this->Session->setFlash(__('You haven\'t added any group.'), 'default', array('class' => 'alert alert-danger'));
                $this->redirect(array('controller'=>'surveys', 'action' => 'add_survey_message/' . $surveyId));
            }
        }
        
        $this->set('standardGroups', $this->StandardGroup->find('all'));
        $groupslist = $this->Group->find('list', array('conditions' => array('Group.survey_id' => $surveyId, 'Group.is_deleted' => 'no'), 'order' => array('Group.id ASC')));
        $this->set('groupslist', $groupslist);
    }

}
