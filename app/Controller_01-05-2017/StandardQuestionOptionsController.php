<?php
App::uses('AppController', 'Controller');
/**
 * StandardQuestionOptions Controller
 *
 * @property StandardQuestionOption $StandardQuestionOption
 */
class StandardQuestionOptionsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->StandardQuestionOption->recursive = 0;
		$this->set('standardQuestionOptions', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->StandardQuestionOption->exists($id)) {
			throw new NotFoundException(__('Invalid standard question option'));
		}
		$options = array('conditions' => array('StandardQuestionOption.' . $this->StandardQuestionOption->primaryKey => $id));
		$this->set('standardQuestionOption', $this->StandardQuestionOption->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->StandardQuestionOption->create();
			if ($this->StandardQuestionOption->save($this->request->data)) {
				$this->Session->setFlash(__('The standard question option has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The standard question option could not be saved. Please, try again.'));
			}
		}
		$questions = $this->StandardQuestionOption->Question->find('list');
		$this->set(compact('questions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->StandardQuestionOption->exists($id)) {
			throw new NotFoundException(__('Invalid standard question option'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->StandardQuestionOption->save($this->request->data)) {
				$this->Session->setFlash(__('The standard question option has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The standard question option could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('StandardQuestionOption.' . $this->StandardQuestionOption->primaryKey => $id));
			$this->request->data = $this->StandardQuestionOption->find('first', $options);
		}
		$questions = $this->StandardQuestionOption->Question->find('list');
		$this->set(compact('questions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->StandardQuestionOption->id = $id;
		if (!$this->StandardQuestionOption->exists()) {
			throw new NotFoundException(__('Invalid standard question option'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->StandardQuestionOption->delete()) {
			$this->Session->setFlash(__('Standard question option deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Standard question option was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
