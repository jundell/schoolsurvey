<?php

App::uses('AppController', 'Controller');

/**
 * Groups Controller
 *
 * @property Group $Group
 */
class GroupsController extends AppController {
    
    public $uses = array('Survey', 'Group', 'StandardGroup', 'RespondentPasscode');
    
    public $paginate = array(
        'Group' => array(
            'limit' => 10,
       )
    );

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Group->recursive = 0;
        $this->set('groups', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Group->exists($id)) {
            throw new NotFoundException(__('Invalid group'));
        }
        $options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
        $this->set('group', $this->Group->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($surveyId, $build = 0, $addedMoreGroups = 0) {
        $this->layout = 'survey_builder';

        $this->Survey->id = $surveyId;
        $grouplist = $this->Group->find('list', array('conditions' => array('Group.survey_id' => $surveyId, 'Group.is_deleted' => 'no'), 'order' => array('Group.id ASC')));
        $passcodelist = $this->RespondentPasscode->find('list',array('conditions'=>array('survey_id'=>$surveyId), 'fields'=>array('name', 'passcode')));
        if (!$this->Survey->exists()) {
            throw new NotFoundException(__('Invalid survey.'));
        }

        //open or close survey
        //active or inactive survey
        $surveySystem = $this->Survey->read(array('open','status','admin_password'), $surveyId);

        if ($this->request->is('post')) {
            
            if(sizeof($grouplist) < Configure::read('site_config.max_groups')){

                $add_another_group = $this->request->data['add_another_group']; //Save and add another group button clicked

                $groups = $this->request->data['group']; //chosen standard groups
                $resPass = $this->request->data['resPass']; //chosen standard groups' respondent passcodes
                $ownGroup = $this->request->data['ownGroup']['name']; //own group
                $ownGroupResPass = $this->request->data['ownGroup']['resPass']; //own group respondent passcodes

                $errorMessage = array();


                if (sizeof($groups) || $ownGroup) {

                    // validate duplicate names and passcodes
                    if (!$surveySystem['Survey']['open']) {
                        $errorMessage = $this->validate_duplicate_respondent_passcodes($resPass, $ownGroupResPass, $passcodelist, $surveySystem['Survey']['admin_password']);
                    }

                    $groupCount = sizeof($grouplist);

                    //validate chosen groups
                    if(!sizeof($errorMessage)){
                        $savedChosenGroups = $this->chosen_standard_groups($surveyId, $groups, $resPass, $surveySystem);
                        $errorMessage = array_merge($errorMessage, $savedChosenGroups['errorMessage']);
                        $groupsSaveData = $savedChosenGroups['groupsSaveData'];
                    } 

                    //validate own group
                    if(!sizeof($errorMessage)){
                        $savedOwnGroup = $this->own_group($surveyId, $ownGroup, $groups, $ownGroupResPass, $surveySystem);
                        $errorMessage = array_merge($errorMessage, $savedOwnGroup['errorMessage']);
                        $ownGroupSaveData = $savedOwnGroup['ownGroupSaveData'];
                    }
                       
                    if (!sizeof($errorMessage)) {
                        
                        //save groups
                        $addedMoreGroups = $this->save_groups($groupsSaveData, $groupCount, $addedMoreGroups, $surveyId, $ownGroup, $ownGroupSaveData);

                        $this->Session->setFlash(__('Your survey group(s) have been saved.'), 'default', array('class' => 'alert alert-success'));
                        if ($add_another_group) {
                            $this->redirect($this->referer() . '/#ownGroup');
                        } else {
                            if ($build) {
                                $this->redirect(array('controller' => 'surveys', 'action' => 'add_theme_option/' . $surveyId));
                            } else {
                                $this->redirect(array('action' => 'add/' . $surveyId.'/0/'.$addedMoreGroups));
                            }
                        }
                    }   
                    else {
                        if (sizeof($errorMessage)) {
                            $this->Session->setFlash(__(implode("<br />", $errorMessage)), 'default', array('class' => 'alert alert-danger'));
                        } else {
                            $this->Session->setFlash(__('You haven\'t added any group.'), 'default', array('class' => 'alert alert-danger'));
                        }
                    }
                } else {
                    $this->Session->setFlash(__('You haven\'t added any group.'), 'default', array('class' => 'alert alert-danger'));
                }
            }
        }
        
        //survey status
        $surveyStatus = 0;
        if($surveySystem['Survey']['status']){
            $this->Session->setFlash(__('The survey has already started. You should stop the survey to edit/delete any group information.'), 'default', array('class' => 'alert alert-danger'));
        
            $surveyStatus = 1;
        }
        $this->set('surveyStatus', $surveyStatus);
        
        $this->set('addedMoreGroups', $addedMoreGroups);

        $this->set('build', $build);
        $this->set('standardGroups', $this->StandardGroup->find('all'));
        $this->set('grouplist', $grouplist);
        $this->set('groupsPaginate', $this->paginate('Group', array('Group.survey_id'=>$surveyId, 'Group.is_deleted'=>'no')));
        $this->set('surveyId', $surveyId);
        $this->set('surveySystem', $surveySystem['Survey']['open']);
    }

    /**
     * validate_duplicate_respondent_passcodes method validates respondents' names and passcodes for duplication
     * @param  array $resPass         
     * @param  array $ownGroupResPass 
     * @param  array $passcodelist    
     * @return array                  
     */
    private function validate_duplicate_respondent_passcodes($resPass, $ownGroupResPass, $passcodelist, $adminPassword){
        $errorMessage = array();
        $namesArr = array();
        $passcodesArr = array();
        
        foreach($resPass as $rp){
            $namesArr = array_merge($namesArr, array_filter($rp['name']));
            $passcodesArr = array_merge($passcodesArr, array_filter($rp['passcode']));
        }

        $namesArr = array_merge($namesArr, array_filter($ownGroupResPass['name']));
        $passcodesArr = array_merge($passcodesArr, array_filter($ownGroupResPass['passcode']));

        // respondent names
        if(count(array_unique($namesArr))<count($namesArr)){ 
            // submitted names should be unique
            $errorMessage['duplicateName'] = 'Respondent names should be unique.'; 
        }else{
            $namesArr = array_merge($namesArr, array_keys($passcodelist)); 
            
            //submitted passcodes and existing names should be unique                       
            if(count(array_unique($namesArr))<count($namesArr)){ 
                $errorMessage['duplicateName'] = 'Respondent name(s) already exists.';
            }
        }

        if(in_array($adminPassword, $passcodesArr)){
            $errorMessage['duplicateName'] = 'You cannot use the admin password as a respondent passcode.';
        }

        // duplicate passcodes
        if(count(array_unique($passcodesArr))<count($passcodesArr)){ 
            // submitted passcodes should be unique
            $errorMessage['duplicatePasscode'] = 'Respondent passcodes should be unique.'; 
        }else{
            $passcodesArr = array_merge($passcodesArr, $passcodelist); 
            //submitted passcodes and existing passcodes should be unique                       
            if(count(array_unique($passcodesArr))<count($passcodesArr)){ 
                $errorMessage['duplicatePasscode'] = 'Respondent passcode(s) already exists.';
            }
        }

        return $errorMessage;        
    }

    /**
     * chosen_standard_groups method validates chosen standard groups
     * @param  int $surveyId        
     * @param  array $groups       
     * @param  array $resPass      
     * @param  array $surveySystem 
     * @return array               
     */
    private function chosen_standard_groups($surveyId, $groups, $resPass, $surveySystem) {
        //chosen standard groups
        $groupsSaveData = array();
        $errorMessage = array();
        foreach ($groups as $key => $group) {
            $this->Group->create();
            if ($group) {
                //closed system
                //groups should have at least one respondent
                $groupsSaveData[$key] = array('name' => $group);
                if (!$surveySystem['Survey']['open']) {

                    $arrayResPass = array();

                        foreach ($resPass[$key]['name'] as $kn => $name) {
                            if (($name && strlen(trim($name)) != 0) && ($resPass[$key]['passcode'][$kn] && strlen(trim($resPass[$key]['passcode'][$kn])) != 0)) {
                                $arrayResPass[] = array('name' => $name, 'passcode' => $resPass[$key]['passcode'][$kn]);
                            } else {
                                $errorMessage['emptyRessPass'] = "Please fill in the missing fields.";
                                continue;
                            }
                        }
                        if (sizeof($arrayResPass)) {
                            foreach ($arrayResPass as $arrResPass) {
                                $groupsSaveData[$key]['ressPass'][] = array('name' => $arrResPass['name'], 'passcode' => $arrResPass['passcode']);
                            }
                        } else {
                            $errorMessage['noRespondent'] = "You have to add at least one respondent for each checked standard group.";
                        }
                }
            }
        }
        return array('errorMessage' => $errorMessage, 'groupsSaveData' => $groupsSaveData);
    }

    /**
     * own_group description method validates created group
     * @param  int $surveyId        
     * @param  array $ownGroup        
     * @param  array $groups          
     * @param  array $ownGroupResPass 
     * @param  array $surveySystem    
     * @return array                 
     */
    private function own_group($surveyId, $ownGroup, $groups, $ownGroupResPass, $surveySystem) {
        if ($ownGroup) {
            $errorMessage = array();
            $groupSaveData = array('name' => $ownGroup, 'survey_id'=>$surveyId);
            
            //duplicate group
            if(in_array($ownGroup, $groups)){
                $errorMessage['duplicate'] = "Dulplicate group(s).";
            }
            
            //close system
            //groups should have at least one respondent
            if (!$surveySystem['Survey']['open']) {
                $arrayResPass = array();
                $resPassBlank = 0; //blank respondent name or password

                    foreach ($ownGroupResPass['name'] as $kn => $name) {
                        if (($name && strlen(trim($name)) != 0) && ($ownGroupResPass['passcode'][$kn] && strlen(trim($ownGroupResPass['passcode'][$kn])) != 0)) {
                            $arrayResPass[] = array('name' => $name, 'passcode' => $ownGroupResPass['passcode'][$kn]);
                        } else {
                            $errorMessage['emptyRessPass'] = "Please fill in the missing fields.";
                            continue;
                        }
                    }
                    if (sizeof($arrayResPass)) {
                        foreach ($arrayResPass as $arrResPass) {
                            $groupSaveData['ressPass'][] = array('name' => $arrResPass['name'], 'passcode' => $arrResPass['passcode']);
                        }
                    } else {
                            $errorMessage['ownGroupNoRespondent'] = "You have to add at least one respondent for your group.";
                    }
            }
            $this->Group->set($groupSaveData);
            if(!$this->Group->validates($groupSaveData)){
                $errorMessage['taken'] = $this->Group->validationErrors['name'][0];
            }
        }
        return array('errorMessage' => $errorMessage, 'ownGroupSaveData' => $groupSaveData);
    }

    /**
     * save_groups method saves both chosen groups and created group
     * @param  array $groupsSaveData   
     * @param  int $groupCount       
     * @param  int $addedMoreGroups  
     * @param  int $surveyId         
     * @param  array $ownGroup         
     * @param  array $ownGroupSaveData 
     * @return int                  
     */
    private function save_groups($groupsSaveData, $groupCount, $addedMoreGroups, $surveyId, $ownGroup, $ownGroupSaveData){
        //save chosen groups
        foreach ($groupsSaveData as $gSData) {
            if($groupCount >= Configure::read('site_config.max_groups')){
                $addedMoreGroups = 1;
                break;
            }
            $this->Group->create();
            if ($this->Group->save(array('survey_id' => $surveyId, 'name' => $gSData['name']))) {
                $groupId = $this->Group->getLastInsertId();
                if (sizeof($gSData['ressPass'])) {
                    foreach ($gSData['ressPass'] as $rP) {
                        $this->RespondentPasscode->create();
                        $this->RespondentPasscode->save(array('survey_id' => $surveyId, 'group_id' => $groupId, 'name' => $rP['name'], 'passcode' => $rP['passcode']));
                    }
                }
            }
            $groupCount ++;
        }

        //own group
        if($ownGroup){
            if($groupCount < Configure::read('site_config.max_groups')){
                $this->Group->create();
                if ($this->Group->save(array('survey_id' => $surveyId, 'name' => $ownGroupSaveData['name']))) {
                    $groupId = $this->Group->getLastInsertId();
                    if (sizeof($ownGroupSaveData['ressPass'])) {
                        foreach ($ownGroupSaveData['ressPass'] as $rP) {
                            $this->RespondentPasscode->create();
                            $this->RespondentPasscode->save(array('survey_id' => $surveyId, 'group_id' => $groupId, 'name' => $rP['name'], 'passcode' => $rP['passcode']));
                        }
                    }
                }
            }else{
                $addedMoreGroups = 1;
            }
        }
        return $addedMoreGroups; 
    }

    /**
     * respondents method displays all respondents in a group
     *
     * @throws NotFoundException
     * @param  int $id
     * @return void
     */
    public function respondents($id = null) {
        if (!$this->Group->exists($id)) {
            throw new NotFoundException(__('Invalid group'));
        }
        
        $surveyId = $this->Group->field('survey_id', array('id' => $id));
        
        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey', $surveyId)) {
            throw new NotFoundException(__('Invalid group'));
        }
        
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $data['RespondentPasscode']['survey_id'] = $surveyId;
            $data['RespondentPasscode']['group_id'] = $id;
            $this->RespondentPasscode->create();
            if($this->RespondentPasscode->save($data)){
                $this->Session->setFlash(__('The respondent has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'respondents/' . $id));
            }
            
        }
        
        $this->set('respondents', $this->paginate('RespondentPasscode', array('RespondentPasscode.survey_id'=>$surveyId, 'RespondentPasscode.group_id'=>$id)));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Group->exists($id)) {
            throw new NotFoundException(__('Invalid group'));
        }
        
        $surveyId = $this->Group->field('survey_id', array('id' => $id));
        
        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey', $surveyId)) {
            throw new NotFoundException(__('Invalid group'));
        }
        $this->request->data['Group']['survey_id'] = $surveyId;
        
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Group->save($this->request->data)) {
                $this->Session->setFlash(__('The group has been saved'), 'default', array('class' => 'alert alert-success'));
//                $this->redirect(array('action' => 'add/' . $surveyId));
            } else {
                $this->Session->setFlash(__('The group could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
            $this->request->data = $this->Group->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Group->id = $id;
        if (!$this->Group->exists()) {
            throw new NotFoundException(__('Invalid group'));
        }
        
        $surveyId = $this->Group->field('survey_id', array('id' => $id));
        
        if (!$this->Survey->exists($surveyId) || !$this->user_allowed('Survey', $surveyId)) {
            throw new NotFoundException(__('Invalid group'));
        }
        
        $this->Group->read('survey_id',$id);
        
        $this->request->onlyAllow('post', 'delete');
        //$this->Group->set('is_deleted', 'yes');
        //if ($this->Group->save()) {
        if ($this->Group->delete()) {
            $this->Session->setFlash(__('Group successfully deleted.'), 'default', array('class' => 'alert alert-success'));
//            $this->redirect(array('controller' => 'surveys', 'action' => 'add_group/' . $surveyId));
            $this->redirect(array('action' => 'add/' . $surveyId));
        }
        $this->Session->setFlash(__('Group was not deleted'), 'default', array('class' => 'alert alert-danger'));
        $this->redirect(array('action' => 'index'));
    }

}
